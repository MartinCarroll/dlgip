#!/bin/bash
clear

if [ $HOSTNAME = "aws151-testapp01" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM, so no mapping of drive required"
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

elif [ $HOSTNAME = "aws151-testapp02" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM 2 called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	sudo mount -t nfs 52.64.57.228:/home/Shared /mnt/Shared

elif [ $HOSTNAME = "aws151-testapp03" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM 3 called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	sudo mount -t nfs 52.64.57.228:/home/Shared /mnt/Shared
	
else
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm an unknown VM called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "This script file and the automation code likely need modifying."
fi
