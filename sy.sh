#!/bin/bash
rm /home/Shared/code/*.png
rm -R /home/Shared/code/Source/*.*~
clear

if [ $HOSTNAME = "aws151-testapp01" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM 1, aka the code machine"
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	cd /home/Shared/code/
	chown -R martinc:rvm *
	chmod -R 775 *
	echo "Permissions fixed."
	
	rclone sync /home/Shared/code et:code
	echo '***************************************'
	echo 'Sync with Google Drive complete.'
	echo '***************************************'
	echo

elif [ $HOSTNAME = "DadsPC" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm at home on" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	cd /home/Shared/code/
	chmod -R 775 *
	echo "Permissions fixed."
	
	rclone sync /home/Shared/code et:code
	echo '***************************************'
	echo 'Sync with Google Drive complete.'
	echo '***************************************'
	
elif [ $HOSTNAME = "Mint1" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm Mint1, a standalone linux box"
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	rclone copy et:code /home/Shared/code
	cd /home/Shared/code/
	chmod -R +x *.sh
	chown -R carrollm:adm *
	
elif [ $HOSTNAME = "Mint2" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm Mint2, a standalone linux box"
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	rclone copy et:code /home/Shared/code
	cd /home/Shared/code/
	chmod -R +x *.sh
	chown -R carrollm:adm *
	
elif [ $HOSTNAME = "Mint3" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm Mint3, a standalone linux box"
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	rclone copy et:code /home/Shared/code
	cd /home/Shared/code/
	chmod -R +x *.sh
	chown -R carrollm:adm *

elif [ $HOSTNAME = "aws151-testapp02" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM 2 called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	rsync -avzhP /mnt/Shared/ /home/Shared/

elif [ $HOSTNAME = "aws151-testapp03" ]; then
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm VM 3 called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	
	rsync -avzhP /mnt/Shared/ /home/Shared/
	
else
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "I'm an unknown VM called" $HOSTNAME
	echo xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	echo "This script file and the automation code likely need modifying."
fi
