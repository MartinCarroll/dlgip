sudo yum install glibc gcc gcc-c++ autoconf automake libtool git make nasm pkgconfig
sudo yum install SDL-devel a52dec a52dec-devel alsa-lib-devel faac faac-devel faad2 faad2-devel
sudo yum install freetype-devel giflib gsm gsm-devel imlib2 imlib2-devel lame lame-devel libICE-devel libSM-devel libX11-devel
sudo yum install libXau-devel libXdmcp-devel libXext-devel libXrandr-devel libXrender-devel libXt-devel
sudo yum install libogg libvorbis vorbis-tools mesa-libGL-devel mesa-libGLU-devel xorg-x11-proto-devel zlib-devel
sudo yum install libtheora theora-tools
sudo yum install ncurses-devel
sudo yum install libdc1394 libdc1394-devel
sudo yum install amrnb-devel amrwb-devel opencore-amr-devel 
