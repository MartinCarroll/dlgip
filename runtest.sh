#!/bin/bash
START=1
END=$2
str=$1
to=""
str=${str//'CSVs/'/$to}

if [ "$1" = "" ]; then
	echo "Missing filename parameter"
	echo './runtest <CSVs/yourFilename.csv>'
	exit
fi

if [ "$2" = "" ]; then
	END=1
fi
# Close existing Firefox sessions


dos2unix CSVs/$str

for ((c=$START; c<=$END; c++))
do
	clear
	ASTR="Run number $c of CSVs/$str"
   	echo -ne "\033]0;$ASTR\007"
	#ruby Source/main.rb CSVs/$str > test_results/$str.log &
	ruby Source/main.rb CSVs/$str
	#sleep 1
	#pid=$(pgrep ruby)
	#tail -f --pid $pid test_results/$str.log
#	killall firefox
done

