

####################################################
# This is the main file that reads CSV test script 
# files and executes the methods listed in them.
#
# Written by: Martin Carroll, April 2014
####################################################

# Required libraries
require 'rubygems' 
require 'selenium-webdriver' # rubygems must come before this line!
require 'simple-spreadsheet'  # Library file for handling spreadsheets
require 'faker'  # Used for creating realistic random data, i.e. names. http://rubydoc.info/gems/faker/1.3.0/frames
require '//home//Shared//code//Source//tools.rb'
require '//home//Shared//code//Source//portal.rb'
require '//home//Shared//code//Source//tempo.rb'
require '//home//Shared//code//Source//timesheet.rb' # Ignore, only for filling in timesheets ;-)

####################################################

$OS = Tools.getOS
if $OS == "windows"
	$CurrentWinUser = ENV['USERNAME']
	$rubyHome = "C:\\Users\\#{$CurrentWinUser}\\Programs\\ruby\\"
else
	$rubyHome = "/home/Shared/code/"
end
############# JMeter #####################
begin
	if  $JMeterRunning == nil
		$JMeterRunning = false
	end
rescue
end
########### Headless #####################
begin
	if $RunningHeadless == nil
		$RunningHeadless = false
	end
rescue
end
#########################################
$error = false
$mainBrowser = ""
$USER = ""
$PASS = ""
$PaymentRequired = true # Default to do reconciliation.
$myDasType = "" # MyDas type of either Tempo or Portal
$amounts = Array.new # Payment Amounts for reconciliation
$aFile1 = "#{$rubyHome}UploadFiles/Doc1.txt" # Dummy files to upload
$aFile2 = "#{$rubyHome}UploadFiles/Doc2.txt"	
# Get the Start time for calculating elapsed time later
$startTime = Time.now
I18n.enforce_available_locales = false

# Default browser position on screen
$startX = 820 # X coord
$startY = 0     # Y coord

# 'Faker' creates random data.
# For more examples see: http://rubydoc.info/github/stympy/faker/master/frames
$applicant = Faker::Name.name
$address = Faker::Address.street_address

# Get and store current date & time for later use
$today = Tools.currentDate         # 2016/03/11
$today = $today.gsub("/", "-")    # 2016-03-11
$now = Tools.getTime					# 1:22
$now = $now.gsub(":", ".")          # 1.22

# Get the current VNC display number (1 or 2) 
displayNumber = Tools.whichVNCSession
# The file to store the process ID of the Firefox browser opened by the automation.
$pidFile = "#{$rubyHome}#{displayNumber}_pid.pid"  # Used by Tools.getFirefoxProcessID

##### Demo Mode = true means no Firepath toolbar ########
$demo_mode = false
####################################################

def executeTests filename
	$filename = filename
	puts "********************************************************"
	puts "********   Script starting - CTRL + C to stop  *********"
	puts "********************************************************"
	puts "Processing: #{$filename}"
	puts ""
	
	if File::exists?( $filename ) # Check the input file exists
		# Get the file type.
		inputFileType = $filename.split(//).last(4).join("").to_s.downcase
		if inputFileType == "xlsx"
			inputFileType = ".xlsx"
		end
		puts "Spreadsheet is: " + inputFileType
		s = SimpleSpreadsheet::Workbook.read($filename, inputFileType)
		s.selected_sheet = s.sheets.first

		i = 1
		skip = false
		begin
			s.first_row.upto(s.last_row) do |line|
				if $appID != nil
					if $myDasType == ""
						if Tools.isPortal
							$myDasType = "Portal"
						else
							$myDasType = "Tempo"
						end
					end	
					dividerLine = Tools.getTime	+ " 000000000000000000000000000000 " + $myDasType + ": "+ $appID + " 000000000000000000000000000000000000"
				else
					dividerLine = Tools.getTime	+ " 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				end

				puts dividerLine
				begin
					@commentCol = s.cell(line, 1).lstrip.rstrip.downcase # Remove leading / trailing whitespace and make lower case
					methodName = s.cell(line, 2, 1).lstrip.rstrip.downcase # Remove leading / trailing whitespace and make lower case
				rescue
					begin
						puts "Error processing: " + s.cell(line,i).to_str
					rescue
						puts "Error processing: Unprintable characters found."
					end
				end

				if methodName == "" or methodName == nil or methodName == "stop" or methodName == "quit"
					puts "Found blank row / 'Stop' / 'Quit' at line #{@commentCol} so quitting."
					break

				elsif methodName == "endskip"
					skip = false # Start processing again
					puts "Stopping skipping lines"
					appID = s.cell(line, 3)
					if appID != nil
						$appID = appID
						puts "Application ID to use " << $appID 
					end

				elsif skip == true
					# Do nothing until skip == false
					i = i + 1 # Increment row counter
					puts "Skipped line: #{@commentCol}"

				elsif methodName == "x" or methodName == "parameter" or methodName == "parameters" or methodName == "description"
					# Skip the line
#					puts "xxx Skipping commented out line: #{s.cell(line,3)}"
				else
					# puts "Line #{i}: Executing #{methodName}"
					puts "Line #{@commentCol}: Executing #{methodName}"
				end
								
				if skip == false and @commentCol != "x"
					
					$method = methodName # For timing method execution time
					Tools.startTimer
					
					case methodName
					#when "task"
					#	Tempo.task(s.cell(line,3),s.cell(line,4))
						
					when "confirmpaymentdetails"
						Tools.confirmPaymentDetails

					when "approvedecisionnotice"
						Tools.ApproveDecisionNotice
						
					when "harness"
						Tools.harness
						
					when "selecttechnicalagenciestoprovideresponse"
						Portal.selectTechnicalAgenciesToProvideResponse
					
					when "validateearlyreferralrequest"
						Tempo.ValidateEarlyReferralRequest
						
					when "enterbillingdetails"
						Tempo.EnterBillingDetails
						
					when "contactassessmentmanageranduploadtheirdecisionnotice"
						Portal.contactAssessmentManagerAndUploadTheirDecisionNotice
						
					when "verifyresponseandassessmentreport"
						Portal.verifyResponseAndAssessmentReport(s.cell(line,3))
						
					when "getprocessid"
						Tools.getProcessID(s.cell(line,3))
						
					when "getprocesscoverage"
						Tools.getProcessCoverage
						
					when "processxmlresponse"
						Tools.processXMLResponse(s.cell(line,3),s.cell(line,4))
						
					when "selectTechnicalAgenciesToProvideResponse"
						Portal.selectTechnicalAgenciesToProvideResponse
						
					when "provideirrequirements"
						Tempo.provideIRRequirements
						
					when "whichvncsession"
						Tools.whichVNCSession
						
					when "sendingoutourresponsetoam"
						Portal.sendingOutOurResponseToAM
						
					when "endorserefund"
						Portal.endorseRefund
						
					when "reviewcreditcardvoucher"
						Portal.reviewCreditCardVoucher(s.cell(line,3))
												
					when "issuedecisionnotice"
						Portal.IssueDecisionNotice
											
					when "runjmeter"
						Tools.runJMeter
						
					when "provideassessment"
						Tempo.provideAssessment
						
					when "stopvideorecording"
						Tools.stopVideoRecording

					when "startvideorecording"
						Tools.startVideoRecording
						
					when "approvalofconcurrenceagencyresponse"
						Portal.approvalOfConcurrenceAgencyResponse
						
					when "setapplicationid"
						Tools.setApplicationID(s.cell(line,3))
						
					when "splitlotplan"
						Tools.splitLotPlan(s.cell(line,3))
						
					when "servertotest"
						Tools.serverToTest(s.cell(line,3), s.cell(line,4))
						
					when "collateandsendrunlog"
						Tools.collateAndSendRunLog
						
					when "consolidateandfinaliseresponses"
						Portal.consolidateAndFinaliseResponses
						
					when "reviewcasedecisiontempo"
						Tempo.reviewCaseDecision

					when "reviewcasedecision"
						Portal.reviewCaseDecision
						
					when "sendemail"
						Tools.sendEmail(s.cell(line,3), s.cell(line,4), s.cell(line, 5), s.cell(line, 6))
						
					when "preparetosendir"
						Portal.prepareToSendIR
						
					when "grabscreenshot"
						Tools.grabScreenshot(s.cell(line,3))
					
					when "pleaseproviderequestedinformation"
						Portal.pleaseProvideRequestedInformation
						
					when "approvedecisionnoticetempo"
						Tools.ApproveDecisionNotice

					when "approvedecisionnotice"
						Tools.ApproveDecisionNotice
						
					when "advice"
						Tempo.advice
						
					when "recordnotificationdetails"
						Tempo.recordnotificationdetails
						
					when "approveproperlyreferrednotice"
						Tempo.approveProperlyReferredNotice
						
					when "recorddecision"
						Portal.RecordDecision(s.cell(line,3))
					
					when "requestinformation"
						Tempo.requestInformation
						
					when "reviewproperlyreferrednotice"
						Tempo.reviewProperlyReferredNotice
						
					when "preparetosend"
						Portal.prepareToSend
						
					when "uploadtemplate"
						Portal.uploadTemplate
						
					when "applicationprepared"
						Tempo.applicationPrepared
						
					when "edituser"
						Tempo.editUser(s.cell(line,3), s.cell(line,4),s.cell(line,5))
						
					when "createmydasuser"
						Tempo.createMyDasUser(s.cell(line,3), s.cell(line,4), s.cell(line, 5))
					
					when "updateoutlookpasswords"
						Tempo.updateOutlookPasswords(s.cell(line,3), s.cell(line,4), s.cell(line, 5))
						
					when "initiatingreferralapplication"
						Tempo.initiatingReferralApplication
						
					when "confirmreferraldetails"
						Tempo.confirmReferralDetails
						
					when "checkpagefortext"
						# Tools.checkPageForText(s.cell(line,3))
						
					when "reviewdecisionnotice"
						Tempo.Reviewdecisionnotice(s.cell(line,3))
					
					when "approveproperlymadenotice"
						Tempo.Approveproperlymadenotice(s.cell(line,3))
						
					when "reviewproperlymadenotice"
						Tempo.reviewProperlyMadeNotice(s.cell(line,3))
						
					when "reviewearlyreferralresponses"
						Tempo.ReviewEarlyReferralResponses
						
					when "getstatus"
						Tempo.getStatus(s.cell(line,3), s.cell(line,4), s.cell(line, 5))
						
					when "reviewresponsenotice"
						Tempo.reviewResponseNotice(s.cell(line,3))
						
					when "validatepayment"
						Tempo.validatePayment()
						
					when "emailpaymentfile"
						Tempo.emailPaymentFile
						
					when "confirmpayitems"
						Tempo.confirmPayItems
						
					when "validatechangerequest"
						Tempo.validateChangeRequest
						
					when "approveresponsenotice"
						Tempo.approveResponseNotice

					when "sendpaymentfile"
						Tempo.makeSendPaymentFile
				
					when "makepaymentfile"
						Tempo.makeSendPaymentFile
					
					when "doreconciliation"
						Tools.doreconciliation(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6))
					
					when "getscheduletriggerfeeID"
						Tempo.getScheduleTriggerFeeID
						
					when "getssqrequestid"
						Tempo.getSSQRequestID
						
					when "searchforapplicationindb"
						Tempo.searchForApplicationInDB(s.cell(line,3))
					
					when "opendbtable"
						Tempo.openDBTable(s.cell(line,3))
						
					when "opendb"
						Tempo.openDB
						
					when "confirmpaymentitems"
						Tempo.confirmPaymentItems
						Tempo.confirmPaymentItems
						
					when "enterpostapprovalrequestdetails"
						Tempo.enterPostApprovalRequestDetails
						
					when "requestpreapplicationadvice"
						Tempo.RequestPreApplicationAdvice(s.cell(line,3), s.cell(line,4))
						
					when "registerrecord"
						Tempo.registerRecord(s.cell(line,3), s.cell(line,4), s.cell(line, 5),s.cell(line,6),s.cell(line,7))
						
					when "recordrequestdetails"
						Tempo. recordRequestDetails
					
					when "selectpostapprovalrequesttype"
						Tempo.selectPostApprovalRequestType(s.cell(line,3))
						
					when "requestpostapprovalchanges"
						Tempo.requestPostApprovalChanges(s.cell(line,3))
						
					when "loadvariable"
						Tools.loadVariable(s.cell(line,3), s.cell(line,4))
						
					when "ping"
						Tools.ping(s.cell(line,3))
						
					when "requesttechnicaladvice"
						Tempo.requestTechnicalAdvice(s.cell(line,3), s.cell(line,4))

					when "taresponse"
						Tempo.TAResponse
						
					when "reviewprereferraltriggers"
						Tempo.reviewPreReferralTriggers(s.cell(line,3))

					when "approveinformationrequestnotice"
						Tempo.approveInformationRequestNotice(s.cell(line,3))

					when "validateapplication"
						Tempo.validateApplication(s.cell(line,3),s.cell(line,4),s.cell(line,5))

					when "requesttechnicaladvice"
						Tempo.requestTechnicalAdvice(s.cell(line,3), s.cell(line,4))
						
					when "identifyvalidationoutcome"
						Tempo.identifyValidationOutcome(s.cell(line,3), s.cell(line,4))

					when "reviewvalidationoutcomenotice"
						Tempo.reviewValidationOutcomeNotice(s.cell(line,3), s.cell(line,4))

					when "approvevalidationoutcomenotice"
						Tempo.approveValidationOutcomeNotice(s.cell(line,3))
						
					when "reviewinformationrequestnotice"
						Tempo.reviewInformationRequestNotice(s.cell(line,3),s.cell(line,4))
								
					when "reviewtanotice"
						Tempo.ReviewTANotice(s.cell(line,3))

					when "requestforfurtherinformation"
						Tempo.requestForFurtherInformation

					when "informationrequestresponse"
						Tempo.informationRequestResponse(s.cell(line,3),s.cell(line,4))

					when "providerecommendation"
						Tempo.provideRecommendation(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6))

					when "reviewandmaintaintriggerspreref"
						Portal.reviewAndMaintainTriggersPreRef

					when "exit" # Quit leaving the browser open
						puts "'Exit' found so quit leaving browser open."
						exit

					when "nofeesrequired"
						Portal.noFeesRequired

					when "nominatefasttracktriggers"
						Tempo.nominateFastTrackTriggers

					when "refertodsdip"
						Tempo.referToDSDIP

					when "applicationpackageandemail"
						Tempo.applicationPackageAndEmail

					when "confirmapplicationdetails"
						Tempo.confirmApplicationDetails

					when "providenapproval"
						Portal.provideNApproval(s.cell(line,3),s.cell(line,4))

					when "selectnapprover"
						Portal.selectNApprover(s.cell(line,3),s.cell(line,4))

					when "selectapprover"
						Portal.selectApprover(s.cell(line,3),s.cell(line,4))

					when "extendedapprovaloutcome"
						Portal.extendedApprovalOutcome

					when "checkforscreen"
						Tools.checkForScreen(s.cell(line,3), s.cell(line,4))

					when "technicalagencyresponserequested"
						Portal.technicalAgencyResponseRequested

					when "selectfromalltriggers"
						Portal.selectFromAllTriggers(s.cell(line,3))

					when "completelodgementselectfeesandpay"
						Portal.completeLodgementSelectFeesAndPay(s.cell(line,3))

					when "confirmation"
						Portal.confirmation

					when "providen73_txapproval"
						Portal.provideN73_TXApproval

					when "selectn73_txapprover"
						Portal.selectN73_TXApprover(s.cell(line,3))

					when "changeapprovaloutcome"
						Portal.changeApprovalOutcome

					when "applicantagreement"
						Portal.applicantAgreement

					when "providen3tgapproval"
						Portal.provideN3TGApproval

					when "selectn3tgapprover"
						Portal.selectN3TGApprover

					when "displaytriggers"
						Portal.displaytriggers

					when "confirmpaymentdetailstempo"
						Tempo.confirmPaymentDetailsTempo

					when "enterbillingandconcessiondetails"
						Tempo.enterBillingAndConcessionDetails

					when "completeapplicationchecklist"
						Tempo.completeApplicationChecklist(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7))

					when "enterownerandbuilderdetails"
						Tempo.enterOwnerAndBuilderDetails

					when "enternatureandvalue"
						Tempo.enterNatureAndValue(s.cell(line,3),s.cell(line,4))

					when "enterfurtherlocationdetails"
						Tempo.enterFurtherLocationDetails(s.cell(line,3))

					when "enterfurtherassessmentdetails"
						Tempo.enterFurtherAssessmentDetails(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6))

					when "paymentpending"
						Tempo.paymentPending(s.cell(line,3))

					when "fillit"
						Timesheet.fillIt

					when "logints"
						Timesheet.loginTS(s.cell(line,3),s.cell(line,4))

					when "finduser"
						Tempo.findUser(s.cell(line,3))

					when "opentask"
						Tempo.openTask(s.cell(line,3),s.cell(line,4))

					when "processtriggerfile"
						Triggers.processTriggerFile(s.cell(line,3))

					when "shouldaninformationrequestbeissuedtotheapplicant"
						Portal.shouldAnInformationRequestBeIssuedToTheApplicant(s.cell(line,3))
		
					when "confirmationtempo"
						Tempo.confirmationTempo(s.cell(line,3))

					when "enterapplicantdetails"
						Tempo.enterApplicantDetails(s.cell(line,3),s.cell(line,4))

					when "openbrowserbs"
						Tools.openBrowserBS(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8))

					when "managedocumentseasyprep"
						Tempo.manageDocumentsEasyPrep(s.cell(line,3),s.cell(line,4))

					when "assessmenttriggers"
						Tempo.assessmentTriggers(s.cell(line,3))
						
					when "selectreferraltriggers"
						Tempo.selectReferralTriggers(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14), s.cell(line,15))

					when "referraltriggers"
						Tempo.referralTriggers(s.cell(line,3))

					when "enterassessmentdetails"
						Tempo.enterAssessmentDetails(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14), s.cell(line,15))

					when "application" # previously Easy Prep
						Tempo.application(s.cell(line,3), s.cell(line,4))

					when "addlot"
						Tempo.addLot(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14), s.cell(line,15))

					when "registersuborganisation"
						Tempo.registerSubOrganisation(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14), s.cell(line,15))

					when "managedocuments"
						Tempo.manageDocuments(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6))

					when "applications"
						Tempo.applications(s.cell(line,3))

					when "requestforprelodgementadviceform"
						Portal.requestForPrelodgementAdviceForm

					when "checkpassword"
						Tools.checkPassword(s.cell(line,3), s.cell(line,4),s.cell(line,5))

					when "login365"
						Tools.login365(s.cell(line,3), s.cell(line,4),s.cell(line,5))

					when "endskip"
						# Do nothing

					when "uploaddocuments"
						Portal.uploadDocuments

					when "uploaddocument"
						Portal.uploadDocuments

					when "displaytriggers"
						Portal.displaytriggers

					when "uploadtechnicalresponsedocument"
						Portal.uploadTechnicalResponseDocument

					when "shouldaninformationrequestbeissued"
						Portal.shouldAnInformationRequestBeIssued(s.cell(line,3), s.cell(line,4))

					when "getnewapplicationid"
						Portal.getNewApplicationID

					when "skip"
						skip = true

					when "provideapproval"
						Portal.provideApproval(s.cell(line,3))

					when "selectapprover"
						Portal.selectApprover(s.cell(line,3))

					when "validaterequest"
						Portal.validateRequest(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7))

					when "providelegacyrecordsandadvice"
						Portal.provideLegacyRecordsAndAdvice(s.cell(line,3))

					when "recordnativetitleassessment"
						Portal.recordNativeTitleAssessment(s.cell(line,3))

					when "proceedtopayment"
						Portal.proceedToPayment(s.cell(line,3))

					when "enterdsdiprecieveddate"
						Portal.enterDSDIPRecievedDate(s.cell(line,3))
				
					when "createnewmydasuseraccount"
						#Tempo.createNewMyDasUserAccount(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11))
						Tempo.createNewMyDasUserAccount(s.cell(line,3), s.cell(line,4),s.cell(line,5))

					when "registerorganisation"
						Tempo.registerOrganisation(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14))

					when "tempologin"
						Tools.login(s.cell(line,3), s.cell(line,4), "")
						
					when "closebrowser"
						Tools.closeBrowser
							
					when "opentempo"
						Tempo.openTempo

					when "registernewindividual"
						Tempo.registerNewIndividual(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11), s.cell(line,12), s.cell(line,13), s.cell(line,14), s.cell(line,15), s.cell(line,16), s.cell(line,17))

					when "confirmpaymentstatus"
						Portal.confirmPaymentStatus

					when "getpaymentid"
						Portal.getPaymentID()

					when "createemail"
						Portal.createEmail(s.cell(line,3), s.cell(line,4),s.cell(line,5))

					when "addbackup"
						Portal.addBackUp(s.cell(line,3))

					when "getemail"
						Portal.getEmail(s.cell(line,3), s.cell(line,4),s.cell(line,5))

					when "approvalrequestdetails"
						Portal.approvalRequestDetails(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10), s.cell(line,11))

					when "requestordetails"
						Portal.requestorDetails(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8), s.cell(line,9), s.cell(line,10))

					when "approvalchanges"
						Portal.approvalChanges(s.cell(line,3), s.cell(line,4),s.cell(line,5), s.cell(line,6), s.cell(line,7), s.cell(line,8))

					when "nofeesrequired"
						Portal.noFeesRequired

					when "provideresponse"
						Portal.provideResponse

					when "identifyprereferralresponsestatus"
						Portal.identifyPrereferralResponseStatus(s.cell(line,3))

					when "selecttechnicalagencies"
						Portal.selectTechnicalAgencies(s.cell(line,3), s.cell(line,4))

					when "feepaymentform"
						Portal.feePaymentForm(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))

					when "triggersrequiringfeeselection"
						Portal.triggersRequiringFeeSelection(s.cell(line,3))

					when "reviewtriggerstempo"
						Tools.reviewTriggers(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))

					when "reviewtriggers"
						Tools.reviewTriggers(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))

					when "clickrefresh"
						Portal.clickRefresh

					when "providen6t3approval"
						Portal.provideN6T3Approval

					when "selectn6t3approver"
						Portal.selectN6T3Approver

					when "assessprelodgementinformation"
						Portal.assessPrelodgementInformation

					when "uploadacknowledgementnotice"
						Portal.uploadAcknowledgementNotice

					when "selecttriggerscreen"
						Portal.selectTriggerScreen(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))

					when "prelodgementadviceform"
						Portal.prelodgementAdviceForm

					when "uploadformsupportingdocuments"
						Portal.uploadFormSupportingDocuments
			
					when "preparedevelopmentapplication"
						Portal.prepareDevelopmentApplication(s.cell(line,3),s.cell(line,4))
		
					when "addcontactdetails"
						Portal.addContactDetails(s.cell(line,3),s.cell(line,4))
			
					when "gisinformation"
						Portal.gISInformation(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))
			
					when "developmentaspect"
						Portal.developmentAspect(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))
			
					when "idasform3"
						Portal.iDASForm3
			
					when "selectn35_t10approver"
						$approver = s.cell(line,3)
						if $approver == ""
							puts "No approver supplied so default used."
						end
						puts "Approver to be used is: #{$approver}"
						Portal.selectN35_T10Approver

					when "providen35_t10approval"
						Portal.provideN35_T10Approval

					when "reviewnoticepdf"
						Portal.reviewNoticePDF

					when "reviewnotice"
						Portal.reviewNotice

					when "gettemporeports"
						Tempo.getTempoReports
		
					when "resubmitapplication"
						Portal.resubmitApplication
			
					when "proceedwithapplication"
						Portal.proceedWithApplication
			
					when "uploadproperlymadechecklist"
						Portal.uploadProperlyMadeChecklist

					when "uploadproperlyreferredchecklist"
						Portal.uploadProperlyReferredChecklist
			
					when "reviewandmaintaintriggers"
						Portal.reviewAndMaintainTriggers(s.cell(line,3),s.cell(line,4),s.cell(line,5),s.cell(line,6),s.cell(line,7),s.cell(line,8),s.cell(line,9),s.cell(line,10),s.cell(line,11),s.cell(line,12),s.cell(line,13),s.cell(line,14),s.cell(line,15),s.cell(line,16),s.cell(line,17),s.cell(line,18))
		
					when "reselectpaymentitems"
						Portal.reselectPaymentItems
		
					when "reviewcase"
						Portal.reviewCase(s.cell(line,3))
		
					when "allocatecase"
						Tools.allocateCase(s.cell(line,3), s.cell(line,4), s.cell(line, 5),s.cell(line,6))

					when "allocatecasetempo" # Defunct
						Tools.allocateCase(s.cell(line,3), s.cell(line,4), s.cell(line, 5),s.cell(line,6))
		
					when "idasform2"
						Portal.idasForm2

					when "formsupdate"
						Portal.formsUpdate
			
					when "selectformstoissue"
						Portal.selectFormsToIssue
		
					when "selectpayitems"
						Tools.selectPayItems(s.cell(line,3))

					when "selectpayitemstempo"
						Tools.selectPayItems(s.cell(line,3))

					when "beginlodgement"
						Portal.beginLodgement(s.cell(line,3))
		
					when "getapplication"
						Portal.getApplication(s.cell(line,3), s.cell(line,4))
			
					when "openapplication"
						Tools.openApplication(s.cell(line,3))
						
					when "openapplicationtempo"
						Tools.openApplication(s.cell(line,3))
						
					when "openbrowser"
						Tools.openBrowser(s.cell(line,3), s.cell(line,4))

					when "login"
						Tools.login(s.cell(line,3), s.cell(line,4), s.cell(line,5))
						
					when "prelodgement"
						Portal.prelodgement
			
					when "prerefer" , "prereferral"
						Portal.preRefer
			
					when "change"	
						Portal.change	

					when "createusers"
						Tools.createUsers

					when "logout"
						#Tools.logout
						puts "Defunct code found. Please remove logout from your spreadsheet as it is now part of login"
			
					when "ignore", "x", "parameters", "parameter", "description"
						# This is just so tests can be ignored

					when "method"
						# Skip line
					else
						puts "**************** SCRIPT ERROR *******************************"
						puts ""
						puts "Error: Unknown method name: #{methodName}"
						puts ""
						puts " It needs to be added to the case statement in 'main.rb' !!"
						puts "Halting execution."
						puts ""
						puts "**************** SCRIPT ERROR *******************************"
						$error = "Error: Unknown method name: #{methodName}"
						break
					end
					i = i + 1
					Tools.endTimer($method) # Capture the length of the method's execution time
				end
			end
		end
		
		# Grab a screenshot
		#if $appID == nil
		#	Tools.grabScreenshotOfError("no", "NoAppID")
		#else		
		#	Tools.grabScreenshotOfError("no", $appID)
		#end
		
		# Get the elapsed run time.
		elapsedTime = Tools.elapsedTime($startTime)

		logFile = "#{$rubyHome}#{$filename}.log"
		puts "Creating log file: #{logFile}"

		if $applicationType != nil
			puts "Application type created was: '#{$applicationType}'"
		end

		if $appID != nil
			puts "Application created was: '#{$appID}'"
			creationInfo = $appID + " - " + $startTime.to_s + " - " + elapsedTime
		else
			creationInfo = "No App ID - " + $startTime.to_s + " - " + elapsedTime
		end
		Tools.logger(logFile, creationInfo)

		if $appID == nil
			$appID = "but no application created"
		end
		if $error == false
			puts "******************** FINISHED SUCCESSFULLY " + $appID + " ***************************"
			puts ""
			puts "               Script finished after #{elapsedTime}."
			puts ""
			puts "******************** FINISHED SUCCESSFULLY " + $appID + " ***************************"
			
			# Get & display the code coverage
			Tools.getProcessCoverage
			
			returnValue = true
		elsif $error == true
			if $appID == nil
				$appID = "No app created"
			end
			puts "******************** FINISHED WITH ERROR " + $appID + " *****************************"
			puts ""
			puts "               Script finished after #{elapsedTime}."
			puts ""
			puts "******************** FINISHED WITH ERROR " + $appID + " *****************************"
			returnValue = false
		else
			puts "****************************** FINISHED " + $appID + " **********************************"
			puts ""
			puts "               Script finished after #{elapsedTime}."
			puts ""
			puts "               Script returned " << $error.to_s
			puts "****************************** FINISHED " + $appID + " **********************************"
			returnValue = false
		end
		
		begin
			return returnValue
			$driver.quit				
		rescue
			exit
		end
		
	else
		puts "Error: No file called #{$filename} found."
		puts "Please check the path."
	end
end

# Execute the tests in the supplied file 
if ARGV[0] != nil then
	executeTests ARGV[0]
else
	puts ""
	puts "Error: No test file specified, quitting. Try tests.csv as a parameter."
end
