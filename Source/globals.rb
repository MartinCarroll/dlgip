####################################################
#
# This is a repository for Global Variables and Constants
#
# Written by: Martin Carroll, April 2016
####################################################

require '//home//Shared//code//Source//tools.rb'

if $OS == "windows"
	$CurrentWinUser = ENV['USERNAME']
	$rubyHome = "C:\\Users\\#{$CurrentWinUser}\\Programs\\ruby\\"
else
	$rubyHome = "/home/Shared/code/"
end
############# JMeter #####################
begin
	if  $JMeterRunning == nil
		$JMeterRunning = false
	end
rescue
end
########### Headless #####################
begin
	if $RunningHeadless == nil
		$RunningHeadless = false
	end
rescue
end
#########################################
$error = false
$mainBrowser = ""
$USER = ""
$PASS = ""
$myDasType = "" # MyDas type of either Tempo or Portal
$amounts = Array.new # Payment Amounts for reconciliation
$aFile1 = "#{$rubyHome}UploadFiles/Doc1.txt" # Dummy files to upload
$aFile2 = "#{$rubyHome}UploadFiles/Doc2.txt"	
# Get the Start time for calculating elapsed time later
$startTime = Time.now
I18n.enforce_available_locales = false

# Default browser position on screen
$startX = 820 # X coord
$startY = 0     # Y coord
# 'Faker' creates random data.
# For more examples see: http://rubydoc.info/github/stympy/faker/master/frames
$applicant = Faker::Name.name
$address = Faker::Address.street_address
# Get and store current date & time for later use
$today = Tools.currentDate         # 2016/03/11
$today = $today.gsub("/", "-")    # 2016-03-11
$now = Tools.getTime					# 1:22
$now = $now.gsub(":", ".")          # 1.22

# Get the current VNC display number (1 or 2) 
displayNumber = Tools.whichVNCSession
# The file to store the process ID of the Firefox browser opened by the automation.
$pidFile = "#{$rubyHome}#{displayNumber}_pid.pid"  # Used by Tools.getFirefoxProcessID

##### Demo Mode = true means no Firepath toolbar ########
$demo_mode = false
####################################################