####################################################
#
# These are common methods for both Portal and Tempo
#
# Written by: Martin Carroll, April 2014
####################################################

require 'date'
require 'selenium-webdriver'
require 'rubygems'
require 'ostruct' # For manipulating JSON
require 'clipboard'
require 'ping'
require 'resolv-replace'
require 'fileutils'
require 'uri'
require 'headless' # For video recording
require '//home//Shared//code//Source//portal.rb'
require '//home//Shared//code//Source//tempo.rb'


module Tools
	def Tools.confirmPaymentDetails
		if Tools.isPortal
			Portal.confirmPaymentDetails(s.cell(line,3))
		else
			Tempo.confirmPaymentDetails
		end
	end
	
	
	
	
						
	def Tools.ApproveDecisionNotice
		if Tools.isPortal
			Portal.ApproveDecisionNotice
		else
			Tempo.ApproveDecisionNotice		
		end
	end
	
	
	
	
	
	def Tools.harness
		test = "007971"
		puts test
		test = test.to_i - 3000
		puts test
	end
	
	
	
	def Tools.selectPayItems(*vars)
		puts "Entering Tools.selectPayItems"
		if isPortal
			Portal.selectPayItems
		else
			Tempo.selectPayItems(*vars)
		end
		puts "Exiting Tools.selectPayItems"
	end

	
	
	
	def Tools.reviewTriggers(*vars)
		puts "Entering Tools.reviewTriggers"
		if isPortal
			Portal.reviewTriggers(*vars)
		else
			Tempo.reviewTriggers(*vars)
		end
		puts "Exiting Tools.reviewTriggers"
	end
	
	
	
	
	def Tools.allocateCase(*variables)
		puts "Entering allocateCase"
		# if Tempo then variables are: caseOfficer, adminOfficer, planningManager, appID
		# if Portal then variable is caseOfficer
		if isPortal
			Portal.allocateCase(*variables)
		else
			Tempo.allocateCase(*variables)
		end
		
		puts "Exiting allocateCase"
	end
	
	
	
	
	def Tools.getShortAppID
		puts "Entering get ShortAppID"
		# Remove unwanted text to leave last 4 digits of AppID.
		# i.e. '1509-5100 SDA' becomes '5100'
		if $appID != nil
			puts "$appID: " << $appID
			appID = $appID.sub("SDA", "").strip 
			appID = appID.sub("SRA", "") 
			appID = appID.sub("SPD", "") 
			appID = appID.sub("SMR", "") 
			appID = appID.sub("SER", "") 
			appID = appID.sub("SPA", "") 
			appID = appID.sub("SPL", "") 
			appID = appID.sub(" ", "") 
			appID = appID.lstrip # Remove leading whitespace
			appID = appID.rstrip # Remove trailing whitespace
			apps = Array.new
			apps = appID.split(/-/) # Extract Numbers into array based on the separator '-'
			if apps[2] == nil
				$shortAppID = apps[1] # Get the 1st element in the app id array as this is the bit we want
			else
				$shortAppID = apps[2] # Get the 2nd element in the app id array as this is the bit we want
			end
		
			puts "$shortAppID: " << $shortAppID
			return $shortAppID
		else
			puts "No application ID to shorten so quit."
			exit
		end
		puts "Exiting getShortAppID"
	end
	
	
	
	
	def Tools.getProcessID( environment, applicationID="3921" )
		# This uses SOAPUI from the command line to retrieves the Parent Process ID
		puts "Entering get Process ID"

		# Build the command to execute
		runSOAP = "sh /home/Shared/code/SoapUI/bin/testrunner.sh -s'TestSuite' -G'APP_ID=#{applicationID}' -G'ENVIRONMENT=#{environment}' -a -j -f'/home/Shared/code/SoapUI/scripts/reports' '/home/Shared/code/SoapUI/scripts/getProcessID.xml'"
		# Execute the command
		puts runSOAP
		system( runSOAP )

		# Process the response file
		xMLFile = "/home/Shared/code/SoapUI/scripts/reports/TestSuite-TestCase-GetApplicationProcessId-0-UNKNOWN.txt"
		File.read( xMLFile ).lines.each do |line|  # Read file until last line found
			@processID = line
		end
		puts @processID
		@processID = @processID.sub("[","")
		@processID = @processID.sub("]","")
		@processID = @processID.strip
		
		puts "====================================="
		if @processID != "404 - Not Found"
			puts "Parent Process ID: '#{@processID}'"
		else
			puts "ERROR: Process ID not found."
		end
		puts "====================================="
		puts "Exiting getProcessID"
		return @processID
	end
	
	
	def Tools.getProcessCoverage
		puts "Entering getProcessCoverage"
				
		if $URL == nil
			puts "No url found for performing test coverage. Skipping."
		else
			# Get the base server name, 
			# i.e. https://mydastest.appiancloud.com/suite/tempo becomes mydastest, etc.
			@environment = $URL
			@environment = @environment.sub("https://", "")
			@environment = @environment.sub(".appiancloud.com/suite/tempo", "")
			@environment = @environment.sub(".appiancloud.com/suite/apps", "")
			
			begin
				if $shortAppID == nil
					$shortAppID = Tools.getShortAppID # Get the shortened App ID from the application
				end
				puts "$shortAppID is " << $shortAppID

				# Get the Process ID for the next step
				@processID = getProcessID( @environment, $shortAppID )

				if @processID != "404 - Not Found"

					# Build the command to execute
					runSOAP = "sh /home/Shared/code/SoapUI/bin/testrunner.sh -s'TestSuite' -G'PROCESS_ID=#{@processID}' -G'ENVIRONMENT=#{@environment}' -a -j -f'/home/Shared/code/SoapUI/scripts/reports' '/home/Shared/code/SoapUI/scripts/getCodeCoverage.xml'"

					# Execute the command
					puts runSOAP
					system( runSOAP )

					# Process the response file
					xMLFile = "/home/Shared/code/SoapUI/scripts/reports/TestSuite-TestCase-CodeCoverage__Request_1-0-OK.txt"
					processXMLResponse( xMLFile )
				end
			rescue
				puts "No application ID found."
			end		
		end
		puts "Exiting getProcessCoverage"
	end
	
	
	
	
	
	
	def Tools.processXMLResponse(xMLFile)
		puts "Entering Process XML Response"
		
		if xMLFile == nil
			puts "No XML supplied"
			exit
		else
			puts xMLFile
			# Create the log file name based on the original CSV file used.
			coverage = $filename.sub(".csv", "")
			coverage = coverage.sub(".xls", "")
			coverage = coverage.sub(".xlsx", "")
			logFile = $rubyHome << coverage << "_coverage.log"
			
			deleteFile(logFile) # Delete old log file if found.
			
			count = 0 # Count of processes used
			total = 0
			File.read( xMLFile ).lines.each do |line|
				if line[0..16].to_s == '[{"processModelId' # Found JSON text
					json = JSON.parse( line, object_class: OpenStruct )

					puts "================ Process code coverage ================="
					json.each {|aLine| 
						count = count + 1
						percent = aLine.percentCoverage * 100
						percent = percent.round.to_i
						total = total + percent
						message = percent.to_s << "%, " << aLine.processModelName
						puts message
						Tools.logger(logFile, message)
					} 
					average = total / count
					message = "Average was " << average.to_s << "%"
					puts message
					Tools.logger(logFile, message)
					puts "================ Processed code coverage ================="
				end
			end
		end		
	end # processXMLResponse
	
	
	
	def Tools.clickAppianlink
		# Entering clickAppianlink
		
		# Click the Appian DB link
		i = 1
		linkClicked = false
		while not linkClicked
			begin
				$driver.find_element(:xpath, "//*[text()='Appian']").click
				puts "Appian link clicked"
				sleep 2
				linkClicked = true
			rescue
				sleep 1
				i = i + 1
				if i == 30
					puts "Can't find 'Appian' link."
					exit
				end
			end
		end		
	end # clickAppianlink




	
	def Tools.isPortal
		# This determines whether the SUT is Portal or Tempo
		if $appID != nil
			firstChar = $appID[0]
			result = Integer(firstChar) rescue false
			if not result # First character of application is char, so it's Portal application
				puts "We are in Portal"
				return true
			elsif result # First character of application is integer, so it's Tempo application
				puts "We are in Tempo"
				return false
			end
		elsif $URL["suite/apps"]
			puts "We are in Portal"
			return true
		elsif $URL["suite/tempo"]
			puts "We are in Tempo"
			return false
		else
			puts "No site-determining URL found,"
			exit
		end
	end # isPortal
	
	
	
	
	
	
	def Tools.openApplication( appID )
		# This opens an existing application in either Portal or Tempo
		if appID == nil
			if $appID != nil
				appID = $appID
			else
				puts "No appID provided"
				exit
			end
		end
		
		if Tools.isPortal
			Portal.openApplication(appID)
		else
			Tempo.openApplication(appID)
		end
	end # openApplication
	
	
	
	
	
	
	def Tools.readPID
		# Reads the Firefox PID from file
		begin
			# Get the PCs Hostname
			hostname = `echo $HOSTNAME`
			hostname = hostname.rstrip # Strip carriage return from result
			puts hostname
			
			
			# Get the current VNC display number (1 or 2) 
			displayNumber = Tools.whichVNCSession
			
			# The file that stores the process ID of the Firefox browser opened by the automation.
			$pidFile = "#{$rubyHome}#{hostname}-#{displayNumber}_pid.pid"
			puts $pidFile
			$pid = File.read($pidFile)
			puts "Read PID of '#{$pid} from " << $pidFile
			return true
		rescue
			puts "Unable to read PID file: " << $pidFile
			return false
		end		
	end # readPID
	
	
	
		
	
	def Tools.whichVNCSession
		# This returns the VNC Display number.
		#puts "Entering 'Which VNC Session'."
		displayNumber = "0"
		if not $RunningHeadless
			vncSessionInfo = `xdpyinfo`  # Backticks run 'system' commands  :-)
			displayNumber = vncSessionInfo[21..21] # Get display 1 or 2
			puts "VNC display is #{displayNumber}"
		end
		#puts "Exiting 'whichVNCSession'."
		return displayNumber
	end
	
	
	
	
	
	
	
	def Tools.getFirefoxProcessID
		begin
			# Returns the PID of Firefox (launched by Ruby)
			bridge = $driver.instance_variable_get(:@bridge)
			launcher = bridge.instance_variable_get(:@launcher)
			binary = launcher.instance_variable_get(:@binary)
			process = binary.instance_variable_get(:@process)
			$pid = process.pid.to_s
			puts "Firefox PID is " << $pid
			
			# Write the PID to file
			File.write($pidFile, $pid)
			puts "PID written to file: " << $pidFile
		rescue
			puts "Error getting PID"
		end
	end # getFirefoxProcessID
	
	
	
	
	
	
	
	def Tools.deleteFile(file)
		# Deletes a specified file.
		begin
			File.delete(file)
			puts "File deleted: " << file
		rescue
			puts "File not found to delete: " << file
		end
	end
	
	
	
	
	

	def Tools.runHeadless
		# Allows the automation to know when running 'screen-less'
		$RunningHeadless = true
	end



	
	
	
	def Tools.runJMeter
		# Allows the automation to know when it's to use JMeter,
		# specifically to use the JMeter Proxy as JMeter is recording the script run
		$JMeterRunning = true
	end


	
	
	
	def Tools.startVideoRecording() # pathToVideo )
		puts "Entering Start Video Recording"
		# When initiating Headless you may pass a hash with video options:
		# headless = Headless.new(:video => { :frame_rate => 12, :codec => 'libx264' })
		# Available options:
		# :codec - codec to be used by ffmpeg
    	# :frame_rate - frame rate of video capture
    	# :provider - ffmpeg provider - either :libav (default) or :ffmpeg
		# :provider_binary_path - Explicit path to avconv or ffmpeg. Only required when the binary cannot be discovered on the system $PATH.
		# :pid_file_path - path to ffmpeg pid file, default: "/tmp/.headless_ffmpeg_#{@display}.pid"
		# :tmp_file_path - path to tmp video file, default: "/tmp/.headless_ffmpeg_#{@display}.mov"
		# :log_file_path - ffmpeg log file, default: "/dev/null"
		# :extra - array of extra ffmpeg options, default: []
		begin		
			$RunningHeadless = true
			puts "Running headless."
			
			puts "Try to start recording."
			$movie = "/home/Shared/code/" << $today << "_" << $now << ".mov"
			puts "Saving to movie file " << $movie
			
			#$headless = Headless.new(:video => { :frame_rate => 12, :codec => 'libx264', :provider => 'ffmpeg', :log_file_path => STDERR})
			#$headless = Headless.new(:video => { :display => 99, :frame_rate => 12, :provider => 'libav', :tmp_file_path => '/home/Shared/code/test_results/.headlessTempFile.mov', :log_file_path => STDERR})
			#$headless = Headless.new(:video => { :frame_rate => 12, :provider => 'libav', :tmp_file_path => '/home/Shared/code/test_results/.headlessTempFile.mov', :log_file_path => STDERR})
			#$headless.new(display: 100, reuse: true, destroy_at_exit: true).start
			#$headless = Headless.new
			#$headless = Headless.new(video: {:provider => 'libav',log_file_path: STDERR})
			$headless = Headless.new(video: {:frame_rate => 12, :provider => 'libx264', :screen => 0, :log_file_path => STDERR})

			puts "**********************"
			puts $headless
			#puts headless.keys
			puts "**********************"
			#exit
			$headless.start
			$headless.video.start_capture
		rescue
			puts "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
			puts "Error starting recording of video. Quitting"
			puts "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
			exit
		end
		
		puts "Exiting startVideoRecording"
	end


	
	def Tools.stopVideoRecording
		puts "Entering Stop Video Recording"
			begin
				puts "Saving to movie file " << $movie
				$headless.video.stop_and_save($movie)
				$headless.destroy
			rescue
				puts "Error stopping recording of video."
				#$headless.video.stop_and_discard
			end
			$RunningHeadless = false
		puts "Exiting stopVideoRecording"
	end
	
	
	
	
	
	def Tools.openBrowserBS( url, os, osVersion, browser, browser_version, resolution)
		# Get parameters here: http://www.browserstack.com/automate/ruby#getting-started
		# Get username and key her: https://www.browserstack.com/accounts/automate
		user = "dilgpplanning1"
		key = "UWii3Bqq9rswe9gGZUNX"

		puts "6666666666666666666666666666666666666666666666"
		puts "To view run open: https://www.browserstack.com/automate"
		puts "Username: Browserstack1.automated@dilgp.qld.gov.au"
		puts "Password: BrowserStack"
		puts "6666666666666666666666666666666666666666666666"
		puts ""
		puts "Entering openBrowserBS"
		caps = Selenium::WebDriver::Remote::Capabilities.new
		caps["browser"] = browser
		caps["browser_version"] = browser_version
		caps["os"] = os
		caps["os_version"] = osVersion

		if resolution == nil
			caps['resolution'] = '1920x1080'
		else
			caps['resolution'] = resolution
		end
		caps["browserstack.debug"] = "true"

		$driver = Selenium::WebDriver.for(:remote,
		  :url => "http://#{user}:#{key}@hub.browserstack.com/wd/hub",
		  :desired_capabilities => caps)
		  
		puts "Move browser to the left top"
		$driver.manage.window.move_to(0,0)		  

		puts "Resize the browser"
		$driver.manage.window.resize_to(1200, 1050)
		
		$driver.navigate.to url
		puts "Exiting openBrowserBS"
	end






	def Tools.openBrowser(browser, url)
		puts "Entering openBrowser"

		if browser == nil
			browser = "firefox"
			puts "No 'browser' supplied so using Firefox as default"
		elsif browser["http"] # No browser supplied but url was, so we'll use that.
			url = browser
		end
		
		# Set up some global variables for the test run.
		#$URL = url 				                        # Server to test
		$browser = browser.downcase 		# Browser to use for testing (Firefox/Chrome/IE
		$OS = Tools.getOS			                # Operating system to run tests on (Windows/Linux)

		# Display Environment to be used:
		puts "******************************"
		if url["test"]
			puts "Opening Test environment: " + url
		elsif url["dev"]
			puts "Opening Dev environment: " + url
		elsif url["train"]
			puts "Opening Training environment: " + url
		elsif url["stage"]
			puts "Opening Staging environment: " + url
		else
			puts "Opening non-standard environment: " + url
		end
		puts "******************************"
		
		# This opens a browser for use by the tests.
		if $browser == "firefox"
			profile = Selenium::WebDriver::Firefox::Profile.new

			if not $demo_mode # Use Add-ons
				if $OS == "windows"
				
					if RUBY_PLATFORM == "x64-mingw32" # Windows 8 64 bit
						addon = "C:\\Users\\#{$CurrentWinUser}\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\q6hgdsbu.default\\extensions\\"
					else
						addon = "C:\\Documents and Settings\\#{$CurrentWinUser}\\Application Data\\Mozilla\\Firefox\\Profiles\\8g8a3rdb.default\\extensions\\"
					end
				else	# Linux
					addon = "//home//Shared//code//FFExtensions//"				
				end
				#profile.add_extension("#{addon}firebug@software.joehewitt.com.xpi")
				#profile.add_extension("#{addon}FireXPath@pierre.tholence.com.xpi")
				#profile["extensions.firebug.allPagesActivation"]  = "on" # Always on
				#profile["extensions.firebug.showFirstRunPage"] = false   # Don't show first run page
			end				
			

			begin
				puts "Running Firefox"
				if $JMeterRunning == true
					profile.assume_untrusted_certificate_issuer = false # Allow invalid certificates for JMeter
					#profile.secure_ssl = true
					$PROXY = "localhost:8080"

					profile.proxy = Selenium::WebDriver::Proxy.new(
						:http     => $PROXY,
						:ftp      => $PROXY,
						:ssl      => $PROXY
					)
					puts "JMeter proxy being used"
					$driver = Selenium::WebDriver.for :firefox, :profile=> profile
				else
					puts "JMeter proxy not required"
					puts "Starting '#{$browser}' browser. Please wait..."
					#*****************************************************
					# Erroring on the next code line? 
					# Try: 
					# sudo gem update selenium-webdriver
					# 
					# http://stackoverflow.com/questions/14303161/unable-to-obtain-stable-firefox-connection-in-60-seconds-127-0-0-17055
					#
					# Note:	It could also be that the version of Firefox is too new
					#
					#*****************************************************
					$driver = Selenium::WebDriver.for :firefox, :profile=> "default"
				end
				puts "Firefox is loading..."

			rescue
				puts "Error connecting to Firefox. Try: 'sudo gem update selenium-webdriver'"
				puts "It could also be that the version of Firefox is too new"
				puts "It could also be that the firefox profile is not called 'default'."
				puts "Check with '~/firefox/firefox -ProfileManager'"
				puts "Get older versions of firefox here: https://ftp.mozilla.org/pub/firefox/releases/"
				exit
			end

		elsif $browser == "ie"
			puts "Running IE"
			$driver = Selenium::WebDriver::for :ie
			
		elsif $browser == "chrome"
			# system("/usr/bin/google-chrome") # Use to check if Chrome runs at all
			puts "Running Chrome"
			$driver = Selenium::WebDriver::for :chrome
			
		else
			puts "Error: Unknown browser specified of: #{$browser}"
			exit
		end
		
		# Get the Firefox PID so we can kill Firefox in the event 
		# of an error, as the browser would normally stay open.
		Tools.getFirefoxProcessID
		
		sleep 3
		# Go to URL
		puts "Navigate to #{url}"
		$driver.navigate.to url
		
		if $mainBrowser == ""
			$mainBrowser = $driver
		end

		# $driver.manage.window.maximize
		
		#################################################################
		#
		# Get the screen dimensions on Linux command line: xdpyinfo  | grep 'dimensions:'
		#
		#################################################################
		
		puts "Resize the browser"
		$driver.manage.window.resize_to(1200, 1050)

		sleep 1
		Tools.moveBrowser
		
		# Get the main browser window
		$main_window = $driver.window_handle		

		puts "Exiting openBrowser"
	end


	
	


	def Tools.moveBrowser
		#puts "Entering moveBrowser"
		if not $RunningHeadless
			#puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX, $startY)
		else
			puts "Running headless no no need to move browser" 
		end
		#puts "Exiting moveBrowser"
	end
		
	
	
	
	
	
	def less_than_1day_old
		now = Date.today
		each do |file| # get each file less than a day old
			yield file if (now - File.stat(file).mtime.to_date) < 1
		end
	end
	
	
	
	

	def Tools.collateAndSendRunLog
		puts "Entering collate And Send Run Log"

		$results_folder = "#{$rubyHome}test_results/"
		puts "Getting logs from " << $results_folder
		
		Dir.glob($results_folder << '*').less_than_1day_old do |file|
			#FileUtils.rm(file) if File.file?(file)
			puts file
		end
		puts "Exiting collateAndSendRunLog"
	end
	
	
	
	
	
	
	def Tools.checkPageForText( *textToFind )
		puts "Entering checkPageForText"
		
		if textToFind[0] == nil
			textToFind[1] = 'DSDIP'
		end
		text = textToFind[1]
		
		# Get current time for use in a unique filename
		$now = Tools.getTime
		$now.gsub!(":", ".")
		
		textLink = "(//*[contains(text(),'#{text}')])[2]"
		puts textLink
		
		i = 1
		found = false
		tempo = false
		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			puts "No frames so we must be in Tempo."
			tempo = true
		end

		if tempo
			begin
				$driver.find_element(:xpath, textLink)
				puts "Found " + text
				found = true
			rescue
				puts "#{text} not found in Tempo."
			end
		else
			frames = $driver.find_elements(:xpath, "//iframe")
			puts "Found #{frames.size} frames."
			begin
				$driver.find_element(:xpath, textLink)
				puts "Found " + text
				found = true
			rescue
				frames.each do |frame|
					begin
						$driver.switch_to.frame(i)
						$driver.find_element(:xpath, textLink)
						puts "Found " + text
						found = true
						break
					rescue
						puts "#{text} not found in Portal."
					end
					i = i + 1
				end
			end
		end

		if found 
			puts ".............................................................."
			puts "#{text} was found."
			puts ".............................................................."
			Tools.grabScreenshotOfError("no", "#{$today}_#{$now}_#{text}")
		else
			puts "#{text} not found."
		end
		puts "Exiting checkPageForText"
	end
	
	
	
	
	
	def Tools.loadVariable(varname, varvalue)
		puts "Entering loadVariable"
		
		case varname
		when "appID"
			$appID = varvalue
			puts varname + " = " + $appID
		when "$appID"
			$appID = varvalue
			puts varname + " = " + $appID
		else
			puts "Unknown variable name of: " + varname
		end
		
		puts "Exiting loadVariable"
	end


	def Tools.ping(host)
		# Ping a site every x minutes to 
		# keep the VNC session active
		puts "Entering ping"
		
		host = "google.com"
		
		exitMethod = false
		while exitMethod == false
			exitStatus = system("ping -c #{timeout} #{host}")

			if (exitStatus == true)
				puts "Website is up!"
#				return true
			else
				puts "Website is down!"
#				return false
			end
			sleep 60
		end
		
		puts "Exit ping"
	end
	
	
	
	
	
	def Tools.checkForScreen(title, *timeout)
		puts "Checking for screen '#{title}'."
		
		if timeout[0] == nil
			timeout[0] = 5
		end
		
		if title != nil
			result = false
			for i in 1..timeout[0]
				begin
					$driver.switch_to.default_content()
					$driver.switch_to.frame "fContent"
				rescue
					# Not found so must be in Tempo
				end

				begin 
					if $driver.find_element(:xpath, "//*[@id='pageTitle']").text.include? title
						puts "==============================================="
						puts "1) Screen title '#{title}' found."
						puts "==============================================="
						result = true
						break
					end
				rescue
					begin
						if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? title
							puts "==============================================="
							puts "2) Screen title '#{title}' found."
							puts "==============================================="
							result = true
							break
						end
					rescue
						# Title not found
						result = false
					end
				end
				sleep 1
			end
		else
			puts "=============================================================="
			puts "No parameter of screen title provided, ignoring and hoping bad things don't happen"
			puts "=============================================================="
		end	
		return result
	end






	def Tools.checkPassword(username, password, logout)
		# This checks if an entered username/password returns an "invalid username or password" error.
		# If invalid it is written to file for examination and later rectification.
		puts "Entering checkPassword"
		
		if username == nil or password == nil
			puts "No Username and/or Password supplied to 'checkPassword' method, so am unable to continue"
			exit
		end

		puts "Entering login"
		puts "******************************"
		puts "Logging in as:"
		puts username
		puts password
		puts "******************************"
		# Set up some global variables for the test run.
		#$USER = username
		#$PASS = password
		
		# # # # Tools.checkPageForText
		
		begin

			$driver.find_element(:id, 'un').clear
			element = $driver.find_element(:id, 'un')
			element.send_keys username
			element = $driver.find_element(:id, 'pw')
			element.send_keys password

			element.submit
			sleep 3

			# Check for change password prompt
			#puts "Check for change password prompt"
			tmpPass = "The temporary password has expired. Please contact the system administrator to reset your password."
			invalidPass = "The username/password entered is invalid"
			begin
				if $driver.find_element(:xpath, "//*[@class='message']").text.include? invalidPass
					puts "Invalid username or password provided !!!!!"
					userPass = "Invalid: '" + username + "', '" + password +"'"
					Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Invalid username or password provided ")
					#exit
				elsif $driver.find_element(:xpath, "//*[@class='message']").text.include? tmpPass
					puts "Temporary password expired !!!!!"
					userPass = "Expired:" + username + ", " + password
					Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Temporary password expired")
				end
			rescue

				Tools.defaultFrame				
				Tools.wait(:id, "environmentTitlebar", 10)
				puts "Already logged in..."
				# Already logged in, probably using an IE browser...
				if logout != nil
					puts "Password changed, logging out."
					Tools.logout # <- Used for bulk password changing.
				end
				sleep 3
			end
		end

		sleep 1

		puts "Exiting checkPassword"
	end






	def Tools.login365(user, oldpass, newpass)
		user = user.lstrip # Remove leading whitespace
		user = user.rstrip # Remove trailing whitespace

		puts "Entering login365"
		puts "******************************"
		puts "Logging in as:"
		puts user
		puts oldpass
		puts "******************************"

		Tools.wait(:xpath, ".//*[@name='login']", 10)
		$driver.find_element(:xpath, ".//*[@name='login']").clear
		$driver.find_element(:xpath, ".//*[@name='login']").send_keys user
		sleep 1
		$driver.find_element(:xpath, ".//*[@name='passwd']").clear
		$driver.find_element(:xpath, ".//*[@name='passwd']").send_keys oldpass

		sleep 2
		puts "Mouse click submit"
		system("#{$rubyHome}/Utils/mouseClick365.sh", "992", "520")
		system("#{$rubyHome}/Utils/mouseClick365.sh", "992", "520")

		puts "now check for error or password change form"

		if Tools.wait(:xpath, ".//*[@id='ChangePasswordControl_OldPasswordTextBox']", 12)
			puts "Change password now"
			$driver.find_element(:xpath, ".//*[@id='ChangePasswordControl_OldPasswordTextBox']").send_keys oldpass
			sleep 1
			$driver.find_element(:xpath, ".//*[@id='ChangePasswordControl_NewPasswordTextBox']").send_keys newpass
			sleep 1
			$driver.find_element(:xpath, ".//*[@id='ChangePasswordControl_ConfirmNewPasswordTextBox']").send_keys newpass
			sleep 1
			$driver.find_element(:xpath, ".//*[@id='ChangePasswordControl_OkButton']/span").click
			puts "Submit clicked, now switch user"
			Tools.wait(:xpath, ".//*[@id='switch_user_link']", 8)
			$driver.find_element(:xpath, ".//*[@id='switch_user_link']").click
		elsif Tools.wait(:xpath, ".//*[@id='cta_error_message_text']/p", 2)
			Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Error logging in")
			puts "Error logging in"
		else
			puts "Skip as password already changed"
			Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Skip as password already changed")
			$driver.find_element(:xpath, ".//*[@id='O365_TopMenu']/div[2]/div/div/table/tbody/tr/td[1]/div/button").click
			Tools.wait(:xpath, ".//*[@aria-label='Sign out']", 10)
			$driver.find_element(:xpath, ".//*[@aria-label='Sign out']").click
		end
	end





	def Tools.login(username, password, isDB)
		puts "Entering login"

		# Set up some global variables for the test run.
		$USER = username
		$PASS = password

		puts "******************************"
		puts "Logging in as:"
		puts $USER
		puts $PASS
		puts $URL		
		puts $dbURL		
		puts "******************************"

		if isDB == nil
			# Close any existing browsers
			closeBrowser
			# Open the SUT
			openBrowser("firefox", $URL)
		else
			puts "Skipping opening browser as DB browser should be opened."
		end
		
		i = 0
		Tools.defaultFrame				
		while i != 20
			begin 
				$driver.find_element(:id, 'un')
				print ""
				break
			rescue
				print "#{i}."
				sleep 1
				i = i + 1
				if i == 20
					puts "Mydas window not found, quitting"
					exit
				end
			end
		end
		
		begin
			element = $driver.find_element(:id, 'un')
			element.clear
			element.send_keys $USER
			element = $driver.find_element(:id, 'pw')
			element.clear
			element.send_keys $PASS
			sleep 1
			element.submit
			
			if $demo_mode == false
			# Hide Firebug panel by sending F12 to browser
				$driver.find_element(:xpath, "//input[contains(@id,'un')]").send_keys :f12
				#puts "F12 pressed to hide Firebug"
			else
				puts "Demo mode = #{demo_mode}"
			end
			
			
			puts "Waiting for login to complete..."
			sleep 3
		rescue
			puts "No login fields displayed"
		end
		
		# invalid = false
		begin
			# Check for change password prompt
			puts "Check for change password prompt"
			if $driver.find_element(:xpath, "//*[@class='message']").text.include? "The username/password entered is invalid"
				puts "Invalid username or password provided !!!!!"
				userPass = username + ", " + password
				Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Invalid username or password provided")
				#invalid = true
				exit
			end
		rescue
			begin 
				if $driver.find_element(:xpath, "//*[@legend='Old Password']")
					sleep 1
					puts "Password expired"
					Tools.changePassword(password)
					Tools.defaultFrame
					if not Tools.wait(:id, "applicationNameLink")
						if not Tools.wait(:xpath, "//a[contains(.,'Actions')]")
							Tools.logout # <- Used for bulk password changing.
						end
					end
				end
			rescue
				puts "Password not expired" # Try continuing...
			end
		end
	
		puts "Exiting login"
	end # login








	def Tools.changePassword(old_password)
		puts "Entering changePassword"

		# Create new password
		#new_password = Tools.createNewPassword(old_password)
		new_password = "appian@2015"
		puts "Setting new password of: " + new_password
		i = 1
		password_changed = false
		while password_changed == false
			#puts "password_changed = false"
			element = $driver.find_element(:xpath, "//*[@legend='Old Password']")
			element.send_keys old_password
			element = $driver.find_element(:xpath, "//*[@legend='New Password']")
			element.send_keys new_password
			puts "New password: #{new_password}"
			element = $driver.find_element(:xpath, "//*[@legend='Confirm New Password']")
			element.send_keys new_password
			element.submit
			sleep 2
			
			Tools.logout
			if $driver.find_element(:xpath, "//*[@class='errorMessage']")
				new_password = Tools.createNewPassword(old_password)
				i = i + 1
				if i == 6 
					puts "Tried 6 times to increment password but there is still an issue"
					exit
				end 
			else
				password_changed = true
			end
		end
		puts "********************************************************"
		puts "********************* NOTE *****************************"
		puts "The provided password: #{old_password} had expired."
		puts "It was updated to: #{new_password} and login continued."
		puts "Please update the CSV with this new password."
		puts "********************************************************"
		puts "********************************************************"
		puts "Exiting changePassword"
	end # changePassword





	def Tools.logger(logFile, message)
		# Logs a message to a file, 
		# creating the file if not found else append to it
		if message != nil
			message = message  + "\n" # Add a line break
			begin
				if File.exist?(logFile)
					File.open(logFile, 'a') { |file| file.write(message) }
					# puts "#{message} written to file"
				else
					File.open(logFile, 'w') { |file| file.write(message) }
					# puts "#{message} written to file"
				end
				return true
			rescue
				puts "Error encountered writing to log file"
				return false
			end
		else
			puts "No message supplied for logging!"
		end
	end









	def Tools.createNewPassword(aString)
		puts "Entering createNewPassword"

		# Check if old style password
		if aString[0,1] != "#"
			# Remove @ symbol
			aString = aString.gsub("@", "")
			# Prepend # symbol
			aString = "#" + aString
		else
			# Take a string input and increment the numeric part of it
			# Useful for new passwords

			orgString = aString

			# Get the number
			aNumber = aString.delete("^0-9").to_i
			puts aNumber

			# Increment the number
			incNumber = aNumber + 1
			puts incNumber

			begin
				# Replace original number in string with incremented number
				aString = aString.gsub(aNumber.to_s, incNumber.to_s)
				# puts "String: #{orgString} updated to: #{aString}"
			rescue
				puts "There was a problem changing #{orgString} to #{aString}"
			end	
		end
		puts "***********************************************"
		puts "Created new password: #{aString}"
		puts "***********************************************"
		puts "Exiting createNewPassword"		
		return aString
	end # createNewPassword


	
	
	
	
	

	def Tools.closeBrowser
		puts "Entering closeBrowser"
		begin
			# Get PID from file
			if readPID
 				#puts "PID file found and read successfully."
				begin
					# Kill Firefox using PID
					system("kill -9 #{$pid}")
					#puts "Browser closed successfully"
					begin
						#Delete PID file
						deleteFile($pidFile)	
						#puts "PID file deleted successfully."
					rescue
						#puts "Unable to delete PID file."
					end
				rescue
					#puts "Unable to kill browser."
				end
			end
		rescue
			puts "Issue trying to read the PID file."
			exit
		end
		puts "Exiting closeBrowser"
	end


	
	
	
	
	def Tools.logout
		#puts "Entering Logout"
		sleep 2
		begin
		
			Tools.defaultFrame
			Tools.wait(:xpath, "//a[@class='sprite logout']", 5)
			begin
				# Portal logout
				$driver.find_element(:xpath, "//a[@class='sprite logout']").click
				# puts "Logout clicked."
			rescue
				begin
					# Tempo logout
					$driver.find_element(:xpath, "//*[@class='gwt-Anchor pull-down-toggle']").click
					sleep 1
					$driver.find_element(:xpath, "//*[text()='Sign Out']").click
					sleep 1
					#begin
					#	$driver.find_element(:xpath, "(//*[@class='gwt-Anchor pull-down-menu-item'])[5]").click  # New menu options after 14/10/15
					#rescue
					#	$driver.find_element(:xpath, "(//*[@class='gwt-Anchor pull-down-menu-item'])[3]").click # Old menu options before 15/10/15					
					#end
					puts "Logout clicked."
				rescue
					puts "Unable to logout or no logout found."
				end
			end
			Tools.wait(:id, "un", 15)
			
			# Go to URL
			puts "Navigate to " + $URL
			$driver.navigate.to $URL

			puts "Logged out now."
			return true
			
		rescue
			puts "Logout failed, maybe no browser is open?"
			return false
			
		end
	end # Tools.logout








	def Tools.getFutureDate(aDate, daysToAdd)
		
		if aDate == nil
			aDate = Time.now
		end
		puts aDate
		
		# gem install business_time # Required library
		# See https://github.com/bokmann/business_time
		#	return daysToAdd.business_days.after(aDate)
		
		if daysToAdd == nil
			daysToAdd == 7
		end

		#aDate = DateTime.parse(aDate).to_s
		#puts datetime
		#aDate = DateTime.strftime(aDate, '%A/%b/%d')
		
		begin
			#Date aDate = formatter.parse(aDate); 
			daysToAdd = daysToAdd * 86400 # 86400 is 1 day in seconds
			puts "Add " + daysToAdd +  " to the date: " + aDate
			aDate = aDate + daysToAdd; 
			puts "Future date is: " + aDate
		rescue
			puts "Issue creating future date."
		end
		
		return aDate
	end # Portal.getFutureDate




	
	def Tools.getOS # Operating system
		# puts "OS: #{RUBY_PLATFORM}"		
		# Get the OS Ruby is going to run on
		case RUBY_PLATFORM
		when "i386-mingw32", "x64-mingw32"
			# Windows
			return "windows"
		when "i686-linux"
			# Linux
			return "linux"
		else
			# "Unknown OS"
			return "Unknown OS"
		end
	end
	
	
	def Tools.wait(id, actual, *timeout_in_seconds)
		sleep 2
		if timeout_in_seconds[0] == nil
			timeout = 4 
			#puts "No timeout parameter supplied for Tools.wait, so using a default of 4 seconds."
		else
			timeout = timeout_in_seconds[0] / 2
		end

		# This method waits for an object to be displayed
		begin
			wait = Selenium::WebDriver::Wait.new(:timeout => timeout)
			wait.until {$driver.find_element(id, actual )}
		rescue 
			begin
				wait = Selenium::WebDriver::Wait.new(:timeout => timeout)
				wait.until {$driver.find_element(id, actual )}
			rescue
				puts "Warning: #{id} of #{actual} not found"
				return false
			end
		end		
		# puts "#{id} of #{actual} found successfully."
		return true
	end
	
	


	
	def Tools.getTime
		# Returns the current time in 16_9_2015 format
		time = Time.new
		now = time.hour.to_s + ":" + time.min.to_s
		#puts "Current time is: #{now}"	
		return now
	end
	
	
	
	

	def Tools.sendEmail( emailAddress, subject, body, attachment )
		puts "Entering Send Email"
		puts "Used by the nightly script run to send results"
		# From https://github.com/mikel/mail

		error == false
		if emailAddress == nil
			puts "No email address provided"
			error = true
		end

		if subject == nil
			puts "No email subject text provided"
			error = true
		end

		if body == nil
			puts "No email body text provided"
			error = true
		end

		if attachment == nil
			puts "No email attachment provided"
			error = true
		end
		
		if error == true
			puts "Missing information, can't send email"
		else
			# Send email
			begin
				Mail.deliver do
					from     "#{email}"
					to          "#{email}"
					subject "#{subject}"
					body     "#{body}"
					add_file "#{attachment}"
				end
				puts "The email was emailed successfully to #{email}"
			rescue
				puts "There was a problem sending the email."
			end
		end
		puts "Exiting sendEmail"
	end



	def Tools.currentDate
		# Returns today's date in 2016/02/17 format
		time = Time.new
		today = time.strftime("%Y/%m/%d")
		# puts "Today is #{today}"
		return today
	end






	def Tools.defaultFrame
		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			# puts "No default frame found"
		end	
	end






	def Tools.addDate(id, *date)
		# Click the Add date button/image

		# Open Calendar
		Tools.wait(:xpath, id, 4)
		$driver.find_element(:xpath, id).click
		sleep 2

		if date[0] != nil
			# Convert date string into date format
			dateTime = DateTime.parse(date[0]).to_date
			# Break date into components parts
			day = dateTime.day
			day = day
			month = dateTime.month
			year = dateTime.year # Unused ATM

			# Convert month number into month string (1 -> January)
			month = Date::MONTHNAMES[dateTime.month]

			# Month			
			begin		
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//html/body/div[8]/div[2]/select[1]")).select_by(:text, month)
			rescue
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//html/body/div[9]/div[2]/select[1]")).select_by(:text, month)
			end
			sleep 2
			# Year
			#Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//html/body/div[8]/div[2]/select[2]")).select_by(:text, year)
			begin
				Tools.click(:xpath, ".//*[@id='asiFormDatePicker_today']")
				$driver.find_element(:xpath, "//html/body/div[8]/div[3]/div/a[#{day}]").click
			rescue
			
				$driver.find_element(:xpath, "//html/body/div[9]/div[3]/div/a[#{day}]").click
			end
		else
			Tools.click(:xpath, ".//*[@id='asiFormDatePicker_today']")
		end
		sleep 1
	end
	



	def Tools.checkImageloaded(xpath)
		for i in 0..60
			begin		
				$driver.find_element(:xpath, xpath)
				puts "File loaded successfully."
				break
			rescue
				put "Still waiting for file to load..."
				sleep 1
				if i == 60
					puts "File not finished loading? Tried for 60 seconds, giving up wating."
					break
				end
			end
		end
	end # checkImageloaded







	def Tools.refreshClick
		puts "Clicking Refresh Dashboard"
		sleep 6
		Tools.defaultFrame
		
		# Get an array of frames
		frames = $driver.find_elements(:xpath, "//iframe")

		i = 0
		try = 0
		found = false
		while not found
			frames.each do |frame|
				# Check if frame #{i} contains 'Refresh Dashboard'.
				begin
					Tools.defaultFrame
					$driver.switch_to.frame(i)
					$driver.find_element(:link, "Refresh Dashboard").click
					found = true
					break
				rescue
					$driver.switch_to.default_content()
					begin
						$driver.switch_to.frame(i)
						$driver.find_element(:link, "Refresh Dashboard").click
						found = true
						break
					rescue
						# Not found in frame
					end
				end
				i = i + 1
			end

			i = 0
			try = try + 1
			if try == 5
				puts "'Refresh Dashboard' not found."
				puts "Exiting refreshClick"
				break
			end
		end # while

		# To get mouse location use:
		# $ xdotool getmouselocation

		sleep 5
		puts "Finished clicking Refresh Dashboard"
	end # refreshClick

	
	
	
		
	def Tools.serverToTest(browser="firefox", url )
		if url != nil
			$URL = url
			puts $URL
			$dbURL = $URL.sub("suite/apps", "database")
			$dbURL = $dbURL.sub("suite/tempo", "database")
			puts "Server to test is " << $URL
			puts "Database Server is " << $dbURL
		else
			puts "Fatal error: No server supplied to test" 
			exit
		end
	end
	

	
	def Tools.setApplicationID(appID)
		puts "Entering Set Application ID"
			if appID != nil
				$appID = appID
				puts "Application ID set to: " << $appID
			else
				puts "No application ID provided, quitting"
				exit
			end
		puts "Exiting setApplicationID"
	end
	
	
	
	
	
	def Tools.doreconciliation( appID, paymentType="Visa",adminUser, adminPass )
		puts "Entering DoReconciliation"
		if $PaymentRequired
			
			if adminUser == nil
				adminUser = "martin.carroll"
			end 
			if adminPass == nil
				adminPass = "@appian2016"
			end
			
			if appID != nil
				$appID = appID
			end
			#$adminUser = adminUser
			#$adminPass = adminPass
			# puts $adminUser
			# puts	$adminPass
			
			# Close any existing browsers
			# closeBrowser
			# Open the SUT
			puts "DBURL is: " << $dbURL
			openBrowser($browser="firefox", $dbURL)
			puts "Database should be displayed now."
			# Login 
			isDB = "db"
			login(adminUser, adminPass, isDB)

			$shortAppID = Tools.getShortAppID # Get the shortened App ID from the application
			puts "*******************************************"
			puts "$shortAppID is " << $shortAppID
			puts "*******************************************"
			
			if Tools.isPortal
				$myDasType = "Portal"
				puts "Executing Portal reconciliation"
				Portal.reconciliation( appID, paymentType )
			else
				$myDasType = "Tempo"
				puts "Executing Tempo reconciliation"
				Tempo.reconciliation( appID, paymentType )
			end
		else
			puts "Skipping reconciliation as no payment is required."
		end
		puts "Exiting doreconciliation"
	end

	
	
	
	
	
	def Tools.getSSQRequestID # aka getOrderNum / Get OrderNumb
		puts "Entering Get SSQ Request ID, aka OrderNum"
		
		i = 0
		found = false
		while not found
			print "Looking for Query link"
			begin
				#$driver.find_element(:xpath, "//*[@title='SQL']").click
				#puts ""
				#puts "SQL clicked"	
				$driver.find_element(:xpath, "//*[@title='Query']").click
				puts ""
				puts "Query link clicked"	

				found = true
			rescue
				print "."
				exit
			end
			sleep 1
			i = i + 1
			if i == 10
				#puts "Couldn't find the SQL link"
				puts "Couldn't find the Query link"
				exit
			end
		end
		
		# Wait for the field to display
		found = false
		i = 0
		while not found
			begin
				#$driver.find_element(:xpath, "//div[@id='sqlquerycontainerfull']/div/div/textarea")
				$driver.find_element(:xpath, "//*[@id='textSqlquery']")
				puts "Query box found"	
				found = true
			rescue
				sleep 1
				i = i + 1
				if i == 10
					puts "SQL not found, quitting."
					exit
				end
			end
		end
		
		puts "Create an SQL for " << $myDasType
		shortAppID = $shortAppID
		
		if Tools.isPortal
			# If Portal and a Portal SDA or SPL application then minus 3000 from the appID
			puts "Shortened App ID is " << shortAppID
			shortAppID = shortAppID.to_i
			if $appID["SDA"] or $appID["SPL"]
				shortAppID = shortAppID - 3000
				shortAppID = shortAppID.to_s
				puts "Deducted 3000 from the SDA/SPL appID"
				puts "Shortened App ID minus 3000 is " << shortAppID
			end
			# Create SQL
			sql = "UPDATE `sara_FinanceTransactionSSQRequest` SET `PaymentStatusId`='8',`PaymentStatusAbbrev`='Payment Success', `SSQRequestStatusTypeId`='8',`SSQRequestStatusTypeAbbreviation`='Payment Success' where `applicationDBId`= '#{shortAppID}'"
		else
			sql = "SELECT ssqrequestid FROM `common_ssqpaymenttransactions` WHERE applicationID = '#{shortAppID}'"
		end
		puts sql
		
		begin
			#$driver.find_element(:xpath, "//div[@id='sqlquerycontainerfull']/div/div/textarea").send_keys sql
			#$driver.find_element(:xpath, "//*[@id='sqlquery']").send_keys sql
			$driver.find_element(:xpath, "//*[@id='textSqlquery']").send_keys sql
			puts "SQL written"
		rescue
			puts "Error writing SQL."
			exit
		end
		
		# Click Submit Query button
		#$driver.find_element(:xpath, "//*[@value='Submit Query']").click
		#$driver.find_element(:xpath, "//*[@value='Go']").click
		#puts "Go button clicked"
		$driver.find_element(:xpath, "//*[@value='Submit Query']").click
		puts "Submit Query button clicked"
		sleep 3

		i = 0
		found = false
		while not found
			begin
				result = $driver.find_element(:xpath, "//*[contains(@class,'result_query')]").text
				if $driver.find_element(:xpath, "//*[contains(@class,'result_query')]").text.include? "MySQL returned an empty result set (i.e. zero rows)."
					puts result
				end
			rescue
				sleep 1
				i = i + 1
				if i == 10
					puts "No SQL results returned"
					break
				end
			end
		end

		puts "Query finished."
		if $myDasType == "Tempo"
			begin
				# Get the order number
				$orderNum = $driver.find_element(:xpath, "html/body/div[5]/div[1]/form[2]/div[1]/table[2]/tbody/tr/td[5]/span").text
				puts "'ssqrequestid'/Order Number: #{$orderNum}"
			rescue
				puts "No data found for that Application ID: #{shortAppID}"
				exit
			end
		end
		puts "Exiting getSSQRequestID"
	end	
	




	def Tools.getScheduleTriggerFeeID
		puts "Entering Get Schedule Trigger Fee ID"

		if $shortAppID == nil
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			puts "No application supplied, quitting"
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			exit
		end

		$driver.find_element(:xpath, "//*[@title='Query']").click
		puts "Query link clicked"
		sleep 3
		
		sql = "SELECT scheduletriggerfeeid, amount FROM `common_applicationscheduletriggerpayitems` where applicationid = #{$shortAppID}  and amount <> 0.00"
		puts sql
		$driver.find_element(:xpath, "//*[@id='textSqlquery']").send_keys sql
		
		# Click Submit Query button
		$driver.find_element(:xpath, "//*[@value='Submit Query']").click
		puts "Submit Query button clicked"
		sleep 3

		moreRowsFound = true
		$feeID = Array.new
		
		if Tools.check_text_exists('scheduletriggerfeeid')
			puts "Query finished."
			i = 1
			while moreRowsFound
				puts "Get Fee ID and Amount" 
				col1 = "//html/body/div[5]/div[1]/form[2]/div[1]/table[2]/tbody/tr[#{i}]/td[5]/span"
				col2 = "//html/body/div[5]/div[1]/form[2]/div[1]/table[2]/tbody/tr[#{i}]/td[6]/span"
				puts col1
				puts col2
				begin
					$feeID[i] = $driver.find_element(:xpath, col1).text
					puts "Fee ID: #{$feeID[i]}"
					$amounts[i] = $driver.find_element(:xpath, col2).text
					$amounts[i] = $amounts[i].sub(".","")
					puts "Amount: #{$amounts[i]}"	
					puts "A single row was found."
				rescue
					begin
						col1 = "//html/body/div[5]/div[1]/form[3]/div[1]/table[2]/tbody/tr[#{i}]/td[5]/span"
						col2 = "//html/body/div[5]/div[1]/form[3]/div[1]/table[2]/tbody/tr[#{i}]/td[6]/span"
						puts col1
						puts col2
						$feeID[i] = $driver.find_element(:xpath, col1).text
						puts "Fee ID: #{$feeID[i]}"
						$amounts[i] = $driver.find_element(:xpath, col2).text
						$amounts[i] = $amounts[i].sub(".","")
						puts "Amount: #{$amounts[i]}"	
						puts "Multiple rows found."
					rescue
						moreRowsFound = false
						puts "No more rows found."
					end
				end
				i = i + 1
			end
		else
			puts "scheduletriggerfeeid not found, quitting"
			exit
		end
		
		puts "Exiting getScheduleTriggerFeeID"
	end	
	
	
	
	
	
	
	def Tools.makePaymentFile
		puts "Entering Make Payment File"
	
		# Generate filename
		$paymentFile = $rubyHome + "Documents/" + $today + "_" + $now + ".txt"
		puts "Payment file is: " +$paymentFile
		
		if $myDasType == "Tempo" 
			# Make Tempo payment file
			puts "Make Tempo payment file"
			
			# Create header row for file
			$headerRow = "TransactionID\tOrderNum\tItem\tPayType\tAgency\tPaymentAmt\tService\tItemAmount\tGL Acct\tCost Centre\tNarrative\tTax Code\tReference"
			puts "Header row is: " + $headerRow 
			
			# Write Header row to file
			Tools.logger($paymentFile, $headerRow)
			
			
			$paymentType = "Visa"
			$fileData = ""
			$amountsTotal = "0"
			$shortAppID = Tools.getShortAppID # Get the shortened App ID from the application
			
			i = 1
			stillRows = true
			puts "Amount: '#{$amounts[i]}'"
			while stillRows
				begin
					$fileData = "1008400127-171063268\t" + $orderNum + "\t2556\t" 
					puts "1:" + $fileData
					$fileData = $fileData + $paymentType	+ "\tSDIP\t"
					puts "2:" + $fileData
					$fileData = $fileData + $amounts[i] + "\tDSDIPSARA\t" + $amounts[i] + "\t\t\tAppId:"
					puts "3:" + $fileData
					$fileData = $fileData  + $shortAppID + " FeeId:"
					puts "4:" + $fileData
					$fileData = $fileData  + $feeID[i] + "\t\tReceipt No: 09876575; Reference: " + $appID
					puts "5:" + $fileData
					
					$amountsTotal = $amountsTotal.to_i + $amounts[i].to_i
					
					# Write data rows to file
					Tools.logger($paymentFile, $fileData)
					i = i + 1
				rescue
					stillRows = false
					puts "No more rows found"
				end
			end
			puts "Total amount to be reconciled: #{$amountsTotal}"
		
		else 
			# Make Portal payment file
			puts "Make Portal payment file"
			
			# Create header row for file
			$headerRow = "TransactionID\tOrderNum\tItem\tPayType\tAgency\tPaymentAmt\tService\tItemAmount\tGL Acct\tCost Centre\tNarrative\tTax Code\tReference"
			puts "Header row is: " + $headerRow 
			
			# Write Header row to file
			Tools.logger($paymentFile, $headerRow)
						
			$paymentType = "Visa"
			$fileData = ""
			$amountsTotal = "0"
			i = 1
			stillRows = true
			puts "Amount: '#{$amounts[i]}'"
			while stillRows
				begin
					$fileData = "1008400127-171063268\t" + $orderNum + "\t2556\t" 
					puts "55555555555555555555555555555555555555555555555"
					puts "Row: #{i}"
					puts "1:" + $fileData
					$fileData = $fileData + $paymentType	+ "\tSDIP\t"
					puts "2:" + $fileData
					$fileData = $fileData + $amounts[i] + "\tDSDIPSARA\t" + $amounts[i] + "\t\t\tAppId:"
					puts "3:" + $fileData
					$fileData = $fileData  + $shortAppID + " FeeId:"
					puts "4:" + $fileData
					$fileData = $fileData  + $feeID[i] + "\t\tReceipt No: 09876575; Reference: " + $appID
					puts "5:" + $fileData
					puts "55555555555555555555555555555555555555555555555"
					$amountsTotal = $amountsTotal.to_i + $amounts[i].to_i
					puts $fileData
					# Write data rows to file
					Tools.logger($paymentFile, $fileData)
					i = i + 1
				rescue
					stillRows = false
					puts "No more rows found"
				end
			end
			puts "Total amount to be reconciled: #{$amountsTotal}"
		end
#puts "exit	line 1800 in Tools"
#exit 		
		puts "Exiting makePaymentFile"
	end	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	def Tools.validatePayment( )
		# Debug values
		# $appID = "1509-5100 SDA"
		# puts Tempo.compareValues( "$19,338.00","$19,338.00" )
		# exit	
		puts "Entering validatePayment"
				
		# Open Tempo by modifying the 
		# current browser's URL and opening it
		if $URL["test"] # If the URL contains the word 'test'...
			puts "Opening Test environment."
			$driver.navigate.to "https://mydastest.appiancloud.com/suite/tempo"
		elsif $URL["dev"]
			puts "Opening Dev environment."
			$driver.navigate.to "https://mydasdev.appiancloud.com/suite/tempo"
		elsif $URL["stage"]
			puts "Opening Staging environment."
			$driver.navigate.to "https://dsdipstage.appiancloud.com/suite/tempo"
		elsif $URL["train"]
			puts "Opening Training environment."
			$driver.navigate.to "https://dsdiptrain.appiancloud.com/suite/tempo"
		else
			unknown = $URL
			puts "Unknown test environment of: " + unknown
			unknown = "#{unknown[34]}/suite/tempo"
			puts unknown
			$driver.navigate.to unknown
		end
		Tempo.wait 3
		
		Tools.click(:xpath, "//a[contains(.,'Records')]")
		$driver.switch_to.default_content()

		Tools.click(:link, "Development applications and requests")
		Tempo.wait 3
		puts "Search for '#{$appID}'"

		# Search for the Application
		xpath = "//*[@placeholder='Search Development applications and requests']"
		Tools.wait(:xpath, xpath, 5)
		
		# Search won't work with app iDs that end in SRA, SDA, etc.,
		# so we'll remove it
		appID = $appID[0..-4] # Remove SRA / SDA / SQA, etc.
		appID = appID.sub(" ","") # Remove spaces
		# Now Search for the application
		$driver.find_element(:xpath, xpath).send_keys appID
		$driver.find_element(:xpath, xpath).send_keys :enter
		Tempo.wait 2
		
		# Check for search results and open application
		if $driver.find_element(:xpath, "//*[@class='gwt-InlineHTML']").text.include? "No records available"
			puts "???????????????????????????????????????????"
			puts "'#{$appID}' not found. Panic time! Quitting."
			puts "???????????????????????????????????????????"
			exit
		else
			$driver.find_element(:xpath, "//a[contains(.,'#{$appID}')]").click
			puts "Found '#{$appID}' and clicked."					
			Tempo.wait 2
		end
		
		if $driver.find_element(:xpath, "(//*[starts-with(@class,'aui-Section-Label')])[1]").text.include? "Application summary"
			Tools.clickLinkIfExists( "Financial Details")
			Tempo.wait
			i = 0
			amountReconciledNew = ""
			refundInitiated = false
			
			# Get the reconcile value
			amountReconciledOriginal = $driver.find_element(:xpath, "(//*[contains(@class,'appian_form_readonly')])[3]").text
			
			# Get the amount owing
			amountOwingOriginal = $driver.find_element(:xpath, "(//*[contains(@class,'appian_form_readonly')])[4]").text
						
			# Now keep checking the screen for an updated reconcile value
			while not amountReconciledOriginal == amountReconciledNew
				amountReconciledNew = $driver.find_element(:xpath, "(//*[contains(@class,'appian_form_readonly')])[3]").text
				
				puts "#{appID}: Original reconcile value = #{amountReconciledOriginal}"	
				# Get the amount owing
				latestAmountOwingOriginal = $driver.find_element(:xpath, "(//*[contains(@class,'appian_form_readonly')])[4]").text

				if amountOwingOriginal != latestAmountOwingOriginal
					amountOwingOriginal = latestAmountOwingOriginal
			
					# Do we need to initiate a refund?
					if not refundInitiated
						begin
							$driver.find_element(:xpath, "//*[@aria-label='Initiate the refund process for this record']").click
							wait
							$driver.find_element(:xpath, "//*[@value='Ok']").click
							wait
							refundInitiated = true
						rescue
						end
					end
				end
				puts "#{appID}: Amount owed = #{amountOwingOriginal}"
				puts "#{appID}: Latest reconcile value = #{amountReconciledNew}"
				
				# Format the value from $1,000,23 to 100023
				amountReconciledNew = amountReconciledNew.sub("$", "")
				amountReconciledNew = amountReconciledNew.sub(",", "")
				amountReconciledNew = amountReconciledNew.sub(".", "")
				
				if amountReconciledNew.to_i == $amountsTotal.to_i
					success = true
					puts "**********************************************************"
					puts "Success!! #{amountReconciledNew} = #{$amountsTotal}"
					puts "**********************************************************"
					break
				else
					success = false
					puts "#{amountReconciledNew} <> #{$amountsTotal}"
				end
				i = i + 1
			
				if i == 210 # Check for 2.5 minutes
					puts "No change in reconcile value."
					break
				end
				if not success
					Tools.clickLinkIfExists( "Financial Details")
					# puts "Application is: " << $appID
					Tempo.wait
				end
			end
			
			
			recLog = "#{$rubyHome}CSVs/#{$today}_#{$now}_reconciliation.log"
			
			# Success or failure?
			if success == true
				Tools.logger(recLog, "#{$appID} Pass")
			elsif success == false
				Tools.logger(recLog, "#{$appID} Fail")
			end
			puts "==============================================================="
			Tools.logger(recLog, "===============================================================")
		else
			puts "???????????????????????????????????????????"
			puts "'#{$appID}' not found. Quitting."
			puts "???????????????????????????????????????????"
		end
		
		# Re-assign original browser
		$driver = $mainBrowser
		
		puts "Exiting validatePayment"
	end

	
	
	
	
	
	
	def Tools.emailPaymentFile( demo )
		puts "Entering email Payment File"
		# From https://github.com/mikel/mail

		# Get the URL that the browser is open on, so we 
		# know where to send the payment file email
		# demo = true # debug.
				
		demo = false
		if demo == true
			env = ""
			puts "Sending email to demo address."
		elsif env == nil
			env = $URL
			puts "Current URL is " + env
		end
		puts "env = '#{env}'" 
		
		if $myDasType == nil
			$myDasType = "Tempo"
		end
		if env["test"]
			if $myDasType == "Tempo"
				# For Tempo Test environment send to:
				sendTo = "processmodeluuid0010dae4-41ec-8000-f92f-7f0000014e7a@mydastest.appiancloud.com"
			else	
				# For Portal Test environment send to:
				sendTo = "0003d77b-b16c-8000-f92f-7f0000014e7a@mydastest.appiancloud.com"				
			end
			puts "Detected Test environment, sending corresponding email."
		elsif env["dev"]
			if $myDasType == "Tempo"
				# For Tempo Dev environment send to:
				sendTo = "processmodeluuid0010dae4-41ec-8000-f92f-7f0000014e7a@mydasdev.appiancloud.com"
			else
				sendTo = "0003d77b-b16c-8000-f92f-7f0000014e7a@mydasdev.appiancloud.com"
			end
			puts "Detected Dev environment, sending corresponding email."
		elsif env["train"]
			if $myDasType == "Tempo"
				# For Tempo Training environment send to:
				sendTo = "processmodeluuid0010dae4-41ec-8000-f92f-7f0000014e7a@dsdiptrain.appiancloud.com"
			else
				sendTo = "0003d77b-b16c-8000-f92f-7f0000014e7a@dsdiptrain.appiancloud.com"
			end
			puts "Detected Train environment, sending corresponding email."
		elsif env["stage"]
			if $myDasType == "Tempo"
				# For Tempo Staging environment send to:
				sendTo = "processmodeluuid0010dae4-41ec-8000-f92f-7f0000014e7a@dsdipstage.appiancloud.com"
			else
				sendTo = "0003d77b-b16c-8000-f92f-7f0000014e7a@dsdipstage.appiancloud.com"
			end
			puts "Detected Staging environment, sending corresponding email."
		else
			# For Tempo debugging send to me:
			sendTo = "Martin.Carroll@dilgp.qld.gov.au"
			puts "******************************"
			puts "Sent email in debugging mode"
			puts "******************************"
		end
		
		# Send email
		#begin
			Mail.deliver do
				from     'Martin.Carroll@dilgp.qld.gov.au'
				to         sendTo
				subject 'MyDas reconciliation'
				body     $fileData
				add_file $paymentFile
			end
		#	puts "The payment file was emailed successfully to " + sendTo
		#rescue
		#	puts "There was a problem emailing the payment file."
		#end
	
#		puts "Now to wait 5 minutes for the payment to be processed by SSQ."
#		sleep 300
#		puts "Waiting finished so now to continue."
		
		puts "Exiting emailPaymentFile"
	end
	
	
	
	
	
	def Tools.clickLinkIfExists( link )
		# This finds a link in a frame and clicks it.
		# If a new window is expected, it checks for it.
		puts "Entering clickLinkIfExists"
		
		found = false
		
		puts "Looking for the link '#{link}'."
		sleep 2
		
		# Set the drop downs to 'Active Only' when we are in Portal
		i = 0
		if Tools.isPortal
			Tools.defaultFrame		
			begin
				# Make sure 'Active Only' is selected from the Display Tasks drop down.
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[contains(@name,'qf')])[2]")).select_by(:index, 0)
				#puts "'Active Only' set in first Display Tasks dropdown"
			rescue
				puts "Issue setting 'Active Only' in the 1st drop down."
			end
			
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[contains(@name,'qf')])[4]")).select_by(:index, 0)
				#puts "'Active Only' set in second Display Tasks dropdown"
			rescue
				puts "Issue setting 'Active Only' in the 2nd drop down."
			end
		end
		
		textLink = "(//*[starts-with(text(),'#{link}')])[1]"		
		begin
			Tools.defaultFrame
			$driver.find_element(:xpath, textLink).click
			puts "#{textLink} clicked in default frame"
			found = true
		rescue
			begin
				$driver.switch_to.frame "fContent"
				$driver.find_element(:xpath, textLink).click
				puts "#{textLink} clicked in fContent frame"
				found = true
			rescue
				puts "#{link} not found."
			end
		end
				
		sleep 3
		
		while not found
			begin
				
				#Tools.refreshClick
				windows = $driver.window_handles # Get all window handles
				windows.each do |window|
					if $main_window != window
						@new_window = window
					end
				end
				$driver.switch_to.window(@new_window) {
					Tools.wait(:xpath, textLink, 5)
					found = true
					puts "Try a 'starts-with' xpath of " << textLink
					$driver.find_element(:xpath, textLink).click
					puts "Match found and clicked for '#{link}' in new window"
					sleep 3
				}
			rescue # No new window found
				begin
					Tools.defaultFrame			
					Tools.wait(:xpath, textLink, 5)
					$driver.find_element(:xpath, textLink).click
					puts "Match found and clicked for '#{link}'"
					found = true
				rescue
					begin
						$driver.switch_to.frame "fContent"
						puts "Switched to fContent frame."
						puts "Try a 'starts-with' link"
						$driver.find_element(:xpath, linkText).click 
						puts "Starts-with match found and clicked for '#{link}'"
						found = true
					rescue
						begin
							$driver.switch_to.frame "fContent"
						rescue
							puts "fContent not found"
						end
						
						begin
							puts "Try a 'contains' link"
							$driver.find_element(:xpath, "//*[contains(text(),'#{link}')]").click # Try contains match
							puts "Contains match found clicked for '#{link}'"
							found = true
						rescue 
							puts "Link not found"
							return false
						end
					end
				end
				if not found 
					Tools.refreshClick
					sleep 1
					i = i + 1
					if i == 20
						puts "'#{link}' link not found"
						exit
					end
				end
			end
		end
		sleep 3
		puts "Exiting clickLinkIfExists"
		return true # Found the link

	end # clickLinkIfExists






	def Tools.elapsedTime(startTime)
		total_seconds = Time.now - startTime

		seconds = total_seconds % 60
		minutes = (total_seconds / 60) % 60
		hours = total_seconds / (60 * 60)

		return format("%02d:%02d:%02d", hours, minutes, seconds) #=> "01:00:00"
	end



	
	def Tools.loadFile(button_id, *index)
		# Requires the fully file to be on the clipboard
		# puts "Click the upload button of #{button_id}"

		if index[0] != nil
			case index[0]
			when [1] 
				Tools.click(:xpath, "(#{button_id})[1]", 4)
			when [2]
				Tools.click(:xpath, "(#{button_id})[2]",4)
			when [3]
				Tools.click(:xpath, "(#{button_id})[3]",4)
			else
				Tools.click(:xpath, button_id,4)
			end
		else
			Tools.click(:xpath, button_id,4)
		end

		sleep 2
		if File::exists?("/home/Shared/apache-jmeter-2.13/bin/Doc.docx")
			system("#{$rubyHome}/Utils/fileUpload.sh")		
			puts "Filename entered (for JMeter)."
		elsif File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
			puts "Input filename to upload #{Clipboard.paste}"
			system("#{$rubyHome}/Utils/fileUpload.sh")
			puts "Filename entered."
		else
			puts "#{$rubyHome}/Utils/fileUpload.sh or (if JMeter run) /home/Shared/apache-jmeter-2.13/bin/Doc.docx not found"
		end
		
		# Wait for the Browse button to vanish signifying file load completed
		for i in 1..5
			begin
				if index[0] != nil
					# Check for the button
					case index[0]
					when [1] 
						if $driver.find_element(:xpath, "(#{button_id})[1]").size == 0
							puts "File loaded."
							break
						end
					when [2]
						if $driver.find_element(:xpath, "(#{button_id})[2]").size == 0
							puts "File loaded."
							break
						end
					when [3]
						if $driver.find_element(:xpath, "(#{button_id})[3]").size == 0
							puts "3 File loaded."
							break
						end
					else
						if $driver.find_element(:xpath, button_id).size == 0
							puts "File loaded."
							break
						end
					end
				else
					if $driver.find_element(:xpath, button_id).size == 0
						puts "File loaded."
						break
					end
				end
				sleep 1
			rescue
				# Button not found so file is loaded.
				puts "File loaded."
				break
			end
		end
	end # Loadfile
	






	def Tools.message(message)
		# Display a message
		if File::exists?("#{$rubyHome}/Utils/message.sh")
			if message == nil
				puts "No parameter supplied to showMessage"
			else
				system("#{$rubyHome}/Utils/message.sh", message)
			end
		else
			puts "#{$rubyHome}/Utils/message.sh not found"
		end	
			
	end # showMessage





	
	
	def Tools.switch_to_frame(frame)
		for i in 1..30
			begin
				$driver.switch_to.default_content()
				$driver.switch_to.frame("fContent")


				$driver.switch_to.frame($driver.find_Element(:xpath, "//iframe[contains(@src,#{frame})]"))
			rescue
				# Frame not ready
				puts "Frame: #{frame} not ready"
			end
			sleep 1
		end
	end
		
		
	
	
	def Tools.sendKeys(text)
=begin
	  :null   
	  :cancel 
	  :help   
	  :backspace
	  :tab    
	  :clear  
	  :return 
	  :enter  
	  :shift  
	  :left_shift
	  :control
	  :left_control
	  :alt    
	  :left_al
	  :pause  
	  :escape 
	  :space  
	  :page_up
	  :page_down
	  :end    
	  :home   
	  :left   
	  :arrow_left
	  :up     
	  :arrow_up
	  :right  
	  :arrow_right
	  :down   
	  :arrow_down
	  :insert 
	  :delete 
	  :semicolon
	  :equals 
	  :numpad0
	  :numpad1
	  :numpad2
	  :numpad3
	  :numpad4
	  :numpad5
	  :numpad6
	  :numpad7
	  :numpad8
	  :numpad9
	  :multiply
	  :add    
	  :separator
	  :subtract
	  :decimal
	  :divide 
	  :f1     
	  :f2     
	  :f3     
	  :f4     
	  :f5     
	  :f6     
	  :f7     
	  :f8     
	  :f9     
	  :f10    
	  :f11    
	  :f12    
	  :meta   
	  :command
=end
#		puts "Copy text"
		Clipboard.copy text
		puts "Paste #{text}."
		Clipboard.paste
	end
	
	
	
	
	def Tools.check_text_exists(text)
		# Check that some text exists on a page
		if $driver.page_source.include? text
			return true
		else
			puts "#{text} not found."
			return false
		end
	end # Tools.check_text_exists
	
	
	
	
	
	def Tools.check_element_exists(id, element)
		# Check that an element exists in a page
		$driver.manage.timeouts.implicit_wait = 0
		if $driver.find_elements(id, element).size != 0
			return true
		else
			return false
		end
		$driver.manage.timeouts.implicit_wait = 30
	end # Tools.check_element_exists







	def Tools.wait_element_exists(id, element)
		# Check that an element exists in a page
		$driver.manage.timeouts.implicit_wait = 0
		i = 0
		while i !=  30
			if $driver.find_elements(id, element).size != 0
				return true
			end
			i = i + 1 
		end
		return false
		$driver.manage.timeouts.implicit_wait = 30
	end # Tools.check_element_exists
	



	
	
	def Tools.saveReport(save)
		# Save the Tempo Report by pressing Ok on the dialogue.
		puts "Attempt to save the report or notice"
		if $OS == "windows"
			system("#{$rubyHome}/Utils/SaveReport.exe", save)
			sleep 2
		else
			sleep 1
			system("#{$rubyHome}/Utils/PressRightEnter.sh")
			sleep 1
		end		
	end
	
	
	
	

	
	
	def Tools.send(text)
		# Send some text to active window
		if $OS == "windows"
			system("#{$rubyHome}/Utils/Send.exe", text)
		else # Linux script
			system("#{$rubyHome}/Utils/send_keys.sh", text)
		end
	end
	
	
	
	
	
	def Tools.browserMaximise(browser)
		# Maximise the browser
		width = browser.execute_script("return screen.width")
		height = browser.execute_script("return screen.height")
		browser.driver.manage.window.move_to(0, 0)
		browser.driver.manage.window.resize_to(width,height)
	end
	
	
	
	
	def Tools.randomName
		# Return a random name based on the current date time
		time = Time.new
		
		ranName = time.strftime("%Y%m%d-%H%M%S")
		return ranName
	end # randomName
	
	
	
	
	def Tools.checkForErrorDialogue(link)
		# Checks for an error dialogue.
		# If one is found grab a screenshot and 
		# return true else return false.
		
		sleep 2
		begin
			$driver.find_element(:id, "asiAlert")
			Tools.grabScreenshotOfError("Error clicking the link #{link}")
			$driver.find_element(:xpath, "//input[@value='OK']").click
			puts "Error message displayed, see screenshot."
			return true
		rescue
			begin
				$driver.find_element(:id, "asiAlertText")
				Tools.grabScreenshotOfError("Error clicking the link #{link}")
				$driver.find_element(:xpath, "//input[@value='OK']").click
				puts "Error message displayed, see screenshot."
				return true
			rescue
				begin
					$river.find_element(:id, "asiAlertOK")
					Tools.grabScreenshotOfError("Error clicking the link #{link}")
					$driver.find_element(:xpath, "//input[@value='OK']").click
					puts "Error message displayed, see screenshot."
					return true
				rescue
					# puts "No error found"
					#Tools.grabScreenshotOfError("No Error")
					return false
				end
			end
		end
	end # checkForErrorDialogue
	
	
	
	
	
	

	def Tools.is_numeric?(obj) 
		# Returns true or false 
		return obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
	end
	
	
	
	
	
	
	def Tools.splitLotPlan(lotPlan)
		#This splits a lot plan into a lot and plan, 
		# ie: 15SD5684 becomes (Lot) 15 & (Plan) SD5684
		
		# Without a LotPlan we can't continue
		if lotPlan == nil
			puts "No lotPlan supplied, can't continue"
			exit
		end
		
		puts "Using lotPlan " << lotPlan
		$aLot = ""
		$aPlan = ""
		planFound = false 
		count = lotPlan.length - 1 # Length starts at 0, so 8 is counted 0..7
		for i in 0..count
			if not is_numeric?(lotPlan[i])
				$aPlan = $aPlan << lotPlan[i]
				planFound = true
			elsif planFound == true
				$aPlan = $aPlan << lotPlan[i]
			else
				$aLot = $aLot << lotPlan[i]
			end
		end
		puts "Lot: " << $aLot
		
		# If Plan is empty... do something
		if $aPlan == "" 
			$aPlan = $aLot
			puts "No Plan was found so using Lot as the Plan."
		end
		puts "Plan: " << $aPlan
		
		return planFound
	end
	
	
	
	
	
	
	def Tools.grabScreenshotOfError(isErrorMessageYesNo, message)
		if isErrorMessageYesNo.downcase == "yes"
			begin
				error_message = "#{caller[0]}-{#{error_message}"
				screenshot_name = error_message.gsub!(/[\/:{ }'`]/,'-') # Remove invalid filename characters
			rescue
				screenshot_name = Tools.randomName
			end
		else
			screenshot_name = message
		end
		screenshot_name = "test_results/" << screenshot_name
		
		begin
			puts "Attempting to save #{screenshot_name}.png"
			$driver.save_screenshot("#{screenshot_name}.png")
		rescue
			puts "Error: Unable to capture screenshot. Is a browser open?"
			# Catch the error returned if code doesn't use a browser
			# i.e: Source/tools.rb:686:in `grabScreenshotOfError': undefined method `save_screenshot' for nil:NilClass (NoMethodError)
		end
	end # grabScreenshotOfError


	
	


	def Tools.grabScreenshot( scriptName )
		# This grabs a screenshot of the current screen
		
		dateTime = "#{$today.gsub("/", "-")}_#{$now}"
		
		# If scriptName supplied, add it to the filename being created.
		#if scriptName != nil
			#dateTime = dateTime << "_#{scriptName}"
		#end
		
		if ARGV[1] != nil
			 dateTime = "#{dateTime}_#{ARGV[1]}"
		end
		filename = "#{$rubyHome}test_results/#{dateTime}.png"
		
		puts "Attempting to save #{filename}."
		begin
			#$driver.save_screenshot( filename )
			#system("import", "-window", "root", "-resize", "800x600", "#{filename}")
			system("import", "-window", "root", "-resize", "1280x768", filename)
			puts "#{filename} saved."
		rescue
			puts "Error: Unable to capture screenshot. Is a browser open?"
			# Catch the error returned if code doesn't use a browser
			# i.e: Source/tools.rb:686:in `grabScreenshot': undefined method `save_screenshot' for nil:NilClass (NoMethodError)
		end
		
	end # grabScreenshot
	
	
	
	
	
	
	def Tools.loadIDsArray(id, baseElement, count)
	
		# Load TriggerIDs into array
		@elements = Array(1..count)
		for i in 1..count
			element = "#{baseElement}[#{i}]/td[2]/div"
			#puts "Count of #{@elements.size} found"
			begin 
				@elements[i] = $driver.find_element(id, element)
				@elements[i] = @elements[i].text
				#puts @elements[i]
			rescue
				puts "Found no element at index: #{i}."
				puts element
			end
		end
		return @elements
	end # Tools.loadIDsArray
	
	
	

	
	
	def Tools.getZeroPayItem
		# Returns a pay Item with a zero value.

		# Loop through Prices and search for zero value
		for i in 1..10
			aPriceXpath = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/tbody/tr[#{i}]/td[5]/div"
			begin
				price = $driver.find_elements(:xpath, aPriceXpath)
				puts "Price = #{price.text}"
				if price.text = "0"
					$driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/tbody/tr[#{i}]/td/div/label/input").click
					puts "Found zero price in row #{i}."
					break
				end
			rescue
				puts "Price #{aPriceXpath} not found"
			end
		end
	end # Tools.getZeroPayItem
	



	
	def Tools.clickInFrame(id, element)
		# This is a debugging tool for finding 
		# what frame, iframe, etc. an element is on
		puts "Entering clickInFrame"

		frames = $driver.find_elements(:xpath, "//iframe")
		puts "Found #{frames.size} frames"
		
		i = 0
		frames.each do |frame|
			begin
				$driver.switch_to.default_content()
				$driver.switch_to.frame(i)
				$driver.find_element(id, element).click
				puts "Found #{element} in frame #{i}"
				break
			rescue
				# puts "Frame: #{i} didn't have #{element}"
			end
			i = i + 1
		end
		puts "Exiting clickInFrame"
	end # findFrame
		


		
		
	def Tools.clickLink(link, new_window)
		puts "Entering Tools.clickLink"
		# Click a link and ensure a new window opens.
		error_message = "No link called #{link} found"
		break_out = false
		if new_window
			for i in 1..10 # Attempt for 10 seconds
				begin
					Tools.defaultFrame
					
					$driver.find_element(:xpath, "//a[contains(text(),'#{link}')]").click
					puts "#{link} clicked in clickLink"
					error_message = "A new window did not open when '#{link}' clicked."
					sleep 3
					windows = $driver.window_handles # Get all window handles
					windows.each do |window|
						if window != $main_window
							puts "Found new window"
							break_out = true # Found new window
							return true
						end
					end
				rescue
					Tools.refreshClick("900", "260")

					sleep 7
				end
				if break_out
					break
				end
			end
			if not break_out
				# No new window opened
				grabScreenshotOfError(error_message)
				puts error_message
				puts "Link not found so quitting"
				exit
			end
		else
			sleep 3
			Tools.defaultFrame
			$driver.find_element(:xpath, "//a[contains(text(),'#{link}')]").click
			puts "#{link} clicked in clickLink"
		end
		sleep 2
		puts "Exiting clickLink"
	end # clickLink

	



	def Tools.click(id, element, *timeout)
		if timeout[0] != nil
			Tools.wait(id, element, timeout[0])		
		else
			Tools.wait(id, element, 6)
		end
		begin
			$driver.find_element(id, element).click
			return true
		rescue
			puts "#{element} not found."
			return false
		end
	end
		

	
	



	def Tools.getNewApplicationID
		puts "Getting Application ID"
		
		found = false
		i = 1
		while not found
			Tools.defaultFrame

			appID = $driver.find_element(:xpath, ".//*[@id='pageTitle']").text

			# Remove unwanted text.
			appID = appID.sub("Application Process", "")
			appID = appID.sub("Change Approval", "")
			appID = appID.sub("Extend Approval", "")
			appID = appID.sub("Cancel Approval", "")
			appID = appID.sub("Main Lodgement","")
			appID = appID.sub("Pre-lodgement","")
			appID = appID.sub("Upload acknowledgement notice","")
			appID = appID.sub("Enter location details","")
			appID = appID.sub(" - ", "")
			appID = appID.lstrip # Remove leading whitespace
			appID = appID.rstrip # Remove trailing whitespace

			if appID != ''
				found = true
			else
				puts "Application ID not yet found, trying again..."
				i = i + 1 
				sleep 2
			end
			if i == 20
				puts "Couldn't find the Application ID so quitting."
				exit
			end
		end
		puts "**********************************"
		puts "**********************************"
		puts "Application ID: '#{appID}'"
		puts "**********************************"
		puts "**********************************"

		return appID
	end
	
	
	
	def Tools.activateWindow(title)
		# Bring a window to the foreground
		#
		# Due to unreliability, we use two methods 
		# to bring popup window to the foreground
#		if File::exists?("#{$rubyHome}/Utils/activateWindow.sh")
			# Method 1
#			system("#{$rubyHome}/Utils/activateWindow.sh", title)
			#puts "#{title} should be active window now."
#			sleep 3
			# Method 2
			windows = $driver.window_handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window)  
			puts "#{title} should be the active window now"
#		else
#			puts "#{$rubyHome}/Utils/activateWindow.sh not found"
#		end	
	end
	


	def Tools.isNumber(c)
		# returns true if the first character of string is a number
		if c.length > 0
			b = c[0]
			begin # Check if we have a Number
				f = Integer(b[0]) # Is first char a number?
				return true
			rescue # We have a Character
				return false
			end	
		else
			puts "Empty string supplied to Tools.isNumber, quitting"
			exit
		end
	end # isNumber







	def Tools.dropDown(id, element, selector, value2Select)
		# This is for using Dropdown controls

		# Selector can be :text or :index
		Tools.wait(id, element, 6)
		Selenium::WebDriver::Support::Select.new($driver.find_element(id, element)).select_by(selector, value2Select)
	end







	def Tools.createUsers
		# Create and save to file new random users by using Faker
		# For more examples see: http://rubydoc.info/github/stympy/faker/master/frames
		for i in 1..300
			first = Faker::Name.first_name #=> "Kaci"
			last = Faker::Name.last_name #=> "Ernser"
			outputFile = "#{$rubyHome}NewUsers.txt"
			creationInfo = first + "," + last + "\n"
			if File.exist?(outputFile)
				File.open(outputFile, 'a') { |file| file.write(creationInfo) }
			else
				File.open(outputFile, 'w') { |file| file.write(creationInfo) }
			end
		end
	end
	
	
	
	
	
	def Tools.startTimer
		$startMethod = Time.now
	end
	
	
	
	
	
	def Tools.endTimer(method)
=begin	
		elapsedTime = Tools.elapsedTime($startMethod)
		
		logFile = "#{$rubyHome}Timings.log"
		puts "Creating log file: #{logFile}"
		methodInfo = method  + " " + $startTime.to_s + " " + elapsedTime
		Tools.logger(logFile, methodInfo)
=end
	end	
	
end # Tools
