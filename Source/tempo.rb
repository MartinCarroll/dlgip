#!/bin/env ruby
# encoding: utf-8
# The previous 2 lines allow Ruby to can handle arrows in a string, ie. '► Select'
####################################################
#
# This is the methods used for Tempo
#
# Written by: Martin Carroll, April 2014
####################################################

require 'rubygems'
require 'selenium-webdriver'
require 'clipboard' 
require 'mail'  # ruby mail library. https://github.com/mikel/mail
require '//home//Shared//code//Source//tools.rb'

module Tempo
	def Tempo.openTask( taskName, *appID )
		puts "Entering openTask"
		# Open a task by checking page title, for a link, by taskName & optional application ID and clicking it.
		# Returns: An error and stops the test if the task is not found
		error = nil
		
		if Tools.checkForScreen(taskName, 1) # We are already on the screen 
			# Do nothing
		else
			# Check displayed page for link to open task
			begin 
				if appID[0] != nil # If Application ID provided
					$driver.find_element(:xpath, "//a[contains(.,'#{appID[0]}')]").find_element(:xpath, "//a[contains(.,'#{taskName}')]").click	# Link found and clicked
					puts "#{taskName} #{appID[0]} link clicked."
				else
					$driver.find_element(:xpath, "//a[contains(.,'#{taskName}')]").click	# Link found and clicked
					puts taskName + " link clicked."
				end				
				wait 2
				if not Tools.checkForScreen(taskName, 1) # Check that the screen opens
					puts taskName + " link clicked but task not displayed."
					error = taskName + " link clicked but task not displayed."
				end
			rescue
				# Try to open the Task menu option
				begin
					begin
						wait
						puts "Try to click Task menu option"
						$driver.find_element(:xpath, "//a[contains(.,'Tasks')]").click
						# Task menu option clicked
						puts "Task menu option clicked"
						wait
					rescue
						puts "Task menu option not found"
						sleep 2
					end											
					
					if appID[0] != nil # If Application ID provided
						begin
							$driver.find_element(:xpath, "//a[contains(.,'#{appID[0]}')]").find_element(:xpath, "//a[contains(.,'#{taskName}')]").click # Click the Task
							puts "#{taskName} * #{appID[0]} clicked"
							wait
							if not Tools.checkForScreen(taskName, 1) # Check if the task's screen opened
								puts "#{taskName} * #{appID} link clicked but the task didn't display."
								error = "#{taskName} * #{appID} link clicked but the task didn't display."
							else
								puts "#{taskName} * #{appID} opened successfully" # Task opened successfully
							end
						rescue
							puts "#{taskName} * #{appID} link not found."
							error = "#{taskName} * #{appID} link not found."
							# Click Tasks again
							$driver.find_element(:xpath, "//a[contains(.,'Tasks')]").click
							puts "Task menu option clicked again"
						end
					else	# If no Application ID provided
						begin 
							$driver.find_element(:xpath, "//a[contains(.,'#{taskName}')]").click  # Click the Task
							puts "#{taskName} clicked"
							wait
							if not Tools.checkForScreen(taskName, 1) # Check if the task's screen opened
								puts "#{taskName} link clicked but the task didn't display."
								error = "#{taskName} link clicked but the task didn't display."
							else
								puts "#{taskName} opened successfully" # Task opened successfully
							end
						rescue
							puts "#{task[0]} * #{task[1]} link not found."
							error = "#{task[0]} * #{task[1]} link not found."
							# Click Tasks again
							$driver.find_element(:xpath, "//a[contains(.,'Tasks')]").click
							puts "Task menu option clicked again"
						end
					end
				rescue
					puts "Error. Looked for the task '#{taskName} on the current page and under the Tasks menu option with no luck"
					puts "Logged in as #{$USER} with password #{$PASS}"
					if Tools.isPortal
						puts "Environment was Portal at #{$URL}"
					else
						puts "Environment was Tempo at #{$URL}"
					end
					error = "Panic. Looked for the task on the current page and under the Tasks menu option with no luck"
				end
			end	
			if error != nil
				puts error
				puts "Exiting openTask"
				return false
			else
				puts "Exiting openTask"
				return true	
			end
		end
		puts "Exiting openTask"
	end
	








	
	def Tempo.confirmPaymentDetails
		puts "Entering Confirm Payment Details"
		wait
		i = 0
		found = false
		while not found
			if Tools.clickLinkIfExists("Confirm payment details")
				wait 2
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes		
				wait 2
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click # Select Credit Card
				
				begin
					# Click Submit button
					$driver.find_element(:xpath, "(//*[contains(text(),'Submit')])[2]").click
					puts "Submit button clicked"
					wait 2
					break
				rescue
					puts "Submit not found, quitting"
				end
			else
				i = i + 1
				wait
			end
			if i == 2
				puts "Confirm payment details screen not found, quitting"
				exit
			end
		end
		wait 2
		if Tools.checkForScreen("Application lodged")
			begin
				$driver.find_element(:xpath, "//input[@value='OK']").click
				puts "OK clicked on Application lodged screen"
			rescue
				puts "OK not found on Application lodged screen"
			end
		end
		puts "Exiting ConfirmPaymentDetails"
	end
	
	
	
	
	
	
	def Tempo.EnterBillingDetails
		puts "Entering Enter Billing Details"
		i = 0
		found = false
		while not found
			if Tools.clickLinkIfExists("Enter billing details")
				begin
					wait 5
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "Accept clicked"
					wait 2
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				
				begin
					# Click Next button
					$driver.switch_to.default_content()
					$driver.find_element(:xpath, "//*[contains(text(),'Next')]").find_element(:xpath, "//*[@type='button']").click
					puts "Next clicked"
					found = true
				rescue
					puts "Next button not found."
				end
			else
				i = i + 1
				sleep 1
				if i == 5
					puts "Enter billing details not found.."
					exit
				end			
			end
		end
		wait 4

		# Click Yes on the dialogue 
		$driver.find_element(:xpath, "(//*[@type='button'])[4]").click	
		wait
		
		puts "Exiting EnterBillingDetails"
	end
	
	
	
	
	def Tempo.ValidateEarlyReferralRequest
		puts "Entering Validate Early Referral Request"
		
		i = 0
		found = false
		while not found
			if Tools.clickLinkIfExists("Validate early referral request")
				found = true

				begin
					Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					puts "accept this task clicked"
					Tools.click(:xpath, "//input[@value='Done']",10)
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				wait 5

				# Has SARA's role been correctly identified?
				begin 
					Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 5) 
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes
					puts "Radio 1 selected"
				rescue 
					puts "Error clicking 1st radio button."
				end
				# Have the correct fees been identified?
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click # Yes
					puts "Radio 4 selected"
				rescue 
					puts "Error clicking 4th radio button."
				end
				# Does the proposed development include prohibited development?
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[8]").click # No
					puts "Radio 8 selected"
				rescue 
					puts "Error clicking 8th radio button."
				end
				# Is it possible a future application would involve a prescribed ERA?
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[11]").click # No
					puts "Radio 11 selected"
				rescue 
					puts "Error clicking 11th radio button."
				end
				# Has the applicant previously sought early referral advice?
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[14]").click # No
					puts "Radio 14 selected"
				rescue 
					puts "Error clicking 14th radio button."
				end
				wait 
				

				# The following is only on the second iteration of this screen being displayed
				# If triggers or fee items have been changed during validation, have you tried to advise the applicant?
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[18]").click # N/A-No changes made
					puts "Radio 18 selected"
				rescue 
					puts "Error clicking 18th radio button."
				end
				wait
									
				begin
					$driver.find_element(:xpath, "(//*[@type='button'])[2]").click
					puts "Submit clicked"
					wait 3
				rescue
					puts "Submit button not found"
					exit
				end
			else
				i = i + 1
				sleep 2
				if i == 5
					puts "Validate early referral request link not found."
					exit
				end
			end
		end
		puts "Exiting Validateearlyreferralrequest"
	end
	
	
	
	
	
	
	
	def Tempo.provideIRRequirements
		puts"Entering Provide IR Requirements"
		
		i = 0
		found = false
		while not found
			# Attempt to open the application
			puts "Attempt to open task 'Provide IR requirements  #{$appID}'"
			if openTask( "Provide IR requirements", $appID )
						
				begin
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "Accept clicked."
					wait
				rescue
					puts "Accept not found."
				end
						
				begin
					# 'No Requirements' / Further information is not requested checkbox
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
					wait 2
				rescue
					puts "'No Requirements' / Further information is not requested checkbox not found"
				end

				begin
					#$driver.find_element(:xpath, "(//*[@type='button'])[2]").click # Submit
					$driver.find_element(:xpath, "//*[contains(text(),'Submit')]").click
					puts "1: Submit clicked"
					wait 5
					break
				rescue
					begin
						$driver.find_element(:xpath, "//input[@value='Submit']").click
						puts "2: Submit clicked"
						break
					rescue
						puts "Submit not found"
						exit
					end
				end
			else
				wait
				puts "Unable to find application '#{$appID}'."
				i = i + 1
				if i == 2
					puts "Unable to find 'Provide IR requirements'. Skipping"
					break
				end
			end
		end
		puts"Exiting provideIRRequirements"
	end
	
	
	
	
	
	
	def Tempo.provideAssessment
		puts "Entering Provide Assessment"
		
		i = 1
		found = false
		while not found
			if openTask("Provide assessment", $appID)
				wait 2
				begin		
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "Accept clicked"
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				wait

				begin
					# Response provided without conditions checkbox
					$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click				
					puts "Response provided without conditions selected."
					wait 2
				rescue
					puts "# Response provided without conditions checkbox not found"
				end
				
				begin
					$driver.find_element(:xpath, "//*[contains(text(),'Submit')]").click
					puts "Submit clicked"
					wait 2
					break
				rescue
					begin
						$driver.find_element(:xpath, "//input[@value='Submit']").click
						puts "Submit clicked"
						wait 2
						break
					rescue
						puts "Submit not found"
						exit
					end
				end
			else
				wait 2
				i = i + 1
				if i == 5
					puts "No more Provide assessment tasks found."
					found = true
					break
				end
			end		
		end
		puts "Exiting provideassessment"
	end
	
	
	
	
	
	
	
	def Tempo.approveResponseNotice
		puts "Entering approve Response Notice"
		
		i = 0
		found = false
		while not found
			if openTask("Approve response notice", $appID )
				wait 
				begin
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "'Accept' button clicked"
					wait
				rescue
					puts "No Accept button found. I'll assume the task has been accepted and continue."
				end
				
				begin
					# Approve and send notice'
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
					wait 2
				rescue
					puts "'Approve and send notice' checkbox not found"
				end
				
				begin
					# Click Submit
					puts "Click Submit"
					$driver.find_element(:xpath, "//*[contains(text(),'Submit')]").click
					puts "Submit clicked."
					wait 3
					break
				rescue
					puts "Submit button not found."
					exit
				end
			else
				wait
				i =+ 1
				if i == 2
					puts "Approve Response Notice task not found"
					break
				end
			end
		end
		puts "Exiting approveResponseNotice"
	end

	


	
	
	
	def Tempo.advice
		# Log in as Case Officer
		puts "Entering Advice"
		
		i = 0
		wait
		while not Tools.checkForScreen("Advice")
			wait
			if not openTask( $appID )
				puts "'Advice' screen not found."
				sleep 1
				i = i + 1

			else
				wait 4
				begin
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "'Accept' button clicked"
					wait
				rescue
					puts "No Accept button found. I'll assume the task has been accepted and continue."
				end
				# Select 'No requirements'
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Selected 'No requirements'"
				wait
				
				# Click Submit again
				puts "Click Submit"
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				wait 6			
			end
			if i == 5
				break
			end
		end			
		puts "Exiting Advice"
	end

	
	
	

	def Tempo.ApproveDecisionNotice
		# Log in as Planning manager
		puts "Entering Approve Decision Notice Tempo"
		
		wait
		if not Tools.checkForScreen("Approve Decision Notice")
			wait
			if not openTask("Approve decision notice - " + $appID)
				puts "'Approve decision notice' not found."
			else
				wait
				# Select Approve and send notice
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				puts "Selected 'Approve and send notice'."
				wait

				# Click Submit again
				puts "Click Submit"
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				wait 6		
			end
		end
		
		puts "Exiting ApproveDecisionNoticeTempo"
	end
	
	
	
	
	
	
	
	
	def Tempo.recordnotificationdetails
		# Log in as Native Title Officer
		puts "Entering Record Notification Details"
		wait
		if not Tools.checkForScreen("Record notification details")
			wait
			if not openTask("Record notification details " + $appID)
				puts "'Record notification details' not found."
			else
				begin
					wait 2
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "'Accept' button clicked"
					wait 2
				rescue
					puts "No Accept button found. I'll assume the task has been accepted and continue."
				end
				# Select Submit notice for approval
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Selected No for 'Is native title notification required?'"
				wait
				
				# Click Submit again
				puts "Click Submit"
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				wait 6			
			end
		end		
		puts "Exiting recordnotificationdetails"
	end

	
	
	
	
	def Tempo.requestInformation
		# Log in as Case Officer
		puts "Entering request Information / Consider IR requirements"
		wait
	
		if not Tools.checkForScreen("Consider IR requirements") 
			wait
			if not openTask("Consider IR requirements " + $appID)
				puts "'requestInformation' not found."
			else
				wait 2
				# Select Submit notice for approval
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Selected No for Would you like to make a request for further information?"
				wait
				puts "Selected No for Would you like to send a notice to the applicant confirming a request for further information will not be made?"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
				wait
				
				# Click Submit again
				puts "Click Submit"
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				wait 6			
			end
		end
		puts "Exiting request Information / Consider IR requirements"
	end

	
	
	
	def Tempo.approveProperlyReferredNotice
		# Log in as Planning manager
		puts "Entering Approve properly referred notice"
			
		wait
		i = 0
		found = false
		while not found
			if openTask("Approve properly referred notice", $appID)
				wait 2
				begin
					# Select Approve and send notice
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					puts "Selected 'Approve and send notice'."
					wait
				rescue
					puts "Error clicking 'Approve and send notice'."
					exit
				end

				# Click Submit again
				puts "Click Submit"
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				wait 6		
			else
				puts "'Approve properly referred notice' not found yet."
				i = i + 1
				sleep 1
				if i == 4
					puts "'Approve properly referred notice' not found, quitting"
					exit
				end
			end
		end
		puts "Exiting Approveproperlyreferrednotice"
	end 
	
	
	
	
	
	
	
	def Tempo.reviewProperlyReferredNotice
		puts "Entering review properly referred notice"
		
		wait 20
		i = 0
		found = false 
		while not found 
			if openTask("Review properly referred notice", $appID)

				wait 2
				begin
					# Select Submit notice for approval
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					puts "Submit notice for approval clicked"
					$driver.save_screenshot("#{$rubyHome}Submit notice for approval clicked.png")
#puts "Check the browser now"
#exit	
					wait 2
				rescue
					puts "Submit notice for approval not found"
					exit
				end
				
				begin
					# Click Submit again
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").send_keys :page_down
					wait
					$driver.find_element(:xpath, "//*[text()='Submit']").click
					$driver.save_screenshot("#{$rubyHome}Submit clicked.png")
					puts "Submit clicked"
					wait 6
					found = true
					break				
				rescue
					puts "Submit not found"
					$driver.save_screenshot("#{$rubyHome}Submit not found.png")
					exit
				end
			else
				wait  2
				i = i + 1
				if i == 10
					puts "'Review properly referred notice' not found. Quitting"
					exit
				end
			end		
		end	

		puts "Exiting ReviewProperlyReferredNotice"
	end
	
	
	
	
	
	
	def Tempo.applicationPrepared
		puts "Entering Application Prepared"
		wait
		if not Tools.checkForScreen("Application prepared")
			wait
			if not openTask("Application prepared " + $appID)
				puts "'Application prepared' not found."
			end
		end
			
		link = "Application prepared #{$appID}"
		$driver.find_element(:xpath, "//a[contains(.,'#{link}')]").click
		puts "'#{link}' clicked."
		wait
		begin
			$driver.find_element(:xpath, "//*[text()='Accept']").click
		rescue
		end
		wait
		$driver.find_element(:xpath, "//*[text()='Ok']").click
		puts "Clicked OK."		
		wait
		puts "Exiting applicationPrepared"
	end
	
	
	def Tempo.editUser( user, password, email )
		puts "Entering Edit user"

		if user == nil
			puts "No user supplied, quitting."
			exit
		end

		if email == nil
			puts "No email supplied, quitting."
			exit
		end
		
		# Click Actions
		$driver.find_element(:xpath, "//a[contains(.,'Records')]").click
		wait
		$driver.find_element(:xpath, "(//a[contains(.,'Users')])[2]").click
		wait 2
		$driver.find_element(:xpath, "//*[starts-with(@class,'gwt-TextBox')]").send_keys user
		$driver.find_element(:xpath, "//*[starts-with(@class,'gwt-TextBox')]").send_keys :enter
		wait 3
		if not $driver.find_element(:xpath, ".//*[starts-with(@class,'gwt-InlineHTML')]").text.include? "No records available"
			begin
				$driver.find_element(:xpath, "//*[contains(text(),'#{email}')]")
				puts "#{email} Email found"
			rescue
				puts "#{email} Email not found"
		
				$driver.find_element(:xpath, "(//*[contains(@class,'appian-feed-entry-author')])[1]").click
				puts "Wait 4"
				wait
				

	
				$driver.find_element(:xpath, "//*[contains(text(),'Related Actions')]").click
				puts "Wait 5"
				wait
				$driver.find_element(:xpath, "//*[contains(text(),'Edit Individual')]").click
				wait
				$driver.find_element(:xpath, "(//*[starts-with(@id,'gwt-uid')])[4]").clear
				$driver.find_element(:xpath, "(//*[starts-with(@id,'gwt-uid')])[4]").send_keys email
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
				wait
				puts "#{user} found."
				Tools.logger("#{$rubyHome}CSVs/editUser-processed.log", "#{user}, User found.")
				puts "Wait 6"

				$driver.find_element(:xpath, "//*[text()='Submit']").click
				puts "Clicked Submit."		
				wait 3
			end
		else
			Tools.logger("#{$rubyHome}CSVs/editUser-noUser.log", "#{user}, #{email}, User not found.")
		end
		
		# Finished - reset so next user can be processed
		#$driver.find_element(:xpath, "(//*[contains(text(),'Users'))[1]").click
		#wait
		puts "Exiting Edit user"
	end
	
	
	
	
	
	
	def Tempo.initiatingReferralApplication
		puts "Entering Initiating referral application"
		
		count = 0 
		if Tools.checkForScreen("Initiating referral application")

			# Get and store the displayed application ID. 
			$appID = Tempo.getNewApplicationID
			# Tools.checkPageForText			
			
			# Now to wait for Processing to equal 100%
			puts "Now to wait for Processing to equal 100%"
			ready = false
			counter = 0
			while not ready
				begin
					if $driver.find_element(:xpath, "//*[text()='100%']")
						ready = true
						puts "Progress = 100%"
					end
				rescue
					# Click Refresh
					puts "Clicking Refresh"
					refreshBtn = "//*[text()='Refresh']"
					$driver.find_element(:xpath, refreshBtn).click
					wait 2
					count = counter * 2
					puts "Waited #{count} seconds."
				end
				
				counter = counter + 1
				if counter == 150
					puts "Processing still not complete."
					exit
				end
				# wait
			end
			#wait 2
			
			begin
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				puts "Clicked Submit."		
				wait 3
			rescue
				puts "Issue clicking Submit button"
			end
		else
			puts "Initiating referral application screen not found"
			# Do nothing
		end
		puts "Exiting Initiating referral application"
	end
	
	
	
	
	def Tempo.confirmReferralDetails
		puts "Entering Confirm referral details"

		wait
		found = true
	
		if not Tools.checkForScreen("Select pay items")
			if not Tools.checkForScreen("Confirm referral details")
				wait
				if not openTask("Confirm referral details - " + $appID)
					puts "Confirm referral details not found."
					found = false
				end
			end
			
			if found
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Proceed with lodgement of the referral application?
					puts "Proceed with lodgement of the referral application? = YES"
				rescue
				end

				# Click Next button
				$driver.find_element(:xpath, "//*[text()='Next']").click
				puts "Next button clicked."
				wait 2
			end
		end
		puts "Exiting confirmReferralDetails"
	end
	
	
	
	def Tempo.Reviewdecisionnotice( approve)
		puts "Entering Review decision notice"
		# as Case Officer
		
		if Tools.checkForScreen("Review decision notice")
			# Click Next button
			puts "Click Next button"
			nextBtn = "//*[text()='Next']"
			$driver.find_element(:xpath, nextBtn).click
			wait 2
		else
			wait
			openTask("Review decision notice - " + $appID)
			wait	
		end

		# Tools.checkPageForText
		
		begin
			# Click Accept button
			$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
			puts "'Accept' button clicked"
			wait 2
		rescue
			puts "No Accept button found. I'll assume the task has been accepted and continue."
		end	
	
		if approve != nil
			puts "Approve choice of #{approve} supplied."
			if approve.downcase == "go back and review original selection"		
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # Go back and review original selection
				puts "Go back and review original selection selected"	
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Submit notice for approval
				puts "Submit notice for approval selected"
			end
		else
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Submit notice for approval
			puts "Submit notice for approval selected"
		end
		
		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		puts "Clicked Submit"
		wait 3
		
		puts "Exiting Review decision notice"
	end
	
	
	
	
	def Tempo.Approveproperlymadenotice( approve )
		puts "Entering Approve properly made notice"
		# Processed by Planning Manager
		
		wait
		i = 0
		found = false
		while not found
			if openTask("Approve properly made notice", $appID)
				wait	
				# Tools.checkPageForText
		
				begin
					# Click Accept button
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "'Accept' button clicked"
					wait 2
				rescue
					puts "No Accept button found. I'll assume the task has been accepted and continue."
				end
				
				if approve != nil
					puts "Approve choice of #{approve} supplied."
					if approve.downcase == "go back and review original selection"		
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # Go back and review original selection
						puts "Go back and review original selection selected"	
					else
						$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Approve and send notice
						puts "Approve and send notice selected" 
					end
				else
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Approve and send notice
					puts "Approve and send notice selected"
				end
				
				# Click Submit
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				puts "Clicked Submit"
				wait 3
			else
				i = i + 1
				sleep 1
				if i == 5
					puts "Approve properly made notice #{$appID} not found"
					exit
				end
			end
		end
		puts "Exiting approveProperlyMadeNotice"
	end
	
	
	
	
	
	
	def Tempo.reviewProperlyMadeNotice( approve )
		puts "Entering Review properly made notice"
		
		wait 2
		i = 0
		found = false
		while not found
			if openTask("Review properly made notice", $appID)
				wait 2
				# Tools.checkPageForText

				begin
					wait 5
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "Accept clicked"
					wait 2
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				
				begin
					if approve != nil
						puts "Approve choice of #{approve} supplied."
						if approve.downcase == "go back and review original selection"		
							$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # Go back and review original selection
							puts "Go back and review original selection selected"	
						else
							$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Submit notice for approval
							puts "Submit notice for approval selected"
						end
					else
						$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # 'Submit notice for approval' clicked
						puts "'Submit notice for approval' selected"
					end
					
					# Click Submit
					$driver.find_element(:xpath, "//*[text()='Submit']").click
					puts "Clicked Submit"
					wait 3
					break
				rescue
					puts "Error clicking radio button"
					exit
				end
			else
				sleep 1
				if i == 10
					puts "Review properly made notice - #{$appID} not found, quitting"
					exit
				end
			end			
		end
		puts "Exiting  Review properly made notice"
	end
	
	
	
	
	def Tempo.ReviewEarlyReferralResponses
		puts "Entering Review early referral responses"
		
		wait
		if Tools.checkForScreen("Review early referral responses")
			# Click Next button
			puts "Click Next button"
			nextBtn = "//*[text()='Next']"
			$driver.find_element(:xpath, nextBtn).click
			wait 2
			# Tools.checkPageForText
		else
			puts "Review early referral responses screen not found, continuing anyway"
		end
		puts "Exiting Review early referral responses"
	end
	
	
	
	
	
	def Tempo.getStatus( expectedStatus, expectedStep, appID )
		# puts "Entering Get Status" # aka. Get State
		
		wait
		if appID == nil
			if $appID != nil
				appID = $appID
			else
				puts "No Application ID supplied so we'll use one that is displayed."
				$appID = Tools.getNewApplicationID				# Get current Application ID
				Tempo.openApplication( $appID )       # Open Application ID to view state/step
				wait
			end
		else
			# Check if application is open. If not, open it.
			$appID = appID
			
			begin
				$driver.find_element(:xpath, "(//*[starts-with(@id,'milestone_gwt)])[1]")
			rescue
				# Browser not found
				Tempo.openApplication( $appID )       # Open Application ID to view state/step
			end
		end
		
		wait 2
		# STATUS
		# Get the current Status for the supplied App ID
		i = 1
		begin
			puts "Look for status of " + expectedStatus
			if $driver.find_element(:xpath, "(.//*[starts-with(@class,'appian_form_readonly')])[4]").text.include? expectedStatus
				actualStatus = $driver.find_element(:xpath, "(.//*[starts-with(@class,'appian_form_readonly')])[4]").text
				actualStatus = actualStatus.downcase.strip # Down case and remove leading / trailing spaces
				expectedStatus = expectedStatus.downcase.strip
			else
				puts "Error retrieving Status of #{expectedStatus}, quitting"
				exit
			end
		rescue
			puts "Error retrieving Status, quitting"
			exit
		end

		# Now compare actual to expected status
		# and return true or false
		puts "============================="
		if actualStatus == expectedStatus
			puts actualStatus + " = " + expectedStatus
			puts "============================="
			passed = true
		else
			puts actualStatus + " <> " + expectedStatus
			puts "============================="
			passed = false
		end
		
=begin
		# STEP
		# Get the current Step for the supplied App ID
		i = 1
		found = false
		puts "Look for current step of " + expectedStep
		while not found
			begin 
				# puts "Try Index: " + i.to_s	
				puts $driver.find_element(:xpath, "(.//*[starts-with(@class,'accessibilityhidden')])[#{i}]").value
				
				if $driver.find_element(:xpath, "(.//*[starts-with(@class,'accessibilityhidden')])[#{i}]").text.include? "Current Step: "		
					actualStep = $driver.find_element(:xpath, "(.//*[starts-with(@class,'accessibilityhidden')])[#{i}]").text
					actualStep = actualStep.sub("Current Step: ","")
					actualStep = actualStep.downcase.strip # Down case and remove leading / trailing spaces
					expectedStep = expectedStep.downcase.strip
					found = true
				end
			rescue
				print "."
			end
			i = i + 1 # increment to try the next one.
			if i == 30
				puts "Error retrieving step with index: #{i}, quitting"
				exit
			end

		end

		# Now compare actual to expected step
		# and return true or false
		puts "============================="
		if actualStep == expectedState
			puts actualStep + " = " + expectedState
			puts "============================="
			passed = true
		else
			puts actualStep + " <> " + expectedState
			puts "============================="
			passed = false
		end
=end

		# puts "Exiting getStatus"
	end
	
	
	
	
	
	
	def Tempo.confirmPayItems
		puts "Entering Confirm Pay Items"
		
		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		$driver.find_element(:xpath, nextBtn).click
		wait 2
		
		# Tools.checkPageForText
		
		puts "Exiting confirmPayItems"
	end
	
	
	
	
	def Tempo.validateChangeRequest
		puts "Entering validate Change Request"

		if $appID == nil
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			puts "No application ID supplied, quitting"
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			exit
		end
		
		if not Tempo.openApplication($appID)  # if Application not found, quit
			puts "Application #{$appID} not found, quitting"
			exit
		end		

		# Tools.checkPageForText
		
		begin 
			linkText = "Validate change request"
			$driver.find_element(:xpath, "//*[contains(text(),'#{linkText}')]").click
			puts "Clicked 'Validate change request' link,"
			wait 2
		rescue
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			puts "'Validate change request' link not found, quitting"
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			exit
		end

		# Has SARA's role on this change request been correctly identified?
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes
		puts "Selected 'Has SARA's role on this change request been correctly identified?'"

		# Is the proposed change a minor change?
		$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click # Yes
		puts "Selected 'Is the proposed change a minor change?'"

		# Has SARA's role on the original development application been correctly identified?*
		$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click # Yes
		puts "Selected 'Has SARA's role on the original development application been correctly identified?'"

		# Are there any affected entities associated with this request?
		$driver.find_element(:xpath, "(.//*[@type='radio'])[8]").click # No
		puts "Selected 'Are there any affected entities associated with this request?'"

		# Has the owner's consent been provided?
		$driver.find_element(:xpath, "(.//*[@type='radio'])[11]").click # Not Required
		puts "Selected 'Has the owner's consent been provided?'"

		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		$driver.find_element(:xpath, nextBtn).click
		wait 2
		
		puts "Exiting validateChangeRequest"
	end
	
	
	
	

	
	
	
	def Tempo.reconciliation( appID, *paymentType )
		puts "Entering Tempo.reconciliation"
		if appID != nil  # When using created application
			$appID = appID
		elsif $appID == nil # When creating application first $appID should not be nil
			puts "No appID supplied so it's likely no application ID was created or supplied, quitting."
		end
		
		if $shortAppID == nil
			$shortAppID = Tools.getShortAppID # Get the shortened App ID from the application
			puts "$shortAppID is " << $shortAppID
		end
		
		if paymentType == nil
			$paymentType = "Visa"
			puts "No Payment type provided so Visa used"
		else
			$paymentType = paymentType
		end
		
		# Click the Appian DB link
		Tools.clickAppianlink
		
		puts "00000000000000000000000000000000000000000000000000000"
		# Get the order Number
		Tools.getSSQRequestID

		# Re-click the Appian DB link
		Tools.clickAppianlink
		sleep 2
		
		puts "00000000000000000000000000000000000000000000000000000"
		# Get the Fee ID
		Tools.getScheduleTriggerFeeID
		
		puts "00000000000000000000000000000000000000000000000000000"
		# Make payment file and email it.
		Tools.makePaymentFile

		puts "00000000000000000000000000000000000000000000000000000"
		# Email payment file
		Tools.emailPaymentFile( demo = false)		
		
		puts "00000000000000000000000000000000000000000000000000000"
		# Validate the payment
		Tools.validatePayment
		
		puts "Exiting Tempo.reconciliation"
	end
	
	
	
	
	
	
	
	
	
	
	def Tempo.compareValues(expected, actual)
		# Process the two values
		# Convert nil to empty string

		# If not nil
		# Remove , (Comma)
		# Remove . (Decimal point)
		# Remove $ (Dollar symbol)
		# Remove leading and trailing spaces
		# if nil set it to empty string ""
		if actual != nil 
			actual = actual.sub(",","")
			actual = actual.sub("$","")
			actual = actual.sub(".","")
			actual = actual.strip
		else
			actual = ""
		end
		if expected != nil
			expected = expected.sub(",","")
			expected = expected.sub("$","")		
			expected = expected.sub(".","")
			expected = expected.strip
		else
			expected = ""
		end

		# Compare the two values
		if expected == actual
			puts "'#{expected}' equals '#{actual}'"
			return "Equal"
		else
			puts "'#{expected}' doesn't equal '#{actual}'"
			return "Not equal"
		end
	end
	
	
	
	
	
	def Tempo.reviewCaseDecisionTempo
		puts "Entering Review Case Decision"
		
		puts "Code required here"
		exit
		
		puts "Exiting reviewCaseDecision"
	end
	
	


	
	
	
	
	

	
	
	
		
	def Tempo.enterPostApprovalRequestDetails
		puts "Enter 'Enter post approval request details'"
		
		aDate = Tools.currentDate
		
		
		# Extend Period of Currency
		begin
			# Attempt to enter 2 dates
			 # Set Current Approval date
			Tempo.enterDate("(//*[starts-with(@class,'aui-DateInput-Placeholder')])[1]")
			wait
			
			# Set the new future date
			Tools.getFutureDate(aDate, 7)
			$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-DateInput-Placeholder')])[2]").click
			sleep 1
			$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-DateInput-Placeholder')])[2]").send_keys aDate
			wait
		rescue
			puts "Date fields not found. Continuing..."
		end

		# Tools.checkPageForText
		
		# Upload a file
		# Click Browse button
		puts "Click Browse button"
		$driver.find_element(:xpath, "(//*[@type='file'])[1]").click
		wait 3
		Clipboard.copy($aFile1)
		puts "Copied: #{Clipboard.paste}"
		wait
		if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
			puts "Input filename to upload #{Clipboard.paste}"
			system("#{$rubyHome}/Utils/fileUpload.sh")
		else
			puts "#{$rubyHome}/Utils/fileUpload.sh not found"
		end
		wait 4

		# Click Next button
		begin
			$driver.find_element(:xpath, "//*[text()='Next']").click
			puts "Next button clicked"
			wait 4
		rescue
			puts "Next button not found, quitting"
			exit
		end
	
		puts "Exiting 'enter post approval request details'."
	end




	def Tempo.RequestPreApplicationAdvice( assMan, natureOfDev )
		puts "Entering RequestPreApplicationAdvice"
		
		# Proposed development details
		if natureOfDev == nil
			natureOfDev = "Material change of use"
		end
		
		case natureOfDev
		when "Building work"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			puts "Selected Building work"
			
		when "Material change of use"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
			puts "Select Material change of use"
			
		when "Operational work"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[3]").click
			puts "Select Operational work"
			
		when "Reconfiguring a lot"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[4]").click			
			puts "Select Reconfiguring a lot"
			
		when "To be confirmed"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[5]").click
			puts "Selected To be confirmed"
			
		else
			puts "Unknown 'Nature of Development' of " + natureOfDev
			puts "Selected default of Material change of use"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
		end
		
		# Tools.checkPageForText
		
		# Enter a description
		puts "Enter a description"
		description = "(//*[contains(@class, 'aui-TextAreaInput')])[1]"
		$driver.find_element(:xpath, description).send_keys "Some text description"

		# Select Assessment Manager
		if assMan == nil
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:index, 1)
			puts "Selected default Assessment manager"
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:text, assMan)			
			puts "Selected Assessment manager of " + assMan	
		end
				
		# Enter a description
		puts "Enter Some details of the request"
		description = "(//*[contains(@class, 'aui-TextAreaInput')])[2]"
		$driver.find_element(:xpath, description).send_keys "Some details of the request"
			
		# Would you like a meeting
		$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # No												
		puts "Exiting RequestPreApplicationAdvice"
		
		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		puts "Clicked Submit"
		wait 3
		
		puts "Exiting RequestPreApplicationAdvice"
	end
	
	
	
	
	def Tempo.registerRecord( recordType, idOtherParty, assMan, contact, details)
		puts "Entering registerRecord"

		wait
		if Tools.checkForScreen("Register record")
		
			# Tools.checkPageForText
		
			if recordType != nil
				recordType = "general"
				puts "recordType = general"
			end
			
			if recordType == "general"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click												
				puts "General selected"
				wait 2
				$driver.find_element(:xpath, "(.//*[@type='text'])[1]").send_keys contact
				puts "Added Contact"
				$driver.find_element(:xpath, "//*[contains(@class, 'aui-TextAreaInput printable')]").send_keys assMan
				puts "Added Details"
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click												
				puts "DILGP (SARA) as missed referral agency selected"
				wait 2
				
				if idOtherParty != nil
					idOtherParty = "dilgp"
				end
				
				if idOtherParty == "dilgp"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click												
					puts "Identified by set as DILGP (SARA)"
					wait 2	
					#	Select Assessment Manager
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:text, assMan)
					puts "Selected Assessment manager"		
				else
					$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click												
					puts "Identified by = Other Party"
					wait 2
					#	Select Identify other party
					if idOtherParty == nil
						idOtherParty = "Joseph King"
					end
					
					$driver.find_element(:xpath, "(.//*[@type='text'])[1]").send_keys idOtherParty
					puts "Added Identify other party of " + idOtherParty
					
					# Date of missed Referral Notice
					Tempo.enterDate
					wait
					
					#	Select Assessment Manager
					if assMan == nil
						Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:index, 1)
					else
						begin
							Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:text, assMan)
						rescue
							puts "##########################################"
							puts "Error: Unable to select " + assMan
							puts "##########################################"
							exit
						end
					end
					
					puts "Selected Assessment manager"		
				end
				wait
				# Click Submit
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				puts "Clicked Submit"
				wait 3
				return true

			end
			
			# Click Submit
			$driver.find_element(:xpath, "//*[text()='Submit']").click
			puts "Clicked Submit"
			wait 3
			return true
		else
			puts "Register record screen not found, quitting"
			#exit
		end
		puts "Exiting registerRecord"
	end
	
	
	
	
	
	
	def Tempo. recordRequestDetails
		puts "Entering recordRequestDetails"

		# Upload a file
		begin
			# Click Browse button
			puts "Click Browse button"
			$driver.find_element(:xpath, "(//*[@type='file'])[1]").click
			wait 3
			Clipboard.copy($aFile1)
			puts "Copied: #{Clipboard.paste}"
			wait
			if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
				puts "Input filename to upload #{Clipboard.paste}"
				system("#{$rubyHome}/Utils/fileUpload.sh")
			else
				puts "#{$rubyHome}/Utils/fileUpload.sh not found"
			end
			wait 4
		rescue
			puts "File upload button not found, continuing anyway."
		end
		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		$driver.find_element(:xpath, nextBtn).click
		wait 4	
		
		puts "Exiting recordRequestDetails"
	end
	
	
	

	def Tempo.selectPostApprovalRequestType( type )
		puts "Entering selectPostApprovalRequestType"
		
		if type == nil
			puts "No Request Type supplied, quitting."
			exit
		end

		# Get the application ID for later user
		$appID = Tempo.getNewApplicationID
		
		# Tools.checkPageForText
		
		puts "Looking for '#{type}'"
		if type["responsible entity"]
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click									
			
		elsif type["affected entity"]
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click									
		
		elsif type["referral agency"]
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click									
		
		elsif type["Other than minor change where DILGP (SARA) is the responsible entity"]
			$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click									
			
		elsif type["Extend period of currency"]
			$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click									
			
		else
			puts "Unknown Request Type of " + type + ", quitting"
			exit
		end

		# Click Next
		$driver.find_element(:xpath, "//*[text()='Next']").click
		puts "Next clicked"
		wait 3
		
		puts "Exiting selectPostApprovalRequestType"
	end
	
	
	
	
	def Tempo.requestPostApprovalChanges(appID)
		puts "Entering Request Post Approval Changes"

		if appID == nil
			if $appID != nil
				appID = $appID
			else
				puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
				puts "No application ID supplied, quitting."
				exit
			end
		end

=begin		
		# Tools.checkPageForText
		
		if no == nil
			puts "Click Yes - I want to change a SARA development approval"
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click							
		else
		puts "Click No - I want to change a pre-SARA development approval"
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click							
		end
		wait 
=end
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click	# Yes - I want to change a SARA development approval
			puts "'Yes - I want to change a SARA development approval' selected."
			wait
		rescue
		end
		
		begin
			# Enter Application ID
			$driver.find_element(:xpath, ".//*[@type='text']").send_keys $appID			
			puts "Entered application ID of: " +$appID
		rescue
		end
		wait
		
=begin
			# 'The assessment manager I am looking for is not listed.'
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			wait
		rescue
			puts "'The assessment manager I am looking for is not listed.' checkbox not found."
		end
		
		# Enter Assessment manager
		begin
			$driver.find_element(:xpath, "(.//*[starts-with(@id,'gwt-uid')])[14]").send_keys "Joe King"
			puts "Assessment manager entered"
		rescue
			puts "Assessment manager field not found."
		end

		# File upload
		begin
			if $driver.find_element(:xpath, "//*[@type='file']")  # Check for the File Upload button
				# Click Browse button
				puts "Click Browse button"
				$driver.find_element(:xpath, "//*[@type='file']").click
				wait 3

				Clipboard.copy($aFile2)
				puts "Copied: #{Clipboard.paste}"
				wait
				if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
					puts "Input filename to upload #{Clipboard.paste}"
					system("#{$rubyHome}/Utils/fileUpload.sh")
				else
					puts "#{$rubyHome}/Utils/fileUpload.sh not found"
				end
				wait 4
			else
				puts "Upload button not found"
			end
		rescue
			puts "No file upload functionality found."
=end

		begin
			# Click Search if found
			$driver.find_element(:xpath, "//*[text()='Search']").click
			puts "Search clicked"
			wait
		rescue
			puts "Search button not found."
		end

		# Check for error about missing / unknown Application number
		begin
			if $driver.find_element(:xpath, "//*[@class='validationMessage']").text.include? "The entered number could not be found."
				puts "The entered Application number could not be found. Quitting."
				exit
			end
		rescue
			# No missing application error displayed, phew...
		end
		
		begin
			puts "Look for a 'Select' link,"
			$driver.find_element(:link, "Select").click
			puts "Select clicked."
			wait
		rescue
			puts "Select link not found."
		end
		
		# Click Next
		$driver.find_element(:xpath, "//*[text()='Next']").click
		puts "Next clicked"
		wait 2
			
		begin 
			puts "Look for a 'Select' link,"
			$driver.find_element(:link, "Select").click
			puts "Select clicked."
			wait 2
			# Click Next
			$driver.find_element(:xpath, "//*[text()='Next']").click
			puts "Next clicked"
			wait 6
		rescue
		end
				
		puts "Exiting RequestPostApprovalChanges"		
	end
	
	
	
	
	def Tempo.ManageReferral(agency, responseProvided, appID)
		puts "Entering ManageReferral"
		wait

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
				
		wait
		openTask("Manage referral " << appID)
		wait			
	
		# Tools.checkPageForText		
		
		#	Select agency
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:index, 0)
		puts "Selected Agency"		
		wait 2
		
		#	Select ‘Response provided’ as response outcome, 
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:text, responseProvided)
		puts "Response provided"
		wait 2		
		
		# Select a referral date of today’s date, 
		Tempo.enterDate
				
		# ‘Click SAVE’ 
		puts "Click Save"
		$driver.find_element(:xpath, "//*[text()='Save']").click
		wait 2
				
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6		
		
		puts "Exiting ManageReferral"
	end
	
	
	
	
	
	def Tempo.enterDate( *xpath )
		puts "Enter today's date"
		
		if xpath[0] == nil
			xpath = "//*[starts-with(@class,'aui-DateInput-Placeholder')]"
		end
	
		begin
			# Click the date text field 
			$driver.find_element(:xpath, xpath).click
			wait
			# Select today
			$driver.find_element(:xpath, "//*[contains(@class,'datePickerDayIsToday')]").click
			wait 3
			puts "Today's date entered."
		rescue
			puts "Error selecting date."
		end
	end
	
	
	
	
	def Tempo.informationRequestResponse( response, appID )
		puts "Entering information Request Response / Provide information request response"

		wait

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
				
		wait
		openTask("Information request response " + appID) # Old
		#openTask("Provide information request response " + appID) # New
		wait	
		
		# Tools.checkPageForText
		
		begin
			# Click Accept button
			$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
			puts "'Accept' button clicked"
			wait 2
		rescue
			puts "No Accept button found. I'll assume the task has been accepted and continue."
		end
		
		begin
			if response == nil
				response = "Not Supplied"
			end
			
			case response.downcase
			when "1"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click					
			when "2"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click								
			when "3"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click								
			when "4"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click										
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click					
			end
		rescue
			puts "Unknown Response supplied of " + response
		end
		
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		puts "Exiting informationRequestResponse"
	end
	
	
	

	def Tempo.approveInformationRequestNotice( appID)
		puts "Entering approveInformationRequestNotice"
		
		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
				
		wait
		openTask("Approve information request notice - " + appID)
		wait	

		# Tools.checkPageForText
		
		# Approve
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
		wait 2
		
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		puts "Exiting approveInformationRequestNotice"
	end
	
	
	
	
	
	def Tempo.TAResponse
		puts "Entering TAResponse"
		
		wait 2
		taFound = true
		while taFound
			task = "TA Response - Formal"
			puts "Looking for #{task}"
			begin
				$driver.find_element(:xpath, "(//*[starts-with(text(),'#{task}')])[1]").find_element(:xpath, "//*[contains(text(),#{$appID})]").click
			rescue
				puts "No (more) TA Responses found."
				break
			end
			puts "'#{task}' clicked."
			wait

			# Tools.checkPageForText
			
			begin
				$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
				puts "'Accept' button clicked"
				wait 2
			rescue
				puts "No Accept button found. I'll assume the task has been accepted and continue."
			end
			
			# Response Provided'
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
			wait 2
					
			# Click Submit
			puts "Click Submit"
			$driver.find_element(:xpath, "//*[text()='Submit']").click
			wait 6
		end
		
		puts "Exiting TAResponse"
	end

	
	
	
	
	
	def Tempo.requestTechnicalAdvice( ta, appID )
		puts "Entering requestTechnicalAdvice"
		
		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
				
		wait
		openTask("Request technical advice " + appID)
		wait 3
		
		# Tools.checkPageForText
		
		# Select TA
		if ta == nil
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:index, 1)
			puts "No TA provided so the second one was chosen by default"
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:text, ta)
			puts "TA selected of " + ta
		end
				
		# Send Notice'
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
		wait 2
		
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		puts "Exiting requestTechnicalAdvice"
	end
	
	

	
	def Tempo.reviewInformationRequestNotice(approver, appID)
		puts "Exiting reviewInformationRequestNotice"

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
				
		wait
		openTask("Review information request notice - " + appID)
		wait
		
		# Tools.checkPageForText
		
		# Select 'Approve Review'
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
		wait 2
		
		# Select Approver
		if approver == nil
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:index, 2)
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:text, approver)
		end
		
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		puts "Exiting reviewInformationRequestNotice"
	end
	
	
	
	
	
	def Tempo.ReviewTANotice( appID )
		puts "Entering ReviewTANotice"

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
		
		wait
		openTask("Review TA notice - " + appID)
		
		# Tools.checkPageForText
		
		wait 2
		# Select 'Approve Review'
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click				
		wait 2

		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		puts "Exiting ReviewTANotice"
	end

	
	

	def Tempo.requestForFurtherInformation
		puts "Entering requestFurtherInformation / Consider IR requirements"

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		end
		wait

		found = false
		i = 0
		while not found
			# Open the Task
			if openTask("Consider IR requirements ", $appID)
				wait 2
				wait 2
				found = true
			else
				sleep 1
				i = i + 1
				if i == 60
					puts "'Consider IR requirements' not found"
					break
				end
			end
		end 
		# Tools.checkPageForText
		
		# Select No for the drop down/s
		i = 0
		dropdownsFound = true
		while dropdownsFound
			begin
				i = i + 1
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])#[{i}]")).select_by(:text, "No")
			rescue
				dropdownsFound = false
				break
			end
		end
		
		begin
			# Would you like to make a request for further information?
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # No	
			wait
		rescue
		end

		begin
			# Would you like to send a notice to the applicant confirming a request for further information will not be made?
			$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click # Yes
		rescue
		end
		wait 3

		begin
			# Click Submit
			$driver.find_element(:xpath, "//*[text()='Submit']").click
			puts "Submit clicked"
			wait 6
		rescue
			puts "Error clicking Submit."
		end
		puts "Exiting requestForFurtherInformation"
	end
	
	




	def Tempo.provideRecommendation(recommendation, subtype, currrencyPeriod, appID)
		puts "Entering provideRecommendation"

		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end

		i = 0 
		found = false
		while not found
			if openTask("Provide recommendation " + appID)
				found = true
				wait
				begin
					# Have all required fees been paid and reconciled?
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes
					puts "Have all required fees been paid and reconciled = Yes"		
				rescue
					puts "Have all required fees been paid and reconciled not found. Likely removed so continue anyway"
				end
				
				begin
					# Have all owner's consent requirements been satisfied?
					$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click # Yes
					puts "Have all owner's consent requirements been satisfied? = Yes"
				rescue
					puts "'Have all owner's consent requirements been satisfied?' not found. Likely removed so continue anyway"
				end
					
				begin
					# Is the native title process complete?
					$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click # Yes
					puts "Is the native title process complete? = Yes"
				rescue
					puts "'Is the native title process complete?' not found. Likely removed so continue anyway"
				end
					
				begin
					# Has portable long service levy been paid?
					$driver.find_element(:xpath, "(.//*[@type='radio'])[7]").click # Yes
					puts "Has portable long service levy been paid? = Yes"
				rescue
					puts "'Has portable long service levy been paid?' not found. Likely removed so continue anyway"
				end
				wait 2		

				begin			
					# if Approval dropdown found...
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:index, 1) 
				rescue
					puts "Recommendation drop down not found."
				end
				wait 2
				
				puts "Looking for '#{recommendation}'."
				case recommendation
				when "Direct the assessment manager that conditions must attach to any approval (where no variation request, and no part or preliminary approval)"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Direct the assessment manager to refuse the application"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "For other approval circumstances, direct the assessment manager"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "No requirements"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Variation request only"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Direct the assessment manager that any approval given must require a stated currency period"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Request is approved"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Approval"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				when "Refusal"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, recommendation)
				else
					puts "Unknown or misspelled recommendation of '#{recommendation}', quitting"
					exit
				end
				wait 3			
				
				begin
					if subtype != nil
						puts "Subtype of '#{subtype}' provided." 
						case subtype
						when "Part approval only"
							$driver.find_element(:xpath, "(.//*[@type='checkbox'])[4]").click
							
						when "Require a stated currency period"
							$driver.find_element(:xpath, "(.//*[@type='checkbox'])[5]").click
							case subtype
							when  "2 years"
								Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, sub)
							when "4 years"
								Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, sub)
							when "6 years"
								Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, sub)
							else
								puts "unknown or misspelled subtype supplied of #{subtype}, quitting"
							end
							
						when "Model conditions only", "Includes model conditions only"
							begin
								$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
							rescue
								puts "Model conditions only not found."
								exit
							end
							
						when "Includes non-standard conditions", "Includes non-model conditions"
							$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
							
						when "Give a preliminary approval only", "Give preliminary approval only (development permit applied for)"
							$driver.find_element(:xpath, "(.//*[@type='checkbox'])[3]").click
							
						else
							puts "unknown or misspelled subtype supplied of #{subtype}, quitting"
							exit
						end
					else
						puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
						puts "No subtype provided. It's likely not required so we'll try to continue."
						puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
					end
				rescue
					puts "Problem selecting subtype of: #{subtype}"
					exit
				end
				wait 2

				begin
					# Click Submit
					$driver.find_element(:xpath, "//*[text()='Submit']").click
					puts "Submit clicked"
					wait 6
				rescue
					puts "Problem clicking Submit"
					exit
				end

			else
				i = i + 1
				if i == 2
					puts "Provide recommendation " + appID + " link not found"
					exit
				end
			end
		end
	
		puts "Exiting provideRecommendation"
	end

	
	
	
	
	def Tempo.approveValidationOutcomeNotice( appID )
		puts "Entering approveValidationOutcomeNotice"
		# Used by PlanningManager
		
		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		elsif appID != nil
			$appID = appID
		end
		
		wait
		openTask("Approve validation outcome notice - " + appID)
		wait 2
		
		# Tools.checkPageForText
		
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Approve and send notice
		wait 2
		
		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		puts "Submit clicked"
		wait 6
		
		puts "Exiting approveValidationOutcomeNotice"
	end





	def Tempo.reviewValidationOutcomeNotice(approver, appID)
		puts "Entering reviewValidationOutcomeNotice"
		
		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
		
		if approver == nil
			puts "No Approver supplied so using default instead."
		end
		
		wait 2

		openTask("Review validation outcome notice - " + appID)
		wait

		# Tools.checkPageForText
		
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click		
		wait
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, approver)
		
		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		puts "Submit clicked"
		wait 6

		puts "Exiting reviewValidationOutcomeNotice"
	end
	
	
	
	
	

	def Tempo.allocateCase(adminOfficer, caseOfficer, planningManager, appID)
		puts "Entering allocateCase"
				
		puts "***************************"
		puts adminOfficer
		puts caseOfficer
		puts planningManager

		if $appID != nil
			appID = $appID
		end			
		appID = appID.sub("QDA", "SDA") # QDA -> SDA
		appID = appID.sub("Enter location details ","")
		$appID = appID
		
		puts "appID: '#{appID}'"
		puts "***************************"

		
		i = 0
		found = false
		while not found
			if openTask("Allocate case", appID)
				wait 3

				begin
					$driver.find_element(:xpath, "//*[contains(text(),'Accept')]").click
					puts "'Accept' button clicked"
					wait 2
				rescue
					puts "No Accept button found. I'll assume the task has been accepted and continue."
				end	

				# Set Business Support Officer
				begin
					puts "Business Support Officer (adminOfficer): #{adminOfficer}"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, adminOfficer)
					puts "Business Support Officer clicked."
					wait
				rescue
					puts "*********************************************************************************************************************"
					puts "Can't set 'Business Support Officer': #{adminOfficer}. Either user is wrong or field is read only or non-existent. Quitting"
					puts "*********************************************************************************************************************"
					exit
				end

				# Set Case Officer
				begin
					puts "caseOfficer: #{caseOfficer}"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, caseOfficer)
					puts "Case Officer set"
					wait
				rescue
					puts "*********************************************************************************************************************"
					puts "Can't set 'Case Officer': #{caseOfficer}. Either user is wrong or field is read only or non-existent. Quitting"
					puts "*********************************************************************************************************************"
					exit
				end

				# Set Planning Manager
				begin
					puts "Planning Manager: #{planningManager}"
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[3]")).select_by(:text, planningManager)
					puts "Planning Manager set"
					wait
				rescue
					puts "*********************************************************************************************************************"
					puts "Can't set 'Planning Manager': #{planningManager}. Either user is wrong or field is read only or non-existent."
					puts "*********************************************************************************************************************"
				end
				
				# Click Submit
				$driver.find_element(:xpath, "//*[text()='Submit']").click
				puts "Submit clicked"
				wait 6
				break
			else
				sleep 1
				i = i + 1
				if i == 5
					puts "Allocate case not found, quitting"
					exit
				end
			end
		end
		puts "Exiting allocateCase"
	end





	def Tempo.reviewTriggers( *vars )
		puts "Entering reviewTriggers"
		
		if vars[0] != nil
			appID = vars[0]
		end
		
		if vars[1] != nil
			new_trigger =vars[1]
		end
		
		if $appID == nil and appID == nil 
			puts "Issue as no appID exists or provided, quitting"
			exit
		elsif appID == nil
			appID = $appID
		end
		
		i = 0
		found = false
		while not found 
			if openTask("eview triggers ", appID)
				# Tools.checkPageForText
				found = true
				
				if new_trigger != nil # New trigger to add
					begin
						$driver.find_element(:xpath, "//*[text()='Edit']").click
						puts "Edit clicked"
						wait
						$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-TextInput')])[1]").clear
						$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-TextInput')])[1]").send_keys new_trigger
						wait
					rescue # Likely Referral triggers found...
						$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-TextInput')])[1]").clear
						$driver.find_element(:xpath, "(//*[starts-with(@class,'aui-TextInput')])[1]").send_keys new_trigger
						wait			
						# Click Search
						$driver.find_element(:xpath, "//*[text()='Search']").click
						puts "Search clicked"
						wait 2
					end
					
					$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
					wait
					$driver.find_element(:xpath, "//*[text()='Add triggers']").click
					wait
					# Click Next
					$driver.find_element(:xpath, "//*[text()='Next']").click
					puts "Next clicked"
					wait 2
					
					# Click Next again
					$driver.find_element(:xpath, "//*[text()='Next']").click
					puts "Next clicked"
					wait 2
				end
				
				begin
					# Click Submit
					$driver.find_element(:xpath, "//*[text()='Submit']").click
					puts "Submit clicked"
					wait 6
				rescue
					# Click Next
					$driver.find_element(:xpath, "//*[text()='Next']").click
					puts "Next clicked"
					wait 6
				end

				# Check for "Review pre-referral triggers"" screen
				if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Review pre-referral triggers"
					$driver.find_element(:xpath, "//*[text()='Submit']").click
					puts "Submit clicked"
					wait 6
				end
				
				validateFastTrackTriggers
				wait 2
				break
			else
				i = i + 1
				sleep 2
				if i == 4
					puts "triggers " + appID +  " task not found, quitting"
					exit
				end
			end
			wait 4
		end

		puts "Exiting reviewTriggers"
	end






	def Tempo.reviewPreReferralTriggers(appID)
		puts "Entering reviewPreReferralTriggers"

		begin
			if $appID == nil and appID == nil 
				puts "Issue as no appID exists or provided, quitting"
				exit
			end

			if appID == nil
				appID = $appID
			end
			openTask("Review triggers " + appID)
			wait 4
		rescue
			puts "No Task found"
		end
		
		# Tools.checkPageForText

		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		puts "Submit clicked"
		wait 6
		puts "Exiting reviewPreReferralTriggers"
	end






	def Tempo.validateFastTrackTriggers
		puts "Entering validateFastTrackTriggers"
		begin
			
			# Tools.checkPageForText
			
			if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Validate fast track triggers"
				# Click Next
				$driver.find_element(:xpath, "(//*[text()='Next'])[2]").click
				puts "Next clicked"
				wait 6
			else
				puts "'Validate fast track triggers' screen not found. Continuing anyway."
			end
		rescue
			puts "Validate fast track triggers screen not found. Continuing anyway."
		end
		puts "Exiting validateFastTrackTriggers"
	end






	def Tempo.validateApplication(app, appID, assManReference) # as Case Officer
		puts "Entering Validate Application"
		
		begin
			appID = appID.rstrip.lstrip
		rescue
			appID = nil
		end
		
		if appID == nil
			if $appID != nil
				appID = $appID
			else
				puts "No AppID found."
				exit
			end
		end	

		puts "Attempt to open task: Validate application " << appID
		if  openTask("Validate application ", appID)
			puts "Task opening."
			wait 3
			wait
		
			# Tools.checkPageForText
			
			puts "Check if #{appID} contains SRA or SDA."
			if appID["SRA"]
				puts "Application type SRA found."
				begin
					Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 5)
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					puts "Radio 1 selected"
				rescue 
					puts "Error clicking 1st radio button."
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
					puts "Radio 4 selected"
				rescue 
					puts "Error clicking 4th radio button."
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[8]").click
					puts "Radio 8 selected"
				rescue 
					puts "Error clicking 8th radio button."
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[10]").click
					puts "Radio 10 selected"
				rescue 
					puts "Error clicking 10th radio button."
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[16]").click
					puts "Radio 16 selected"
				rescue 
					puts "Error clicking 16th radio button."
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[21]").click
					puts "Radio 21 selected"
				rescue 
				end
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[24]").click
					puts "Radio 24 selected"
				rescue
					puts "Error clicking 24th radio button."
				end
				wait 5
				i = 0
				while 1
					begin
						$driver.find_element(:xpath, "(.//*[@type='radio'])[13]").click
						puts "Radio 13 selected"
						break
					rescue 
						puts "Error clicking 13th radio button."
						sleep 1
						i = i + 1
						if i == 20
							puts "I give up trying."
							break					
						end
					end
				end

				# Enter the assessment manager's reference number
				begin
					if assManReference != nil
						$driver.find_element(:xpath, "(.//*[starts-with(@class, 'aui-TextInput')])[1]").send_keys assManReference
						puts "'#{assManReference}' entered for Assessment Manager Reference."
					else
						$driver.find_element(:xpath, "(.//*[starts-with(@class, 'aui-TextInput')])[1]").send_keys "123456"
						puts "'123456' entered for Assessment Manager Reference."
					end
				rescue
					puts "Enter the assessment manager's reference number not found. Continuing anyway"
				end			
			elsif appID["SDA"]
				puts "Application type SDA found."
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				puts "Radio 1 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
				puts "Radio 4 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[7]").click
				puts "Radio 7 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[11]").click
				puts "Radio 10 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[13]").click
				puts "Radio 13 selected"
				wait 2
				
				#$ Need to page down to 'see' the next radio buttons
				puts "Need to page down to 'see' the next radio buttons"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[13]").send_keys :page_down
				wait 2
				
				$driver.find_element(:xpath, "(.//*[@type='radio'])[16]").click
				puts "Radio 16 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[23]").click
				puts "Radio 20 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[25]").click			
				puts "Radio 25 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[31]").click
				puts "Radio 28 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[34]").click
				puts "Radio 32 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[37]").click
				puts "Radio 36 selected"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[40]").click
				puts "Radio 39 selected"
				#$driver.find_element(:xpath, "(.//*[@type='radio'])[42]").click
				#puts "Radio 42 selected"
			else
				if app == nil
					puts "No application type provided, quitting"
				else
					puts "Unknown application type of #{app}, quitting"
				end	
				exit
			end
		else
			puts "Validate Application not found, quitting"
			exit
		end
		wait 2
	
		# Click Submit
		begin
			$driver.find_element(:xpath, "//*[text()='Submit']").click
			puts "Submit clicked"
		rescue	
			begin

				$driver.find_element(:xpath, "//*[text()='Next']").click
				puts "Next clicked"
			rescue
				puts "Could not find Submit or Next button, quitting"
				exit
			end
		end
		wait 6

		puts "Exiting validateApplication"
	end





	def Tempo.identifyValidationOutcome(validationOutcome, appID)
		puts "Entering identifyValidationOutcome"

		if appID == nil
			if $appID != nil
				appID = $appID
			else
				puts "*************** ERROR ************************"
				puts "No Application ID provided so can't continue"
				puts "*************** ERROR ************************"
				exit
			end
		end	
		
		wait
		if not Tools.checkForScreen("Identify validation outcome", 2)
			puts "Unable to find 'Identify validation outcome' screen, let's look for link"
			openTask("Identify validation outcome " + appID)
			wait 3
		end

		## Tools.checkPageForText
		
		begin
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			# Validation outcomes		
			case validationOutcome.downcase
			when "properly" # Properly referred
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
					puts "Properly referred selected"
				rescue
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					puts "'Yes - continue assessing this request' selected"				
				end
				
			when "not"	# Not Properly referred
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
				puts "Not Properly referred selected"
				
			when "invalid" # Invalid referral
				$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
				puts "Invalid referral selected"
				
			else
				puts "Unknown Validation Outcome of " + validationOutcome + " so selecting Properly made as default"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			end
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		rescue
			puts "Couldn't set Validation Outcome of " + validationOutcome + "."
		end
		wait
		
		begin
			# Has the correct fee amount been paid?
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes
			puts "Selected 'Has the correct fee amount been paid?'"
		rescue
			puts "'Has the correct fee amount been paid?' was not found. ignoring."
		end
		wait
		# Click Submit
		puts "Click Submit"
		$driver.find_element(:xpath, "//*[text()='Submit']").click
		wait 6
		
		begin
			# Click Yes
			$driver.find_element(:xpath, "(//*[text()='Yes'])[3]").click
			puts "Yes clicked."
			wait 3
		rescue
		end
	
		puts "Exiting identifyValidationOutcome"
	end

	
		
	

	def Tempo.confirmPaymentItems
		puts "Entering Confirm Payment Items"

		# Tools.checkPageForText
		
		# Click Next
		puts "Click Next"
		$driver.find_element(:xpath, "//*[text()='Next']").click
		wait 3
		
		puts "Exiting confirmPaymentItems"
	end
	
	
	
	
	

	def Tempo.confirmPaymentDetailsTempo
		puts "Entering Confirm Payment Details Tempo"
		puts "aka. Application summary screen"
		
		if Tools.checkForScreen("Confirm payment details")
		
			# Tools.checkPageForText
			
			# Click 'Yes, I have completed my payment'
			puts "Select Yes for 'Have you completed your payment?'"
			Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 5)
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			wait 4
		
			# Select Payment Type (of Credit Card)
			puts "Select payment type of Credit Card"
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			wait 4

=begin # BPay code		
			# Enter receipt number
			$driver.find_element(:xpath, "(.//*[@type='text'])[1]").send_keys "123456789"
			wait 6

			# Open Date Picker
			$driver.find_element(:xpath, "//*[starts-with(@class,'aui-DateInput-Placeholder')]").click
			wait 4

			# Select today
			$driver.find_element(:xpath, "//*[contains(@class,'datePickerDayIsToday')]").click
			wait 3
=end

			# Click Submit
			puts "Click Submit"
			$driver.find_element(:xpath, "(//*[text()='Submit'])[2]").click
			wait 6
	 
			# Application should be lodged so click OK
			puts "'Application lodged' screen displayed"

			# Click OK
			puts "Click OK"
			$driver.find_element(:xpath, "//*[text()='Ok']").click
			wait 4
			begin
				# Click Yes
				$driver.find_element(:xpath, "//*[text()='Yes']").click		
				wait 3
			rescue
				puts "Yes button not found, continuing anyway."
			end
		else
			puts "'Confirm payment details' not found."
		end
		puts "Exiting confirmPaymentDetailsTempo"
	end





	def Tempo.selectPayItems( appID )
		puts "Entering Tempo.selectPayItems"
		wait 3
		if appID != nil
			$appID = appID
		elsif $appID != nil
			appID = $appID
		elsif appID == nil
			puts "No App ID provided, quitting."
			exit
		end
		begin
			openTask("Select pay items")
			wait 2
		rescue
		end
		
		if not Tools.checkForScreen("Select pay items")
			begin
				# Click Refresh
				puts "Select pay items link not found so click Refresh button."
				refreshBtn = "//*[text()='Refresh']"
				$driver.find_element(:xpath, refreshBtn).click
				wait 2
			rescue
				puts "Refresh button not found"
			end
		elsif Tools.checkForScreen("Initiating referral application")
			# Initiating referral application"
			count = 0 
			# Get the new Application ID
			selectPayItemsLink = "//a[starts-with(.,'Select pay items')]"
			appID = $driver.find_element(:xpath, selectPayItemsLink).text
			appID = appID.sub("Select pay items ", "")
			$appID = appID.strip
		
			# Tools.checkPageForText
		
			selectPayItemsFound = false
			while selectPayItemsFound == false
				begin
					$driver.find_element(:xpath, selectPayItemsLink).click
					selectPayItemsFound = true
					wait
				rescue
					# Click Refresh
					puts "Select pay items link not found so click Refresh button."
					refreshBtn = "//*[text()='Refresh']"
					$driver.find_element(:xpath, refreshBtn).click
					wait
				end
				count = count + 1
				if count == 40
					puts "Select pay items link not found, quitting."
					exit
				end
			end
			puts "Select pay items found: " + selectPayItemsLink
			
			$driver.find_element(:xpath, selectPayItemsLink).click
			wait 6
			
			# Get and store the displayed application ID. 
			$appID = Tempo.getNewApplicationID

			if Tools.wait(:xpath, "(//*[text()='Select pay items'])[1]")
				morePayItems = true
				i = 1
				while morePayItems
					begin
						$driver.find_element(:xpath, "(//*[text()='Select pay items'])[#{i}]").click
						puts "Select pay items number #{i} clicked."
						wait 4
						# Select all Pay Items
						puts "Select all Pay Items"
						$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
						wait 4
						wait 4
						begin
							$driver.find_element(:xpath, "//*[text()='Add pay item(s)']").click
							wait 4
						rescue
							puts "'Add pay item(s)' button inactive or not found, so quitting."
							exit
						end
					rescue
						if i > 1
							puts "#{i-1} Pay Items found and there are no more Pay Items to be found."
						end
						morePayItems = false
					end
					i = i + 1
				end
			else
				puts "Select pay items link/s not found. It's likely reviewing or validating so try continuing."
			end
			
			wait 4
			
			begin
				$driver.find_element(:xpath, "//*[text()='Next']").click
				puts "Clicked Next."		
				wait 3
			rescue
				puts "Issue clicking Next button"
			end
		else
			wait 2
			# Process each Select Pay Items link 		 
			puts "Process each 'Select Pay Items' link"
			keepProcessing = true
			i = 0
			while keepProcessing
				begin
					# Increment 'i' to get next link
					i = i + 1
				
				# For debugging
				#	puts "i = " << i.to_s
				
					# Click Pay Items Link
					$driver.find_element(:xpath, "(//*[text()='Select pay items'])[1]").send_keys :page_up
					sleep 1
					$driver.find_element(:xpath, "(//*[text()='Select pay items'])[#{i}]").click
					puts "#{i}: Clicked 'Select pay items' link"
					wait 4
					
					#begin
						$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
						puts "Clicked first pay item(s)' checkbox."
					#rescue
					#	puts "Unable to click 'select all checkbox': '(.//*[@type='checkbox'])[1]'."
					#end
					wait 3
					
					$driver.find_element(:xpath, "//*[text()='Add pay item(s)']").click
					puts "Clicked 'Add pay item(s)' button"
					wait 4

				rescue
					keepProcessing = false
					wait 2
					 puts "No 'Select pay items' links left to click"
					begin
						$driver.find_element(:xpath, "//*[text()='Next']").click
						puts "Next button clicked."
						
						if $driver.find_element(:xpath, "//*[@class='validationMessage']").text.include? "Please select at least one pay item per trigger before submitting the form."
							#$driver.save_screenshot$driver.save_screenshot('ErrorNextBtn.png')
							puts "Next button error."
							exit
						end
					rescue
						puts "Next button not found."
					end
					wait 4
				end
			end
			
			if Tools.checkForScreen("Select pay items")
				puts "Error: Select pay items screen still displayed."
				exit
			end
		end
		puts "Exiting Tempo.selectPayItems"
	end








	def Tempo.enterBillingAndConcessionDetails
		puts "Entering Enter Billing And Concession Details"

		if Tools.checkForScreen("Enter billing and concession details")
		
			# Tools.checkPageForText
			
			begin
				# Click 'No third party'
				Tools.wait(:xpath, "(.//*[@type='radio'])[2]", 5)
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Third party notice clicked."
			rescue
				puts "'No third party' not found"
			end
			begin
				# Click 'No concession'
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
				puts "'No concession' clicked"
				wait 4
			rescue
				puts "'No concession' not found."
			end
			
			$driver.find_element(:xpath, "//*[text()='Next']").click
			puts "Next clicked"
			wait 2
			wait 2

			# Click Yes on dialogue
			begin
				$driver.find_element(:xpath, "(//*[text()='Yes'])[3]").click		
				puts "Yes clicked."
				wait 3
			rescue
				begin
					$driver.find_element(:xpath, "(//*[text()='Yes'])[2]").click		
					puts "Yes clicked."
					wait 3				
				rescue
					puts "Unable to find and click Yes on the confirmation dialogue, so quitting."
				    exit
				end
			end
		else
			puts "'Enter billing and concession details' not found"
		end
		puts "Exiting enterBillingAndConcessionDetails"
	end






	def Tempo.wait(*waitTime)
		# This is a custom wait method for Tempo. 
		# It waits for 'Working..' message to disappear

		# Some screens we need to 'wait' first until 'Working' is displayed.
		if waitTime[0] != nil
			sleep waitTime[0].to_i
		else
			sleep 1
		end

		found = false
		i = 1
		print "Working."
		while not found
			begin
				if $driver.find_element(:xpath, "//*[@class='appian-indicator-message']").text.include? "Working..."
					print "."
				else
					puts "."
					found = true
				end
			rescue
				print "."
			end
			i = i + 1
			sleep 1
			if i == 120
				puts "Timeout waiting."
				Tools.grabScreenshot( $filename )
				exit
				#`pkill ruby`  # Kill ruby leaving the browser open
			end
		end
	end # End Tempo.wait







	def Tempo.enterFurtherLocationDetails( easementsYesNo )
		puts "Entering enterFurtherLocationDetails"
		
		# Tools.checkPageForText
		
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
		rescue
			puts "No 'Is owner's consent required for this development application?'. Continuing anyway."
		end

		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
		rescue
			puts "No 'Is the applicant the owner of the premises?'. Continuing anyway."
		end

		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
		rescue
			puts "No 'Are there any easements over the premises?'. Continuing anyway."
		end

		wait 3
		$driver.find_element(:xpath, "//*[text()='Next']").click
		wait 2

		puts "Exiting enterFurtherLocationDetails"
	end










	def Tempo.completeApplicationChecklist( heritage, seaward, devApp, feesWaived, fees )
		puts "Entering completeApplicationChecklist"
		
		if Tools.checkForScreen("Complete application checklist")
		
			Tools.wait(:xpath, "(.//*[@type='checkbox'])[1]", 6)

			# Tools.checkPageForText
			
			# Heritage register
			if heritage != nil		
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			end

			# Seaward
			if seaward != nil		
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
			end
			# The relevant parts of Building work are completely and accurately completed
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[3]").click

			# This development application includes a material change of user, 
			# reconfiguring a lot or operational work and is accompanied by a 
			# completed DAQ Form 1 - Application details
			Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 8)
			if devApp != nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click			
				puts "radio 1 selected"
			else # No
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "radio 2 selected"
			end
			wait
			
			# This development application has addressed any applicable assessment benchmarks 
			# (e.g. relevant building codes, local government planning schemes, State Planning 
			# Policy, State Development Assessment Provisions)
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[4]").send_keys :page_up	 # Need to ensure checkbox is visible!
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[4]").send_keys :page_up			
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[4]").click
			puts "Checkbox 4 selected"
			
			# All required assessment manager fees will be provided with this development 
			# application at lodgement unless the assessment manager has agreed to waive them
			if feesWaived != nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click			
				puts "radio 3 selected"
			else # No
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
				puts "radio 4 selected"
			end

			# Relevant plans of the development are attached to this development application
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[5]").click
			puts "checkbox 5 selected"
			
			# Matters for referral will be provided to the referral entity(s) at time of lodgement 
			# to the assessment manager with the required fees
			if fees != nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click			
				puts "radio 5 selected"
			else # No
				$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
				puts "radio 6 selected"
			end

			# I am aware and accept that all information relating to this development application 
			# (other than personal applicant details) will be available for inspection and purchase 
			# unless I have notified the assessment manager of this development application otherwise
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[6]").click
			puts "checkbox6 selected"
			
			# By making this development application, I declare that all information in this development 
			# application is true and correct.
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[7]").click
			puts "checkbox 7 selected"
			wait

			begin	# Additional checkboxs/radios that may display.
				# Matters for referral identified will be provided to the referral entity(s) at time of lodgement to the assessment manager with the required fees
				$driver.find_element(:xpath, "(.//*[@type='radio'])[7]").click
				puts "radio 7 selected"
				# By making this development application, I declare that all information in this development application is true and correct
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[8]").click
				puts "checkbox 8 selected"
				wait
			rescue
			end

			$driver.find_element(:xpath, "//*[text()='Next']").click
			puts "Next clicked"
			wait 4
		else
			puts "?????????????????????????????????????????????????????"
			puts "'Confirm application checklist' screen not displayed, continuing."
			puts "?????????????????????????????????????????????????????"
		end
		
		puts "Exiting completeApplicationChecklist"
	end



	def Tempo.enterFurtherAssessmentDetails( preLimApproval, furthInfo, supercededInfo, longService )
		puts "Entering Enter Further Assessment Details"

		wait 2
		if Tools.checkForScreen("Enter further assessment details")
			
			# Tools.checkPageForText
			
			if preLimApproval != nil 
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "PreLimApproval set"
			end		
		
			if furthInfo != nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
				puts "Further Information set"
			end		

			if longService != nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
				puts "Long Service set"
			end		
			wait 3
			$driver.find_element(:xpath, "//*[text()='Next']").click
			wait 4
		else
			puts "Enter Further Assessment Details screen not found, skipping"
		end
		puts "Exiting enterFurtherAssessmentDetails"
	end







	def Tempo.enterOwnerAndBuilderDetails
		puts "Entering enterOwnerAndBuilderDetails"

		wait 2
		if Tools.checkForScreen("Enter owner and builder details")

			# Tools.checkPageForText
			
			# 'Please tick if applicant is also the owner and proceed to question 10'
			$driver.find_element(:xpath, ".//*[@type='checkbox']").click
			wait 8

			# Can add more functionality if required...

			# Click Next
			puts "Click Next"
			Tools.wait(:xpath, "//*[text()='Next']", 10)
			$driver.find_element(:xpath, "//*[text()='Next']").click
			wait 6

			if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Enter owner and builder details"
				# Click Next again
				puts "Click Next"
				Tools.wait(:xpath, "//*[text()='Next']", 10)
				$driver.find_element(:xpath, "//*[text()='Next']").click
				wait 6
			end
		else
			puts "Enter owner and builder details screen not found, skipping"
		end
		puts "Exiting enterOwnerAndBuilderDetails"
	end







	def Tempo.enterNatureAndValue( value, insurancePaid )
		puts "Entering enterNatureAndValue"

		wait 2
		if Tools.checkForScreen("Enter nature and value")		
			
			# Tools.checkPageForText
			
			#wait 4
			#Tools.wait(:xpath, ".//*[@type='text']", 6)

			# Has Queensland Home Warranty Scheme insurance been paid?
			if insurancePaid != nil
				if insurancePaid.downcase == "yes" # Yes
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					puts "Has Queensland Home Warranty Scheme insurance been paid? = YES"
				else # No
					$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
					puts "Has Queensland Home Warranty Scheme insurance been paid? = NO"
				end
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Has Queensland Home Warranty Scheme insurance been paid? = NO"
			end

			# Enter proposed value
			if value == nil
				value = "100000"
			end
			
			begin		
				$driver.find_element(:xpath, ".//*[@type='text']").send_keys value
			rescue
				puts "Proposed value not found or accepted."
			end
			$driver.find_element(:xpath, ".//*[@type='text']").send_keys :tab
			wait 3

			# Click Next
			$driver.find_element(:xpath, "//*[text()='Next']").click
			wait 4
		else
			puts "Enter nature and value screen not found, skipping"
		end
		puts "Exiting enterNatureAndValue"
	end







	def Tempo.application(appType, choiceType) 
		puts "Entering Applications"

		# Click Actions
		Tools.click(:xpath, "//a[contains(.,'Actions')]")
		wait 2

		## Tools.checkPageForText
		
		$driver.switch_to.default_content()

		# Open Applications screen.
		applicationScreen(appType, choiceType)

		puts "Exiting Applications"
	end








	def Tempo.findUser( user )
		puts "Entering findUser"
			Tools.click(:xpath, "//a[contains(.,'Records')]")
			$driver.switch_to.default_content()

			Tools.click(:link, "Users")
			
			# Tools.checkPageForText
			
			if user != nil
				wait 3
				puts "Search for '#{user}'"
				# Search for the User
				Tools.wait(:xpath, "//*[@placeholder='Search Users']", 5)
				$driver.find_element(:xpath, "//*[@placeholder='Search Users']").send_keys user
				$driver.find_element(:xpath, "//*[@placeholder='Search Users']").send_keys :enter
				wait 2
				# Check for search results
				if $driver.find_element(:xpath, "//*[@class='gwt-InlineHTML']").text.include? "No records available"
					puts "'#{user}' not found."
				else
					$driver.find_element(:xpath, "//a[contains(.,'#{user}')]").click
					puts "Found '#{user}' and clicked."					
				end
			else
				puts "No User supplied so can't continue."
				exit
			end
		puts "Exiting findUser"
	end







	def Tempo.reviewResponseNotice( approver )
		puts "Entering reviewResponseNotice"

		if $appID == nil
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			puts "Application ID not found, so quitting."
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			exit
		end

		# Attempt to open the application
		puts "Attempt to open application: #{$appID}"
		begin
			wait
			openTask("Review response notice - " + $appID)
			wait			
			puts "Review response notice - " + $appID + " clicked."
		rescue
			puts "Unable to find application '#{$appID}'."
			exit			
		end
		wait 2

		begin
			Tools.wait(:link, "accept this task", 15)
			$driver.find_element(:link, "accept this task").click
			puts "accept this task clicked"
		rescue
			puts "Link of 'accept this task' not found. Try continuing."
		end
		wait 5
					
		begin			
			# Select 'Submit notice for approval' radio button
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			puts "'Submit notice for approval' radio button selected"
			wait
		rescue
			puts "'Submit notice for approval' radio button not found."
		end

		# Select Approver
		if approver == nil
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:index, 2)
			puts "No Approver supplied so using first one in drop down"
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'gwt-ListBox')]")).select_by(:text, approver)
			puts "Entered #{approver}."
		end

		puts "Click Submit"
		submitBtn = "//*[text()='Submit']"
		$driver.find_element(:xpath, submitBtn).click
		wait 4

		puts "Exiting reviewResponseNotice"		
	end






	def Tempo.paymentPending(appID)
		puts "Entering paymentPending"
		if appID == nil
			if $appID == nil
				puts "No application ID supplied, quitting."
				exit
			else
				appID = $appID
			end
		end
		payPend = "#{appID} - Payment pending"
		payPrepare = "#{appID} - Prepare"
		payPreparation = "#{appID} - Preparation"
		
		apps = appID.split(/\D/) # Extract Numbers into array
		appID = "#{apps[0]}-#{apps[1]}" # #{apps[2]}"
		appID = appID.strip
		
		puts "Shortened appID: '#{appID}'."

		# Click Records
		wait 2
		puts "Click Records"
		Tools.click(:xpath, "//a[contains(.,'Records')]")
		wait 4

		# Development applications and requests
		puts "Click Development applications and requests"
		Tools.click(:xpath, "//a[contains(.,'Development applications and requests')]")
		wait 2
		
		# Tools.checkPageForText
		
		# Search for the application so we can open it
		searchField = "//*[@placeholder='Search Development applications and requests']"
		Tools.wait(:xpath, searchField, 5)
		$driver.find_element(:xpath, searchField).send_keys appID[0..9] # Supply only the numeric part of the ID
		$driver.find_element(:xpath, searchField).send_keys :enter
		wait 6

		# Attempt to open the application
		puts "Attempt to open application: #{appID}"
		begin
			$driver.find_element(:xpath, "//a[contains(.,'#{appID}')]").click
			puts "#{appID} clicked."
		rescue
			puts "Unable to find application '#{appID}'."
			exit			
		end

=begin
		puts "Attempt to open application: #{payPend}"
		begin
			$driver.find_element(:xpath, "//a[contains(.,'#{payPend}')]").click
			puts "#{payPend} click."
		rescue
			begin
				puts "Attempt to open application: #{payPrepare}"
				begin
					$driver.find_element(:xpath, "//a[contains(.,'#{payPrepare}')]").click
					puts "#{payPrepare} click."
				rescue
					puts "Attempt to open application: #{payPreparation}"
					begin
						$driver.find_element(:xpath, "//a[contains(.,'#{payPreparation}')]").click
						puts "#{payPreparation} click."
					rescue
						puts "Unable to find application '#{appID}'."
						exit
					end
				end
			end
=end

		wait 2

		puts "Exiting paymentPending"
	end




	def Tempo.openApplication(*appID)
		puts "Entering Tempo.open Application"
		
		if $appID == nil
			if appID[0] == nil
				puts "No application ID provided, quitting"
				exit
			else
				$appID = appID[0]
			end
		end

		begin
			$appID = $appID.lstrip.rstrip # Remove leading and trailing whitespace
		rescue
			puts "Application ID: '#{$appID}'"
		end
		
		puts "Now try to open application ID: "  + $appID
		wait 2
		
		i = 1
		foundTempoHomePage = false
		if not foundTempoHomePage
			if $driver.find_element(:xpath, "(//*[@class='gwt-Anchor pull-down-toggle'])[1]")
				foundTempoHomePage = true
				sleep 1
				i = i + 1
				if 1 == 30
					puts "Records menu option not found, quitting"
					exit
				end
			else
				puts "Records menu option not found, quitting"
				exit
			end
		end
		
		# Tools.checkPageForText
		
		puts "Click Records"
		Tools.click(:xpath, "//a[contains(.,'Records')]", 6)		
		puts "Clicked Records"
		wait

		Tools.click(:xpath, "//a[contains(.,'Development applications and requests')]", 6)		
		puts "Clicked Development applications and requests"
		wait
		
		# Tools.checkPageForText
		notFound = true
		i = 0
		while notFound
			begin
				puts "Waiting for the Search field to search for " << $appID
				# New Object ID
				$driver.find_element(:xpath, "//*[starts-with(@class,'GKACFQNDKS')]").send_keys $appID
				$driver.find_element(:xpath, "//*[starts-with(@class,'GKACFQNDKS')]").send_keys :enter
				notFound = false
			rescue
				begin
					# Old Object ID
					$driver.find_element(:xpath, "//*[starts-with(@class,'gwt-TextBox')]").send_keys $appID
					$driver.find_element(:xpath, "//*[starts-with(@class,'gwt-TextBox')]").send_keys :enter
					notFound = false
				rescue
					i = i + 1
					if i == 10
						puts "Search field not found. Quitting"
						exit
					else
						puts "Search field not found."
					end
				end
			end
		end
		wait 4			
		
		begin
			$driver.find_element(:xpath, "//a[contains(.,'#{$appID}')]").click
			puts "'#{$appID}' found"
			wait
			return true
		rescue
			puts "'#{$appID}' not found." 
			return false
		end

		puts "Exiting openApplication"
	end
	
	
	
	
	





	def Tempo.applicationScreen(choice, choiceType)
		puts "Entering Applications screen"	

		if ( choice or choiceType ) == nil
			puts "No valid application type was provided, quitting"
			exit
		else
			choice = choice.to_s
			choiceType = choiceType.to_s
			puts "Choice: " + choice
			puts "Choice Type: " + choiceType
		end
		wait 4
		
		## Tools.checkPageForText
		
		begin
			if $driver.find_element(:xpath, "//*[contains(text(),'Refer application')]")
				case choice
				when "Refer application to DILGP (SARA)", "Refer_application"  # 1) SRA
					$driver.find_element(:xpath, "//*[contains(text(),'Refer application')]").click
					puts "Refer application to DILGP (SARA) clicked"
					wait
					case choiceType
					when "SPA - Sustainable Planning Act 2009"
						puts "SPA - Sustainable Planning Act 2009"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					when "P&D - Planning and Development Act 2016", "P&D - Planning and Development Act 2015", "P&D"
						puts "P&D - Planning and Development Act 2016"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
					else
						puts "Unknown 'Refer' Application Type of: #{choiceType}"
						exit
					end
					wait
					
				when "Complete forms online", "Complete_ forms_online" # 2) SDA
					$driver.find_element(:xpath, "//*[contains(text(),'Complete forms online')]").click
					puts "Complete forms online clicked"
					wait
					case choiceType
					when "Prepare a development application"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click	
					when "Prepare a development application solely for building works. DILGP (SARA) will be a referral agency"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
					when "Prepare a development application solely for building works. DILGP (SARA) will not be a referral agency"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
						wait
					else
						puts "Unknown 'Complete forms online' Application Type of: #{choiceType}"
						exit
					end
					wait
					
				when "Upload completed forms", "Commence_preparation_Upload_completed_forms" # 3) SDA
					begin
						$driver.find_element(:xpath, "//*[contains(text(),'Upload completed forms')]").click				
					rescue
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click	
					end
					puts "Upload completed forms clicked"
					wait
					begin
						case choiceType
						when "SPA - Sustainable Planning Act 2009"
							puts "SPA - Sustainable Planning Act 2009"
							$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
						when "P&D - Planning and Development Act 2016", "P&D - Planning and Development Act 2015", "P&D"
							puts "P&D - Planning and Development Act 2016"
							$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
						else
							puts "Unknown 'Refer' Application Type of: #{choiceType}"
							exit
						end
					rescue
					end
					# Enter location Details screen displayed.
					
				when "Lodge a request for pre-application advice", "Preapplication_advice" # 4) SPL Prelodgement
					begin
						$driver.find_element(:xpath, "//*[contains(text(),'Lodge a request for pre-application advice')]").click				
					rescue
						$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click	
					end
					puts "Lodge a request for pre-application advice clicked"
					wait
					# Enter location Details screen displayed.
					
				when "Lodge an early referral request",  "Early_referral_request" # 5) SER Early Referral
					begin
						$driver.find_element(:xpath, "//*[contains(text(),'Lodge an early referral request')]").click # New code
					rescue
						$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click # old code
					end
					puts "Lodge an early referral request clicked"
					wait
					# Enter location Details screen displayed.
					
				when "Make a post-approval request", "Request_post_approval_changes" # 6) SPD post Approval - 
					begin
						$driver.find_element(:xpath, "//*[contains(text(),'Make a post-approval request')]").click				
						newCode = true
					rescue
						$driver.find_element(:xpath, "(.//*[@type='radio'])[7]").click # old code
						newCode = false
					end
					puts "Make a post-approval request clicked"
					wait
					
					if newCode == true
						case choiceType
						when "Yes - I want to change a DILGP (SARA) development approval", "Yes - I want to change a SARA development approval"
							$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
							puts "Yes - I want to change a DILGP (SARA) development approval selected"
							
						when "No - I want to change a pre-DILGP (SARA) development approval", "No - I want to change a pre-SARA development approval"
							$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
							puts "No - I want to change a pre-DILGP (SARA) development approval selected"
							
						else
							puts "Unknown 'Post-approval change request' application Type of: #{choiceType}"
							$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
							puts "Yes - I want to change a DILGP (SARA) development approval selected"
						end
						wait
						begin
							# Select 'Planning Act 2016'
							$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
							puts "Planning Act 2016 selected."
							wait
						rescue
							puts "'Planning Act 2016' not found, continuing anyway."
						end
					end
					
				when "General (including missed referral)", "Missed_referral" # SMR Missed Referral
					begin
						$driver.find_element(:xpath, "//*[contains(text(),'General (including missed referral)')]").click				
					rescue
						$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
					end
					puts "General (including missed referral) clicked"
				else
					puts "Unknown '#{choice}' provided, quitting"
					exit
				end

				begin
					wait 2
					$driver.find_element(:xpath,"//*[text()='Submit']").click
					puts "Submit clicked"
					wait 4
				rescue
					puts "Submit NOT clicked"
				end
			else
				puts "'Refer application to DILGP (SARA)' link not found."
			end
		rescue
			puts "???????????????????????????"
			puts "Error: Has the Application screen changed?"
			puts "???????????????????????????"
			exit
		end
=begin		
		else
			puts "Old code found"
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
			if Tools.checkForScreen("Applications")
				puts "Applications screen found"
			else
				puts "Applications screen NOT found"
			end
			puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
					
			if $driver.page_source.include? "Missed referral"
				puts "Missed referral found"
				missedReferralDisplayed = true
			else
				puts "Missed referral NOT found"
				missedReferralDisplayed = false
			end
			
			if title == "Applications"
				case choice
				when '1', '1.0', "Complete_ forms_online"
					puts "Complete DAQ forms online"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					wait 2
					case choiceType
					when '1', '1.0', "Building work only with SARA referral"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click	
						wait
					when '2', '2.0', "Building work only with no SARA referral"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
						wait
					when '3', '3.0', "Any other nature of development"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
						wait
					else
						puts "Unknown 'Complete DAQ forms online' Application Type of: #{choiceType}"
						exit
					end
					
				when '2', '2.0', "Commence_preparation_Upload_completed_forms"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
					wait 2
					case choiceType
					when '1', '1.0', "SPA - Sustainable Planning Act 2009"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
						wait
					when '2', '2.0', "P&D - Planning and Development Act 2016"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
						wait
					else
						puts "Unknown 'Commence preparation - Upload completed forms' application type of: #{choiceType}"
						exit
					end
					
				when '3', '3.0', "Refer_application"
					puts "'refer' application'"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
					wait
					case choiceType
					when '1', '1.0', "SPA - Sustainable Planning Act 2009"
						puts "SPA - Sustainable Planning Act 2009"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
						wait
					when '2', '2.0',"P&D - Planning and Development Act 2016"
						puts "P&D - Planning and Development Act 2016"
						$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
						wait
					else
						puts "Unknown 'Refer' Application Type of: #{choiceType}"
						exit
					end

				when '4', '4.0', "Early_referral_request"
					puts "Early referral request application"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
					wait
					
				when '5', '5.0', "Missed_referral"
					puts "Missed referral"
					begin
						$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
					rescue
						puts "#############################################"
						puts "Missed Referral not found. Log in as a valid user to see and select it."
						puts "#############################################"
						exit
					end
					wait
						
				when '6',  '6.0', "Preapplication_advice"
					puts "Preapplication_advice"
					if missedReferralDisplayed 
						$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
					else
						$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
					end
					wait
						
				when '7',  '7.0', "Request_post_approval_changes"
					puts "Request post approval changes"
					if missedReferralDisplayed 
						$driver.find_element(:xpath, "(.//*[@type='radio'])[7]").click
					else
						$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
					end
					wait
									
				else
					puts "Unknown Choice of: #{choice}"
					exit
				end
				wait 2
				puts "Click Submit"
				submitBtn = "//*[text()='Submit']"
				Tools.wait(:xpath, submitBtn, 4)
				$driver.find_element(:xpath, submitBtn).click
				wait 4
			else
				puts "???????????????????????????????????????????????????????????"
				puts "No 'Applications' screen found. Therefore old functionality is in use."
				puts "???????????????????????????????????????????????????????????"
				exit
			end
=end
		puts "Exiting Applications screen"	
	end






	def Tempo.getNewApplicationID
		puts "Getting new Application ID"
		# Get and save the new Application ID
		
		Tools.wait(:xpath, "//*[starts-with(@class,'appian-form-title')]", 10)
		appID = $driver.find_element(:xpath, "//*[starts-with(@class,'appian-form-title')]").text

		# Remove unwanted text.
		appID = appID.sub("Lot search results ", "")   # Single lot
		appID = appID.sub("Search single lot ", "")   # Single lot 
		appID = appID.sub("Search multiple lots ", "") # Multiple lots
		appID = appID.sub("Select pay items ", "")
		appID = appID.sub("Select post approval request type ","")
		appID = appID.sub("Enter location details ","")
		appID = appID.sub("Initiating referral application ","")
		appID = appID.lstrip # Remove leading whitespace
		$appID = appID.rstrip # Remove trailing whitespace

		# Get the short App ID for later use
		Tools.getShortAppID

		puts "*****************************************"
		puts "*****************************************"
		puts "New Application ID: '#{$appID}'"
		puts "*****************************************"
		puts "*****************************************"
		return appID
	end









	def Tempo.addLot(validate,*lot)
		puts "Entering addLots"

		wait 5

		if not Tools.checkForScreen("Enter location details")
			puts "?????????????????????????????????????????????????"
			puts "Enter location details not found, quitting"
			puts "?????????????????????????????????????????????????"
			exit
		end
		
		if lot[0] == nil
			puts "No lot supplied, unable to continue, quitting."
			$error = "No lot supplied, unable to continue, quitting."
			exit
		end

		begin
			if lot[1] != nil
				multipleLots = true
				puts "Multiple lots supplied."
			else
				multipleLots = false
				puts "Single lot supplied."
			end
		rescue
			multipleLots = false
			puts "Single lot supplied."
		end

		if multipleLots 
			$driver.find_element(:xpath, "//*[text()='Add multiple lots']").click
			puts "Clicked 'Add Multiple Lots'"
			xpath = "//*[contains(@class, 'aui-TextAreaInput printable')]"
			Tools.wait(:xpath, xpath, 10) 
			i = 0
			while lot[i] != nil
				puts "Entering lots: " +  lot[i]
				$driver.find_element(:xpath, xpath).send_keys lot[i]
				$driver.find_element(:xpath, xpath).send_keys :return
				i = i + 1
			end
			wait 3
		else
			$driver.find_element(:xpath, "//*[text()='Add lot']").click
			puts "Clicked 'Add lot'"
			wait 2
			puts "Using lot " << lot[0]
			$driver.find_element(:xpath, "//*[starts-with(@class,'aui-TextInput')]").send_keys lot[0]
		end
		wait 2

		# Get the application ID for later user
		$appID = Tempo.getNewApplicationID
		
		# Click Search
		puts "Click Search"
		searchBtn = "//*[text()='Search']"
		Tools.wait(:xpath, searchBtn, 10)
		$driver.find_element(:xpath, searchBtn).click
		wait 7

		begin
			errorMsg = "//*[text()='Plan type and plan number is required.']"
			while $driver.find_element(:xpath, errorMsg)
				puts "************************"
				$driver.find_element(:xpath, planTypeField).clear
				$driver.find_element(:xpath, lotNumField).clear
				puts "Fields cleared"
				$driver.find_element(:xpath, planTypeField).send_keys planType
				$driver.find_element(:xpath, lotNumField).send_keys lotNo
				$driver.find_element(:xpath, lotNumField).send_keys :tab	
				puts "Fields completed"
				# Click Search
				$driver.find_element(:xpath, searchBtn).click
				puts "Search clicked"
				puts "************************"
				wait 5
			end
		rescue
		end

		# Now to wait for Processing bar to vanish
		processFinished = false
		i = 0
		while 1
			begin
				$driver.find_element(:xpath, "(.//*[starts-with(@class, 'appian_form_readonly')])[1]")
				puts "Found lot plan grid"
				break
			rescue
				i = i + 1
				puts "Click Refresh"
				begin
					$driver.find_element(:xpath, "//*[text()='Refresh']").click
					puts "Refresh clicked."
					processFinished = true
				rescue
					puts "Refresh button not found."
				end
				if i == 10
					puts "Can't find lot plan grid. Quitting."
					exit
				end
				wait 2
			end
		end
		
		i = 1		
		k = 2
		# Remove extra & invalid lots
		dupes = true
		# Find and remove duplicate row
		$duplicateLot = false
		while dupes == true
			begin
				lotPlanOrgx =  "(.//*[starts-with(@class, 'appian_form_readonly')])[#{i}]"
				lotPlanNextx =  "(.//*[starts-with(@class, 'appian_form_readonly')])[#{i+3}]"
				
				# Lot plans
				lotPlanOrg = $driver.find_element(:xpath, lotPlanOrgx).text
				lotPlanNext = $driver.find_element(:xpath, lotPlanNextx).text
				puts "------------Look for duplicates------------"
				puts "Current lot: " + lotPlanOrg
				puts "Next lot: " + lotPlanNext
				puts "---------------------------------------------"

				if lotPlanOrg == lotPlanNext # We have a duplicate row
					$duplicateLot = true
			
					puts lotPlanNext + " = " + lotPlanOrg
					firstChkBox = "(//*[@type='checkbox'])[#{k}]"
					$driver.find_element(:xpath, firstChkBox).click
					
					puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
					puts "Found a duplicate lot entry: " + lotPlanNext
					puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
					wait
				else
					puts lotPlanOrg + " <> " + lotPlanNext
				end
			rescue
				#puts "No more rows found."
				#dupes = false
			end
			puts "Processing row #{i}"
			
	
			# Tick Site of development checkbox
			begin
				puts "Select Site of development checkbox/s." 
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+3}]").click 
				puts "S 1"
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+7}]").click 
				puts "S 2"
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+11}]").click 
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+15}]").click 
			rescue
				# puts "No more rows found."
				# break
			end

			# Tick Skip Validation checkbox if enabled
			begin
				puts "Select Skip Validation checkbox/s"
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+2}]").click 
				puts "V 1"
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+6}]").click 
				puts "V 2"
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+10}]").click 
				$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i+14}]").click 
			rescue
			end
			
			i = i + 1
			k = k + 4
								
			puts $duplicateLot
			if $duplicateLot == true
					begin
						$driver.find_element(:xpath, "//*[text()='Remove']").click
						puts "Clicked Remove."
						wait
					rescue
						puts "Unable to click Remove button."
					end
			else
				puts "No duplicate lots found"
				break
			end
		end
		
		if processFinished == false # Old code requires this
			begin
				# Now to wait for Processing to equal 100%
				puts "Now to wait for Processing to equal 100%"
				counter = 0
				ready = false
				while not ready
					begin
						if $driver.find_element(:xpath, "//*[text()='100%']")
							ready = true
							puts "Progress = 100%"
						end
					rescue
						# Click Refresh
						puts "Clicking Refresh"
						refreshBtn = "//*[text()='Refresh']"
						$driver.find_element(:xpath, refreshBtn).click
						wait 2
						puts "Waited #{counter} seconds."
					end
					
					counter = counter + 1
					if counter == 100
						puts "Processing still not complete."
						exit
					end
					sleep 2
				end
				wait 3
			rescue
			
			end
		end
		
		# Click Submit
		submitBtn = "//*[text()='Submit']"
		Tools.wait(:xpath, submitBtn, 10)
		$driver.find_element(:xpath, submitBtn).click
		puts "Submit clicked"
		wait 2

		
		# Click Next
		nextBtn = "//*[text()='Next']"
		Tools.wait(:xpath, nextBtn, 10)
		$driver.find_element(:xpath, nextBtn).click
		puts "Next clicked"
		wait 2
		
		puts "Exiting addLots"
	end







	def Tempo.enterApplicantDetails(actingForApplicant, email)
		puts "Entering enterApplicantDetails"

		# Check for validEmail
		if not email["@"]
			puts "Invalid email (#{email}) provided, quitting"
			exit
		end
		
		if Tools.checkForScreen("Enter applicant details", 2)
		
			Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 2)
			#	 Applicant type
			if actingForApplicant == nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				puts "Individual selected"
				radio1 = true
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "Organisation selected"
				radio1 = false
			end
			wait 2

			begin
				# Select User
				wait
				selectLnk = "(//*[contains(text(),'Select')])[3]"
				$driver.find_element(:xpath, selectLnk).click						
				wait 2
			rescue
				puts "Select not found"
			end
			
			Tools.wait(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[4]", 6)		

			# Email or first name
			if email == nil
				email = "nicholasname@yahoo.com"
			end	
			
			emailFld = "(.//*[starts-with(@class, 'aui-TextInput')])[1]"
			begin			
				emailTXT = $driver.find_element(:xpath, emailFld).text
				puts "Found " + emailTXT
			rescue
				puts "Issue reading field, quitting"
				exit
			end
			
			begin
				if emailTXT == ""
					puts "Email required"
					$driver.find_element(:xpath, emailFld).send_keys email.sub(" ", "")
					puts "Data entered: '#{email}'."
					wait 2
				else
					puts "Existing text found: " + emailTXT
				end
			rescue
				puts "Problem entering text"
			end

			# Click Search button
			begin
				wait
				searchBtn = "//*[text()='Search']"
				$driver.find_element(:xpath, searchBtn).click		
				puts "Search clicked."
				wait 2
			rescue
				puts "Search not found to click"
			end

			begin
				# Select User
				wait
				selectLnk = "(//*[contains(text(),'Select')])[2]"
				$driver.find_element(:xpath, selectLnk).click						
				wait 2
			rescue
				puts "User not set"
			end

			wait
			# Set preferred means of contact or country
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:index, 2)
				puts "Preferred means of contact or country set successfully."
				wait 2
			rescue
				puts "Can't set 'Preferred means of contact'. Must be read only or non-existent."
			end

			# Set title
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:index, 2)
				puts "Set Title successfully."
				wait 2
			rescue
				puts "Can't set 'Title'. Must be read only or non-existent."
			end

			begin 
				puts "Try to click '► Select'"
				$driver.find_element(:xpath,"(//*[text()='► Select'])[1]").click
				puts "Select clicked."
				wait 2
			rescue
				puts "Select NOT clicked."
			
			end			

			# Phone number
			begin
				phoneTXT = $driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[5]").text
				

				if phoneTXT == ""
					$driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[5]").send_keys "0422687687"
					puts "Phone number set"
					wait
				else
					puts "Phone number found: " + phoneTXT
				end
			rescue
				puts "Problem adding phone number."
			end

			# Phone
			begin
				phoneTXT = $driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[11]").text
				# phoneTXT = phone.attribute("value")

				if phoneTXT == ""
					$driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[11]").send_keys "0422666776"
					puts "Phone number set"
					wait
				else
					puts "Phone found: " + phoneTXT
				end
			rescue
				puts "Problem adding phone."
			end
			
			puts "Click Next"
			nextBtn = "//*[text()='Next']"
			Tools.wait(:xpath, nextBtn, 10)
			$driver.find_element(:xpath, nextBtn).click
			wait 2

			# If Next didn't work, try again.
			i = 1
			while $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Enter applicant details"
				# Click Next again
				puts "Click Next again..."
				$driver.find_element(:xpath, nextBtn).click
				wait 4
				i = i + 1
				if i == 10
					puts "Problem found trying to Submit, quitting"
					exit
				end
			end
		else
			puts "Unable to find 'Enter applicant details' screen."		
		end
		puts "Exiting enterApplicantDetails"
	end






	def Tempo.enterAssessmentDetails(desc, assManager, natureOfDev, approvalType, assLevel, notificationReq, assManagerEmail, *triggers)
		puts "Entering enterAssessmentDetails"
		
		i = 0
		if triggers[i] == nil
			puts "No triggers supplied, quitting"
			exit
		end
		
		if not Tools.checkForScreen("Enter assessment details", 2)
			puts "?????????????????????????????????????????????????"
			puts "Unable to find 'Enter assessment details' screen, quitting."
			puts "?????????????????????????????????????????????????"
			exit
		end
		
		## Tools.checkPageForText
		
		assManDrpDn = "(.//*[starts-with(@class,'gwt-ListBox')])[1]"
		Tools.wait(:xpath, assManDrpDn, 10)
		
		# Select the Assessment Manager		
		if assManager != nil
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, assManDrpDn)).select_by(:text, assManager)
				puts "Selected " + assManager + "."
			rescue
				puts "Unable to select " + assManager + ", quitting."
				exit
			end
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, assManDrpDn)).select_by(:index, 1)
			puts "No assessment manager provided, using default."
		end
		wait
		
		# Select the Assessment Manager email
		if assManagerEmail != nil
			begin
				$driver.find_element(:xpath, ".//*[@class='aui-TextInput']").send_keys assManagerEmail
			rescue
				puts "Unable to enter email address"
			end
		else
			puts "No email supplied."
		end
		wait

		# Check for the question 'Has the assessment manager provided pre-lodgement advice for this development application?'
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
		rescue
			puts "No question found: 'Has the assessment manager provided pre-lodgement advice for this development application?', continuing anyway."
		end

		# Nature Of Development
		begin
			if natureOfDev != nil 
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, natureOfDev)
			else
				begin
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:index, 2)		
				rescue

				end
			end
		rescue
			puts "Nature of Development was read only"
		end
		wait

		# Approval Type
		if approvalType != nil 
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[3]")).select_by(:text, approvalType)
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[3]")).select_by(:index, 2)		
		end
		wait

		# Level of Assessment 
		if assLevel != nil 
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[4]")).select_by(:text, assLevel)
		else
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[4]")).select_by(:index, 1)		
		end
		wait

		# Notification Required
		begin
			if notificationReq != nil 
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[5]")).select_by(:text, notifcationReq)
			else
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[starts-with(@class,'gwt-ListBox')])[5]")).select_by(:index, 2)		
			end
		rescue
			
		end
		wait
		
		# Enter a description
		puts "Enter a description"
		description = "//*[contains(@class, 'aui-TextAreaInput printable')]"
		if desc != nil
			$driver.find_element(:xpath, description).send_keys desc
		else
			$driver.find_element(:xpath, description).send_keys "Just some default text"
		end
		$driver.find_element(:xpath, description).send_keys :enter
		wait

		# Click Next
		puts "Click Next"
		nextBtn = "//*[text()='Next']"
		Tools.wait(:xpath, nextBtn, 6)
		$driver.find_element(:xpath, nextBtn).click
		wait 4

		# Assessment triggers
		begin
			if $driver.find_element(:xpath, "//*[contains(text(), 'Assessment triggers')]")
				puts "Assessment triggers found."
							
				begin
					while triggers[i] != nil
						begin
							txtInput = "(.//*[starts-with(@class,'aui-TextInput')])[1]"
							$driver.find_element(:xpath, txtInput).clear
							sleep 1
							$driver.find_element(:xpath, txtInput).send_keys triggers[i]
							sleep 1
							puts "Trigger entered"
						rescue
							puts "Issue finding text box"
							exit
						end

						# Turn off filtered Search
						# This dropdown can change position so we'll try a couple of places
						begin
							listBox = "(.//*[starts-with(@class,'gwt-ListBox')])[6]"
							Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, "All")
							puts "Turned off filtered Search"
						rescue
							begin
								listBox = "(.//*[starts-with(@class,'gwt-ListBox')])[7]"
								Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, "All")
								puts "Turned off filtered Search"
							rescue
								begin
									listBox = "(.//*[starts-with(@class,'gwt-ListBox')])[8]"
									Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, "All")
									puts "Turned off filtered Search"
								rescue
									puts "Issue finding Search filter"
								end
							end
						end
						wait

						begin
							# Click Search
							puts "Click Search"
							searchBtn = "//*[text()='Search']"
							$driver.find_element(:xpath, searchBtn).click
							wait 2
							wait 2
						rescue
							puts "Issue finding Search"
							exit
						end
						
						begin
							$driver.find_element(:xpath, "(//*[@type='checkbox'])[2]").click
							puts "Trigger " + triggers[i] + " selected."
							wait 4
						rescue
							puts "Trigger " + triggers[i] + " not found, quitting"
							exit
						end
						
						begin
							# Click Add triggers
							puts "Click Add triggers"
							$driver.find_element(:xpath, "//*[text()='Add triggers']").click
						rescue
							puts "Issue finding Add Trigger"
							exit
						end
						wait 2
						i = i + 1
					end
					wait 2
			
					begin
						# Click Next
						puts "Click Next"
						nextBtn = "//*[text()='Next']"
						$driver.find_element(:xpath, nextBtn).click
					rescue
						puts "Issue finding Next"
						exit
					end
					wait 4
				rescue
					puts "No triggers supplied, quitting."
					exit
				end
			end
		rescue
			puts "Assessment triggers not found."
		end
		
		if Tools.checkForScreen("Enter assessment details", 2)
			puts "?????????????? #{$appID} ?????????????????????????"
			puts "'Enter assessment details' screen still visible so their must be an issue, quitting."
			puts "?????????????? #{$appID} ?????????????????????????"
			exit
		end
		
		puts "Exiting enterAssessmentDetails"
	end




	
	
	def Tempo.selectReferralTriggers(*triggers)
		puts "Entering Select Referral Triggers / Referral agency details"

		if not Tools.checkForScreen("Referral agency details", 2)
			if not Tools.clickLinkIfExists("Referral agency details")			
				puts "Unable to find 'Select referral trigger' or 'Referral agency details' screens, quitting."
				exit
			end
		end
		wait 1
		
		begin
			# Did you want to opt-out of the information request?*
			$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click		
		rescue
			puts "'Did you want to opt-out of the information request?' not found."
		end
		wait 
			
		begin
			# Did you obtain an early referral response from DILGP (SARA) for this application?*
#			$driver.find_element(:xpath, "//*[text()='Next']").send_keys :page_up
#			$driver.find_element(:xpath, "//*[text()='Next']").send_keys :page_up
			$driver.find_element(:xpath, "(.//*[starts-with(@name, 'gwt-uid')])[2]").click
			puts "'Did you obtain an early referral response from DILGP (SARA) for this application?' was clicked"
		rescue
			puts "'Did you obtain an early referral response from DILGP (SARA) for this application?' not found."
		end
		wait 
		
		found = false
		i = 0
		if triggers[i] != nil
			puts triggers[i]
	
			# Set filter to All
			listBox = "(//*[contains(@class, 'gwt-ListBox')])[2]"
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, "All")
			puts "Set filter search to All"			

			while triggers[i] != nil
				begin
					triggers[i] = triggers[i].downcase
				rescue
					puts "Can't downcase trigger..."
				end
				
				triggerFld = "(//*[starts-with(@class,'aui-TextInput')])[1]"
				$driver.find_element(:xpath, triggerFld).clear
				$driver.find_element(:xpath, triggerFld).send_keys triggers[i]
				puts "Trigger entered."
				wait 2
				
				# Click Search
				$driver.find_element(:xpath, "//*[text()='Search']").click
				puts "Clicked Search."
				wait 2
				begin
					found = false
					j = 1
					while not found
						triggercol = "(//*[starts-with(@__gwt_cell,'cell-gwt-uid')])[2]"
						trigger = $driver.find_element(:xpath, triggercol).text.downcase
						puts "Using trigger: " << trigger
						
						begin
							puts "Does " + trigger + " = " + triggers[i]
							if trigger == triggers[i]
								puts trigger + " = " + triggers[i]
								puts "Attempt to click (//*[@type='checkbox'])[#{j+1}]"
								$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{j+1}]").click
								puts "Clicked (//*[@type='checkbox'])[#{j+1}]"
								found = true
								puts "Trigger " + triggers[i]
							else
								puts trigger + " not equal to " + triggers[i]
								j = j + 1
							end
						rescue
							puts "Problem clicking Trigger: " + triggers[i]
						end
					end			

					wait 2
				rescue
					puts "Trigger " + triggers[i] + " not found, quitting"
					exit
				end
				
				# Click Add triggers
				$driver.find_element(:xpath, "//*[text()='Add triggers']").click
				puts "Clicked Add triggers"
				wait 4
				i = i + 1
			end
		else
			# No triggers supplied so skip this page by ticking checkbox: 
			# "NONE of the above Referral jurisdictions apply to this application"
			#k = 2
			#while k < 20
			#	begin
			#		$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{k}]")
			#		puts "Found checkbox: " + k
			#	rescue
					# We get here when we try to click a (next) checkbox that doesn't exist.
					# We then click the previous checkbox called "NONE of the above Referral jurisdictions apply to this application"
					$driver.find_element(:xpath, "(//*[@type='checkbox'])[12]").click
					puts "'NONE of the above Referral jurisdictions apply to this application' clicked."
			#	end
			#	k = k + 1
			#end
		end
		wait 2

=begin		
		# Click Next
		$driver.find_element(:xpath, "//*[text()='Next']").click
		puts "Next clicked."
		wait 6
		
		# Did you want to opt-out of the information request?*
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click		
		rescue
			puts "'Did you want to opt-out of the information request?' not found."
		end
		wait  6
				
		# Did you obtain an early referral response from DILGP (SARA) for this application?*
		#found = false
		#i = 0
		#begin
			#begin
				# puts "Try to find the checkbox 'Did you obtain an early referral response from DILGP (SARA) for this application?'"
				
				# $driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # This doesn't work and I don't know why :/
				$driver.find_element(:xpath, "//*[text()='Next']").send_keys :page_up
				$driver.find_element(:xpath, "//*[text()='Next']").send_keys :page_up
				sleep 1

				$driver.find_element(:xpath, "(.//*[starts-with(@name, 'gwt-uid')])[2]").click
				
				puts "'Did you obtain an early referral response from DILGP (SARA) for this application?' was clicked"
				found = true
			#rescue
			#	sleep 1
			#	puts "'Did you obtain an early referral response from DILGP (SARA) for this application?' not found."
			#	if i == 4
			#		puts "'Did you obtain an early referral response from DILGP (SARA) for this application?' not found. Stopping looking now."
			#		break
			#	end
			#end
		#	i = i + 1
		#end while not found
=end
		
		begin
			# Click Next
			puts "Click Next"
			$driver.find_element(:xpath, "//*[text()='Next']").click
			wait 4
		rescue
		end
		
		if Tools.checkForScreen("Referral agency details")
			puts "Error: Referral agency details screen still displayed."
			exit
		end
			
		if Tools.checkForScreen("Nominate fast track triggers", 2)
			Tempo.nominateFastTrackTriggers
		end
		
		puts "Exiting selectReferralTriggers"
	end





	def Tempo.assessmentTriggers(*trigId)
		puts "Entering assessmentTriggers"
		
		wait 2

		# Tools.checkPageForText
		
		if trigId[0] != nil
			puts "********************************"
			puts "Select provided triggers"
			puts "********************************"
			
			$driver.find_element(:xpath, "(//*[@type='checkbox'])[2]").click
			wait
			$driver.find_element(:xpath, "//*[text()='Add triggers']").click
			
			# Click Next button
			puts "Click Next button"
			nextBtn = "//*[text()='Next']"
			Tools.wait(:xpath, nextBtn, 10)
			$driver.find_element(:xpath, nextBtn).click
			wait 2	

			triggers = trigId.compact # Remove all nil values from array
			triggers = triggers.sort  # Sort the triggers

			trigCount = triggers.length
			puts "Now to add #{trigCount} triggers."

			begin 
				Tools.click(:xpath, "//input[@value='Add new triggers']", 10)

			rescue
				$driver.find_element(:xpath, "//input[@value='Add New Triggers']").click
			end
			sleep 2

			# Check if new Search functionality exists
			searchBox = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset[1]/div[2]/input"
			Tools.wait(:xpath, searchBox, 20)
			$driver.find_element(:xpath, searchBox)
			count = 1
			triggers.each { |trigger|
				Tools.wait(:xpath, searchBox, 10)
				
				# Select "Search all
				listBox = "(.//*[@class='gwt-ListBox'])[3]"
				puts "Select 'Search On'"
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, "All")
				wait
				
				puts "Adding trigger no. #{count}: #{trigger}"
				$driver.find_element(:xpath, searchBox).clear
				$driver.find_element(:xpath, searchBox).send_keys trigger
				$driver.find_element(:xpath, "//input[@value='Search']").click
				puts "Search clicked"
				sleep 3

				triggerChkBox = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/tbody/tr/td/div/label/input"
				if Tools.wait(:xpath, triggerChkBox, 10) # Then results found
				
					$driver.find_element(:xpath, triggerChkBox).click
					puts "Checkbox checked"
					sleep 1
					begin
						$driver.find_element(:xpath, "//input[@value='Add']").click
						puts "Add clicked"						
					rescue
						$driver.find_element(:xpath, "//input[@value='Done']").click
						puts "Done clicked"						
					end
					count = count + 1
					sleep 3
					begin 
						Tools.click(:xpath, "//input[@value='Add new triggers']", 10)

						puts "Add new triggers clicked" 
					rescue
						$driver.find_element(:xpath, "//input[@value='Add New Triggers']").click
						puts "Add New Triggers clicked" 
					end
					sleep 2
				else
					puts "#{trigger} not found"
				end
			}
			puts "No more triggers to add"

			Tools.click(:xpath, "//input[@value='Cancel']", 4)
			sleep 3

		else
			puts "********************************"
			puts "No triggers provided"
			puts "********************************"

			begin
				# Click Add new triggers
				$driver.find_element(:xpath, "//input[@value='Add new triggers']").click
				puts "Clicked Add new triggers"
				sleep 4
			rescue
				puts "'Add new triggers' not found. Continuing anyway."
				begin
					puts "Select all triggers"
					Tools.wait(:xpath, "(.//*[@type='checkbox'])[1]", 10)
					$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
					sleep 3
					$driver.find_element(:xpath, "//*[text()='Add triggers']").click
				rescue
					puts "Unable to click 'Add triggers'."
				end
			end
		end
		wait 3
		wait 3
		wait 3

		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		begin
			$driver.find_element(:xpath, nextBtn).click
			wait 6
		rescue
		end
		puts "Exiting assessmentTriggers"
	end

	
	
	


	def Tempo.referralTriggers(*triggers)
		puts "Entering referralTriggers"

		wait 3
		
		# Tools.checkPageForText
		
		# Check for the question 'Did you obtain a pre-referral response from a relevant agency(s) for this development application?'
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
			$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
		rescue
			puts "No question found: 'Did you obtain a pre-referral response from a relevant agency(s) for this development application?', continuing anyway."
		end

		if triggers[0] == nil
			# Select all referral triggers
			puts "Select all Referral triggers"
			Tools.wait(:xpath, "(.//*[@type='checkbox'])[1]", 10)
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			wait 4
			# Click Add button
			puts "Click Add triggers button"
			addBtn = "//*[text()='Add triggers']"
			Tools.wait(:xpath, addBtn, 10)
			$driver.find_element(:xpath, addBtn).click
			wait 2
		else
			begin
				i = 0
				puts "Now to select the trigger: "  + triggers[i]
				while triggers[i] != nil
					$driver.find_element(:xpath, "(.//*[@class='aui-TextInput'])[1]").clear
					sleep 1
					$driver.find_element(:xpath, "(.//*[@class='aui-TextInput'])[1]").send_keys triggers[i]
					puts "Trigger entered."
					sleep 1
					
					# Click Search
					puts "Click Search"
					searchBtn = "//*[text()='Search']"
					$driver.find_element(:xpath, searchBtn).click
					wait 2
					begin
						found = false
						j = 1
						while not found
							triggercol2 = "html/body/div[7]/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[6]/div[3]/div/div/div/div/div/div[1]/div/div/div/div[3]/div/div/div[2]/table/tbody[1]/tr[#{j}]/td[2]"
							triggercol1 = "html/body/div[7]/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[6]/div[3]/div/div/div/div/div/div[1]/div/div/div/div[3]/div/div/div[2]/table/tbody[1]/tr[#{j}]/td[1]"
							# puts triggercol2
							# puts triggercol1
							
							trigger = $driver.find_element(:xpath, triggercol2).text
							begin
								puts "Does " + trigger + " = " + triggers[i]
								if trigger == triggers[i]
									puts "j = #{j}"
									puts "i = #{i}"
									$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{j+1}]").click
									found = true
									puts "Trigger " + triggers[i]
									wait 2
									# Click Add Triggers button
									puts "Click Add Triggers button"
									addTriggersBtn = "//*[text()='Add triggers']"
									if not Tools.wait(:xpath, addTriggersBtn, 10)
										addTriggersBtn = "//*[text()='Add Triggers']"
									end
									$driver.find_element(:xpath, addTriggersBtn).click
									wait 3
									i = i + 1
									break
								else
									puts trigger + " not equal to " + triggers[i]
									j = j + 1
								end
							rescue
								puts "Problem clicking Trigger: " + triggers[i]
								if j == 15
									puts "Trigger " + triggers[i] + " not found"
									break
								end
							end
							i = i + 1
						end
					rescue
						puts "Issue selecting trigger: " + triggers[i]
					end		
				end
			rescue
				puts "Issue selecting triggers"
			end
		end
		
		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		Tools.wait(:xpath, nextBtn, 10)
		$driver.find_element(:xpath, nextBtn).click
		sleep 4
		wait

		puts "Exiting referralTriggers"
	end







	def Tempo.manageDocumentsEasyPrep(uploadDocLocation, docName)
		puts "Entering manageDocumentsEasyPrep"

		if Tools.checkForScreen("Upload document(s)", 4)

			## Tools.checkPageForText
			
			if $driver.find_element(:xpath, "//*[@type='file']")  # Check for the File Upload button
			
				# Select Upload Document location
				listBox = "(//*[starts-with(@class,'gwt-ListBox')])[1]"
				Tools.wait(:xpath, listBox, 10)
				if uploadDocLocation != nil 
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:text, uploadDocLocation)
				else
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, listBox)).select_by(:index, 1)		
				end
				wait 3

				# Click Browse button
				puts "Click Browse button"
				$driver.find_element(:xpath, "//*[@type='file']").click
				wait 3

				Clipboard.copy($aFile2)
				puts "Copied: #{Clipboard.paste}"
				wait
				if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
					puts "Input filename to upload #{Clipboard.paste}"
					system("#{$rubyHome}/Utils/fileUpload.sh")
				else
					puts "#{$rubyHome}/Utils/fileUpload.sh not found"
				end
				wait 4

				begin
					# Document name
					documentName = "(//*[@type='text'])[1]"
					Tools.wait(:xpath, documentName, 10)
					if docName != nil
						puts "Using supplied document name of '#{docName}'."
						begin
							$driver.find_element(:xpath, documentName).send_keys docName
						rescue # Field has moved
							$driver.find_element(:xpath, "(//*[@type='text'])[2]").send_keys docName
						end
					else
						puts "Using default document name of 'docName here'."
						begin
							$driver.find_element(:xpath, documentName).send_keys "docName here"
						rescue # Field has moved
						begin
								$driver.find_element(:xpath, "(//*[@type='text'])[2]").send_keys "docName here"
							rescue
								puts "Document name field has been removed."
							end
						end
					end
				rescue
				end
				
				# Document description
				documentDescription = "//*[contains(@class, 'aui-TextAreaInput printable')]"
				Tools.wait(:xpath, documentDescription, 10)
				puts "Entering a description"
				$driver.find_element(:xpath, documentDescription).send_keys "Some text"
				wait 2

				# Click Add button
				puts "Click Add button"
				addBtn = "//*[text()='Add']"
				Tools.wait(:xpath, addBtn, 10)
				$driver.find_element(:xpath, addBtn).click
				wait 4
			else
				puts "No Browse button found. It's likely already done so we'll try continuing without doing anything here."
			end
			
			# Attempt to click Next button
			begin
				nextBtn = "//*[text()='Next']"
				Tools.wait(:xpath, nextBtn, 10)
				$driver.find_element(:xpath, nextBtn).click
				wait 2
			rescue # Else click the Submit button
				begin
					submitBtn = "//*[text()='Submit']"
					Tools.wait(:xpath, submitBtn, 10)
					$driver.find_element(:xpath, submitBtn).click
					wait 2
				rescue
					puts "No Submit button found. Attempt to contnue."
				end
			end
		else
			puts "Manage Documents not found."
		end
		puts "Exiting manageDocumentsEasyPrep"
	end





	def Tempo.confirmationTempo( *lodgeReferral )
		puts "Entering ConfirmationTempo, aka Development assessment"

		Tools.wait(:xpath, "(.//*[@type='radio'])[5]", 10)
		$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
		$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
		begin
			$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
		rescue
			puts "Radio 5 not found, continuing."
		end
		Tempo.wait

		# Click Next button
		puts "Click Next button"
		nextBtn = "//*[text()='Next']"
		Tools.wait(:xpath, nextBtn, 10)
		$driver.find_element(:xpath, nextBtn).click

		skip = true
		
		Tempo.wait 5

		# Application package and email
		begin
			begin # Zip
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
			rescue
				puts "Radio button not found"
			end

			begin
				# Click Submit button
				puts "Click Submit button"
				submitBtn = "//*[text()='Submit']"
				Tools.wait(:xpath, submitBtn, 10)
				$driver.find_element(:xpath, submitBtn).click
				wait 4
			rescue
				puts "No Submit button found."
			end
			
			# Initiate DSDIP (SARA) referral
			if lodgeReferral != nil
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				rescue
					puts "no Radio[1] found."
				end
			else
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				rescue
					puts "no Radio[1] found."
				end
			end
			wait 4

			if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Assigned case officer"
				puts "Assigned Case Officer found."
				# Click Submit
				puts "Click Submit"
				Tools.wait(:xpath, submitBtn, 10)
				$driver.find_element(:xpath, submitBtn).click
				wait 6
			elsif Tools.wait(:xpath, ".//*[@src='/suite/components/toolbar/img/calendar.gif']", 5)
				puts "Date found."
				# Add date
				Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")

				# Click Next button
				nextBtn = "//*[text()='Next']"
				Tools.wait(:xpath, nextBtn, 10)
				$driver.find_element(:xpath, nextBtn).click	
				wait 5
				if Tools.wait(:xpath, "//*[text()='Refresh']", 3)
					# Click Refresh
					puts "Click Refresh"
					ready = false
					counter = 1
					while not ready
						$driver.find_element(:xpath, "//*[text()='Refresh']").click
						wait 2
						begin
							if $driver.find_element(:xpath, "//*[text()='100%']")
								ready = true
								puts "1: Progress = 100%"
							end
						rescue
							print "."
						end
						counter = counter + 1
					end
				end		
				puts ""
				puts "Click 'Select pay items' link"
				Tools.click(:xpath, "//a[contains(.,'Select pay items')]")	
				skip = false
			elsif $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Select pay items"
				puts "Select Pay Items found."
				selectPayItemsTempo
			
			else
				puts "Nothing found."
				# Click Next button
				nextBtn = "//*[text()='Next']"
				Tools.wait(:xpath, nextBtn, 10)
				$driver.find_element(:xpath, nextBtn).click
				wait 4
			
				if Tools.wait(:xpath, "//*[text()='Refresh']", 3)
					# Click Refresh
					puts "Click Refresh"
					ready = false
					counter = 1
					while not ready
						$driver.find_element(:xpath, "//*[text()='Refresh']").click
						wait 2
						begin
							if $driver.find_element(:xpath, "//*[text()='100%']")
								ready = true
								puts "2: Progress = 100%"
							end
						rescue
							print "."
						end
						counter = counter + 1
					end
					puts ""
					$driver.find_element(:xpath, "//*[text()='Close']").click
					Tempo.wait 3

				else 
					puts "Refresh button not found."
				end
			end

			if skip == true
				# Execute the Task to finish confirmation
				puts "wait6 seconds for MyDas to generate a task"
				for i in 1..16
					sleep 1
					print i.to_s
					print "."
				end
				puts "Done."
				puts "Now to check for the task."

				if not executeTaskAfterConfirmation
					puts "Task not found."
				end
			else
				puts "Now to Select Pay Items"
			end
		rescue
			puts "No zip option so skipping"
		end
		puts "Exiting ConfirmationTempo, aka Development assessment"
	end






	def Tempo.executeTaskAfterConfirmation
		puts "Entering executeTaskAfterConfirmation"
		puts "aka initiatingreferralapplication"
		# Click the tasks link
		Tools.click(:xpath, "//a[contains(.,'Tasks')]")	
		wait 2

		#task = "Application prepared #{$appID}"
		#task = "Approval Task 2 #{$appID}"

		task = "Initiating referral application #{$appID}"
		task = task[0..-5] # Strip last 4 characters (SRA, SQA, SDA etc)

		taskFound = false
		counter = 0

		# Now wait for the task to be generated
		while not taskFound
			begin			
				puts "Find and click Task: '#{task}'."
				$driver.find_element(:xpath, "//a[contains(.,'#{task}')]").click
				taskFound = true
				puts "Task found and clicked"
			rescue
				puts "Task not found yet"
				Tools.click(:xpath, "//a[contains(.,'Tasks')]")	
				counter = counter + 1
				if counter == 15
					puts "*********************************"
					puts "Task not found, quitting"
					puts "*********************************"
					exit
				end
				sleep 2
			end
		end
		wait 2

		begin
			# Click OK button
			puts "Click OK button"
			oKBtn = "//*[text()='Ok']"
			Tools.wait(:xpath, oKBtn, 10)
			$driver.find_element(:xpath, oKBtn).click
			return true
		rescue
			puts "OK not found."
			return false
		end
		
		# Tools.checkPageForText		
		
		puts "Exiting executeTaskAfterConfirmation"
	end







	def Tempo.manageDocuments(action, *actions)
		puts "Entering manageDocuments"
		begin
			Tools.wait(:xpath, "//a[contains(.,'Manage Documents')]", 4)
			$driver.find_element(:xpath, "//a[contains(.,'Manage Documents')]").click
			wait 5
		rescue
			puts "'Manage documents' link not found. Try continuing."
		end

		if action != nil
			case action.downcase
			when "upload"
				# Click Upload button
				puts "Click Upload button"
				$driver.find_element(:xpath, "//*[text()='Upload']").click
				wait 2
				manageDocumentsEasyPrep(nil, nil)

			when "delete"
				if actions[0] != nil
					aFile = actions[0]
				else 
					aFile = $aFile1
				end
				i = 1
				found = false
				puts "Find document '#{aFile}' to delete"

				while not found
					begin

						filename = $driver.find_element(:xpath, "*//table/tbody[1]/tr[#{i}]/td[3]/div/a").text.downcase
						aFile = aFile.downcase
						puts "Compare '#{aFile}' to '#{filename}'"

						if aFile.include? filename
							begin
								found = true
								puts "Found document '#{aFile}' to delete."
								# Select matching checkbox
								$driver.find_element(:xpath, "(//*[@type='checkbox'])[#{i}]").click							
								wait 4
								# Click Delete button
								puts "Click Delete button"
								$driver.find_element(:xpath, "//*[text()='Delete']").click
								wait 4
								puts "Click 2nd Delete button"
								$driver.find_element(:xpath, "//*[text()='Delete']").click
								wait 4
								puts "Click Submit button"
								$driver.find_element(:xpath, "//*[text()='Submit']").click
								wait 4
								puts "Document '#{aFile}' deleted successfully"
							rescue
								puts "There was a problem deleting the document '#{aFile}'."
							end
						else
							i = i + 1
						end
					rescue
						puts "Unable to find document '#{aFile}'"
						exit
					end
				end

			when "update"


			when "download"


			else
				puts "Unknown action of: '#{action}'"
				exit
			end
		end
		
		# Click Next
		$driver.find_element(:xpath, "//*[text()='Next']").click
		puts "Next clicked"
		wait 3
		
		puts "Exiting manageDocuments"
	end





	
	
	def Tempo.getTempoReports
		puts "Entering getTempoReports"
		Tools.click(:xpath, "//a[contains(.,'Actions')]")
		sleep 2
		$driver.switch_to.default_content()
		puts "Click Portal Notice Viewer"
		Tools.click(:link, "Portal Notice Viewer")
		wait 6		
		
		# Tools.checkPageForText
		
		# Download Reports
		rowCount = 25
		pageCount = 18 
		
		for j in 1..pageCount
			Tools.wait(:xpath, ".//*[@type='text']", 10)

			for i in 1..rowCount
				# Original Template
				begin
					$driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[2]/div/a").click
					puts $driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[2]/div/a").text
					Tools.saveReport("Save")
				rescue
					puts "No more reports found. Finished on page: #{j} and row #{i}"
					# No Notice found
				end

				# Word Document (Merged Fields)
				begin
					$driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[3]/div/a").click
					puts $driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[3]/div/a").text
					Tools.saveReport("Save")
				rescue
					puts "No Word Document (Merged Fields) found"
				end
				
				# PDF Document (Merged Fields)
				begin 
					$driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[4]/div/a").click
					puts $driver.find_element(:xpath, "//html/body/div[7]/div[3]/div/div[2]/div/div[2]/div[3]/div/div/div/div/div/form/div[2]/div/div/div/div/div/div/div[2]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[3]/div/div/div[2]/table/tbody/tr[#{i}]/td[4]/div/a").text
					Tools.saveReport("Save")					
				rescue
					# No PDF on this row
					puts "No PDF found"
				end
			end # rows on a page
					
			# Next page
			begin
				$driver.find_element(:xpath, ".//*[@aria-label='Next page']").click
				sleep 8
			rescue
				# Must be disabled as no more pages
				break
			end
		end
		puts "Exiting getTempoReports"
	end









	def Tempo.registerNewIndividual(email, fname, sname, position, phone, mobile, title, address1, address2, state, suburb, postcode, country, acknowledge, surveys)
		puts "Entering registerNewIndividual"

		# Click menu option Action
		Tools.click(:xpath, "//a[contains(.,'Actions')]", 6)
		wait 2
		
		# Click option Register new individual
		Tools.click(:xpath, "//a[contains(.,'Register New Individual')]", 6)
		wait 2

		# Tools.checkPageForText
		
		# Email
		emailxp = "(.//*[@type='text'])[1]"
		if email == nil
			email = Faker::Internet.email
		end
		$driver.find_element(:xpath, emailxp).send_keys email
		system("#{$rubyHome}/Utils/mouseClick.sh", "782", "165")
		wait 8

		puts "***********************************************"
		puts email

		# First name
		fnamexp = "(.//*[@type='text'])[2]"
		if fname == nil
			fname = Faker::Name.first_name
		end
		$driver.find_element(:xpath, fnamexp).send_keys fname

		# Surname
		snamexp = "(.//*[@type='text'])[3]"
		if sname == nil
			sname = Faker::Name.last_name
		end
		$driver.find_element(:xpath, snamexp).send_keys sname
		puts fname + " " + sname

		# Title
		titlexp = "(.//*[@class='gwt-ListBox'])[1]"
		if title == nil
			title = "1"
		end
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, titlexp)).select_by(:index, title)

		# Position
		positionxp = "(.//*[@class='gwt-ListBox'])[2]"
		if position == nil
			position = "1"
		end
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, positionxp)).select_by(:index, position)
		puts position

		# Phone
		phonexp = "(.//*[@type='text'])[4]"
		if phone == nil
			phone = "0422685626"
		end
		$driver.find_element(:xpath, phonexp).send_keys phone
		puts phone

		# Mobile
		mobilexp = "(.//*[@type='text'])[5]"
		if mobile == nil
			mobile = "0422685626"
		end
		$driver.find_element(:xpath, mobilexp).send_keys mobile
		puts mobile

		# Address line 1
		addressxp = "(.//*[@type='text'])[6]"
		if address1 == nil
			address1 = Faker::Address.street_address
		end
		$driver.find_element(:xpath, addressxp).send_keys address1
		puts address1

		# Address line 2
		address2xp = "(.//*[@type='text'])[7]"
		if address2 == nil
			address2 = "2nd address line"
		end
		$driver.find_element(:xpath, address2xp).send_keys address2
		puts address2

		# Suburb
		suburbxp = "(.//*[@type='text'])[8]"
		if suburb == nil
			suburb = "Miami"
		end
		$driver.find_element(:xpath, suburbxp).send_keys suburb
		puts suburb

		# Post Code
		postcodexp = "(.//*[@type='text'])[9]"
		if postcode == nil
			postcode = "4220"
		end
		$driver.find_element(:xpath, postcodexp).send_keys postcode
		puts postcode

		# Country
		countryxp = "(.//*[@class='gwt-ListBox'])[4]"
		if country == nil
			country = "14"
		end
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, countryxp)).select_by(:index, country)
		puts country

=begin
		wait 2 # Wait for state field to be enabled.

		# State
		statexp = "(.//*[@class='gwt-ListBox'])[3]"
		if state == nil
			state = "4"
		end
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, statexp)).select_by(:index, state)
		puts state
=end

		# Accept Terms & Conditions (etc.) checkBoxes
		$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
		wait
		
		# Agree to being spammed
#		$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click

		# Sometimes the first name field clears so re-add it, just in case
		#$driver.find_element(:xpath, fnamexp).clear
		#$driver.find_element(:xpath, fnamexp).send_keys fname
		#wait 6
	
		# Click Submit
		$driver.find_element(:xpath, "//*[text()='Submit']").click	
		wait 3

		puts "***********************************************"

		puts "Exiting registerNewIndividual"
	end




	def Tempo.createMyDasUser(email, fname, sname)
		puts "Entering createMyDasUser"

		Tools.clickLinkIfExists( "Registration")
		
		# Wait for screen to display
		i = 1
		while i < 10 
			begin
				$driver.find_element(:id,"firstName")
				i = 10
			rescue
				sleep 1
				i = i + 1
			end
		end
		$driver.find_element(:id,"firstName")
		
		# Fill in the fields
		Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "title")).select_by(:index, 1) # Title
		$driver.find_element(:id,"firstName").send_keys fname # First name
		$driver.find_element(:id,"lastName").send_keys sname # Surname
		$driver.find_element(:id,"email").send_keys email	# Email
		Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "position")).select_by(:index, 1) # Position
		$driver.find_element(:id,"phoneNumber").send_keys "0755285626"	# Phone number
		$driver.find_element(:id,"mobileNumber").send_keys "0422685626"	# Mobile number
		$driver.find_element(:id,"addressLine1").send_keys "10 Downing st"	# Address Line 1
		$driver.find_element(:id,"suburb").send_keys "Miami"	# Suburb
		$driver.find_element(:id,"postcode").send_keys "4220"	# Post code
		$driver.find_element(:id,"terms").click # Terms and conditions
exit
		$driver.find_element(:id,"signupbutton").click # Submit button
	end
	
	
	
	
	
	#def Tempo.createNewMyDasUserAccount(email, uname, fname, sname, password, appDesigner, sysAdmin, notifyUser, announceOnTempo)
	def Tempo.createNewMyDasUserAccount(email, sname, fname)
		#if not Tools.checkForScreen("Register individual")
			# Click menu option Action
			Tools.click(:xpath, "//a[contains(.,'Actions')]", 6)

			# Click option Register new individual
			Tools.click(:xpath, "//a[contains(.,'Register New Individual')]", 6)
			sleep 2
		#end

		# Email
		$driver.find_element(:xpath, "(.//*[@type='text'])[1]").clear
		sleep 1
		$driver.find_element(:xpath, "(.//*[@type='text'])[1]").send_keys email
		$driver.find_element(:xpath, "(.//*[@type='text'])[1]").send_keys :tab
		# system("#{$rubyHome}/Utils/mouseClick.sh", "1726", "541")
		sleep 6
		
		begin
			if $driver.find_element(:xpath, "//*[@class='component_error']").text.include? "This email address is already being used."
				puts "Email already in use: #{email}"
				Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Email already in use: #{email}")
			end
		rescue
			puts "*************** User Details *****************"
			
			# Title
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:index, 1)

			# Position
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:index, 1)

			# First name
			$driver.find_element(:xpath, "(.//*[@type='text'])[2]").send_keys "John"
			#$driver.find_element(:xpath, "(.//*[@type='text'])[2]").send_keys fname

			# Surname
			$driver.find_element(:xpath, "(.//*[@type='text'])[3]").send_keys sname

			# Phone number
			$driver.find_element(:xpath, "(.//*[@type='text'])[4]").send_keys "0755685626"

			# Mobile number
			$driver.find_element(:xpath, "(.//*[@type='text'])[5]").send_keys "0422685626"
			
			# Address line 1
			$driver.find_element(:xpath, "(.//*[@type='text'])[6]").send_keys "10 Downing street"

			# Suburb
			$driver.find_element(:xpath, "(.//*[@type='text'])[8]").send_keys "Miami"

			# Post code
			$driver.find_element(:xpath, "(.//*[@type='text'])[9]").send_keys "4221"
			
			# I have read and acknowledge the terms and conditions of system usage.
			$driver.find_element(:xpath, "(.//*[@type='checkbox'])[1]").click
			
			# Click Submit
			$driver.find_element(:xpath, "(//*[@type='button'])[2]").click
			Tools.logger("#{$rubyHome}CSVs/newUserCreated.log", "#{email}, created.")

			puts "***********************************************"
		end
		
		puts "Exiting createNewMyDasUserAccount"
	end






	def Tempo.registerOrganisation(abn, orgname, email, tradingName, contactnumber, contactname, address1, address2, state, suburb, postcode, country)
		puts "Entering registerNewOrganisation"

		# Click menu option Action
		Tools.click(:xpath, "//a[contains(.,'Actions')]", 10)

		# Click option Register new individual
		Tools.click(:xpath, "//a[contains(.,'Register New Organisation')]")
		sleep 4

		puts "***********************************************"

		# ABN
		if abn == nil 
			puts "Error cannot continue without a valid ABN."
			exit
		end

		abnxp = "(.//*[@type='text'])[1]"
		Tools.wait(:xpath, abnxp, 10)
		$driver.find_element(:xpath, abnxp).send_keys abn
		system("#{$rubyHome}/Utils/mouseClick.sh", "782", "165")
		sleep 4
		puts abn

		# Now to wait for the ABN to be validated.
		# Once done, remaining fields become writable

		# Organisation name
		orgnamexp = "(.//*[@type='text'])[2]"
		if orgname == nil 
			orgname = "Miami spares"
		end
		$driver.find_element(:xpath, orgnamexp).send_keys orgname
		puts orgname

		# Email
		emailxp = "(.//*[@type='text'])[3]"
		if email == nil 
			puts "Error cannot continue without a valid email address (Unique email domain)."
			exit
		end
		$driver.find_element(:xpath, emailxp).send_keys email
		puts email

		# Trading name
		tradingnamexp = "(.//*[@type='text'])[4]"
		if tradingName == nil 
			tradingName = "Miami Spares"
		end
		$driver.find_element(:xpath, tradingnamexp).send_keys tradingName
		puts tradingName

		# Contact number
		contactnumberxp = "(.//*[@type='text'])[5]"
		if contactnumber == nil 
			contactnumber = "0422685635"
		end
		$driver.find_element(:xpath, contactnumberxp).send_keys contactnumber
		puts contactnumber

		# Contact name
		contactnamexp = "(.//*[@type='text'])[6]"
		if contactname  == nil 
			contactname = $applicant
		end
		$driver.find_element(:xpath, contactnamexp).send_keys contactname
		puts contactname

		# Address line 1
		address1xp = "(.//*[@type='text'])[7]"
		if address1 == nil 
			address1 = Faker::Address.street_address
		end
		$driver.find_element(:xpath, address1xp).send_keys address1
		puts address1

		# Address line 2
		address2xp = "(.//*[@type='text'])[8]"
		if address2 == nil 
			address2 = "2nd address line"
		end
		$driver.find_element(:xpath, address2xp).send_keys address2
		puts address2

		# Suburb
		suburbxp = "(.//*[@type='text'])[9]"
		if suburb == nil 
			suburb = "Miami"
		end
		$driver.find_element(:xpath, suburbxp).send_keys suburb
		puts suburb

		# Post Code
		postcodexp = "(.//*[@type='text'])[10]"
		if postcode == nil 
			postcode = "4220"
		end
		$driver.find_element(:xpath, postcodexp).send_keys postcode
		puts postcode

		# Country
		countryxp = "(.//*[@class='gwt-ListBox'])[2]"
		if country != nil 			
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, countryxp)).select_by(:index, country)
			puts country
			sleep 5
		else
			puts "Australia"
		end

		# State
		statexp = "(.//*[@class='gwt-ListBox'])[1]"
		if state != nil 			
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, statexp)).select_by(:index, state)
			puts state
		else
			puts "Queensland"
		end
		
		sleep 6

		# Click Submit
		$driver.find_element(:xpath, "(//*[@type='button'])[2]").click		
		puts "Submit clicked"
		sleep 3

		# Click Yes on the dialogue 
		Tools.wait(:xpath, "(//*[@type='button'])[3]", 3)
		$driver.find_element(:xpath, "(//*[@type='button'])[3]").click	

		puts "Exiting registerNewOrganisation"
	end




	def Tempo.registerSubOrganisation(abn, parentOrganisation, orgname, email, tradingName, contactnumber, contactname, address1, address2, state, suburb, postcode, country)
		puts "Entering createSubOrganisation"

		if parentOrganisation == nil
			puts "Error cannot continue without a valid parent organisation."
			exit
		end
		if email == nil
			puts "Error cannot continue without a valid, unique email."
			exit
		end

		Tools.click(:xpath, "//a[contains(.,'Records')]")
		$driver.switch_to.default_content()
		Tools.click(:link, "Organisations")
		wait 3
		# Open the Parent so we can create a child on it
		Tools.wait(:xpath, "//*[@placeholder='Search Organisations']", 5)
		$driver.find_element(:xpath, "//*[@placeholder='Search Organisations']").send_keys parentOrganisation
		$driver.find_element(:xpath, "//*[@placeholder='Search Organisations']").send_keys :enter
		wait 3
		Tools.click(:xpath, "//a[contains(.,'#{parentOrganisation}')]")
		puts "Clicked #{parentOrganisation}"
		wait 4
		# Register Sub-organisation link
		if not Tools.click(:xpath, "//a[contains(.,'Register Sub-organisation')]")
			puts "Can't continue as the 'Register Sub-organisation' link was not found."
			exit
		end
		wait 6

		abnxp = "(.//*[@type='text'])[2]"
		Tools.wait(:xpath, abnxp, 10)
		$driver.find_element(:xpath, abnxp).send_keys abn
		system("#{$rubyHome}/Utils/mouseClick.sh", "782", "165")
		wait 6
		puts abn

		# Now to wait for the ABN to be validated.
		# Once done, remaining fields become writable

		# Organisation name
		orgnamexp = "(.//*[@type='text'])[3]"
		if orgname == nil 
			orgname = "Miami spares"
		end
		$driver.find_element(:xpath, orgnamexp).send_keys orgname
		puts orgname

		# Email
		emailxp = "(.//*[@type='text'])[4]"
		if email == nil 
			puts "Error cannot continue without a valid email address (Unique email domain)."
			exit
		end
		$driver.find_element(:xpath, emailxp).send_keys email
		puts email

		# Trading name
		tradingnamexp = "(.//*[@type='text'])[5]"
		if tradingName == nil 
			tradingName = "Miami Spares"
		end
		$driver.find_element(:xpath, tradingnamexp).send_keys tradingName
		puts tradingName

		# Contact number
		contactnumberxp = "(.//*[@type='text'])[6]"
		if contactnumber == nil 
			contactnumber = "0422685635"
		end
		$driver.find_element(:xpath, contactnumberxp).send_keys contactnumber
		puts contactnumber

		# Contact name
		contactnamexp = "(.//*[@type='text'])[7]"
		if contactname  == nil 
			contactname = $applicant
		end
		$driver.find_element(:xpath, contactnamexp).send_keys contactname
		puts contactname


		# Address line 1
		address1xp = "(.//*[@type='text'])[8]"
		if address1 == nil 
			address1 = Faker::Address.street_address
		end
		$driver.find_element(:xpath, address1xp).send_keys address1
		puts address1

		# Address line 2
		address2xp = "(.//*[@type='text'])[9]"
		if address2 == nil 
			address2 = "2nd address line"
		end
		$driver.find_element(:xpath, address2xp).send_keys address2
		puts address2

		# Suburb
		suburbxp = "(.//*[@type='text'])[10]"
		if suburb == nil 
			suburb = "Miami"
		end
		$driver.find_element(:xpath, suburbxp).send_keys suburb
		puts suburb

		# Post Code
		postcodexp = "(.//*[@type='text'])[11]"
		if postcode == nil 
			postcode = "4220"
		end
		$driver.find_element(:xpath, postcodexp).send_keys postcode
		puts postcode

		# Country
		countryxp = "(.//*[@class='gwt-ListBox'])[3]"
		if country != nil 			
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, countryxp)).select_by(:index, country)
			puts country

			wait 3
		end

		# State
		statexp = "(.//*[@class='gwt-ListBox'])[2]"
		if state != nil 			
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, statexp)).select_by(:index, state)
			puts state
		end

		# Click Submit
		$driver.find_element(:xpath, "(//*[@type='button'])[2]").click	

		wait 3

		# Click Yes on the dialogue 
		Tools.wait(:xpath, "(//*[@type='button'])[3]", 3)
		$driver.find_element(:xpath, "(//*[@type='button'])[3]").click	

		# Check for errors
		if $driver.find_element(:xpath, "//*[@class='form_error']").text.include? "The email domain is blocked"
			puts "Email address is blocked so can't create sub organisation."
		end

		puts "Exiting createSubOrganisation"
	end


	
	
	

	def Tempo.confirmApplicationDetails
		puts "Entering confirmApplicationDetails"
		wait 3

		# Tools.checkPageForText
		
		begin
			# Select Yes for 'Are the assessment triggers identified complete and correct?*?'
			puts "Select Yes for 'Have you completed your payment?'"
			Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 5)
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			wait 3
		rescue
			puts "Select Yes for 'Have you completed your payment?' not found"
		end
		
		found = false
		i = 0
		begin
			begin
				# Are the referral triggers identified complete and correct?
				puts "Are the referral triggers identified complete and correct?"
				$driver.find_element(:xpath, "(.//*[@type='radio'])[3]").click
				found = true
				wait 3
			rescue
				puts "Are the referral triggers identified complete and correct? not found"
				sleep 1
			end
			i = i + 1
			if i == 20
				break
			end
		end while not found
		
		begin
			# Is this documentation complete?
			puts "Is this documentation complete?"
			$driver.find_element(:xpath, "(.//*[@type='radio'])[5]").click
			wait 4
		rescue
			puts "Is this documentation complete? not found"
		end
		
		# Click Next
		puts "Click Next"
		$driver.find_element(:xpath, "//*[text()='Next']").click
		wait 4
		puts "Exiting confirmApplicationDetails"
	end







	def Tempo.applicationPackageAndEmail
		puts "Entering packageAndEmail (aka Application package and email)"

		if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Package and email"

			# Select No for 'Do you want to package and email all your application documents?'
			puts "Select No for 'Do you want to package and email all your application documents?'"
			$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
			sleep 1
		
			# Click Submit again
			puts "Click Submit"
			$driver.find_element(:xpath, "//*[text()='Submit']").click
			wait 6
		else
			puts "'Package and email' screen not found."
			begin
				appID = appID.sub("QDA", "") # QDA -> SDA
				openTask("Application package and email " + appID)
			rescue
				puts "'Application package and email #{appID}' not found, continuing anyway"
			end
		end

		puts "Exiting applicationPackageAndEmail"
	end






	def Tempo.referToDSDIP
		puts "Entering referToDISDIP"

		Tools.checkForScreen("Refer to")
		
		if $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text.include? "Refer to DSDIP"

			# Select Yes
			puts "Select Yes"
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click

			begin
				# Add date by opening the Date Picker
				$driver.find_element(:xpath, "//*[starts-with(@class,'aui-DateInput-Placeholder')]").click
				wait

				# Select today
				$driver.find_element(:xpath, "//*[contains(@class,'datePickerDayIsToday')]").click
				wait 3
				puts "Date added."
			rescue
				puts "Date picker not found, continuing anyway"
			end
			
			# Click Next again
			puts "Click Next"
			Tools.wait(:xpath, "//*[text()='Next']", 10)
			$driver.find_element(:xpath, "//*[text()='Next']").click
			puts "The 'Initiating referral application' screen should display."
			wait 6
		
			# Now to wait for Processing to equal 100%
			puts "Now to wait for Processing to equal 100%"
			ready = false
			counter = 0
			while not ready
				$driver.find_element(:xpath, "//*[text()='Refresh']").click
				wait
				begin
					if $driver.find_element(:xpath, "//*[text()='100%']")
						ready = true
						puts "."
						puts "Progress = 100%"
						break
					end
				rescue
					if counter == 0
						puts "Not ready yet."
					else
						print "."
					end
				end
				counter = counter + 1
				if counter == 12 # 6 minutes
					puts "Processing still not complete."
					exit
				end
				sleep 14
			end
		else
			text = $driver.find_element(:xpath, "//*[contains(@class,'appian-form-title')]").text
			puts "'Refer to DSDIP' screen not found."
			puts "Found '#{text}'"
		end
		puts "Exiting referToDSDIP"
	end




	def Tempo.nominateFastTrackTriggers
		# Nominate Fast Track Triggers
		puts "Entering nominateFastTrackTriggers"
		wait 4
		
		
		## Tools.checkPageForText
=begin		
		fastTrack = true
		
		i = 1
		begin
			while fastTrack
				# Nominate (for Fast Track)
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".(//*[contains(@class,'gwt-ListBox')])[#{i}]")).select_by(:index, 0)
				puts "Selected Yes for Nominate (for Fast Track)"
				wait 3

				# Click Upload
				puts "Click Upload link."
				upload = "(//*[text()='Upload'])[#{i}]]"
				Tools.wait(:xpath, upload, 5)
				$driver.find_element(:xpath, upload).click
				wait 3
			
				i = i + 1 
				
				$driver.find_element(:xpath, "//*[contains(@class,'gwt-FileUpload')]").click
				wait
				
				# Enter filename to upload
				Clipboard.copy($aFile2)
				puts "Copied: #{Clipboard.paste}"
				wait
				if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
					puts "Input filename to upload #{Clipboard.paste}"
					system("#{$rubyHome}/Utils/fileUpload.sh")
				else
					puts "#{$rubyHome}/Utils/fileUpload.sh not found"
				end
				wait 4

				# Click Add
				puts "Click Add"
				$driver.find_element(:xpath, "//*[text()='Add']").click
				wait 2
				# Click Next
				puts "Click Next."
				$driver.find_element(:xpath, "//*[text()='Next']").click
				wait 5
=end
			
			# Click Next
			puts "Click Next"
			$driver.find_element(:xpath, "//*[text()='Next']").click
			wait 3
			
		#rescue
		#	puts "No Fast Track found"
		#end
		
		puts "Exiting nominateFastTrackTriggers"
	end
	
	
	
	
	
	def Tempo.updateOutlookPasswords(user, oldPass, newPass)
		puts "Entering Update Outlook Passwords"
		
		if user != "x"
			# Login
			$driver.find_element(:id,"cred_userid_inputtext").send_keys user
			$driver.find_element(:id,"cred_password_inputtext").send_keys oldPass
			$driver.find_element(:id,"cred_sign_in_button").click # Sign in button
			sleep 3
			
			begin
				if $driver.find_element(:id,"recover_container") # Unknown account
					Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, unknown account")
					puts "Unknown account"
				end
			rescue
			
				# Update password
				$driver.find_element(:id,"old_password_inputtext").send_keys oldPass
				$driver.find_element(:id,"new_password_inputtext").send_keys newPass
				$driver.find_element(:id,"retype_new_password_inputtext").send_keys newPass
				$driver.find_element(:id,"change_pwd_button").click # Update password and sign in button
				
				i = 1
				while i < 10
					begin
						# Sign out after login
						$driver.find_element(:xpath, ".//*[@id='O365_TopMenu']/div/div[2]/div[1]/div[11]/button").click # Click menu
						sleep 1
						$driver.find_element(:xpath,".//*[@id='_ariaId_106']/div/div[2]/div[3]/div/div[5]/button").click # Sign out
					rescue
						puts "Logout not found yet"
						sleep 1
					end
					i = i + 1
					if i == 10
						Tools.logger("#{$rubyHome}CSVs/Logins.txt", "#{$USER}, Error ;logging out")
						puts "Error logging out"
					end
				end
			end		
		else
			puts "Skipping row marked 'x'"
		end
		# Re-open login page
		$driver.navigate.to $URL
		sleep 1
				
		puts "Exiting updateOutlookPasswords"
	end
end # Tempo
