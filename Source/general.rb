require 'rubygems'
require 'selenium-webdriver'
require 'clipboard' 

# Requires 'sudo apt-get install xclip' for Linux
# Requires 'sudo apt-get install xdotool' for Linux

module General
	def General.doReconciliation( appID, *paymentType )
# Email: 0003d77b-b16c-8000-f92f-7f0000014e7a@mydastest.appiancloud.com

# Table : sara_FinanceTransactionSSQRequest

# Important columns of sara_FinanceTransactionSSQRequest: applicationDBId and 
# FinanceTransactionSSQRequestId (order number in disbursement report)

# applicationDBId is equal to last part of application reference-3000.  
# So, if application reference is SDA-0216-007428 then applicationDBid will be 23609
	
		puts "Entering 'Do Reconciliation'"
		
		if appID != nil  # When using created application
			$appID = appID
		elsif $appID == nil # When creating application first $appID should not be nil
			puts "No appID supplied so it's likely no application ID was created or supplied, quitting."
		end
		
		# Remove unwanted text to leave last 4 digits of AppID.
		# i.e. '1509-5100 SDA' becomes '5100'
		puts $appID
		appID = $appID.sub("SDA", "") 
		appID = appID.sub("SRA", "") 
		appID = appID.sub("SPD", "") 
		appID = appID.sub(" ", "") 
		appID = appID.lstrip # Remove leading whitespace
		appID = appID.rstrip # Remove trailing whitespace
		apps = Array.new
		apps = appID.split(/-/) # Extract Numbers into array based on the seperator '-'
		puts "............................................"
		$shortAppID = apps[2]
		$shortAppID = $shortAppID.to_i
		$shortAppID = $shortAppID - 3000
		
		puts "Full Application ID is '#{$appID}'"
		puts "Short (Application ID - 3000) is '#{$shortAppID}'"
		puts "............................................"
		
		if paymentType == nil
			$paymentType = "Visa"
			puts "No Payment type provided so Visa used"
		else
			$paymentType = paymentType
		end
		
		# Click the Appian DB link
		Tempo.clickAppianlink
		
		puts "00000000000000000000000000000000000000000000000000000"
		# Get the order Number
		Tools.getSSQRequestID # This method just runs an Update SQL for Portal, that's all that is needed. Easy.

		puts "00000000000000000000000000000000000000000000000000000"
		# Validate the payment
		#Tools.validatePayment
		
		# Close the DB browser window.
		Tools.closeBrowser
		
		puts "Exiting doReconciliation"
	end

	
	
	
	
	
	
	def General.sendingOutOurResponseToAM
		# Log in as Admin Officer
		puts "Entering 'Sending out our response to AM'"
		
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Sending out our response to AM")	
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				$driver.manage.window.move_to($startX,$startY)
				begin
					Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Done']",10)
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				# Load response file
				Clipboard.copy($aFile2)
				puts "Copied: #{Clipboard.paste}"
				sleep 1
				Tools.loadFile(".//*[@name='$ifu_file']",[1])

				begin
					$driver.find_element(:xpath, "//input[@value='Done']").click
					puts "Done clicked"
					sleep 3
				rescue
					puts "Done not found"
				end
			}
		else
			puts "Please Provide Requested Information screen not found."
		end	
		Accept btn
		Close btn
	end
	
	
	
	def General.pleaseProvideRequestedInformation
		puts "Entering Please Provide Requested Information"
		
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Please provide requested information")	
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				$driver.manage.window.move_to($startX,$startY)
				begin
					Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Done']",10)
				rescue
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				# Load response file
				Clipboard.copy($aFile2)
				puts "Copied: #{Clipboard.paste}"
				sleep 1
				Tools.loadFile(".//*[@name='$ifu_file']",[1])

				begin
					$driver.find_element(:xpath, "//input[@value='Done']").click
					puts "Done clicked"
					sleep 3
				rescue
					puts "Done not found"
				end
			}
		else
			puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
			puts "Please Provide Requested Information screen not found."
			puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		end		
		puts "Exiting PleaseProvideRequestedInformation"
	end
	
	
	
	

	def General.ApproveDecisionNotice
		
		puts "Entering Approve Decision Notice"
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Approve Decision Notice")	
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Done']",10)
				else
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				begin
					$driver.find_element(:xpath, "//input[@value='Next']").click
					puts "Next clicked"
					sleep 3
				rescue
					puts "Next not found"
				end
			}
		else
			puts "Approve Decision Notice screen not found."
		end				
		puts "Exiting ApproveDecisionNotice"
	end
	
	
	
	
	
	def General.RecordDecision( planning_manager )
		puts "Entering  Record Decision"
		
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Record decision")	
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Done']",10)
				else
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
					sleep 2
					$driver.find_element(:xpath, "//input[@value='Next']").click
					puts "Next clicked"
					sleep 3
				rescue
					puts "Next not found"
				end
				
				if planning_manager != nil
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@id,'dropdownControl')]")).select_by(:text, planning_manager)
				else
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@id,'dropdownControl')]")).select_by(:index, 2)
				end
			}
		else
			puts "Record Decision screen not found."
		end		
		puts "Exiting RecordDecision"
	end
	
	
	
	
	
	def General.uploadTemplate
		puts "General upload Template."

		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5
		if Tools.clickLinkIfExists("Upload Template")	
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)		
				# Upload a file
				# Click Browse button
				puts "Click Browse button"
				$driver.find_element(:xpath, "(//*[@type='file'])[1]").click
				wait 3
				Clipboard.copy($aFile1)
				#puts "Copied: #{Clipboard.paste}"
				wait
				if File::exists?("#{$rubyHome}/Utils/fileUpload.sh")
					puts "Input filename to upload #{Clipboard.paste}"
					system("#{$rubyHome}/Utils/fileUpload.sh")
					puts "File uploaded."
				else
					puts "#{$rubyHome}/Utils/fileUpload.sh not found"
				end
				wait 4

				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@id,'dropdownControl')]")).select_by(:index, 2)
				puts "Planning manager selected."

				# Click Next button
				puts "Click Next button"
				$driver.find_element(:xpath, "//input[@value='Next']").click
				
				wait 4		
			}
		else
			puts "Upload Template screen not found."
		end
		puts "Exiting uploadTemplate."
	end
	
	
	
	
	
	
	def General.prepareToSend
		puts "Entering Prepare To Send"
		
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5
		if Tools.clickLinkIfExists("Prepare to send IR")	
		
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Done']",10)
				else
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				begin
					$driver.find_element(:xpath, "//input[@value='Done']").click
					puts "Done clicked"
					sleep 3
				rescue
					puts "Done not found"
				end
			}
		else
			puts "Prepare To Send screen not found."
		end
		puts "Exiting prepareToSend"
	end
	
	
	
	def General.wait(*waitTime)
		# This is a custom wait method for Tempo. 
		# It waits for 'Working..' to disappear

		# Sometimes we need to 'sleep' first
		if waitTime != nil
			sleep waitTime[0].to_i
		end

		found = false
		i = 1
		while not found
			if $driver.find_element(:xpath, "//*[@class='appian-indicator-message']").text.include? "Working..."
				if i == 1
					print "Working."
				else
					print "."
				end
			else
				puts "Finished working."
				found = true
			end
			i = i + 1
			sleep 1
			if i == 120 
				puts "Timeout waiting."
				exit
			end
		end
7	end # End wait

	



	
	def General.prepareDevelopmentApplication(devApp, schedule3)

		$applicationType = devApp.downcase

		puts "Entering PrepareDevelopmentApplication"
		puts "*****************************************"
		puts "Creating #{devApp} application type."
		puts "*****************************************"
		sleep 3

		# Tools.checkPageForText
		
		Tools.defaultFrame

=begin		
		if Tools.checkForScreen("Prepare Application Online")
			puts "Prepare Application Online link found"
=end
		
		link = "//*[contains(text(), 'Prepare Application Online')]"
		frames = $driver.find_elements(:xpath, "//iframe")
		# puts "Found #{frames.size} frames."
		i = 1
		found = false
		while not found
			frames.each do |frame|
				begin
					Tools.defaultFrame
					$driver.switch_to.frame(i)
					$driver.find_element(:xpath, link).click
					puts "Clicked 'Prepare Application Online' in frame #{i}"
					found = true
					break
				rescue
					puts "Prepare Application Online link NOT found yet."
				end
				i = i + 1
				if i == 100
					puts "Prepare Application Online link NOT found, quitting."
					exit
				end
			end
		end

		begin 
			sleep 3
			puts "Now to select Application Type of #{devApp}"

			Tools.defaultFrame
			$quick = false # Used by the method "developmentAspect"
			Tools.wait(:xpath, ".//*[@name='radioChange']", 5)
			devApp = devApp.downcase
			case devApp
			when "pre-lodgement", "prelodgement"
				$driver.find_element(:xpath, ".//*[@name='radioPrelodge']").click
				
			when "lodgement", "lodge" 
				$driver.find_element(:xpath, ".//*[@name='radioLodge']").click
				$quick = true			

			when "quick" # Quick lodgement
				$driver.find_element(:xpath, ".//*[@name='radioLodge']").click
				$quick = true

			when "refer", "referral"
				$driver.find_element(:xpath, ".//*[@name='radioRefer']").click
				
			when "pre-referral", "prereferral"
				$driver.find_element(:xpath, ".//*[@name='radioEarly']").click
							
			when "change", "extend", "cancel"
				$driver.find_element(:xpath, ".//*[@name='radioChange']").click

			else
				puts "Unknown Prepare a Development Application checkbox selected of: #{devApp}, quitting"
				exit
			end
			
			#Click proceed
			Tools.click(:xpath, "//input[contains(@value,'Proceed')]", 4)
		end
		
		puts "Exiting PrepareDevelopmentApplication"
	end # prepareDevelopmentApplication
	
	
	
	
	
	

	def General.prelodgementAdviceForm
		puts "Entering prelodgementAdviceForm"
		# Request For Pre-lodgement Advice Form
		sleep 1
		Tools.wait(:xpath, "//*[@id='radio_fd_component_JSON_TASK_NS_169radio43_1']", 15)
		
		# Tools.checkPageForText
		
		begin
			$driver.find_element(:xpath, "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[6]/div/div/div/fieldset/div[2]/div/span[2]/label/input").click
		rescue
			begin
				$driver.find_element(:xpath, "//*[@id='radio_fd_component_JSON_TASK_NS_125radio43_1']").click
			rescue
				begin
					$driver.find_element(:xpath, "//*[@id='radio_fd_component_JSON_TASK_NS_169radio43_1']").click
				rescue
					puts "Can't click Form of advice requested checkbox so quitting"
					exit
				end
			end

		end
		# Click Next button
		$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
		sleep 1
		# Click 'Continue' link
		Tools.click(:link, "Continue", 15)

		sleep 3

		puts "Exiting prelodgementAdviceForm"
	end 		
	
	
	
	

	def General.requestForPrelodgementAdviceForm
		puts "Entering requestForPrelodgementAdviceForm"
		sleep 1

		# Click 'Form of advice requested' = Meeting
		Tools.wait(:xpath, "(//*[@type='radio'])[1]", 5)
		$driver.find_element(:xpath, "(//*[@type='radio'])[1]").click

		# Tools.checkPageForText
		
		Tools.defaultFrame
		
		# Click Next button
		$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click

		sleep 1
		# Click 'Continue' link
		Tools.click(:link, "Continue", 15)

		sleep 3

		puts "Exiting requestForPrelodgementAdviceForm"
	end # requestForPre-lodgementAdviceForm


	




	def General.developmentAspect(*values)	
		puts "Entering Development Aspect"

		# Add Development Aspect
		sleep 4

		Tools.defaultFrame
		Tools.checkForScreen("Add development aspect")
		# Tools.checkPageForText
		Tools.defaultFrame
		
		if Tools.wait(:xpath,  "//input[contains(@value,'Add development aspect')]", 6)
			$driver.find_element(:xpath,  "//input[contains(@value,'Add development aspect')]").click
		else
			$driver.find_element(:xpath,  "//input[contains(@value,'Add Development Aspect')]").click
		end			

		# Wait for screen
		Tools.wait(:xpath, ".//*[@valueid='Building Work']", 7)				
		if values[0] != nil
			# What is the nature of the development?
			case values[0].downcase 
			when "material change of use"
				$driver.find_element(:xpath, ".//*[@valueid='Material Change of Use']").click
				puts "Material Change of Use selected" 

			when "operational work"
				$driver.find_element(:xpath, ".//*[@valueid='Operational Work']").click
				puts "Operational Work selected"

			when "reconfiguring a lot"
				$driver.find_element(:xpath, ".//*[@valueid='Reconfiguring a Lot']").click
				puts "Reconfiguring a lot selected"

			when "building work"
				$driver.find_element(:xpath, ".//*[@valueid='Building Work']").click
				puts "Building work selected"
			else
				puts "Unknown option of : #{values[0]}, so selecting Building Work."
				$driver.find_element(:xpath, ".//*[@valueid='Building Work']").click
			end

			# What is the approval type?
			case values[1].downcase
			when "preliminary approval"
				$driver.find_element(:xpath, ".//*[@valueid='Preliminary approval']").click
				puts "Preliminary approval selected"

			when "preliminary approval under s241 of395: spa"
				$driver.find_element(:xpath, ".//*[@valueid='Preliminary approval under s241 of SPA']").click
				puts "Preliminary approval under s241 of SPA selected"

			when "preliminary approval under s241 and s242 of spa"
				$driver.find_element(:xpath, ".//*[@valueid='Preliminary approval under s241 and s242 of SPA']").click
				puts "Preliminary approval under s241 and s242 of SPA selected"

			when "development permit"
				$driver.find_element(:xpath, ".//*[@valueid='Development permit']").click
				puts "Development permit selected."
		
			else
				$driver.find_element(:xpath, ".//*[@valueid='Development permit']").click
				puts "Unknown option of: #{values[1]}, so Development permit selected."
			end
		else
			puts "**************************************************"
			puts "Expected parameters but nil found, quitting"
			puts "**************************************************"
			exit
		end
		sleep 2

		# Provide a brief description of the proposal, including use definition and number of buildings or structures where applicable
		text = "This is a description"
		if values[2] != nil
			text = values[2]
		end
		$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]").send_keys text
		
		# What is the level of assessment?
		if values[3] != nil
			if values[3].downcase == "impact assessment"
				$driver.find_element(:xpath, ".//*[@valueid='Impact Assessment']").click
				puts "Impact Assessment selected"
			else	
				# Assume default of Code Assessment
				$driver.find_element(:xpath, ".//*[@valueid='Code Assessment']").click
				puts "Code Assessment selected"
			end
		else
			# Assume default of Code Assessment
			$driver.find_element(:xpath, ".//*[@valueid='Code Assessment']").click
			puts "Code Assessment selected"
		end

		$driver.find_element(:xpath, "//input[contains(@value,'Add')]").click
		
		Tools.click(:xpath, "//input[contains(@value,'Next')]", 7)
		sleep 5

		if $quick == false
			# Wait for IDAS Form 1 Application Details - Location Details
			Tools.wait(:xpath, ".//*[@name='text3']", 15)
			$driver.find_element(:xpath, ".//*[@name='text3']").send_keys "1000"
			$driver.find_element(:xpath, ".//*[@name='text4']").send_keys "building"
		
			$driver.find_element(:xpath, "(.//*[@name='anyCurrentApprovals'])[2]").click # No
			$driver.find_element(:xpath, "(.//*[@name='isOwnersConsentRequired'])[2]").click # No
			$driver.find_element(:xpath, "(.//*[@name='doesApplicantInvolveStateResource'])[1]").click # No
			$driver.find_element(:xpath, "(.//*[@name='truefalse20'])[2]").click # No
			$driver.find_element(:xpath, "(.//*[@name='isSupersededplanningscheme'])[2]").click # No

			Tools.click(:xpath, "//input[contains(@value,'Next')]")
			sleep 1		

			# IDAS Form 1 Application Details - Location Questions
			Tools.wait(:xpath, "(.//*[@valueid='None'])[1]", 7)
		
			for i in 1..20
				begin
					$driver.find_element(:xpath, "(.//*[@valueid='NONE'])[#{i}]").click # No	
				rescue
					break
				end
			end
			for i in 1..20
				begin
					$driver.find_element(:xpath, "(.//*[@valueid='None'])[#{i}]").click # No	
				rescue
					break
				end
			end

			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click

			if Tools.wait(:link, "Click here", 6) # For old code (Training)
				$driver.find_element(:link, "Click here").click
			end
		end
		
		sleep 2
		Tools.click(:link, "Continue", 6)

		puts "Exiting Development Aspect"
	end # DevelopmentAspect	

	
	
	
	
	def General.addContactDetails(customname, email)
		puts "Entering AddContactDetails"
		if customname != nil 
			$applicant = $applicant + customname
		end
		if email != nil 
			$email = email
		end
		
		sleep 5

		# Tools.checkPageForText
		
		Tools.defaultFrame	
				
		begin
			# Check for Internal user recieving the "Enter DSDIP received date" prompt
			if Tools.wait(:xpath, "//*[@type='date']", 5)
				$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
				puts "'Enter DSDIP received date' found"
			else
				puts "'Enter DSDIP received date' not found"
			end
		rescue
			
		end

		# IDAS Form 1 Application Details - Contact Details
		Tools.wait(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]", 15)		
		$driver.find_element(:xpath, ".//*[@name='text5']").send_keys $applicant
		$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]").send_keys $address
		$driver.find_element(:xpath, ".//*[@name='text10']").send_keys "Coffs"
		$driver.find_element(:xpath, ".//*[@name='text11']").send_keys "NSW"
		$driver.find_element(:xpath, ".//*[@name='text12']").send_keys "2021"
		$driver.find_element(:xpath, ".//*[@name='text13']").send_keys "Australia"
		$driver.find_element(:xpath, ".//*[@name='text21']").send_keys "021234567"
		$driver.find_element(:xpath, ".//*[starts-with(@name, 'email')]").send_keys $email
		$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
		puts "Exiting AddContactDetails"
	end # addContactDetails


	
	
	def General.gISInformation(lotNo, planTypeLotNo, lGA, skipLotPlanValidation)
		puts "Entering GISInformation"

		sleep 5
		
		#Tools.checkForScreen("IDAS Form 1 Application Details — GIS details")
		
		Tools.defaultFrame
		# Add Details for a lot

		Tools.click(:xpath, "//input[@value='Add details for a lot']")
		sleep 2

		# Wait for the Lot No to be displayed
		Tools.wait(:xpath, ".//*[@name='fieldLotNo']", 10)
		Tools.defaultFrame
		puts "*********************************"
		#LGA
		if lGA == nil
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[@name='fieldLGA']")).select_by(:index, 2)
		else
			puts "Using LGA of: #{lGA}"
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[@name='fieldLGA']")).select_by(:text, lGA.upcase)
		end

		# LotNo
		if lotNo == nil
			$driver.find_element(:xpath, ".//*[@name='fieldLotNo']").send_keys "68"  # Valid
		else
			puts "Using Lot no of: #{lotNo}"
			$driver.find_element(:xpath, ".//*[@name='fieldLotNo']").send_keys lotNo
		end

		# Plan type and plan no.*
		if planTypeLotNo == nil
			$driver.find_element(:xpath, ".//*[@name='fieldPlanType']").send_keys "RP164525" # Valid
		else
			puts "Using Plan Type/Plan No: #{planTypeLotNo}"
			$driver.find_element(:xpath, ".//*[@name='fieldPlanType']").send_keys planTypeLotNo
		end
		puts "*********************************"

		# Skip lot Plan Validation checkbox by using any value as a parameter
		if skipLotPlanValidation != nil
			puts "Skipping Lot Plan Validation"
			$driver.find_element(:xpath, ".//*[@name='checkbox10']").click
		end

		Tools.defaultFrame
		# Click Add button
		Tools.click(:xpath, "//input[contains(@value,'Add')]", 4)
		
		# Click Add a set of coordinates button
		Tempo.wait 6
		Tools.defaultFrame
		Tools.click(:xpath, "//a[contains(.,'Add a set of coordinates')]", 30)

		sleep 1

		# Add Coordinates
		Tools.defaultFrame
		$driver.find_element(:xpath, ".//*[@name='text20']").send_keys "021234567"
		$driver.find_element(:xpath, ".//*[@name='text21']").send_keys "021234567"
		$driver.find_element(:xpath, ".//*[@name='text22']").send_keys "021234567"
		$driver.find_element(:xpath, ".//*[@name='text23']").send_keys "021234567"
		$driver.find_element(:xpath, ".//*[@name='text24']").send_keys "021234567"
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@name='dropdown25']")).select_by(:text, "GDA94")
		$driver.find_element(:xpath, ".//*[@name='text26']").send_keys "021234567"

		# Submit button
		begin
			Tools.wait(:xpath, "//input[contains(@value,'Submit')]", 10)
			$driver.find_element(:xpath, "//input[contains(@value,'Submit')]").click
		rescue
			# Next Button
			puts "Submit not found so we'll try Next instead"
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
		end
		puts "Exiting GISInformation"
	end # gISInformation
	
	





	

	def General.getNewApplicationID
		puts "Entering getNewApplicationID"

		# Get the Application ID
		$appID = Tools.getNewApplicationID

		puts "Exiting getNewApplicationID"
	end






	
	
	def General.uploadAcknowledgementNotice
		puts "Entering Upload Acknowledgement Notice"

		sleep 2
		Tools.defaultFrame
		# Tools.checkPageForText

		Tools.wait(:xpath, ".//*[@src='/suite/components/toolbar/img/calendar.gif']", 5)
		# Add date
		Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")

#		$driver.find_element(:xpath, "//*[contains(@class,'datePickerDayIsToday')]").click
		appID = $driver.find_element(:xpath, ".//*[contains(@id,'formTitle_fd_component')]").text

		# Remove unwanted text.
		appID = appID.sub("Upload acknowledgement notice","")
		appID = appID.sub(" - ", "")
		appID = appID.lstrip # Remove leading whitespace
		appID = appID.rstrip # Remove trailing whitespace
		$appID = appID # Assign global variable
		puts "*******************************"
		puts "Application ID: #{$appID}"
		puts "*******************************"
		# Load some files
		Clipboard.copy($aFile1)
		puts "Copied: #{Clipboard.paste}"
		sleep 3
		Tools.loadFile(".//*[@name='$ifu_file']",[1])
		sleep 2
		Tools.defaultFrame

		Clipboard.copy($aFile2          )
		puts "Copied: #{Clipboard.paste}"
		sleep 1
		Tools.loadFile(".//*[@name='$ifu_file']",[2])
		sleep 2
		Tools.click(:xpath, "//input[contains(@value,'Continue')]",6)

		puts "Exiting Upload Acknowledgement Notice"

	end # Upload Acknowledgement Notice








	
	def General.shouldAnInformationRequestBeIssued( moreInfoNeeded, planningManager )
		puts "Entering shouldAnInformationRequestBeIssued"

		puts "Clicking Refresh"
		Tools.refreshClick
		sleep 4

		Tools.clickLinkIfExists("Should an information request be issued")
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			Tools.wait(:xpath, "//input[@value='Submit']", 4)

			if moreInfoNeeded != nil
				if moreInfoNeeded.downcase == "no"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				end
			end
			Tools.click(:xpath, "//input[@value='Next']", 4)
			sleep 2
	
			# Load file
			Tools.defaultFrame
			Tools.wait(:xpath, ".//*[@name='$ifu_file']", 10)
			Clipboard.copy($aFile1)
			Tools.loadFile(".//*[@name='$ifu_file']",[1])
			sleep 2

			# Choose Planning Manager
			if planningManager != nil
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:text, planningManager)
				puts "Using #{planningManager}"
			else
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:index, 1)
				puts "No Planning Manager supplied so using first one"
			end
			sleep 2
			Tools.click(:xpath, "//input[@value='Next']", 4)
			sleep 2
			
		}
		puts "Exiting shouldAnInformationRequestBeIssued"
	end	





	
	def General.resubmitApplication
		puts "Entering resubmitApplication"
		Tools.clickLinkIfExists("Resubmit Application")
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			# Select Add/Remove Triggers
			Tools.click(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset/div[2]/div/span[2]/label/input]",10)

			Tools.click(:xpath, "//input[@value='Proceed']", 10)
		}		
		puts "Exiting resubmitApplication"
	end # resubmitApplication
	




	def General.searchAddTrigger(*trigId)
		puts "Entering searchAddTrigger" 

		# Tools.checkPageForText
		
		trigId.each do |trigger|
			if trigger != nil

				Tools.defaultFrame

				Tools.wait(:xpath,  "//input[@value='Add new triggers']", 10)
				begin
					$driver.find_element(:xpath, "//input[@value='Add new triggers']").click
					sleep 3
				rescue
					puts "'Add new triggers' not found"
				end
				
				if Tools.isNumber(trigger) # Check if we have a Trigger
					puts "We have Trigger Reference: #{trigger}"
					$xpath = "(.//*[@type='text'])[1]"
				else # No, we have a description
					puts "We have a Trigger description: #{trigger}"
					$xpath = "(.//*[@type='text'])[2]" 
				end
				Tools.wait(:xpath, $xpath, 6)
				$driver.find_element(:xpath, $xpath).clear
				$driver.find_element(:xpath, $xpath).send_keys trigger
				sleep 1
				Tools.defaultFrame

				$driver.find_element(:xpath, "//input[@value='Search']").click
				sleep 3
				Tools.wait(:xpath, "//*[@class='emptyGridText pagingGridEmptyGridText']", 5)
				if not $driver.find_element(:xpath, "//*[@class='emptyGridText pagingGridEmptyGridText']").text.include? "No items available"
					Tools.defaultFrame

					# Select the first result's checkbox
					puts "Select the first result's checkbox"
					Tools.click(:xpath, "(.//*[@type='checkbox'])[2]", 5)
					$driver.find_element(:xpath, "//input[@value='Add']").click
					puts "********************"
					puts "Added #{trigger}"
					puts "********************"
				else
					# No Triggers found
					puts "*********************************"
					puts "Trigger #{trigger} not found"	
					puts "*********************************"
					Tools.grabScreenshotOfError("Yes", "Trigger #{trigger} not found")			
					$driver.find_element(:xpath, "//input[@value='Cancel']").click
				end
				Tools.click(:xpath, "//input[contains(@value,'Add')]", 10)
				
				Tools.wait(:xpath,  "//input[@value='Add new triggers']", 10)
				begin
					$driver.find_element(:xpath, "//input[@value='Add new triggers']").click
				rescue
					puts "'Add new triggers' not found"
				end

			else
				# No more triggers
				puts "No more triggers"
				sleep 2
				Tools.defaultFrame

				Tools.click(:xpath, "(//input[@value='Cancel'])[1]", 5)
				puts "Cancel clicked"
				break
			end
		end
		Tools.grabScreenshotOfError("No", "Triggers added")	
		puts "Exiting searchAddTrigger" 
	end








	def General.checkForFastrackTriggerNomination(*nominate)
		puts "Entering checkForFastrackTriggerNomination"

		# Tools.checkPageForText
		
		begin
			Tools.click(:xpath, "//input[contains(@value,'Nominate')]", 10)

			radioBtns = $driver.find_elements(:xpath, ".//*[@type='radio']")
			puts "Found #{radioBtns.size} Fast Track triggers."
			i = 1
			if nominate[0] == nil
				radioBtns.each do |radioBtn|
					Tools.defaultFrame
					Tools.wait(:xpath, "//input[contains(@value,'Nominate')]", 15)
					xpath = "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/tbody/tr[#{i}]/td/div/label/input"
					$driver.find_element(:xpath, xpath).click
					$driver.find_element(:xpath, "//input[contains(@value,'Nominate')]").click
					sleep 3

					# Load file
					Tools.defaultFrame
					Tools.wait(:xpath, ".//*[@name='$ifu_file']", 10)
					Clipboard.copy($aFile1)
					Tools.loadFile(".//*[@name='$ifu_file']",[1])
					sleep 2

					Tools.click(:xpath, "//input[contains(@value,'Submit')]", 8)

					sleep 5
					i = i + 1
				end
				sleep 3
				puts "Attempt to click Submit"
				Tools.click(:xpath, "//input[contains(@value,'Submit')]", 5)

				sleep 3
				# Wait for Appian "Have you finished nominating your triggers for fast track assessment?" dialogue to display
				Tools.click(:xpath, "//input[@value='Yes']", 15)

				puts "Clicked Yes on dialogue"
				sleep 2 
			else
				sleep 3
				puts "Attempt to click Submit"
				Tools.click(:xpath, "//input[contains(@value,'Submit')]", 20)
				puts "Clicked Submit on Fast Track trigger screen"

				# Wait for Appian "Have you finished nominating your triggers for fast track assessment?" dialogue to display
				Tools.click(:xpath, "//input[@value='Yes']", 15)
				puts "Clicked Yes on dialogue"
				sleep 2 
			end
		rescue
			begin
				sleep 3
				puts "Attempt to click Submit"
				Tools.click(:xpath, "//input[contains(@value,'Submit')]", 20)

				puts "Submit clicked on Fast Track trigger screen"
				# Wait for Appian "Have you finished nominating your triggers for fast track assessment?" dialogue to display
				Tools.click(:xpath, "//input[@value='Yes']", 15)
				puts "Clicked Yes on dialogue"

				sleep 2 
			rescue

			end
		end
		puts "Exiting checkForFastrackTriggerNomination"
	end # checkForFastrackTriggerNomination










	def General.displaytriggers
		puts "Entering displaytriggers"
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			
			# Tools.checkPageForText

			# Click the "Are the triggers identified complete and correct" checkbox
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			# Click 'No' to 'Do you wish to upload any further documentation'
			$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
			Tools.click(:xpath, "//input[contains(@value,'Submit')]")
			Tempo.wait 4
		}
		puts "Exiting displaytriggers"
	end



	
	
	def General.selectTriggerScreen(*trigId)
		puts "Entering selectTriggerScreen"

		popUp = false
		
		# Tools.checkPageForText
		
		# Check if we are already on the Triggers screen
		if Tools.wait(:xpath, "//input[contains(@value,'Add new triggers')]", 10)
			puts "'Add new triggers' button found"
			sleep 1
			begin
				$driver.find_element(:xpath, "//input[contains(@value,'Add new triggers')]").click
			rescue
				$driver.find_element(:xpath, "//input[contains(@value,'Add New Triggers')]").click
			end
		else
			sleep 5
			puts "Wait for popup trigger screen"
	
			# Check for popup window instead
			begin
				windows = $driver.window_handles # Get all window handles
				windows.each do |window|
					if $main_window != window
						@new_window = window
						puts "Found popup window."
						popUp = true
					end
				end
				$driver.switch_to.window(@new_window)
				popUp = true
			rescue
				# No popup window
				puts "No popup window"
			end
		end

		if popUp == false
			# Wait for 'Select from all triggers to add' screen to load
			Tools.defaultFrame
			Tools.wait(:xpath, "(.//*[@type='text'])[1]", 5)
		end		
	
		if not trigId.empty?
			searchAddTrigger(*trigId)	
		else
			# No triggers supplied so load some generic triggers
			puts "No triggers supplied so loading some generic triggers."
			searchAddTrigger("7.1.16","7.2.2","7.2.5")	
		end
		sleep 7
		# Click the Done button
		puts "Click Done"
		Tools.click(:xpath, "//input[@value='Done']", 10)
		sleep 2

		# Wait for MyDas "Have you finished adding triggers" dialogue to display
		begin
			$driver.find_element(:xpath, "//input[@value='Yes']").click
			puts "Click Yes on dialogue"
		rescue
			begin
				$driver.find_element(:xpath, "//input[@value='OK']").click
				puts "Clicked OK on dialogue"
			rescue
				puts "Couldn't find dialogue, quitting."
				exit
			end
		end
		sleep 5
		
		# Click the "Are the triggers identified complete and correct" checkbox
		if popUp == false
			begin
				Tools.click(:xpath, "(.//*[@type='radio'])[1]")
			rescue
				puts "No dialogue"
			end
		
			# Click the Next button
			puts "Click Next, Submit or Done button"
			begin
				$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click	
				puts "Next clicked"
			rescue
				begin
					$driver.find_element(:xpath, "//input[contains(@value,'Submit')]").click	
					puts "Submit clicked"
				rescue
					$driver.find_element(:xpath, "//input[contains(@value,'Done')]").click	
					puts "Done clicked"
				end
			end
			sleep 5

			# Check for Fast Track trigger nomination
			General.checkForFastrackTriggerNomination("Nominate")

			# Click the Proceed button
			begin 		
				Tools.click(:xpath, "//input[contains(@value,'Proceed')]", 15)
			rescue
				fastTrackClaimed = "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/thead/tr/th[4]/div/div"
				if $driver.find_element(:xpath, fastTrackClaimed).text == "fast Track claimed"
					$driver.find_element(:xpath, "//input[contains(@value,'Submit')]").click	
					puts "Claim Application as Fast Track displayed"
					sleep 3
					# Wait for Appian "Do you want to 'Submit' your claim for Fast Track triggers?" dialogue to display
					puts "Click Yes on dialogue"
					Tools.click(:xpath, "//input[@value='Yes']", 15)
	
					Tools.click(:xpath, "//input[contains(@value,'Proceed')]", 15)
				else
					puts "Proceed button not displayed and Claim Application as Fast Track not displayed."
				end
			end

			# Click the Click here link
			begin
				Tools.wait(:link, "Click here", 10)
				$driver.find_element(:link, "Click here").click
				puts "Click here link clicked."
			rescue
				begin
					$driver.find_element(:link, "Continue").click
					puts "Continue link clicked."
				rescue
					begin
						$driver.switch_to.frame "fContent"
						# Check if Fast Track nominated screen displayed
						fastTrackNominatedColumnHeading = "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/thead/tr/th[4]/div/div"
						puts "Check for FT heading"
						$driver.find_element(:xpath, fastTrackNominatedColumnHeading)
						puts "FT heading not found"
					rescue
						puts "Unknown error in selectTriggerScreen"
					end
				end
			end
		else
			puts "Using popup path"

			# Load some files
			Clipboard.copy($aFile1)
			Tools.loadFile(".//*[@name='$ifu_file']")

			# Click Next
			Tools.click(:xpath, "//input[contains(@value,'Next')]", 15)

			puts "Click: Are the triggers identified complete and correct?"
			Tools.click(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div[2]/div/span[1]/label/input")
			sleep 2
			
			Tools.click(:xpath, "//input[contains(@value,'Submit')]")

			Tools.click(:xpath, "//input[contains(@value,'Proceed')]")
		end
		
		puts "Exiting selectTriggerScreen"
	end # General.selectTriggerScreen
	
	



	
	def General.beginLodgement(*isSaraAM)
		puts "Entering beginLodgement"
		sleep 5

		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Begin Lodgement")
		sleep 5
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			Tools.click(:xpath, "//input[@value='Proceed']",2) 
			sleep 1
			begin
				Tools.click(:link, "Click here", 2)
			rescue
				# No click here link displayed
				puts "No click here link displayed"
			end

			if isSaraAM[0] != nil  # Is SARA the Assessment Manager?
				if isSaraAM[0].downcase == "yes" # Yes
					$driver.find_element(:xpath, ".//*[@valueid='SARAisAM']").click
					puts "SARA is assessment manager"
				else  # No
					$driver.find_element(:xpath, ".//*[@valueid='SARAisNOTAM']").click
					puts "SARA is NOT assessment manager"
				end	
			end

			begin
				$driver.find_element(:xpath, "//input[@value='Next']").click
				puts "Next button clicked"
			rescue
				puts "Begin Lodgement Window closed"
			end
			sleep 5
		}

		# Click Refresh Dashboard
		$driver.action.send_keys([:control, :home]).perform
		Tools.refreshClick
		sleep 5
		puts "Exiting beginLodgement"
	end # General.beginLodgement
	
	
	
	
	
	
	def General.selectPayItems
		puts "Entering selectPayItems (This one)"
		sleep 5
		puts "Click Refresh"
		Tools.refreshClick
		sleep 5
		puts "Refresh clicked"

		# Get the Application ID		
		puts "Get the Application ID"
		$appID = Tools.getNewApplicationID
		puts $appID

		Tools.clickLinkIfExists("Select Pay Items")
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Click "accept this task" link
			Tools.wait(:xpath, "//html/body/div[3]/span/div[2]/div/div/span/a", 5)
			begin
				$driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/span/a").click
			rescue
				puts "Accept this task link not found, ignoring it"
			end
		
			sleep 3 # NOTE: Need to wait before checking
		
			# Select first 6 fees
			Tools.wait(:xpath,  "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr/td/div/label/input", 5)   
			for i in 1..6      
				begin
					$driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{i}]/td/div/label/input").click
				rescue
					break
				end
			end
		
			# Click Next
			Tools.wait(:xpath, "//input[@value='Next']", 5)
			begin
				$driver.find_element(:xpath, "//input[@value='Next']").click
			rescue
				begin
					$driver.find_element(:xpath, ".//*[@class='checkbox'][1]").click
					$driver.find_element(:xpath, "//input[@type='submit']").click
					return
				rescue
					puts "No Next or Submit button found"
				end
			end

			if parameters[0] == "stop"
				puts "Stop parameter supplied so quitting now"
				$driver.close
				exit
			end
			sleep 5			
			Tools.wait(:xpath, ".//*[@valueid='creditCard']", 5)	
			if parameters[1] != nil
				puts "Payment type is: #{parameters[1].downcase}"
				case paymentType.downcase
				when "bpay"
					$driver.find_element(:xpath, ".//*[@valueid='BPAY']").click
					puts "BPAY selected"
				when "eft"
					$driver.find_element(:xpath, ".//*[@valueid='EFT']").click
					puts "EFT selected"
				when "credit card", "credit"
					$driver.find_element(:xpath, ".//*[@valueid='creditCard']").click
					puts "Credit Card selected"
				else
					$driver.find_element(:xpath, ".//*[@valueid='creditCard']").click
					puts "Default of Credit Card selected"
				end
			else
				$driver.find_element(:xpath, ".//*[@valueid='creditCard']").click
				puts "Payment type was nil so selecting Credit Card"
				exit
			end
			sleep 3

			$driver.find_element(:xpath, "//input[@value='Next']").click
			puts "Next clicked"
			sleep 1
			Tools.click(:xpath, "//input[@value='OK']")

			puts "Clicked OK on dialogue"
			sleep 1
			Tools.click(:xpath, "//input[@value='Pay and submit']")

			sleep 1
			Tools.click(:xpath, "//input[@value='OK']")

			puts "Clicked OK on dialogue"
			sleep 3

			# Pay by Credit Card
			begin
				Tools.click(:xpath, "//html/body/div[3]/div/div[2]/div/div/div/div/div[5]/div/ul/li/form/div/strong/input")
				sleep 1
				# Pay Now
				Tools.click(:xpath, "//html/body/div[3]/div/div[2]/div/div/div/div/div[2]/form/ol/li[6]/ul/li/strong/input")
				sleep 10
				$driver.close
			rescue
				
			end
		}
		puts "Exiting selectPayItems"
	end # General.selectPayItems
	
	
	
	
	
	def General.selectFormsToIssue(*forms)
		puts "Entering selectFormsToIssue"
		sleep 10
		Tools.refreshClick
		sleep 4
		
		# Click Select Forms to Issue
		if not Tools.clickLinkIfExists("Select Forms To Issue")	
			if not Tools.clickLinkIfExists("Select Forms To Issue")	
				Tools.clickLinkIfExists("Select Forms To Issue")	
			end
		end
		
		# Select Forms To Issue
		$main_window = $driver.window_handle # Store current window
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 4
			end

			if forms[0] != nil
				# TBD
			end
			if Tools.wait(:xpath, "//input[@value='Submit']", 15)
				$driver.find_element(:xpath, "//input[@value='Submit']").click		
			else
				Tools.checkForErrorDialogue(driver, "No Submit button found, so check for error")
				begin
					$driver.find_element(:xpath, "//input[@value='Submit']").click		
				rescue
					Tools.grabScreenshotOfError("Yes", "Submit button not found")
				end
			end
		}
		puts "Exiting selectFormsToIssue"
	end



	
	def General.clickUpdate
		puts "Entering clickUpdate"
		# Update the screen
		$driver.switch_to.window($main_window)
		
		sleep 2 # Wait for screen to load
		
		$driver.switch_to.frame("fContent")
		$driver.switch_to.frame("miniWebIframe_25")
		
		# Tools.checkPageForText
		
		begin 
			Tools.click(:link, "Update", 15)

			$driver.switch_to.default_content()
			
			# Ensure we are back on the main window
			$driver.switch_to.window($main_window)

			return true
		rescue
			puts "Update link not found."
			return false
		end
		puts "Exiting clickUpdate"
	end # clickUpdate


	
	


	def General.allocateCase(*caseOfficer)
		puts "Entering allocateCase"
		if caseOfficer[0] == nil
			caseOfficer[0] = $caseOfficer
		end
		officer = caseOfficer[0]
		
		sleep 2
		Tools.refreshClick
		sleep 7

		j = 1
		while not Tools.clickLinkIfExists("Allocate Case")	
			Tools.refreshClick
			sleep 7
			j = j + 1
			if j == 5 
				puts "xxxxxxxxxxxxxxxxxxxxxxxxx"
				puts "    Allocate Case not found"
				puts "xxxxxxxxxxxxxxxxxxxxxxxxx"
				exit
			end
		end
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
		
			# Tools.checkPageForText		
			
			# Activate the window
			Tools.activateWindow("Allocate")
			Tools.wait(:link, "accept this task", 5)
			begin
				$driver.find_element(:link, "accept this task").click
				puts "accept this task clicked"
			rescue
				puts "accept this task not found"
			end

			sleep 5

			begin
				# Activate the window
				Tools.activateWindow("Allocate")
				Tools.click(:xpath, "//input[@value='Next']")

				puts "Next button clicked."
				sleep 2
			rescue
				puts "No Next button found"
			end
			
			if officer != nil
				# Select Case Officer
				puts "Officer to use is: #{officer}"
			end

			# Activate the window
			Tools.activateWindow("Allocate")

			# Click Submit
			begin
				Tools.click(:xpath, "//input[@value='Submit']")
				puts "Submit clicked"
			end

			sleep 5
			puts "Allocate case done"
		}	
		puts "Exiting allocateCase"		
	end

	
	
	
	
	def General.assessPrelodgementInformation
		puts "Entering assessPrelodgementInformation"

		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Assess Pre-lodgement Information")
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				Tools.click(:xpath, "//input[@value='Next']",10)

			else
				# Wait for Radio button
				Tools.click(:xpath, "//input[@value='Next']", 10)

			end
			sleep 5
			$driver.find_element(:xpath, "//input[@value='Next']").click

			# Click ' No Meeting Required" checkbox
			Tools.click(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset/div[2]/div/span/label/input", 15)
			
			# Click Submit
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}	
		puts "Exiting assessPrelodgementInformation"
	end
	
	
	
	
	def General.selectN6T3Approver
		puts "Entering selectN6T3Approver"
		Tools.clickLinkIfExists("Select N6 T3 Approver")
		
		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				Tools.click(:xpath, "//input[@value='Next']",10)

			else
				# Wait for Radio button
				Tools.click(:xpath, "//input[@value='Next']", 10)

			end
			sleep 5
			$driver.find_element(:xpath, "//input[@value='Next']").click

			# Select Approver
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:index, 1)
			rescue
				begin
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "PlanningManager_SEQSouthGC_1")
				rescue	
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
				end
			end
			
			# Click Next
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}
		puts "Exiting selectN6T3Approver"
	end
	
	
	
	
	
	def General.selectN3TGApprover
		puts "Entering selectN3TGApprover"

		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Select N3 TG Approver")

		# Tools.checkPageForText
		
		windows = $driver.window_handles # Get all window handles
		
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				Tools.click(:xpath, "//input[@value='Next']",10)

			else
				# Wait for Radio button
				Tools.click(:xpath, "//input[@value='Next']", 10)

			end
			sleep 5
			$driver.find_element(:xpath, "//input[@value='Next']").click

			# Select Approver
			begin
				Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:index, 1)
			rescue
				begin
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "PlanningManager_SEQSouthGC_1")
				rescue	
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
				end
			end
			
			# Click Next
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}
		puts "Exiting selectN3TGApprove"
	end




	def General.provideN3TGApproval
		puts "Entering provideN3TGApproval"
		
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Provide N3 TG Approval")
		begin		
			sleep 4
	
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)
				
				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Next']",10)

				else
					# Wait for Radio button
					Tools.click(:xpath, "//input[@value='Next']", 10)

				end
				sleep 5
				$driver.find_element(:xpath, "//input[@value='Next']").click
			}
		rescue
			puts "New window not found."
		end
		puts "Exiting provideN3TGApproval"
	end





	
	def General.provideN6T3Approval
		puts "Entering provideN6T3Approval"
		Tools.clickLinkIfExists("Provide N6 T3 Approval")
				
		windows = $driver.window_handles # Get all window handles
		
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				Tools.click(:xpath, "//input[@value='Next']",10)

			else
				# Wait for Radio button
				Tools.click(:xpath, "//input[@value='Next']", 10)

			end
			sleep 5
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}
		puts "Exiting provideN6T3Approval"
	end
	
	
	
	
	
	def General.provideN14T9Approval
		puts "Entering provideN14T9Approval"
		Tools.clickLinkIfExists("Provide N14 T9 Approval")
				
		windows = $driver.window_handles # Get all window handles
		
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			
			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 3
				Tools.click(:xpath, "//input[@value='Next']",10)

			else
				# Wait for Radio button
				Tools.click(:xpath, "//input[@value='Next']", 10)

			end
			sleep 5
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}
		puts "Exiting provideN14T9Approval"
	end # provideN14T9Approval
	
	





	
	
	
	def General.reviewCase(caseOfficer)
		puts "Entering reviewCase"
		
		puts "Case Officer to use is '#{caseOfficer}'"
		
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Review Case")	
			sleep 10
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 20)
					$driver.find_element(:link, "accept this task").click
					sleep 2
				end
			
				$driver.switch_to.default_content()
				Clipboard.copy($aFile1)
				Tools.loadFile(".//*[@name='$ifu_file']")
				sleep 2
				#Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[contains(@class,'gwt-ListBox')]")).select_by(:index, 1)
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@id='dropdownControl_fd_component_JSON_TASK_NSdropdown3']")).select_by(:text, caseOfficer)
				puts "Case Officer selected"
				
				begin
					$driver.find_element(:xpath, "(//input[@value='Submit'])[1]").click			
					puts "Submit button clicked"
				rescue
					puts "Submit button not found"
					exit
				end
				sleep 5
			}
		else
			puts "%%%%%%%%%%%%%%"
			puts "Review Case link not found"
			puts "%%%%%%%%%%%%%%"
		end
		puts "Exiting reviewCase"
	end # reviewCase"

	
	
	def General.reviewCaseDecision
		puts "Entering Review Case Decision"
		
		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Review Case")
			sleep 10
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				# if Tools.wait(:link, "accept this task", 20)
				#	$driver.find_element(:link, "accept this task").click
				#	sleep 2
				# end
			
				# Select checkbox
				# To Be Done if required
				
				# Upload a file
				$driver.switch_to.default_content()
				Clipboard.copy($aFile1)
				Tools.loadFile(".//*[@name='$ifu_file']")
				sleep 2
				
				# Click Submit
				begin
					$driver.find_element(:xpath, "(//input[@value='Submit'])[1]").click			
					puts "Submit button clicked"
				rescue
					puts "Submit button not found"
					exit
				end
				sleep 5
			}
		end
		puts "Exiting reviewCaseDecision"
	end
	
	




	
	
	def General.reviewAndMaintainTriggers(*trigId)
		puts "Entering reviewAndMaintainTriggers"
		sleep  5
		
		Tools.defaultFrame
		Tools.refreshClick
		sleep 5

		if not Tools.clickLinkIfExists("Review and Maintain Triggers")
			Tools.refreshClick
			puts "Refresh clicked."
			if not Tools.clickLinkIfExists("Review and Maintain Triggers")
				Tools.refreshClick
				puts "Refresh clicked."
				Tools.clickLinkIfExists("Review and Maintain Triggers")
			end
		end
		sleep 8

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			sleep 1
			
			if Tools.wait(:link, "accept this task", 5)
				$driver.find_element(:link, "accept this task").click
				puts "Accept this task clicked."
				sleep 2
				begin
					$driver.find_element(:xpath, "//input[@value='Next']").click		
					sleep 4
				rescue
					puts "Next button not found. Is it still expected?"
				end
			end
			sleep 5
			
			if trigId[0] != nil
				puts "********************************"
				puts "Select provided triggers"
				puts "********************************"
 				triggers = trigId.compact # Remove all nil values from array
				triggers = triggers.sort  # Sort the triggers

				trigCount = triggers.length
				puts "Now to add #{trigCount} triggers."

				begin 
					Tools.click(:xpath, "//input[@value='Add new triggers']", 10)
				rescue
					$driver.find_element(:xpath, "//input[@value='Add new triggers']").click
				end
				sleep 2

				# Check if new Search functionality exists
				searchBox = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset[1]/div[2]/input"
				Tools.wait(:xpath, searchBox, 20)
				$driver.find_element(:xpath, searchBox)
				count = 1
				triggers.each { |trigger|
					Tools.wait(:xpath, searchBox, 10)
					puts "Adding trigger no. #{count}: #{trigger}"
					$driver.find_element(:xpath, searchBox).clear
					$driver.find_element(:xpath, searchBox).send_keys trigger
					$driver.find_element(:xpath, "//input[@value='Search']").click
					puts "Search clicked"
					sleep 3

					triggerChkBox = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div[2]/div[2]/table/tbody/tr/td/div/label/input"
					if Tools.wait(:xpath, triggerChkBox, 10) # Then results found
					
						$driver.find_element(:xpath, triggerChkBox).click
						puts "Checkbox checked"
						sleep 1
						begin
							$driver.find_element(:xpath, "//input[@value='Add']").click
							puts "Add clicked"						
						rescue
							$driver.find_element(:xpath, "//input[@value='Done']").click
							puts "Done clicked"						
							break
						end
						count = count + 1
						sleep 3
						begin 
							Tools.click(:xpath, "//input[@value='Add new triggers']", 10)

							puts "Add new triggers clicked" 
						rescue
							$driver.find_element(:xpath, "//input[@value='Add New Triggers']").click
							puts "Add New Triggers clicked" 
						end
						sleep 2
					else
						puts "#{trigger} not found"
					end
				}
				Tempo.wait 2
				
				puts "No more triggers to add"

				Tools.click(:xpath, "//input[@value='Cancel']", 4)
				sleep 3
			else
				puts "********************************"
				puts "No triggers provided"
				puts "********************************"

				# Click Add new triggers
				puts "Click Add new triggers"
				$driver.find_element(:xpath, "//input[@value='Add new triggers']").click
				sleep 4
				#puts "Select all triggers"
				puts "Select first trigger"
				Tools.wait(:xpath, "(.//*[@type='checkbox'])[2]", 10)
				$driver.find_element(:xpath, "(.//*[@type='checkbox'])[2]").click
				sleep 1
				$driver.find_element(:xpath, "//input[@value='Add']").click
				#$driver.find_element(:xpath, "//input[@value='Add New Triggers']").click
				sleep 10
			end
			
			begin
				$driver.find_element(:xpath, "//input[@value='Done']").click
				sleep 3
			rescue
				puts "Done button not found"
				exit				
			end
			begin
				# Wait for Appian 'Have you finished selecting triggers?' dialogue to display
				Tools.click(:xpath, "//input[@value='Yes']", 10)
				sleep 10
			rescue
				puts "************************************************"
				puts "Yes dialogue not found. Try continuing anyway"
				puts "************************************************"
			end

			# puts "Check for Fast Track triggers"
			# General.checkForFastrackTriggerNomination
		}
		puts "Exiting reviewAndMaintainTriggers"
	end # General.reviewAndMaintainTriggers






	def General.reviewAndMaintainTriggersPreRef
		puts "Entering reviewAndMaintainTriggersPreRef"

		Tools.refreshClick
		sleep 5

		i = 0
		while not Tools.clickLinkIfExists("Review and Maintain Triggers")
			sleep 1
			i = i + 1
			if i == 5
				break
			end
		end
		sleep 8

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		begin
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 5)
					$driver.find_element(:link, "accept this task").click
					puts "'accept this task' clicked."
					sleep 2
					begin
						$driver.find_element(:xpath, "//input[@value='Next']").click		
						puts "Next clicked."
						sleep 4
					rescue
						puts "Next button not found. Is it still expected?"
					end
				end

				found = false
				while not found
					begin
						$driver.find_element(:xpath, "//input[@value='Done']").click
						sleep 3
						found = true
					rescue
						puts "Done button not found yet"
						sleep 1
					end
					i = i + 1
					if i == 30
						puts "Done button not found, giving up."
						break
					end
				end
				
				if found == true
					begin
						# Wait for Appian 'Have you finished selecting triggers?' dialogue to display
						Tools.click(:xpath, "//input[@value='Yes']", 10)
						Tempo.wait 4
						Tempo.wait 4
					rescue
						puts "************************************************"
						puts "Yes dialogue not found. Try continuing anyway"
						puts "************************************************"
					end
					begin
						$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
					rescue
						puts "No 'No pre-referral response' found. Continuing anyway."
					end
					$driver.find_element(:xpath, "//input[@value='Submit']").click
					sleep 3
				end
			}
			
			begin
				# Perform the following if Identify Request Status is displayed
				$driver.find_element(:xpath, "(.//*[contains(@name,'radio3')])[2]").click
				puts "'No pre-referral response' selected"
				$driver.find_element(:xpath, "//input[@value='Submit']").click
				puts "Submit clicked"
				sleep 3
			rescue
			end
			
		rescue
			puts "Review and Maintain Triggers link not found."
		end
		puts "Exiting reviewAndMaintainTriggersPreRef"
	end # General.reviewAndMaintainTriggersPreRef
	





	
	def General.reselectPaymentItems
	
		# Click Reselect Payment Items
		Tools.clickLinkIfExists("Reselect Payment Items")

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			
			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				puts "Accept this task clicked"
				sleep 8
			end
			Tools.click(:xpath, "//input[@value='Submit']", 15)


			# Wait for MyDas "Confirm your selection" dialogue to display
			if Tools.wait(:xpath, "//input[@value='OK']", 15)
				puts "Click OK on dialogue"
				$driver.find_element(:xpath, "//input[@value='OK']").click
				sleep 5
			end
		}
		sleep 5
	end # General.reselectPaymentItems

	
	
	
	
	
	
	def General.uploadProperlyReferredChecklist(*properly_referred_yes_no)
		puts "Entering  uploadProperlyReferredChecklist"

		sleep 3
		Tools.defaultFrame
		if not Tools.clickLinkIfExists("Upload Properly")	
			Tools.defaultFrame
			if not Tools.clickLinkIfExists("Upload Properly")
				puts "Upload Properly not found"
				exit
			end
		end
		sleep 5
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 2
				$driver.find_element(:xpath, "//input[@value='Next']").click		
			else
				puts "No 'accept this task' found"
			end
			sleep 10
			Tools.activateWindow("Upload")

			if properly_referred_yes_no[0] != nil
				if properly_referred_yes_no[0].downcase = "no"
					# Tick No for "Is Properly Referred?"
					Tools.click(:xpath, "(.//*[@name='truefalse2'])[1]")
				else
					# Tick Yes for "Is Properly Referred?"
					$driver.find_element(:xpath, "(.//*[@name='truefalse2'])[0]")
				end
			else
				# Tick No for "Is Properly Referred?"
				$driver.find_element(:xpath, "(.//*[@name='truefalse2'])[1]")
			end

			sleep 2
			# Load a file
			Clipboard.copy($aFile1)
			sleep 1
			Tools.loadFile(".//*[@name='$ifu_file']")

			begin
				$driver.find_element(:xpath, "//input[@value='Next']").click	
				puts "Next button clicked"		
			rescue
				puts "Next button was not found. So we'll try Submit"
				$driver.find_element(:xpath, "//input[@value='Submit']").click			
				puts "Submit button clicked"
			end
			sleep 5
		}
		puts "Exiting  uploadProperlyReferredChecklist"
	end # General.uploadProperlyReferredChecklist


	

	

	def General.applicantAgreement
		puts "Entering Applicant Agreement"
		sleep 5
		puts "Click Refresh"
		Tools.refreshClick
		puts "Exit Refresh"
		sleep 10
		linkfound = false
		
		if not Tools.clickLinkIfExists("Applicant Agreement")
			puts "Click Refresh"
			Tools.refreshClick
			sleep 5
			if Tools.clickLinkIfExists("Applicant Agreement")
				sleep 3	
				linkfound = true
			end
		else
			linkfound = true
		end
		
		if linkfound == true
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end

			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Next']", 8)
				else
					puts "No 'accept this task' found"
				end
			}
			sleep 5
			begin
				$driver.find_element(:xpath, "//input[@value='Next']").click	
				puts "Next button clicked"		
			rescue
				puts "Next button was not found. So we'll try Submit"
				begin
					$driver.find_element(:xpath, "//input[@value='Submit']").click			
					puts "Submit button clicked"
				rescue
					puts "Submit not found."
				end
			end
		else
			puts "Applicant Agreement screen not found."
		end
		puts "Exiting Applicant Agreement"
	end	






	def General.uploadProperlyMadeChecklist
		puts "Entering upload Properly Made Checklist."
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5
		if Tools.clickLinkIfExists("Upload Properly")	
			sleep 3	
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Next']", 8)
				else
					puts "No 'accept this task' found"
				end
				sleep 10
				Tools.activateWindow("Upload")

				# Select: Properly Made - no acknowledgement notice is required
				Tools.click(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset/div[2]/div/span[2]/label/input", 15)

				sleep 2
				# Load a file
				Clipboard.copy($aFile1)
				sleep 1
				Tools.loadFile(".//*[@name='$ifu_file']")

				begin
					$driver.find_element(:xpath, "//input[@value='Next']").click	
					puts "Next button clicked"		
				rescue
					puts "Next button was not found. So we'll try Submit"
					$driver.find_element(:xpath, "//input[@value='Submit']").click			
					puts "Submit button clicked"
				end
				sleep 5
			}
		else
			puts "upload Properly Made Checklist screen not found"
		end
		puts "Exiting uploadProperlyMadeChecklist"
	end # General.uploadProperlyMadeChecklist




		
	def General.selectN14_T9Approver

		Tools.clickLinkIfExists("Select N14 T9 Approver")	
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 2
			end
			Tools.wait(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6", 15)
			puts "Approver is Timmy Planner"
			Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
			$driver.find_element(:xpath, "//input[@value='Next']").click					
		}
	end # General.selectN14_T9Approver
	
	
	
	
	
	
	
	def General.selectN35_T10Approver

		Tools.clickLinkIfExists("Select N35 T10 Approver")	
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				puts "Clicked Accept this task"
				sleep 8
			end
			
			# Activate window
			#Tools.activateWindow("N35")
			puts "Select the Approver: Timmy Planner"
			Tools.wait(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6", 15)
			Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
			$driver.find_element(:xpath, "//input[@value='Next']").click					
		}
	end # General.selectN35_T10Approver







	def General.selectN73_TXApprover( approver )
		puts "Entering selectN73_TXApprover"
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Select N73 TX Approver")		
		sleep 2
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					sleep 3
				rescue
					puts "Accept this task not found"
				end
			end
			if approver == nil
				puts "Select the Approver: Timmy Planner"
				Tools.wait(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6", 15)
				Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
			else
				puts "Select the Approver: " + approver
				Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, approver)				
			end

			Tools.click(:xpath, "//input[@value='Next']", 15)								

		}
		puts "Exiting provideN35T10Approver"
	end # selectN73_TXApprover







	def General.provideN73_TXApproval
		puts "Entering provideN73TXApproval"
		Tools.refreshClick
		sleep 5
		Tools.clickLinkIfExists("Provide N73 TX Approval")		
		sleep 2
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					sleep 3
				rescue
					puts "Accept this task not found"
				end
			end
			Tools.activateWindow("N35")
			Tools.click(:xpath, "//input[@value='Next']", 15)								

		}
		puts "Exiting provideN73TXApproval"
	end # provideN73_TXApproval



	
	
	
	
	def General.provideN35_T10Approval
		puts "Entering provideN35T10Approval"
		Tools.clickLinkIfExists("Provide N35 T10 Approval")		
		sleep 2
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					sleep 3
				rescue
					puts "Accept this task not found"
				end
			end
			Tools.activateWindow("N35")
			Tools.click(:xpath, "//input[@value='Next']", 15)								

		}
		puts "Exiting provideN35T10Approval"
	end # provideN35T10Approval
	
	

	
	
	def General.isMoreInformationNeeded
		puts "Entering isMoreInformationNeeded"
		Tools.clickLinkIfExists("Is More Information Needed")		

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 2
			end
			
			# Need to click NO here
			
			$driver.find_element(:xpath, "//input[@value='Next']").click					
		}
		puts "Exiting isMoreInformationNeeded"
	end # isMoreInformationNeeded
	
	
	

	

	def General.initialConsiderationOfResponses
		puts "Entering initialConsiderationOfResponses"
		Tools.clickLinkIfExists("Initial consideration of responses")		

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 3
			end
						
			Tools.click(:xpath, "//input[@value='Done']", 15)

		}
		puts "Exiting initialConsiderationOfResponses"
	end # initialConsiderationOfResponses





	def General.consolidateAndFinaliseResponses	
		# Log in as Allocation Officer
		puts "Entering consolidateAndFinaliseResponses"
		if Tools.clickLinkIfExists("Consolidate and finalise responses")		

			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				begin 
					Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					sleep 3
				rescue
					puts "Accept button not found, continuing anyway"
				end
							
				# Our Recommendation
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # We have No Requirements Relating To the Application
				puts "'We have No Requirements Relating To the Application' selected"
				
				# *Recommendation Where Preliminary Approval Is Sought
				$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click #  has no requirements for part of the application
				puts "'has no requirements for part of the application' selected"
				
				# Third Party Advice To Be Included?
				$driver.find_element(:xpath, "(.//*[@type='radio'])[9]").click # No
				puts "No selected"

				# Select Planning Manager
				Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, approver)				
				puts "Planning Manager selected" 
				
				Tools.click(:xpath, "//input[@value='Next']", 15)
				puts "Next clicked."
			}
		else
			puts "'Consolidate And Finalise Responses' link not found"
		end
		puts "Exiting consolidateAndFinaliseResponses"
	end # consolidateAndFinaliseResponses



	
	
	
	def General.reviewNoticePDF
		puts "Entering reviewNoticePDF"
		sleep 5
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		Tools.clickLinkIfExists("Review Notice PDF")		

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		begin
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				
				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					puts "Accept this task clicked."
					sleep 2
				end
				
				
				Tools.click(:xpath, "//input[@value='Next']", 15)
				puts "Next clicked"
			}
		rescue
			puts "Review Notice PDF link not found."
		end
		puts "Exiting reviewNoticePDF"
	end

	
	
	
	def General.technicalAgencyResponseRequested
		puts "Entering technicalAgencyResponseRequested"
		sleep 5
		Tools.refreshClick
		sleep 15
		Tools.refreshClick
		sleep 5
		
		i = 1
		# Perform the next until no more 'Technical Agency Response Requested' links exist.
		while Tools.clickLinkIfExists("Technical Agency Response Requested")

			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 5)
					$driver.find_element(:link, "accept this task").click
					sleep 3
				end
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				sleep 2
				begin
					$driver.find_element(:xpath, "//input[@value='Submit']").click
				rescue
					puts "Submit button not found"
					begin
						$driver.find_element(:xpath, "//input[@value='Next']").click
					rescue
						puts "Next button not found, so no idea what to do and am quitting."
						exit
					end
				end
				puts "#{i} link processed"
				i = i + 1
				sleep 5
			}
		end
		puts "Exiting technicalAgencyResponseRequested"
	end # technicalAgencyResponseRequested
	
	
	
	
	
	
	def General.approvalOfConcurrenceAgencyResponse
		puts "Entering approvalOfConcurrenceAgencyResponse"
		sleep 2
		Tools.clickLinkIfExists("Approval of Concurrence Agency Response")		

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			begin
				Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 3
			rescue
				puts "Accept not found, continuing"
			end

			Tools.click(:xpath, "//input[@value='Next']", 15)
			puts "Next clicked"
		}
		puts "Exiting approvalOfConcurrenceAgencyResponse"
	end # approvalOfConcurrenceAgencyResponse
		
	
	
	
	
	def General.reviewNotice
		puts "Entering reviewNotice"

		sleep 3
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		Tools.clickLinkIfExists("Review Notice")		

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				sleep 2
			end
			begin				
				Tools.click(:xpath, "//input[@value='Next']", 15)

			rescue
				begin
					$driver.find_element(:xpath, "//input[@value='Submit']").click					
					puts "Submit button clicked"
				rescue
					$driver.find_element(:xpath, "//input[@value='Next']").click					
					puts "Submit button not found but Next clicked instead."
				end
			end
		}
		puts "Exiting reviewNotice"
	end





	
	def General.formsUpdate
		puts "Entering Forms Update"
		sleep 3
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 2
		if Tools.clickLinkIfExists("Review Notice")		
			$driver.switch_to.window($main_window)
			Tools.refreshClick
			Tools.clickLinkIfExists("Forms Update")		

			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {		
				$driver.find_element(:xpath, "(//*[@class='checkbox'])[0]").click # Forms are completed
				puts "'Forms are completed' clicked"
				sleep 1 
				$driver.find_element(:xpath, ".//*[@class='submitButton']").click
				puts "Submit clicked."
			}
			sleep 1
		else
			puts "Forms Update not found, skipping"
		end
		puts "Exiting formsUpdate"
	end # General.formsUpdate
	
	
	
	
	
	def General.idasForm2
		puts "Entering IDAS Form 2"
		
		if Tools.checkForScreen("IDAS Form 2", 1)
			$driver.find_element(:xpath, ".//*[@class='submitButton']").click
			puts "Submit clicked."
			sleep 1
		end
		
		puts "Exiting idasForm2"			
	end # General.idasForm2
	
	
	


	
	

	
	def General.openApplication(anApp)
		puts "Entering openApplication"
		if anApp == nil
			anApp = $appID
		else
			$appID = anApp # Set this so the script 'On Exit' code knows the application used!
		end
		sleep 8
		puts "Attempt to open application: #{anApp}"

		found = false
		count = 0
		while not found
			puts "Now click it '#{anApp}'."
			Tools.wait(:link, anApp, 8)
			begin
				$driver.find_element(:link, anApp).click
				puts "Application: #{anApp} clicked."
				found = true
				# Wait for the MyDas dashboard to open
				Tools.wait(:xpath, ".//*[@id='pageTitle']", 10)
				puts "Application: #{anApp} opened."
				found = true
			rescue
				count = count + 1
				# Could not find it so Refresh.
				puts "Application not found so Refresh"
				begin
					Tools.defaultFrame
					$driver.find_element(:xpath, "(.//*[@src='/suite/components/toolbar/img/refresh.gif'])[1]").click
					sleep 1
					$driver.find_element(:xpath, "(.//*[@src='/suite/components/toolbar/img/refresh.gif'])[2]").click
					sleep 1
					$driver.find_element(:xpath, "(.//*[@src='/suite/components/toolbar/img/refresh.gif'])[3]").click
					sleep 1
				rescue
				end
			end

			# Tools.checkPageForText
			if count == 5
				puts "Can't find application so quiting"
				exit
			end
		end
		puts "Exiting openApplication"
	end # General.openApplication



	
	def General.getApplication(applicant2Find)
		puts "Entering getApplication"
		$reference = ""
		sleep 5
		
		# Tools.checkPageForText

		applicants = $driver.find_elements(:xpath, "//td[@class='asiGridTD1']")
		references = $driver.find_elements(:xpath, "//td[@class='asiGridTD0 rowWithCheckbox']")
		
		count = applicants.size
		#puts "Found #{count} Applications"
		
		for i in 0..count-1
			puts "#{applicants[i].text} = #{applicant2Find} ?"
			if applicants[i].text == applicant2Find
				#puts "Found #{references[i].text}."
				$reference = references[i].text.strip # Return the Application ID stripped of leading/trailing whitespace.
				puts "#{applicant2Find} has Reference of: #{$reference}"
				
				openApplication($reference)
				
				return $reference
			end
		end
		if $reference == "" 
			puts "Error: Could not find #{applicant2Find}"
			exit
		end
		puts "Exiting getApplication"
	end # General.getApplication			




	def General.proceedWithApplication
		puts "Enter ProceedWithApplication"
		
		$appID = Tools.getNewApplicationID

		if not Tools.clickLinkIfExists("Proceed with Application")
			Tools.clickLinkIfExists("Proceed with Application")
		end
		sleep 8

		begin
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText

				if Tools.wait(:xpath, "//input[@value='Proceed with no further changes']", 15)
					$driver.find_element(:xpath, "//input[@value='Proceed with no further changes']").click
				else
					$driver.find_element(:xpath, "//input[@value='Next']").click # Old code (training)
				end
				sleep 3
			}
			$driver.switch_to.window($main_window)
		rescue
			if Tools.wait(:xpath, "//input[@value='Proceed with no further changes']", 15)
				$driver.find_element(:xpath, "//input[@value='Proceed with no further changes']").click
			else
				$driver.find_element(:xpath, "//input[@value='Next']").click # Old code (training)
			end
			sleep 3
		end
		sleep 2
		puts "Exiting ProceedWithApplication"
	end	# ProceedWithApplication




	def General.oldSchoolTriggerAdd(*triggers)
		puts "Entering oldSchoolTriggerAdd"
		triggers = triggers.compact
		triggers = triggers.sort

		$driver.find_element(:xpath, "//input[@value='Next']").click
		sleep 2
		Tools.click(:xpath, "//input[@value='Add New Triggers']", 5)

		puts "Clicked Add New Triggers"
		sleep 2

		triggersFound = 0
		allTriggersFound = false
		count = 0
		
		# Tools.checkPageForText
		
		for k in 1..48 # 48 pages of triggers
			# Wait for triggers to display
			Tools.wait(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{k}]/td[2]/div", 5)

			# For each Trigger do
			trigCount = triggers.length
			for k in 3..51
				triggers.each { |trigger|
					i = 1
					for j in 1..10 # Items per page
						Tools.defaultFrame

						# Get trigger text, i.e 7.1.8
						begin
							aTrigger = $driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{i}]/td[2]/div").text
						rescue
							aTrigger = $driver.find_element(:xpath, "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{i}]/td[2]/div").text
						end
						if aTrigger == trigger
							puts "We found a trigger: #{aTrigger}."
							# Check the checkbox for current trigger
							begin
								$driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{i}]/td/div/label/input").click
							rescue
								$driver.find_element(:xpath, "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div[2]/table/tbody/tr[#{i}]/td/div/label/input").click
							end
							triggers = triggers - [triggers[i]] # Remove the found trigger from the array
							trigCount = trigCount - 1     # Update count of triggers
							if trigCount == 0
								puts "No more triggers to find."
								break
							else
								puts "Now we have #{trigCount} left to find."
							end
						else
							#puts "#{aTrigger} != #{trigger}"
						end
						i = i + 1 # Get the next trigger on the page
					end
					if trigCount == 0
						break
					end
				}

				begin
					$driver.find_element(:xpath, "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div/div/div/span[2]/a[#{k}]").click
				rescue
					$driver.find_element(:xpath, "//html/body/div/div/div[4]/div[3]/div/div/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div[2]/div/div/div/span[2]/a[#{k}]").click
				end
				sleep 2
				if trigCount == 0
					break
				end
			end
			if trigCount == 0
				break
			end
		end # For loop
		$driver.find_element(:xpath, "//input[@value='Next']").click		
		sleep 3
		puts "Exiting oldSchoolTriggerAdd"
	end	







	def General.reviewTriggers(*triggers)

		puts "Enter reviewTriggers"
		sleep 5		
		i = 1
		notFound = true
		while notFound
			if Tools.clickLinkIfExists("Review Triggers")
				notFound = false
			else
				i = i + 1
				if i == 20
					puts "Review Triggers link not found"
					exit
				end
			end
		end

		newWindow = false
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
				newWindow = true
				puts "Opened in new window."
			else
				puts "No new window found."
			end
		end

		if not newWindow
			Tools.defaultFrame
		else
			$driver.switch_to.window(@new_window)
			begin
				$driver.switch_to.frame "fContent"
			rescue 
			end
			puts "Switched to new window."
		end

		# Tools.checkPageForText
		
		# Check for Fast Track
		puts "Check for Fast Track"
		begin
			$driver.find_element(:xpath, ".//*[@value='Nominate']")
			puts "Nominate found"
			i = 1
			while i < 30
				begin
					$driver.find_element(:xpath, "(.//*[@type='radio'])[#{i}]").click
					puts "Selected Fast Track trigger"
					i = i + 1
				rescue
					puts "No more fast track triggers to click."
				end
			end
			$driver.find_element(:xpath, ".//*[@value='Nominate']").click
			puts "Nominate clicked"
			sleep 3
			if Tools.wait(:xpath, "(.//*[@name='$ifu_file'])[#{i}]", 15)
				sleep 1

				# Load some files
				Clipboard.copy($aFile1)
	
				Tools.loadFile("(.//*[@name='$ifu_file'])[#{i}]")
				puts "File #{i} loaded."
			else
				puts "File upload not found"
			end
			$driver.find_element(:xpath, ".//*[@class='submitButton']").click
			sleep 2
			# Wait for Appian "Have you finished nominating your triggers for fast track assessment?" dialogue to display
			puts "Wait for Appian 'Have you finished nominating your triggers for fast track assessment?' dialogue to display"
			Tools.click(:xpath, "//input[@value='Yes']", 15)
		rescue
			Tools.wait(:xpath, ".//*[@class='submitButton']", 8)		
			begin
				$driver.switch_to.frame "fContent"
			rescue
			end

			begin	
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Are the triggers identified complete and correct = No
			rescue
				$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click # Are the triggers identified complete and correct = No
			end
			puts "Are the triggers identified complete and correct clicked"
			$driver.find_element(:xpath, ".//*[@class='submitButton']").click
			puts "1. Submit button clicked."

			sleep 3
		end
				
		puts "Exiting reviewTriggers"
	end	# reviewTriggers
	




	
	def General.iDASForm3
		puts "Entering Open IDAS Form 3"
		sleep 2
		Tools.clickLinkIfExists("IDAS Form 3")
		sleep 5
		puts "Should be on IDAS Form 3 now"
		Tools.defaultFrame
			
		# Tools.checkPageForText
		
		Tools.wait(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset[2]/div[2]/input", 5)
		# Name of the Queensland heritage place
		$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset[2]/div[2]/input").send_keys "Parliament house"
		# Queensland Heritage Register number
		$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset[3]/div[2]/input").send_keys "123"
		
		# Add an item					           
		$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[3]/div/div/div/fieldset/div/div[2]/a").click
		sleep 1
		# Description of work
		$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[3]/div/div/div/fieldset/div/div[2]/div/table/tbody/tr/td/div/textarea").send_keys "Some text"
		# Estimated value
		$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[3]/div/div/div/fieldset/div/div[2]/div/table/tbody/tr/td[2]/div/input").send_keys "100000"
		
		# Click Next
		Tools.click(:xpath, "//input[contains(@value,'Next')]", 15)

		puts "Exiting Open IDAS Form 3"
	end # IDASForm3
	




	def General.uploadDocuments
		puts "Entering uploadDocuments"	

		newWindow = false
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
				newWindow = true
				puts "Opened in new window."
			else
				puts "No new window found."
			end
		end

		if not newWindow
			Tools.defaultFrame
		else
			$driver.switch_to.window(@new_window)
			begin
				$driver.switch_to.frame "fContent"
			rescue 
			end
			puts "Switched to new window."
		end
		
		## Tools.checkPageForText
		
        for i in 1..2 # Try to load 2 files
			if Tools.wait(:xpath, "(.//*[@name='$ifu_file'])[#{i}]", 15)
				sleep 1
	
				# Load some files
				Clipboard.copy($aFile1)
		
				Tools.loadFile("(.//*[@name='$ifu_file'])[#{i}]")
				puts "File #{i} loaded."
			else
 				puts "File #{i} NOT loaded, attempt to continue anyway."
			end
		end
		# Click Next
		Tools.click(:xpath, "//input[contains(@value,'Next')]", 15)
		puts "Next button clicked"
		sleep 10

		if newWindow
			$driver.switch_to.window($main_window)
		end

		puts "Exiting uploadDocuments"	
	end



	def General.uploadFormSupportingDocuments
		puts "Entering uploadFormSupportingDocuments"
		
		Tools.defaultFrame

		link = "//*[text()='Upload Form Supporting Documents']"
		frames = $driver.find_elements(:xpath, "//iframe")
		# puts "Found #{frames.size} frames."
		i = 1
		try = 0
		found = false
		while not found
			frames.each do |frame|
#				puts "Check if frame #{i} contains 'Upload Form Supporting Documents'."
				begin
					Tools.defaultFrame
					$driver.switch_to.frame(i)
					$driver.find_element(:xpath, link).click
					puts "Clicked 'Upload Form Supporting Documents'."
					found = true
					sleep 5
					break
				rescue
					sleep 1
				end
				i = i + 1
				if i == 30
					found = false
					break
				end
			end
		end
		
		if found == true
			puts "Now to select the documents"
			sleep 13 # Failed on 12
			Tools.defaultFrame
			
			# Click select all documents checkbox
			puts "Click select all documents checkbox"

			Tools.click(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset[2]/div/div[2]/div[2]/table/thead/tr/th/input", 15)
			
			# Click Upload button
			puts "Click Upload button"
			$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[4]/fieldset[3]/input").click

			sleep 3
			# Click File Upload dialogue
			Tools.defaultFrame

			Tools.wait(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset[2]/div[2]/form/ul/input", 15)
			sleep 1

			# Load some files
			Clipboard.copy($aFile1)
			
			Tools.loadFile(".//*[@name='$ifu_file']")

			# Click Next
			Tools.click(:xpath, "//input[contains(@value,'Next')]", 15)

			puts "Next button clicked"
			sleep 10

			# Click Done
			Tools.defaultFrame

			Tools.wait(:xpath, "//input[contains(@value,'Done')]", 15)
			begin		
				$driver.find_element(:xpath, "//input[contains(@value,'Done')]").click
				puts "1. Done button clicked"
			rescue
				begin
					$driver.find_element(:xpath, "//html/body/div/div/div[4]/span/div[2]/div/div/fieldset/form/ul/div[4]/fieldset[4]/input").click
					puts "2. Done button clicked"
				rescue
					puts "Couldn't find Done button by name or xpath"
					exit
				end
			end
		else
			puts "'Upload Form Supporting Documents' not found."
		end
		puts "Exiting uploadFormSupportingDocuments"
	end # uploadFormSupportingDocuments	




	


	
	def General.triggersRequiringFeeSelection(*all)
		puts "Enter triggersRequiringFeeSelection"
		# all = select all triggers, else first trigger chosen
		
		sleep 5
		begin
			Tools.refreshClick
			sleep 4
		rescue
			puts "Refresh Dashboard not found."
		end

		if not Tools.clickLinkIfExists("Triggers Requiring Fee Selection")
			Tools.refreshClick
			sleep 4
			Tools.clickLinkIfExists("Complete lodgement: Select fees and pay")
		else
			puts "'Triggers Requiring Fee Selection' link found."
		end
			
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			
			# Tools.checkPageForText

			if Tools.wait(:link, "accept this task", 5)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			sleep 3
			begin
				Tools.wait(:xpath, "(.//*[@type='radio'])[1]", 10)
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				sleep 1

				# Click Select Fees
				$driver.find_element(:xpath, "//input[@value='Select fees']").click
				puts "Select Fees clicked"
				sleep 2

				i = 0
				while not Tools.wait(:xpath, "//input[@value='Continue with application']", 8)
					begin
						if all[0] != nil
							$driver.find_element(:xpath, "(//*[@class='checkbox'])[0]").click # Select All
							puts "Selected all fees"
						else
							$driver.find_element(:xpath, "(//*[@class='checkbox'])[1]").click # Select first as no Select All button
							puts "Selected first fee only."
						end
					rescue
						begin
							$driver.find_element(:xpath, "(//*[@class='checkbox'])[1]").click # Select first as no Select All button
							puts "Selected first fee only."
						rescue
							puts "There is an issue selecting fees, so quitting."
							break
						end
					end
					puts "Fee selected"
					sleep 1
					$driver.find_element(:xpath, "//input[@value='Next']").click
					puts "Next clicked"
					Tempo.wait 2
				end
				begin
					$driver.find_element(:xpath, "//input[@value='Continue with application']").click
					puts "'Continue with application' clicked"
				rescue
					begin
						$driver.find_element(:xpath, "//input[@value='Next']").click
						puts "Next clicked."
					rescue
						puts "Next not found."
					end
					puts "'Continue with application' not found."
				end
			rescue
				begin
					puts "Attempt to click 'Complete payment' button."
					completePaymentBtn = "//input[@value='Complete payment']"
					Tools.wait(:xpath, completePaymentBtn, 10)
					$driver.find_element(:xpath, completePaymentBtn).click

					# Click OK on the dialogue
					puts "Click OK on the dialogue."
					$driver.find_element(:xpath, "//input[@value='OK']").click
					puts "Clicked OK on the dialogue."
					sleep 6
					return
				rescue
					puts "'Complete payment' button not found"
				end
			end
			sleep 6
		}
		$driver.switch_to.window($main_window)

		puts "Exiting triggersRequiringFeeSelection"
	end	# triggersRequiringFeeSelection








	def General.completeLodgementSelectFeesAndPay( *all )
		puts "Enter completeLodgementSelectFeesAndPay"
		sleep 10
		begin
			Tools.refreshClick
			sleep 4
		rescue
		end

		if not Tools.clickLinkIfExists("Triggers Requiring Fee Selection")
			Tools.clickLinkIfExists("Complete lodgement: Select fees and pay")
		end

		# Tools.checkPageForText
		
		# See Previous method above
		triggersRequiringFeeSelection()
		
		puts "Exiting completeLodgementSelectFeesAndPay"
	end	# completeLodgementSelectFeesAndPay







	def General.feePaymentForm(payMethod, *thirdParty)
		puts "Entering feePaymentForm"
		
		sleep 2
		puts "Clicking Refresh"
		Tools.refreshClick
		sleep 4
		
		# puts "Set default frame"
		Tools.defaultFrame
		
		if Tools.clickLinkIfExists("Fee Payment Form")
			# Test Credit Card details.
			visa = "4111111111111111"
			mastercard = "5555555555554444"
			month = "99"
			year = "16"
			cvn = "123"
			#creditCard = "visa"
			
			if payMethod != nil
				payMeth = payMethod.downcase
			else
				payMethod = "credit"
			end
			
			begin
				$driver.switch_to.window($main_window)
				Tools.refreshClick
				Tools.clickLinkIfExists("Fee Payment Form")
				sleep 5
			rescue
				puts "Fee Payment Form not found, attempt to continue anyway."
			end

			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 5)
					begin
						$driver.find_element(:link, "accept this task").click
						puts "Link of accept this task clicked"
						sleep 2
					rescue
						puts "Link of accept this task not found"
					end
				end
				sleep 2

				# Choose Payment Method
				Tools.wait(:xpath, ".//*[@valueid='EFT']", 10)
				case payMeth
				when "credit card", "credit"
					$driver.find_element(:xpath, ".//*[@valueid='creditCard']").click
					puts "Credit card selected"

				when "bpay"
					$driver.find_element(:xpath, ".//*[@valueid='BPAY']").click
					puts "BPay selected"

				when "eft"
					$driver.find_element(:xpath, ".//*[@valueid='EFT']").click
					puts "EFT selected"

				else
					puts "Unknown or no payment method, choosing Credit Card"
					$driver.find_element(:xpath, ".//*[@valueid='creditCard']").click
					puts "Credit card selected"
				end
				# Send Bill to third party
				if thirdParty[1] != nil
					if thirdParty[1].downcase == "no"
						$driver.find_element(:xpath, "(.//*[@name='radioThirdPartyBill'])[2]")
					else
						$driver.find_element(:xpath, "(.//*[@name='radioThirdPartyBill'])[1]")
						sleep 1
						# Name 
						$driver.find_element(:xpath, "(.//*[@name='thirdPartyName']").send_keys thirdParty[1]
						# Email 
						if thirdParty[2] != nil
							$driver.find_element(:xpath, "(.//*[@name='thirdPartyEmail']").send_keys thirdParty[2]
						else
							$driver.find_element(:xpath, "(.//*[@name='thirdPartyEmail']").send_keys $email
						end
					end
				else
					$driver.find_element(:xpath, "(.//*[@name='radioThirdPartyBill'])[2]")
				end

				# Not For Profit
				if thirdParty[3] != nil
					$driver.find_element(:xpath, "(.//*[@name='radioNpConcession'][1]").click
					sleep 1
					# Name of NFP
					$driver.find_element(:xpath, "(.//*[@name='NpName']").send_keys thirdParty[3]
					# ABN
					if thirdParty[4] != nil
						$driver.find_element(:xpath, "(.//*[@name='NpABN']").send_keys thirdParty[4]
					else
						$driver.find_element(:xpath, xpath).send_keys "123456789"
					end
				else
					#$driver.find_element(:xpath, "(.//*[@name='radioNpConcession'][2]").click
				end

				$driver.find_element(:xpath, "//input[@value='Next']").click
				puts "Next clicked"
				sleep 10

				# Wait for Appian "Are you sure you want to pay these fees using this method? Click 'OK' to continue or 'Cancel' to return to the form." dialogue to display
				Tools.wait(:xpath, "//input[@value='OK']", 15)
				begin
					$driver.find_element(:xpath, "//input[@value='OK']").click
					puts "Clicked OK on dialogue"
					sleep 3
				rescue
					puts "No dialogue displayed" 
				end
				Tools.click(:xpath, "//input[@value='Pay and submit']", 10)
				puts "Pay and submit clicked"
				
				if thirdParty[5] == nil # No parameter so we'll pay it
					# Go to payment gateway
						
					# Wait for Appian the dialogue:
					# "You are about to leave MyDAS and enter the Queensland Government payment gateway. 
					# Click 'OK' if you are ready to complete your payment or 'Cancel' to return to the form." 
					# dialogue to display
					Tools.click(:xpath, "//input[@value='OK']", 15)

					puts "Clicked OK on dialogue"
					sleep 10
					# Check for gateway error
					gateway_error = "There was a problem contacting the payment gateway. Please try again in a few minutes."
					begin
						if $driver.page_source.include? gateway_error
							puts gateway_error
							Tools.grabScreenshotOfError("Yes", gateway_error)
						else
							if payMeth == "credit card" or payMeth == "credit"				
								# Pay for item
								puts "Click 'Pay by Credit Card'."	
								payByXBtn = "(.//*[@type='submit'])[1]"
								Tools.click(:xpath, payByXBtn, 10)
								sleep 3

								# Payment Option (Visa or Mastercard)
								puts "Choose Payment Option (Visa or Mastercard)"
								payNowBtn = ".//*[@class='visa']"
								Tools.click(:xpath, payNowBtn, 10)
								sleep 3

								begin
									puts "Enter Credit Card Details"
									# Enter Credit Card number
									puts "Enter Credit Card number: " << visa
									Tools.click(:xpath, ".//*[@id='CardNumber']", 10)
									$driver.find_element(:xpath, ".//*[@id='CardNumber']").send_keys visa
									# Enter Month
									puts "Enter Month: " << month
									sleep 2
									$driver.find_element(:xpath, ".//*[@id='ExpiryMonth']").send_keys month
									# Enter Year
									puts "Enter Year: "  << year
									sleep 2
									$driver.find_element(:xpath, ".//*[@id='ExpiryYear']").send_keys year
									# Enter CVN
									puts "Enter CVN: " << cvn
									sleep 2
									$driver.find_element(:xpath, ".//*[@id='CVC']").send_keys cvn
									# Click 'Submit >>'
									sleep 1
									puts "Click 'Submit >>'"
									$driver.find_element(:xpath, "(.//*[@id='submitbutton'])[1]").click
								rescue
									puts "There was an issue entering card details, so quitting."
									exit
								end
								
								# Now wait for processing to finish
								puts "Now wait for processing to finish"
								processed = false
								i = 1
								while not processed
									begin
										if $driver.find_element(:xpath, "//*[text()='Payment successful']")
											puts "Payment successful found."						
											# Get payment ID
											paymentIDTxt = $driver.find_element(:xpath, "(//*[contains(@class,'inner')])[1]").text
											paymentIDTxt = paymentIDTxt.sub("Your receipt number is","")
											paymentIDTxt = paymentIDTxt.sub("Payment successful","")
											paymentIDTxt = paymentIDTxt.sub(" ","")
											paymentIDTxt = paymentIDTxt.sub(".","")
											$paymentID = paymentIDTxt.rstrip.lstrip
											puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
											puts "Payment ID is: '#{$paymentID}'"
											puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
											
											# close the @new_window
											$driver.close 
											processed = true			
										end
									rescue
										puts "'Payment successful' not found."
									end		
									if i == 1
										print "Waiting."
									else
										print "."
									end								
									sleep 1
									i = i + 1
									if i == 61
										puts "."
										puts "Should not be still processing payment after a minute, so quitting."
										exit
									end
								end
							
							else
							
								puts "BPay / EFT ready to be performed."
								# Get the new BPAY/EFT Reference number for use on the 'Confirm Payment Details' screen.
								#refXpath = "//html/body/div[3]/div/div[2]/div/div/div/div/div[3]/dl/dd[2]"
								#Tools.wait(:xpath, refXpath, 10)
								#$bPayEFTReference = $driver.find_element(:xpath, refXpath).text
							end

							# Grab screenshot of completion
							screenshot_name = $applicant.gsub!(/ /,'_') # Remove spaces from Applicant name
							$driver.save_screenshot("#{screenshot_name}.png")

							begin
								Tools.click(:link, "Return to online service", 90)
								puts "'Return to online service' clicked."
							rescue
							end
						
							# Close payment window
							$driver.close 
							puts "Payment window closed."
						end
					rescue
						puts "Payment window closed"
					end
				else
					pay = thirdParty[5].downcase
					if pay != "no"
						puts "Not paying as parameter of: #{pay} supplied."
					end
				end
			}
		else
			if Tools.clickLinkIfExists("No fees required")
				windows = $driver.window_handles # Get all window handles
				windows.each do |window|
					if $main_window != window
						@new_window = window
					end
				end
				
				i = 0
				notFound = true
				while notFound
					begin
						sleep 1
						puts "Switch to the new window."
						$driver.switch_to.window(@new_window) {
							# Click Close  (and popup window closes)
							$driver.find_element(:xpath, "//input[@value='Close']").click
							notFound = false
							puts "Close clicked."
						}
					rescue
						i = i + 1
						if i == 5
							puts "Close button not found."
							exit
						end
					end
				end
			else
				puts "'No fees required' not found."
			end
		end
		puts "Exiting feePaymentForm"
	end # feePaymentForm





	def General.confirmPaymentDetails( random ) # Now called 'Confirm Payment Status'
		# This is used after a BPay or EFT transaction
		puts "Entering 'Confirm Payment Details' / 'Confirm Payment Status'"

		if $paymentID == nil		
			$paymentID = "123456"
		end
		
		puts "*********************************"
		puts "Payment ID: " + $paymentID
		puts "*********************************"
			
		Tools.refreshClick
		sleep 5
		Tools.refreshClick
		sleep 5
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Confirm Payment Status")
		sleep 10
		
		if not Tools.checkForScreen("Confirm Payment Status") # No screen found so try continuing.
			begin
				windows = $driver.window_handles # Get all window handles
				windows.each do |window|
					if $main_window != window
						@new_window = window
					end
				end

				$driver.switch_to.window(@new_window) {
					puts "Move browser to the right so the terminal can be displayed on the left"
					$driver.manage.window.move_to($startX,$startY)

					# Tools.checkPageForText
					
					if Tools.wait(:link, "accept this task", 10)
						begin
							$driver.find_element(:link, "accept this task").click
							puts "Link of accept this task clicked"
							sleep 4
						rescue
							puts "Link of accept this task not found"
						end
					end
					begin
						$driver.find_element(:xpath, "(//*[@class='displayLabel'])[1]").click
						$driver.find_element(:xpath, "//input[@value='Next']").click
					rescue
						begin
							# Add Payment ID
							Tools.wait(:xpath, ".//*[@name='text3']", 5)
							$driver.find_element(:xpath, ".//*[@name='text3']").send_keys $paymentID

							# Add the current date to the form
							dateButton = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset/ul/li/fieldset/div[2]/span/span/label/a/img"
							Tools.addDate(dateButton) # No Date parameter means use today's date
							sleep 2

							# Finished
							$driver.find_element(:xpath, "//input[@value='Submit']").click
				#			$driver.save_screenshot("PaymentConfirmed-#{$paymentID}.png")
							puts "Payment Details confirmed."
						rescue
						end
					end
				}
				sleep 10
			rescue
				puts "'Confirm payment details' screen not found."
				exit
			end
		end
		puts "Exiting confirmPaymentDetails"
	end # confirmPaymentDetails






	def General.prepareToSendIR()
		puts "Entering Prepare To Send IR"

		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Prepare to send IR")
			sleep 2
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 10)
					begin
						$driver.find_element(:link, "accept this task").click
						puts "Link of accept this task clicked"
						sleep 2
					rescue
						puts "Link of accept this task not found"
					end
				end
				sleep 2

				$driver.find_element(:xpath, "//input[@value='Done']").click
				puts "Done clicked."
				sleep 2
			}
		else
			puts "%%%%%%%%%%%%%%%%%%%%%%%"
			puts "'Prepare to send IR' not found. Skipping"
			puts "%%%%%%%%%%%%%%%%%%%%%%%"
		end
		
		puts "Exiting  prepareToSendIR"
	end

	
	
	
	
	def General.selectTechnicalAgencies(technicalAgency, advice="No")
		puts "Entering selectTechnicalAgencies"

		Tools.refreshClick
		sleep 5

		if Tools.clickLinkIfExists("Select Technical Agenc")
			sleep 2
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				if Tools.wait(:link, "accept this task", 10)
					begin
						$driver.find_element(:link, "accept this task").click
						puts "Link of accept this task clicked"
						sleep 2
					rescue
						puts "Link of accept this task not found"
					end
				end

				
				if Tools.wait(:xpath, ".//*[@valueid='Yes']", 6)
					if advice != nil
						case advice.downcase
						when "yes"
							$driver.find_element(:xpath, ".//*[@valueid='Yes']").click
							puts "Advice"					
						when "doc"
							$driver.find_element(:xpath, ".//*[@valueid='doc']").click					
							puts "Advice with documentation"
						else
							$driver.find_element(:xpath, ".//*[@valueid='No']").click					
							puts "Skip advice"		
						end
										
						begin
							$driver.find_element(:xpath, ".//*[@valueid='No']").click					
							puts "'Skip advice' selected (as no parameter provided)"		
						rescue
							puts "No check boxes found but continuing anyway."
						end
					end
				end
				begin
					if technicalAgency != nil
						Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:text, technicalAgency)
					else	
						Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:index, 1)
						puts "No Technical Agency provided so using the first one listed"
					end
				rescue
					puts "Dropdown not found, ignoring"
				end
				begin
					$driver.find_element(:xpath, "//input[@value='Done']").click
					puts "Done clicked."
				rescue
					$driver.find_element(:xpath, "//input[@value='Submit']").click
					puts "Submit clicked."
				end
				sleep 2
			}
		else
			puts "%%%%%%%%%%%%%%%%%%%%%%%"
			puts "'selectTechnicalAgencies' not found. Skipping"
			puts "%%%%%%%%%%%%%%%%%%%%%%%"
		end
		
		puts "Exiting selectTechnicalAgencies"
	end




	def General.provideResponse
		puts "Entering provideResponse"
		sleep 3
		if Tools.clickLinkIfExists("Provide Response")
			sleep 3
			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 15)
					begin
						$driver.find_element(:link, "accept this task").click
						puts "Link of accept this task clicked"
						sleep 2
					rescue
						puts "Link of accept this task not found"
					end
				end
				
				begin
					# Assessment Provided
					dropDown = "/html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset[2]/div/div/div/fieldset/div[2]/label/select"
					Tools.wait(:xpath, dropDown,8)
					Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, dropDown)).select_by(:text, "Assessment Provided")
				rescue
				end
				
				begin
					# Load a file
					Clipboard.copy($aFile1)
					Tools.loadFile(".//*[@name='$ifu_file']")
					sleep 3
				rescue
				end
				
				$driver.find_element(:xpath, "//input[@value='Done']").click
				puts "Done clicked."
				sleep 3
			}
		else
			puts "'Provide Response' screen not found."
		end
		puts "Exiting provideResponse"
	end





	def General.identifyPrereferralResponseStatus(response)
		puts "Entering identifyPrereferralResponseStatus"
		sleep 3
		Tools.clickLinkIfExists("Identify Pre-referral Response Status")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end

			if response == nil # No parameter supplied
				response = "No pre-referral response"
				puts "No response supplied so using 'No pre-referral response' as default"
			end

			# Search for Response
			for i in 1..10
				radioTxt = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset/div[2]/div/span[#{i}]/label/span"
				radioBtn = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset/div[2]/div/span[#{i}]/label/input"
				begin
					Tools.wait(:xpath, radioTxt,)
					txt = $driver.find_element(:xpath, radioTxt).text.downcase
					puts "#{response.downcase} and #{txt}"
					if txt == response.downcase
						puts "#{response.downcase} == #{txt}"
						$driver.find_element(:xpath, radioBtn).click
						break
					else
						puts "#{response.downcase} != #{txt}"
					end
				rescue
					puts "Invalid Text: '#{txt}' or Invalid Response '#{response.downcase}'"
				end
			end
			$driver.find_element(:xpath, "//input[@value='Submit']").click
			sleep 3
		}
		puts "Exiting identifyPre-referralResponseStatus"
	end # identifyPre-referralResponseStatus





	def General.noFeesRequired
		puts "Entering noFeesRequired"
		sleep 3
		Tools.clickLinkIfExists("No Fees Required")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 15)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			Tools.click(:xpath, "//input[@value='Close']", 5)

		}
		puts "Exiting noFeesRequired"
	end






	def General.approvalChanges(approval_type, sARARole, applicationID, role, approvingDept, newRequest)
		puts "Entering approvalChanges"

		sleep 5
		# Tools.checkPageForText
		
		Tools.defaultFrame
		
		# Enter Original Application Number
		if applicationID == nil
			applicationID = "SDA-0515-024457"
		end
		puts "Entered Original Application Number of '#{applicationID}'."
		$driver.find_element(:xpath, ".//*[@name='appReference']").send_keys applicationID


		# Enter Original Approving department 
		if approvingDept == nil
			approvingDept = "Some department"
		end
		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			puts "(1) Couldn't find frame."
		end
		begin # Approving department found
			$driver.find_element(:xpath, ".//*[@name='originalApprovingDepartment']").send_keys approvingDept
			puts "Entered Original Approving department of '#{approvingDept}'."
		rescue
			puts "Original Approving department not required."
		end
		begin # Date field found
			Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")
		rescue
			puts "Date not required."
		end

		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			puts "(2) Couldn't find frame."
		end
		if role != nil
			begin
				puts "Try to set role of: '#{role}'"
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:text, role)
			rescue
				puts "Problem setting Role"
				exit
			end
		else
			Tools.dropDown(:xpath, ".//*[@data='dropdownControl']", :index, 2)
		end
		sleep 2

		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			puts "(3) Couldn't find frame."
		end
	
		if approval_type != nil
			case approval_type.downcase 
			when "cancel"
				$driver.find_element(:xpath, ".//*[@valueid='Cancel Approval']").click
				puts "Cancel Approval selected"

			when "extend"
				$driver.find_element(:xpath, ".//*[@valueid='Extend Approval']").click
				puts "Extend Approval selected"

 			when "change"
				$driver.find_element(:xpath, ".//*[@valueid='Change Approval']").click
				puts "Change Approval selected"
			else
				puts "No valid Approval type supplied so quitting."
				exit
			end
		else
			puts "No valid Approval type supplied so quitting."
			exit
		end
		sleep 2

		# SARA is Responsible entity
			if sARARole != nil
				begin
			
					case sARARole.downcase 
					when "responsible entity"
						$driver.find_element(:xpath, ".//*[@valueid='Responsible Entity']").click

					when "relevant entity"
						$driver.find_element(:xpath, ".//*[@valueid='Relevant Entity']").click

					when "assessment manager"
						$driver.find_element(:xpath, ".//*[@valueid='Assessment Manager']").click

					when "concurrence referral agency"
						$driver.find_element(:xpath, ".//*[@valueid='Concurrence Referral Agency']").click

					else
						$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
					end
				rescue
					puts "Sara Role of '#{sARARole}' not found."
				end
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[6]").click
			end
		sleep 1

		# Click Next
		$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
		puts "Next clicked"
		puts "Using '#{applicationID}'."
		sleep 2

		begin
			$driver.switch_to.default_content()
			$driver.switch_to.frame "fContent"
		rescue
			puts "(4) Couldn't find frame."
		end	
		if Tools.wait(:xpath, ".//*[@type='radio']", 5)
			$driver.find_element(:xpath, ".//*[@type='radio']").click	
		else
			puts "Radio button not found, continuing anyway."
		end
		$driver.find_element(:xpath, "//input[contains(@value,'Proceed')]").click
		sleep 4
		
		if approval_type.downcase == "cancel"
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
			sleep 4

			begin
				$driver.switch_to.default_content()
				$driver.switch_to.frame "fContent"
			rescue
				puts "(4) Couldn't find frame."
			end
			if newRequest == nil
				$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
				puts "'Initiate New Request' selected"
			else
				$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click
				puts "'Cancellation Notice From Assessment Manager' selected"
			end
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
			puts "Next clicked."
			sleep 4
		else 
			# Not Cancel type

			Tools.defaultFrame	
			# Check for Internal user recieving the "Enter DSDIP received date" prompt
			if Tools.wait(:xpath, "//*[@type='date']", 5)
				$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
			else
				puts "'Enter DSDIP received date' not found"
			end

			# Load a file
			Clipboard.copy($aFile1)
			Tools.loadFile("(.//*[@name='$ifu_file'])[1]")
			sleep 1

			# Click Next
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click	
			sleep 2
			begin
				Tools.wait(:link, "Click here", 2)
				$driver.find_element(:link, "Click here")
			rescue
				# No click here link displayed
				puts "No click here link displayed"
			end
		end
		puts "'#{approval_type}' finished"
		sleep 3
		
		puts "Exiting approvalChanges"
	end # approvalChanges








	def General.requestorDetails(*details)
		puts "Entering requestorDetails"
 
		if details[0] != nil
			name = $applicant + details[0]
		else
			name = $applicant
		end
		if details[1] != nil
			email = details[1]
		else
			email = $email
		end
		if details[7] != nil
			address  = details[7]
		else
			address = $address
		end
		if details[2] != nil
			suburb  = details[2]
		else
			suburb = "Burleigh Heads"
		end
		if details[3] != nil
			state  = details[3]
		else
			state = "Queensland" 
		end
		if details[4] != nil
			postcode  = details[4]
		else
			postcode = "4220"
		end
		if details[5] != nil
			country  = details[5]
		else
			country = "Australia"
		end
		if details[6] != nil
			phone  = details[6]
		else
			phone = "071234567"
		end

		sleep 3
		
		Tools.defaultFrame
		# Check for Internal user recieving the "Enter DSDIP received date" prompt
		if $driver.find_element(:xpath, "//*[@keytag='formTitleform0']").text.include? "Enter DSDIP received date"
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
			puts "Enter DSDIP received date found and Next clicked."
			sleep 2
		end
		Tools.defaultFrame
		Tools.wait(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]", 8)
		$driver.find_element(:xpath, ".//*[@name='text5']").send_keys name
		$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]").send_keys address
		$driver.find_element(:xpath, ".//*[@name='text24']").send_keys suburb
		$driver.find_element(:xpath, ".//*[@name='text11']").send_keys state
		$driver.find_element(:xpath, ".//*[@name='text12']").send_keys postcode
		$driver.find_element(:xpath, ".//*[@name='text13']").send_keys country
		$driver.find_element(:xpath, ".//*[@name='text21']").send_keys phone
		$driver.find_element(:xpath, ".//*[starts-with(@name, 'email')]").send_keys email
		puts "Details entered."
		
		$driver.find_element(:xpath, "//input[@value='Next']").click
		puts "Next clicked."
		sleep 1

		puts "Exiting requestorDetails"
	end # requestorDetails





	def General.approvalRequestDetails(devStatus, sARARole, changesRequested, changeRole, preResponseRequest, approvalDetails, extendDays, assessmentManager,managerEmail)
		puts "Entering approvalRequestDetails"
		sleep 5
		Tools.defaultFrame
		
		# Tools.checkPageForText
		
		# Development Status
		if devStatus != nil 
			if devStatus.downcase == "development commenced"
				$driver.find_element(:xpath, ".//*[@valueid='Development Commenced']").click
			end
		else
			$driver.find_element(:xpath, ".//*[@valueid='Development not commenced']").click
		end

		# SARA Role
		if sARARole != nil 
			if sARARole.downcase != "assessment manager"
				$driver.find_element(:xpath, ".//*[@valueid='Concurrence Referral Agency']").click
			end
		end

		# Extend days
		begin
			$driver.find_element(:xpath, ".//*[@name='number19']")
			if extendDays != nil
				$driver.find_element(:xpath, ".//*[@name='number19']").send_keys extendDays.to_i
			else
				$driver.find_element(:xpath, ".//*[@name='number19']").send_keys 15
			end
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[@data='dropdownControl'])[1]")).select_by(:index, 1)
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(.//*[@data='dropdownControl'])[2]")).select_by(:index, 1)
		rescue
			puts "'Enter the required extension period' not found"
		end
		
		# Change Requested
		begin
			$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]")
			if changesRequested != nil 
				$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]").send_keys changesRequested
			else
				$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph')]").send_keys "Some changes requested"
			end
		rescue
			# Field not found
		end

		# Change Role
		begin
			$driver.find_element(:xpath, ".//*[@valueid='Responsible Entity']")
			if changeRole != nil 
				if changeRole.downcase == "responsible entity"
					$driver.find_element(:xpath, ".//*[@valueid='Responsible Entity']").click
				else
					$driver.find_element(:xpath, ".//*[@valueid='Relevant Entity']").click
					sleep 1
					# New radio buttons appear.
					if preResponseRequest != nil 
						if preResponseRequest.downcase == "yes"
							$driver.find_element(:xpath, ".//*[@valueid='Yes']").click
						else
							$driver.find_element(:xpath, ".//*[@valueid='No']").click
						end
					else
						$driver.find_element(:xpath, ".//*[@valueid='No']").click
					end
				end
			end
		rescue
			# Field not found
		end

		# Approval Details
		if approvalDetails != nil 
			if approvalDetails.downcase != "mydas record exists"
				$driver.find_element(:xpath, ".//*[@valueid='Pre-SARA Application']").click
			end
		end
		begin
			if assessmentManager == nil 
				assessmentManager = "Not Defined"
			end
			$driver.find_element(:xpath, ".//*[@name='AssessmentManager']").send_keys assessmentManager
		rescue
			puts "'assessmentManager' field not found."
		end
		
		begin
			if managerEmail == nil
				managerEmail = "not@defined.com"
			end
			$driver.find_element(:xpath, ".//*[@name='assesmentManagerEmail']").send_keys managerEmail
		rescue
			puts "'assessmentManagerEmail' field not found."
		end

		begin
			Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")
		rescue
			puts "'date' field not found."
		end

		Clipboard.copy($aFile2)
		puts "Copied: #{Clipboard.paste}"
		sleep 1
		Tools.loadFile(".//*[@name='$ifu_file']",[1])

		$driver.find_element(:xpath, "//input[@value='Next']").click
		sleep 1
		
		# Click the Continue link
		Tools.wait(:link, "Continue", 15)
		begin
			$driver.find_element(:link, "Continue").click
		rescue
			begin
				puts "Continue button not found."
				$driver.find_element(:link, "Click here").click
			rescue
				puts "Click here not found."
			end		
		end
		puts "Exiting approvalRequestDetails"
	end # approvalRequestDetails





	def General.confirmation
		puts "Entering confirmation"

		sleep 3
		begin
			Tools.wait(:link, "Click here", 5)
			$driver.find_element(:link, "Click here").click
			puts "'Click here' link clicked successfully."
		rescue
			# No click here link displayed
			puts "No click here link displayed"
		end	
		# Tools.checkPageForText
		
		puts "Exiting confirmation"
	end




	def General.createEmail(firstname, surname, username)
		password = "appian@2015"

		openBrowser("firefox", "http://m.mail.com")
		Tools.click(:link, "Get your free email account")
		$driver.find_element(:xpath, ".//*[@tabindex='1']").send_keys firstname
		$driver.find_element(:xpath, ".//*[@tabindex='2']").send_keys surname
		$driver.find_element(:xpath, ".//*[@tabindex='8']").send_keys username # Email
		$driver.find_element(:xpath, ".//*[@tabindex='10']").send_keys password # password 1
		$driver.find_element(:xpath, ".//*[@tabindex='11']").send_keys password # password 2
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@tabindex='4']")).select_by(:index, 1) # day
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@tabindex='5']")).select_by(:index, 1) # month
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@tabindex='6']")).select_by(:index, 35) # year
		Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@tabindex='13']")).select_by(:index, 1) # What city born in?
		$driver.find_element(:xpath, ".//*[@tabindex='14']").send_keys "Brisbane"
	end












	def General.addBackUp(username)
		openBrowser("firefox", "http://m.mail.com")
		sleep 1
		$driver.find_element(:xpath, ".//*[@name='username']").send_keys username
		$driver.find_element(:xpath, ".//*[@name='password']").send_keys "Appian@2015"
		$driver.find_element(:xpath, ".//*[@id='btn-submit']").click
		sleep 5
		Tools.click(:xpath, ".//*[@title='E-mail']")
		Tools.click(:xpath, ".//*[@link='New folder']")
	end









	def General.getEmail(username, password, forwardToEmail)
		# This opens Mail.com, logs in, forwards the first email and 
		# moves the email to a Backup folder (leaving the Inbox empty).

		puts "Entering getEmail"
		if username == nil or password == nil or emailTo == nil
			puts "Either missing email, password or forwarding address."
			return
		end
		openBrowser("firefox", "http://m.mail.com")
		sleep 1
		$driver.find_element(:xpath, ".//*[@name='username']").send_keys username
		$driver.find_element(:xpath, ".//*[@name='password']").send_keys password
		$driver.find_element(:xpath, ".//*[@id='btn-submit']").click
		sleep 5
		puts "Now to open the email"
		# Inbox
		Tools.click(:xpath, "(.//*[starts-with(@href, './mails/folder?folderId')])[1]")

		sleep 2

		# First email
		Tools.wait(:xpath, "//html/body/form/section/article/div/a[1]")
		begin
			$driver.find_element(:xpath, "//html/body/form/section/article/div/a[1]").click

			# Forward
			puts "Forward the email"
			Tools.click(:xpath, "//html/body/section/nav/ul/li[3]/a")


			# Forward to email address
			puts "Add email address"
			$driver.find_element(:xpath, ".//*[@id='mail-to']").send_keys forwardToEmail
			$driver.find_element(:xpath, ".//*[@id='senden2']").click
			sleep 1
			puts "Back to inbox"
			Tools.click(:xpath, "(.//*[starts-with(@href, '../mails/folder?folderId')])[1]")

			# Backup email

			# Select first email
			puts "Select first email"
			$driver.find_element(:xpath, "//html/body/form/section/article/div/input").click
			sleep 1

			# Click Move To menu option
			$driver.find_element(:xpath, ".//*[@value='moveto']").click
			puts "Move To selected"

			# Click Start to move email
			$driver.find_element(:xpath, ".//*[@id='los']").click
			puts "Email moved"

			# Select Backup folder
			$driver.find_element(:xpath, "//html/body/nav/ul/li[7]/a").click
			puts "Backup folder selected"

			# Delete the email code
			# Back to Inbox
#			puts "Back to inbox"
#			Tools.wait(:xpath, "(.//*[starts-with(@href, '../mails/folder?folderId')])[1]")
#			$driver.find_element(:xpath, "(.//*[starts-with(@href, '../mails/folder?folderId')])[1]").click
#			sleep 2
			
			# Select first email
#			puts "Select first email"
#			$driver.find_element(:xpath, "//html/body/form/section/article/div/input").click
#			sleep 1

			# Delete first email
#			puts "Delete first email"
#			$driver.find_element(:xpath, ".//*[@id='del']").click

		rescue
			Tools.grabScreenshotOfError("email_issue")
			puts "Email not found or other error. See 'email_issue' screenshot."
			exit
		end

		sleep 3
		# Logout
		$driver.switch_to.default_content()
		$driver.find_element(:xpath, ".//*[@id='logout']/span").click
		
		puts "Exiting getEmail"
	end		





	def General.getPaymentID()
		# Not really needed so just return a default number!

		# Open the Payments tab and try double-clicking 
		# the Payment ID to capture it.

		# Click Refresh Dashboard
		#$driver.action.send_keys([:control, :home]).perform
		#Tools.refreshClick
		#sleep 5

	#	Tools.clickLinkIfExists("Payments")

		#if not Tools.clickLinkIfExists("Payments", false, "KeepGoing")
		#	Tools.clickLinkIfExists("Payment", false)
		#	puts "*** Payments tab not found but Payment tab found instead (Jira 4142)***"
		#end
	#	sleep 2

		# $ xdotool getmouselocation
	#	x = 68 
	#	init_y = 650 # Save original y for later use if required
	#	y = init_y

	#	Clipboard.copy '' # Empty the clipboard
	#	system("#{$rubyHome}/Utils/getPaymentID.sh", x.to_s, y.to_s) # try doubleclicking the payment ID
	#	$paymentID = Clipboard.paste
	#	$paymentID = $paymentID.to_i
	#	puts "Tried #{x.to_s} #{y.to_s}, got: '#{$paymentID}'."

	#	if $paymentID == 0 or $paymentID == " " # loop & move mouse until Payment ID found
	#		y = y + 10
	#		while $paymentID == 0  or $paymentID == " " #.to_s.length < 5
	#			system("#{$rubyHome}/Utils/getPaymentID.sh", x.to_s, y.to_s)
	#			puts "Tried #{x.to_s} #{y.to_s}, got: '#{$paymentID}'."
	#			$paymentID = Clipboard.paste
	#			$paymentID = $paymentID.to_i
	#			y = y + 10
	#			if y > (init_y + 160)
	#				puts "Unable to get Payment ID"
	#				exit
	#			end
	#		end
	#	end
	#end
		$paymentID = "123456"
		puts "****************************"
		puts "Payment ID: '#{$paymentID}'"
		puts "***************************"
		return $paymentID
	end




	def General.confirmPaymentStatus()
		# This is used after a BPay or EFT transaction
		puts "Entering confirmPaymentStatus"

		if $paymentID == nil
			General.getPaymentID
		end
		sleep 5
		if Tools.clickLinkIfExists("Confirm Payment Status")
			sleep 10

			windows = $driver.window_handles # Get all window handles
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)
				
				# Tools.checkPageForText
				
				sleep 4
				Tools.click(:link, "accept this task", 10)
				Tools.click(:xpath, "(.//*[@name='fieldIsPaymentMade'])[1]")
				Tools.click(:xpath, "//input[contains(@value,'Next')]")
				sleep 2

				# Add BPAY/EFT Reference
				Tools.wait(:xpath, ".//*[@name='text3']", 5)
				$driver.find_element(:xpath, ".//*[@name='text3']").send_keys $paymentID

				# Add the current date to the form
				dateButton = "//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset/ul/li/fieldset/div[2]/span/span/label/a/img"
				today = Tools.currentDate
				Tools.addDate(dateButton, today)

				# Finished
				$driver.find_element(:xpath, "//input[@value='Submit']").click
				$driver.save_screenshot("PaymentConfirmed-#{$paymentID}.png")
				puts "Payment Details confirmed."
			}
		else
			puts "%%%%%%%%%%%%%%%%%%"
			puts "'Confirm Payment Status' not found"
			puts "%%%%%%%%%%%%%%%%%%"
		end
		puts "Exiting confirmPaymentStatus"
	end # confirmPaymentStatus





	def General.enterDSDIPRecievedDate(new_date)
		puts "Entering enterDSDIPRecievedDate"
		sleep 1

		Tools.defaultFrame
		
		# Tools.checkPageForText
		
		# Check for Internal user recieving the "Enter DSDIP received date" prompt
		begin
			if Tools.wait(:xpath, "//*[@type='date']", 2)
				$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
				puts "Found 'enter DSDIP Recieved Date', clicking Next"
			end	
		rescue
			$driver.find_element(:xpath, "//input[contains(@value,'Next')]").click
		end

		puts "Exiting enterDSDIPRecievedDate"
	end






	def General.proceedToPayment( sARAIsTheAssessmentManager )
		puts "Entering proceedToPayment"
		sleep 3
		begin
			Tools.clickLinkIfExists("Proceed to payment")
		rescue
			# possibly a new window already open
			puts "'Proceed to payment' not found, quitting."
			exit
		end
		sleep 3

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			Tools.click(:xpath, "//input[contains(@value,'Proceed')]")
		
			# SARA is the Assessment Manager
			puts "*********************************"
			if sARAIsTheAssessmentManager != nil
				if sARAIsTheAssessmentManager.downcase == "yes" # Yes
					Tools.click(:xpath, ".//*[@valueid='SARAisAM']", 6)
					puts "SARA is the Assessment Manager"
				else  # No
					Tools.click(:xpath, ".//*[@valueid='SARAisNOTAM']", 6)
					puts "SARA is NOT the Assessment Manager"
				end	
			end
			puts "*********************************"
			Tools.click(:xpath, "//input[contains(@value,'Next')]", 6)
		}
		puts "Exiting proceedToPayment"
	end





	def General.recordNativeTitleAssessment(*type)
		puts "Entering recordNativeTitleAssessment"
		sleep 10
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		sleep 5
		Tools.refreshClick

		Tools.clickLinkIfExists("Record native title assessment")
		sleep 5

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			begin
				Tools.wait(:link, "accept this task", 15)
				$driver.find_element(:link, "accept this task").click
				puts "Accept clicked"
			rescue
				puts "Link of 'accept this task' not found. Try continuing."
			end
			
=begin			
			if type[0] != nil 
				case type[0].downcase
				when "24ha"
					Tools.click(:xpath, ".//*[@valueid='24HA']", 2)
					Tools.click("//html/body/div[3]/span/div[2]/div/div/fieldset/form/ul/div[2]/div/fieldset/div/div/div/fieldset[3]/div/div/div/fieldset/div/div[2]/a", 2)
#TB Finished
				when "24ka"
					Tools.click(:xpath, ".//*[@valueid='24HA']", 2)
#TB Finished
				when "other"
					Tools.click(:xpath, ".//*[@valueid='other']", 2)
#TB Finished
				else
					Tools.click(:xpath, ".//*[@valueid='no']", 2)
				end
			else
				Tools.click(:xpath, ".//*[@valueid='no']", 2)
			end
			Tools.click(:xpath, "//input[contains(@value,'Submit')]")
			sleep 2
=end
			$driver.find_element(:xpath, "(.//*[@type='radio'])[4]").click
			sleep 2

			# Submit button
			begin
				Tools.wait(:xpath, "//input[contains(@value,'Submit')]", 10)
				$driver.find_element(:xpath, "//input[contains(@value,'Submit')]").click
			rescue
				puts "Submit not found"
			end
			sleep 3
		}
		puts "Exiting recordNativeTitleAssessment"
	end 







	def General.shouldAnInformationRequestBeIssuedToTheApplicant(type)
		puts "Entering shouldAnInformationRequestBeIssuedToTheApplicant"
		sleep 10
		$driver.switch_to.window($main_window)
		Tools.refreshClick
		Tools.clickLinkIfExists("Should an information request be issued to the applicant?")
		sleep 3

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			Tools.click(:link, "accept this task", 10)
			if type != nil 
				case type.downcase
				when "yes"
					Tools.click(:xpath, "(.//*[@type='radio'])[1]", 2)
				else # No
					Tools.click(:xpath, "(.//*[@type='radio'])[2]", 2)
				end
			else # No
				Tools.click(:xpath, "(.//*[@type='radio'])[2]", 2)
			end
			Tools.click(:xpath, "//input[contains(@value,'Next')]")
			sleep 4
		}
		puts "Exiting shouldAnInformationRequestBeIssuedToTheApplicant"
	end







	def General.provideLegacyRecordsAndAdvice(text)
		puts "Entering provideLegacyRecordsAndAdvice"
		sleep 3
		Tools.clickLinkIfExists("Provide Legacy records and Advice")
		sleep 3

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end

			sleep 2
			if text == nil
				text = "This a note."
			end
			$driver.find_element(:xpath, ".//*[starts-with(@name, 'plainTextControl_paragraph4')]").send_keys text
			Tools.click(:xpath, "//input[contains(@value,'Proceed')]")
		}
		puts "Proceed clicked"
		sleep 2
		puts "Exiting provideLegacyRecordsAndAdvice"
	end






	def General.validateRequest(*parameters)
		puts "Entering validateRequest"
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Validate Request")
		sleep 3

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)
			
			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			# Check List - Title Search Performed
			if parameters[0] != nil
				if parameters[0].downcase == "yes" 
					Tools.click(:xpath, ".//*[@valueid='Title Search Performed']", 2)
				end
			end

			# Check List - Bond Paid 
			if parameters[1] != nil
				if parameters[1].downcase == "yes" 
					Tools.click(:xpath, ".//*[@valueid='Bond Paid']", 2)
				end
			end

			# Development Activity
			if parameters[2] != nil
				if parameters[2].downcase == "yes" 
					Tools.click(:xpath, ".//*[@valueid='Started']", 2)
				else
					Tools.click(:xpath, ".//*[@valueid='Not Started']", 2)
				end
			end

			# Add today's date
			Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")
		
			# Owner's Consent
			if parameters[3] != nil
				if parameters[3].downcase == "yes" 
					Tools.click(:xpath, ".//*[@valueid='Confirmed']", 2)
				else
					Tools.click(:xpath, ".//*[@valueid='Not Confirmed']", 2)
				end
			end

			# Is Request appropriately made?
			if parameters[4] != nil
				if parameters[4].downcase == "yes" 
					Tools.click(:xpath, ".//*[@valueid='yes']", 2)
				else
					Tools.click(:xpath, ".//*[@valueid='no']", 2)
				end
			end

			Tools.click(:xpath, "//input[contains(@value,'Next')]")
		}
		sleep 2
		puts "Exiting validateRequest"
	end





	def General.selectApprover(approver)
		puts "Entering selectApprover"
		sleep 3
		Tools.clickLinkIfExists("Select Approver")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if approver != nil
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:text, approver)
			else
				Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, ".//*[@data='dropdownControl']")).select_by(:index, 1)
				puts "No Approver provided so using the first one listed"
			end
			sleep 1
			Tools.click(:xpath, "//input[contains(@value,'Next')]")
		}
		sleep 2
		puts "Exiting selectApprover"
	end




	def General.provideApproval(approveYesNo)
		puts "Entering provideApproval"

		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists("Provide Approval")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			if approveYesNo != nil
				if approveYesNo.downcase == "no"
					$driver.find_element(:xpath, "(.//*[@type='radio'])[2]").click # No
				else
					$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click # Yes
				end
			end
			sleep 1
			Tools.click(:xpath, "//input[contains(@value,'Submit')]")
		}
		puts "Exiting provideApproval"
	end



	

	def General.uploadTechnicalResponseDocument
		puts "Entering uploadTechnicalResponseDocument"

		Tools.refreshClick
		sleep 3

		Tools.clickLinkIfExists("Upload Technical Response Document")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			$driver.find_element(:xpath, "//*[contains(@name,'plainTextControl_paragraph')]").send_keys "This is a note."
			sleep 1
			Tools.click(:xpath, "//input[contains(@value,'Proceed')]")
		}
		puts "Exiting uploadTechnicalResponseDocument"
	end





	def General.changeApprovalOutcome
		puts "Entering changeApprovalOutcome"

		Tools.refreshClick
		sleep 3

		Tools.clickLinkIfExists("Change Approval Outcome")
		sleep 3
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			$driver.find_element(:xpath, "(.//*[@type='radio'])[1]").click
			sleep 1
			Tools.click(:xpath, "//input[contains(@value,'Close')]")
		}
		puts "Exiting changeApprovalOutcome"
	end






	def General.extendedApprovalOutcome
		puts "Entering extendedApprovalOutcome"
		Tools.refreshClick
		sleep 3

		Tools.clickLinkIfExists("Extended Approval Outcome")
		sleep 3

		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			if Tools.wait(:link, "accept this task", 10)
				begin
					$driver.find_element(:link, "accept this task").click
					puts "Link of accept this task clicked"
					sleep 2
				rescue
					puts "Link of accept this task not found"
				end
			end
			# Expiry date
			puts "Enter expiry date"
			Tools.wait(:xpath, ".//*[@src='/suite/components/toolbar/img/calendar.gif']", 5)
			# Add date
			Tools.addDate(".//*[@src='/suite/components/toolbar/img/calendar.gif']")


			sleep 1
			Tools.click(:xpath, "//input[contains(@value,'Next')]")
		}
		puts "Exiting extendedApprovalOutcome"
	end



	def General.selectApprover( linkName, appover )
		puts "Entering selectApprover"
		sleep 3

		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists( linkName )
		
		windows = $driver.window_handles # Get all window handles
		windows.each do |window|
			if $main_window != window
				@new_window = window
			end
		end
		$driver.switch_to.window(@new_window) {
			puts "Move browser to the right so the terminal can be displayed on the left"
			$driver.manage.window.move_to($startX,$startY)

			# Tools.checkPageForText
			
			begin
				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Next']",10)
				else
					# Wait for Next button
					Tools.click(:xpath, "//input[@value='Next']", 10)
				end
			rescue
				puts "Link of 'Accept this task' not found.'"
			end
			sleep 5
			begin
				$driver.find_element(:xpath, "//input[@value='Next']").click
			rescue
				puts "Next button not found."
			end

			# Select Approver
			begin
				if approver != nil
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, approver)
				else
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:index, 1)
				end
			rescue
				begin
					Selenium::WebDriver::Support::Select.new($driver.find_element(:id, "dropdownControl_fd_component_JSON_TASK_NSdropdown6")).select_by(:text, "Timmy Planner")
				rescue
					puts "Approver not found."
				end
			end
			
			# Click Next
			$driver.find_element(:xpath, "//input[@value='Next']").click
		}
		puts "Exiting selectApprover"
	end



	def General.noFeesRequired
		puts "Entering noFeesRequired"
		
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists( "No Fees Required" )
		begin		
			sleep 4
	
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			$driver.switch_to.window(@new_window) {
				puts "Move browser to the right so the terminal can be displayed on the left"
				$driver.manage.window.move_to($startX,$startY)

				# Tools.checkPageForText
				
				if Tools.wait(:link, "accept this task", 15)
					$driver.find_element(:link, "accept this task").click
					Tools.click(:xpath, "//input[@value='Next']",10)

				else
					puts "Link of 'accept this task' not found. Try continuing."
				end
				sleep 5
				$driver.find_element(:xpath, "//input[@value='Close']").click
				sleep 3
			}
		rescue
			puts "New window not found."
		end
		puts "Exiting noFeesRequired"
	end








	def General.selectNApprover( type, planningManager )
		puts "Entering selectNApprover"
		
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists( type )
		begin		
			sleep 4
	
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			begin
				$driver.switch_to.window(@new_window) {
					puts "Move browser to the right so the terminal can be displayed on the left"
					$driver.manage.window.move_to($startX,$startY)

					# Tools.checkPageForText
					
					if Tools.wait(:link, "accept this task", 15)
						$driver.find_element(:link, "accept this task").click
						Tools.click(:xpath, "//input[@value='Next']",10)

					else
						puts "Link of 'accept this task' not found. Try continuing."
					end
					
					sleep 5
					begin
						# Select the Planning Manager
						Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'select')]")).select_by(:text, planningManager)
						sleep 2
					rescue
						puts "********************** ERROR ***********************************"
						puts "Planning Manager: '#{planningManager}' not found"
						Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "//*[starts-with(@class,'select')]")).select_by(:index, 1)
						#exit
						puts "********************** ERROR ***********************************"
					end
					
					# Click Next
					$driver.find_element(:xpath, "//input[@value='Next']").click
					puts "Next clicked"
					sleep 3
				}
			rescue
				puts "'#{type}' link not found"
			end
		rescue
			puts "New window not found."
		end
		puts "Exiting selectNApprover"
	end
	





	def General.provideNApproval( type, planningManager )
		puts "Entering provideNApproval"
		
		Tools.refreshClick
		sleep 5

		Tools.clickLinkIfExists( type )
		begin		
			sleep 4
	
			windows = $driver.window_handles # Get all window handles
		
			windows.each do |window|
				if $main_window != window
					@new_window = window
				end
			end
			begin
				$driver.switch_to.window(@new_window) {
					puts "Move browser to the right so the terminal can be displayed on the left"
					$driver.manage.window.move_to($startX,$startY)

					if Tools.wait(:link, "accept this task", 15)
						$driver.find_element(:link, "accept this task").click
						Tools.click(:xpath, "//input[@value='Next']",10)
					else
						puts "Link of 'accept this task' not found. Try continuing."
					end
					sleep 5
					begin
						$driver.find_element(:xpath, "//input[@value='Next']").click
						puts "Next clicked"
						sleep 3
					rescue
						puts "Next not found"
					end
				}
			rescue
				puts "'#{type}' link not found."
			end
		rescue
			puts "New window not found."
		end
		puts "Exiting provideNApproval"
	end

end # General

