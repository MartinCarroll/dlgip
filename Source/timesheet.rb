require 'rubygems'
require 'selenium-webdriver'
require 'clipboard' 
require '//home//Shared//code//Source//portal.rb'

module Timesheet


	def Timesheet.loginTS(username, password)	
		begin
			element = $driver.find_element(:id, 'usernameId_new')
			element.clear
			element.send_keys username
			sleep 1
			element = $driver.find_element(:id, 'passwordId_new')
			element.clear
			element.send_keys password
			sleep 1
			element.submit
			sleep 2
		end
	end



	def Timesheet.fillIt
		Tools.wait(:xpath, "//a[contains(.,'Enter hours in Time Sheet')]", 10)
		begin
			$driver.find_element(:xpath, "(//a[contains(.,'Enter hours in Time Sheet')])[2]").click
		rescue
			$driver.find_element(:xpath, "(//a[contains(.,'Enter hours in Time Sheet')])[1]").click
		end
		sleep 3
		for i in 0..4
			start = "//*[@name='timein0#{i}']"
			lunch = "//*[@name='timein#{i+10}']"
			lchnd = "//*[@name='timein#{i+20}']"
			ndDay = "//*[@name='timein#{i+70}']"
			total = "(//*[@type='text'])[#{i+57}]"
			$driver.find_element(:xpath, start).send_keys "8"
			$driver.find_element(:xpath, lunch).send_keys "12pm"
			$driver.find_element(:xpath, lchnd).send_keys "12:30pm"
			$driver.find_element(:xpath, ndDay).send_keys "4:30pm"
			$driver.find_element(:xpath, total).send_keys "8"
		end
		$driver.find_element(:xpath, total).send_keys :tab
#exit
		sleep 2
		begin
			$driver.find_element(:xpath, "//input[@value='Submit']").click
		rescue
			puts "Couldn't Submit. It's likely still not COB for last day of week."
		end
	end
end #Timesheet
