require 'rubygems'
require 'selenium-webdriver'
require 'clipboard' 

require './/Source//General.rb'
require './/Source//tempo.rb'

module Triggers


	def Triggers.processTriggerFile(csvfile)
		# Execute the tests in the supplied file 
		puts "Processing: #{csvfile}"
		$csvfile = csvfile
		$logFile = "#{$rubyHome}#{$filename}.log"
		
		Tempo.application("1","1")
		if File::exists?( csvfile ) # Check the input file exists
			skip = false
			begin
				CSV.foreach(csvfile, headers:true) do |row| # Process each CSV row except the first header row
=begin
					trigger = row[0]
					assessmentManager = row[1]
					technicalAgency = row[2]
					referralAgencyType = row[3]
					referralAgency = row[4]
					description = row[5]
					gISLayer = row[6]
					developmentAspect = row[7]
					category = row[8]
					lotPlan = row[9]
					if lotPlan == nil
						lotPlan = "223SP245353"
					end

					puts "******************************"
					puts "0: #{trigger}"
					puts "1: #{assessmentManager}"
					puts "2: #{technicalAgency}"
					puts "3: #{referralAgencyType}"
					puts "4: #{referralAgency}"
					puts "5: #{description}"
					puts "6: #{gISLayer}"
					puts "7: #{developmentAspect}"
					puts "8: #{category}"
					puts "9: #{lotPlan}"
					puts "******************************"

					checkTriggers(trigger, assessmentManager, description, gISLayer, lotPlan, developmentAspect, referralAgency)
=end
#					trigger = row[0]
					assessmentManager1 = row[2]
					assessmentManager2 = row[3]
					assessmentManager3 = row[4]
					gISLayer1 = row[5]
					gISLayer2 = row[6]
					puts "******************************"
					puts "3: #{assessmentManager1}"
					puts "4: #{assessmentManager2}"
					puts "5: #{assessmentManager3}"
					puts "6: #{gISLayer1}"
					puts "7: #{gISLayer2}"
					puts "******************************"
					

					checkLGAs(assessmentManager1, assessmentManager2, assessmentManager3, gISLayer1, gISLayer2)

				end
			end
			Tools.grabScreenshot("no", $appID)
			elapsedTime = Tools.elapsedTime($startTime)
			puts "Script finished successfully after #{elapsedTime}."

			logFile = "#{$rubyHome}#{csvfile}.log"
			puts "Creating log file: #{logFile}"

			if $applicationType != nil
				creationInfo = $applicationType + " - " + $appID + " - " + $startTime.to_s + " - " + elapsedTime
			else
				if $appID != nil
					creationInfo = $appID + " - " + $startTime.to_s + " - " + elapsedTime
				else
					creationInfo = $startTime.to_s + " - " + elapsedTime
				end
			end
			Tools.logger(logFile, creationInfo)
		else
			puts "Error: No file called #{csvfile} found."
			puts "Please check the path."
		end
	end # processFile









	def Triggers.checkLGAs(assessmentManager1, assessmentManager2, assessmentManager3, gISLayer1, gISLayer2)
		puts "Entering checkLGAs"

		
		# Open EasyPrep
		for j in 1..2
			if j == 1
				gis = gISLayer1
			else
				gis = gISLayer2
			end
			found = "#{gis},#{assessmentManager1},#{assessmentManager2},#{assessmentManager3}:"

			validLot = Tempo.addLot("",gis)
			if validLot
				Tempo.enterFurtherLocationDetails("No")
				Tempo.enterApplicantDetails(nil, nil)
				General.wait 4
				text = $driver.find_element(:xpath, ".//*[@class='gwt-ListBox']").text
			end
			found = found + " #{text}"
			Tools.logger($logFile, found)
			if validLot
				$driver.find_element(:xpath, "//*[text()='Back']").click
				General.wait 2
				puts "Clicked Back"
				begin
					$driver.find_element(:xpath, "(//*[text()='Yes'])[1]").click
					General.wait
					$driver.find_element(:xpath, "(//*[text()='Yes'])[2]").click
				rescue
					begin
						$driver.find_element(:xpath, "(//*[text()='Yes'])[3]").click
					rescue
						puts "Nope 1"
					end
				end
				General.wait 2
				puts "Clicked Yes"
			end
			begin
				$driver.find_element(:xpath, "//*[text()='Back']").click
				General.wait 2
				puts "Clicked Back"
			rescue
			end
			begin
				$driver.find_element(:xpath, "(//*[text()='Yes'])[1]").click
				General.wait
				$driver.find_element(:xpath, "(//*[text()='Yes'])[2]").click
			rescue
				begin
					$driver.find_element(:xpath, "(//*[text()='Yes'])[3]").click
				rescue
					puts "Nope 1"
				end

				General.wait 2
				puts "Clicked Yes"
			end
			begin
				$driver.find_element(:xpath, "//*[text()='Back']").click
				General.wait 2
				puts "Clicked Back"
			rescue
			end
			begin
				$driver.find_element(:xpath, "(//*[text()='Yes'])[1]").click
				General.wait
				$driver.find_element(:xpath, "(//*[text()='Yes'])[2]").click
			rescue
				begin
					$driver.find_element(:xpath, "(//*[text()='Yes'])[3]").click
				rescue
					puts "Nope 1"
				end
			end
			begin
				General.wait 2
				puts "Clicked Yes"
				$driver.find_element(:xpath,"//*[contains(text(),'Remove')]").click
				General.wait 2
			rescue
			end
		end

		puts "Exiting checkLGAs"
	end # checkLGAs








	def Triggers.checkTriggers(triggerNumber, assManager, description, gISLayer, lotPlan, devAspect, referralAgency)
		puts "Entering checkTriggers"

		# Open EasyPrep
		Tempo.application("1","1")
		Tempo.addLot("",lotPlan)
		Tempo.enterFurtherLocationDetails("No")
		Tempo.enterApplicantDetails(nil, nil)
		desc = "Test of #{triggerNumber}"
		#Tempo.enterAssessmentDetails(desc, assManager, natureOfDev, approvalType, assLevel)
		Tempo.enterAssessmentDetails(desc, assManager, devAspect, nil, nil, nil)

		if triggerNumber[0] == "6"
			checkForTrigger(triggerNumber)
		else 
			# Referral triggers

			# Select all triggers
			allTrigsChkBox = "(.//*[@type='checkbox'])[1]"
			Tools.wait(:xpath, allTrigsChkBox, 10)
			$driver.find_element(:xpath, allTrigsChkBox).click
			sleep 3
			Tempo.wait

			# Click Add Triggers button
			addTriggersBtn = "//*[text()='Add triggers']"
			$driver.find_element(:xpath, addTriggersBtn).click
			sleep 3
			Tempo.wait

			nextBtn = "//*[text()='Next']"
			$driver.find_element(:xpath, nextBtn).click
			sleep 3
			Tempo.wait

			# Set dropdowns to All
			# Category
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[1]")).select_by(:text, "All")

			# Triggers to show
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[2]")).select_by(:text, "All")

			# Referral Agencies
			Selenium::WebDriver::Support::Select.new($driver.find_element(:xpath, "(//*[starts-with(@class,'gwt-ListBox')])[3]")).select_by(:text, "All")

			# Enter Trigger to find
			Tools.wait(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[1]", 6)
			$driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[1]").clear
			$driver.find_element(:xpath, "(.//*[starts-with(@id, 'gwt-uid')])[1]").send_keys triggerNumber
			sleep 1
			# Click Search button
			$driver.find_element(:xpath, "//*[text()='Search']").click
			sleep 2
			wait			
	
			# Check if Trigger found
			if checkForTrigger(triggerNumber)
				message = "Success: #{triggerNumber}"			
			else
				message = "Failure: #{triggerNumber}"			
			end
			Tools.logger($logFile, message)

			# Check if Referral Agency found
			if checkForReferralAgency(referralAgency)
				message = "Success: #{referralAgency}"			
			else
				message = "Failure: #{referralAgency}"			
			end
			Tools.logger($logFile, message)
		end

		# Check if description found
		if checkForDescription(description)
			message = "Success: #{description}"			
		else
			message = "Failure: #{description}"			
		end
		Tools.logger($logFile, message)

		puts "Exiting checkTrigger"
	end







	def Triggers.checkForTrigger(triggerNumber)
		# This checks for a trigger 
		puts "Entering checkForTrigger"

		triggerTxt = "//*[text()='#{triggerNumber}']"
		puts "*********************************"
		begin
			if $driver.find_element(:xpath, triggerTxt)
				puts "Trigger #{triggerNumber} found"
				puts "*********************************"
				return true
			end
		rescue
			puts "Trigger #{triggerNumber} NOT found"
			puts "*********************************"
			return false
		end
		puts "Exiting checkForTrigger"
	end

		






	def Triggers.checkForDescription(description)
		# This checks for a trigger's description 
		puts "Entering checkForDescription"

		descriptionTxt = "//*[text()='#{description}']"
		puts "*********************************"
		begin
			if $driver.find_element(:xpath, description)
				puts "Description:"
				puts description
				puts "found"
				puts "*********************************"
				return true
			end
		rescue
			puts "Description:"
			puts description
			puts "NOT found"
			puts "*********************************"
			return false
		end
		puts "Exiting checkForDescription"
	end




	def Triggers.checkForReferralAgency(referralAgency)
		# This checks for a trigger's referralAgency 
		puts "Entering checkForReferralAgency"

		referralAgencyTxt = "//*[text()='#{referralAgency}']"
		puts "*********************************"
		begin
			if $driver.find_element(:xpath, referralAgencyTxt)
				puts "Referral Agency:"
				puts referralAgency
				puts "found"
				puts "*********************************"
				return true
			end
		rescue
			puts "Referral Agency:"
			puts referralAgency
			puts "NOT found"
			puts "*********************************"
			return false
		end
		puts "Exiting checkForReferralAgency"
	end
end # Triggers
