wget http://libav.org/releases/libav-10.1.tar.gz
tar -xvzf libav-10.1.tar.gz
cd libav-10.1
./configure --extra-cflags=-I/opt/local/include --extra-ldflags=-L/opt/local/lib --enable-gpl --enable-version3  --enable-libvpx
make
sudo make install
INSTALL libavcodec/dv_profile.h
INSTALL libavcodec/dxva2.h
INSTALL libavcodec/vaapi.h
INSTALL libavcodec/vda.h
INSTALL libavcodec/vdpau.h
INSTALL libavcodec/version.h
INSTALL libavcodec/xvmc.h
whereis avconv
