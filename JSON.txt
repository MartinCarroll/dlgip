[{
	"processModelId": 13477,
	"processModelName": "Call webservice to get address details",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.7777777777777778
},
{
	"processModelId": 18533,
	"processModelName": "Write gis layers for lots to DB",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.8461538461538461
},
{
	"processModelId": 15510,
	"processModelName": "Call ESRI lotplan validator",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 1.0
},
{
	"processModelId": 15511,
	"processModelName": "Send http request",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.8571428571428571
},
{
	"processModelId": 13289,
	"processModelName": "Call ESRI lotplan intersector",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 1.0
},
{
	"processModelId": 19946,
	"processModelName": "Add Lot details",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.7083333333333334
},
{
	"processModelId": 18475,
	"processModelName": "Write added lot plans and coordinates",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.6666666666666666
},
{
	"processModelId": 19966,
	"processModelName": "Search by multiple lots",
	"taskIdsAll": null,
	"taskIdsComplete": null,
	"percentCoverage": 0.6666666666666666
}]7