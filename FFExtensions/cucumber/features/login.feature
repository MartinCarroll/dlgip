@login

Feature: login

Login should only be possible with a valid username and password.

Rules:
- Username is valid
- Password is valid

Scenario: Logging in with valid credentials
	Given the login screen is displayed
	When valid username and password credentials are used
	Then login is successful

