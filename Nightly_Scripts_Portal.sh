#!/bin/bash
# This script is for running Portal automation nightly

# 1) Clean up results folder by deleting log files and screenshots older than 7 day/s
find /home/Shared/code/test_results -type f -mtime +7 | xargs rm

# 2) Ensure scripts are in Unix format
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Cancel_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Change_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Extend_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Lodgement_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Prelodgement_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_PreReferral_P.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_Refer_P.csv

# 3) Get the current time
time=$(/bin/date +%H.%M)
# 4) Get the current date
dateTime=$(/bin/date +"%Y-%m-%e")
# Concatenate date and time for log filename use.
dateTime=$dateTime"_"$time"_"

clear
echo "================================"
echo $time ": Running Portal scripts"
echo "================================"

# 4) Run the scripts
echo "Running Lodgement - Portal..."
echo -ne "\033]0;Lodgement - Portal\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Lodgement_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Lodgement.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Lodgement.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Lodgement" > /dev/null 2>&1
echo "Finished running Lodgement- Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running PreLodgement - Portal..."
echo -ne "\033]0;PreLodgement - Portal\007"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Prelodgement_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Prelodgement.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Prelodgement.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Prelodgement" > /dev/null 2>&1
echo "Finished running PreLodgement- Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo -ne "\033]0;PreReferral - Portal\007"
echo "Running SMR - Portal..."
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_PreReferral_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-PreReferral.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-PreReferral.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-PreReferral" > /dev/null 2>&1
echo "Finished running SMR - Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running Refer - Portal..."
echo -ne "\033]0;Refer - Portal\007"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Refer_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Refer.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Refer.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Refer" > /dev/null 2>&1
echo "Finished running Refer- Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running Cancel - Portal..."
echo -ne "\033]0;Cancel - Portal\007"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Cancel_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Cancel.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Cancel.log"
exit
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Cancel" > /dev/null 2>&1
echo "Finished running Cancel - Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running Change - Portal..."
echo -ne "\033]0;Change - Portal\007"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Change_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Change.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Change.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Change" > /dev/null 2>&1
echo "Finished running Change - Portal..."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running Extend - Portal..."
echo -ne "\033]0;Extend - Portal\007"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_Extend_P.csv > "/home/Shared/code/test_results/"$dateTime"Portal-Extend.log"
sleep 2
pid=$(pgrep /usr/bin/ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Portal-Extend.log"
/usr/bin/ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Portal-Extend" > /dev/null 2>&1
echo "Finished running Extend - Portal..."
sleep 1
killall firefox

# Fix the log file permissions (as cron makes root owner log files)
cd /home/Shared/code/test_results
sudo chown carrollm:adm *

sleep 1
echo "================================"
pkill firefox
echo "================================"
echo $now" Finished running Portal scripts"
echo "================================"
