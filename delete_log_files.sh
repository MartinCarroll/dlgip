#!/bin/bash
# Clean up test results folder by deleting log files and screenshots older than x day/s
x='1'
find /home/Shared/code/test_results/ -type f -mtime +$x | xargs rm

