#!/bin/bash
# This script is for running Tempo automation nightly
#debug
# 1) Clean up results folder by deleting log files and screenshots older than 7 day/s
find /home/Shared/code/test_results -type f -mtime +7 | xargs rm

# 2) Ensure scripts are in Unix format
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SDA_Upload_Completed.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SER_EarlyReferralRequest.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SMR_MissedRefer.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SPA_SPL_PreApplicationAdvice.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SPD_RequestPostApprovalChanges.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SRA_Refer.csv
/usr/bin/dos2unix /home/Shared/code/CSVs/MartinCarroll_SDA_Online_Forms.csv

# 3) Get the current time
time=$(/bin/date +%H.%M)
# 4) Get the current date
dateTime=$(/bin/date +"%Y-%m-%e")
# Concatenate date and time for log filename use.
dateTime=$dateTime"_"$time"_"

clear
echo "================================"
echo $time ": Running Tempo scripts"
echo "================================"

# 4) Run the scripts
echo "Running SDA Online..."
echo -ne "\033]0;SDA Online- Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SDA_Online_Forms.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SDA_Online.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SDA_Online.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SDA_Online" > /dev/null 2>&1
echo "Finished running SDA Online - Tempo..."
sleep 1
killall firefox
sleep 1
echo "================================"
sleep 1

echo "Running SRA - Tempo..."
echo -ne "\033]0;SRA - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SRA_Refer.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SRA_Refer.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SRA_Refer.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SRA_Refer" > /dev/null 2>&1
echo "Finished running SRA - Tempo."
sleep 1
killall firefox
sleep 1

echo "Running SDA..."
echo -ne "\033]0;SDA - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SDA_Upload_Completed.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SDA.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SDA.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SDA" > /dev/null 2>&1
echo "Finished running SDA - Tempo..."
sleep 1
killall firefox
sleep 1
echo "================================"
sleep 1

echo "Running SPD..."
echo -ne "\033]0;SPD - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SPD_RequestPostApprovalChanges.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SPD_RequestPostApprovalChanges.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SPD_RequestPostApprovalChanges.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SPD_RequestPostApprovalChanges" > /dev/null 2>&1
echo "Finished running SPD - Tempo."
sleep 1
killall firefox
sleep 1
echo "================================"

echo "Running SER..."
echo -ne "\033]0;SER - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SER_EarlyReferralRequest.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SER_EarlyReferralRequest.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SER_EarlyReferralRequest.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SER_EarlyReferralRequest" > /dev/null 2>&1
echo "Finished running SER - Tempo..."
sleep 1
killall firefox
sleep 1
echo "================================"
sleep 1

echo "Running SPA_SPL..."
echo -ne "\033]0;SPA_SPL - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SPA_SPL_PreApplicationAdvice.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SPA_SPL_PreApplicationAdvice.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SPA_SPL_PreApplicationAdvice.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SPA_SPL_PreApplicationAdvice" > /dev/null 2>&1
echo "Finished running SPA_SPL - Tempo..."
sleep 1
killall firefox
sleep 1
echo "================================"
sleep 1

echo "Running SMR..."
echo -ne "\033]0;SMR - Tempo\007"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/MartinCarroll_SMR_MissedRefer.csv > "/home/Shared/code/test_results/"$dateTime"Tempo-SMR.log"
sleep 2
pid=$(pgrep ruby)
tail -f --pid $pid "/home/Shared/code/test_results/"$dateTime"Tempo-SMR.log"
ruby /home/Shared/code/Source/main.rb /home/Shared/code/CSVs/screenshotRunResult.csv "/home/Shared/code/test_results/"$dateTime"Tempo-SMR_MissedRefer" > /dev/null 2>&1
echo "Finished running SMR - Tempo..."
sleep 1
killall firefox

# Fix the log file permissions (as cron makes root owner log files)
cd /home/Shared/code/test_results
sudo chown carrollm:adm *

sleep 1
echo "================================"
sleep 1
pkill firefox
echo "================================"
echo "Finished running Tempo scripts"
echo "================================"
