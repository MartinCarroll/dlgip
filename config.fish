set -gx PATH /bin $PATH
set -gx PATH /usr/bin $PATH
set -gx PATH /usr/local/bin $PATH
set -gx PATH /usr/local/rvm/bin $PATH
set -gx PATH /usr/lib64/qt-3.3/bin $PATH
set -gx PATH /usr/local/rvm/gems/ruby-2.2.1/bin $PATH
set -gx PATH /usr/local/rvm/gems/ruby-2.2.1@global/bin $PATH
set -gx PATH /usr/local/rvm/rubies/ruby-2.2.1/bin $PATH
set -gx PATH $HOME/.rvm/bin $PATH
set -gx PATH $HOME/.rvm/gems/ruby-2.2.1/bin $PATH
set -gx PATH $HOME/.rvm/rubies/ruby-2.2.1/bin $PATH
set -gx PATH $HOME/.rvm/gems/ruby-2.2.1@global/bin $PATH



rvm default
